/**
 * @file pendulum.cpp
 * @ Pendulum BLSC control and estimation
 * @author Savva Morozov <savva@mit.edu>
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 Dec 2019
 */

#include "dtmpc_pendulum/dtmpc_pendulum.h"
#include <chrono>
extern "C" {
#include "solver.h"
}



Vars vars;
Params params;
Workspace work;
Settings settings;


namespace acl {
namespace pendulum {

DTMPC_Pendulum::DTMPC_Pendulum(): Q_(nullptr), Qfinal_(nullptr), R_(nullptr), M_(nullptr), d0_(nullptr), x0_(nullptr), xf_(nullptr), u0_(nullptr), xk_(nullptr), uk_(nullptr), d_(nullptr), w_(nullptr), delta_alpha_(nullptr), omega_(nullptr), new_phi_(nullptr),
                  cos_x1_(nullptr), abs_x2_(nullptr), disturbance_(nullptr), old_phi_(nullptr), alpha_(nullptr)
{
  set_defaults();
  setup_indexing();

  new (&Q_) Eigen::Map<Qmat>(params.Q, dim::n, dim::n);
  new (&Qfinal_) Eigen::Map<Qfinalmat>(params.Q_final, dim::n, dim::n);
  new (&R_) Eigen::Map<Rmat>(params.R, dim::m, dim::m);
  new (&M_) Eigen::Map<Rmat>(params.M, 1, 1);

  new (&d0_) Eigen::Map<State>(params.d_0, dim::n);
  new (&x0_) Eigen::Map<State>(params.xk_0, dim::n);
  new (&xf_) Eigen::Map<State>(params.x_final, dim::n);
  new (&u0_) Eigen::Map<Command>(params.uk_0, dim::m);

  new (&xk_) Eigen::Map<XStateStack>(&params.xk[0][0], dim::n, dim::T+2);
  new (&uk_) Eigen::Map<CommandStack>(&params.uk[0][0], dim::m, dim::T+1);
  // // There is no x[0] variable, so skip the first timestep (column)
  new (&d_) Eigen::Map<DStateStack>(&vars.d[1][0], dim::n, dim::T+1);
  new (&w_) Eigen::Map<CommandStack>(&vars.w[0][0], dim::m, dim::T+1);
  new (&delta_alpha_) Eigen::Map<tp1Stack>(&vars.delta_alpha[0][0], 1, dim::T+1);
  new (&omega_) Eigen::Map<tp1Stack>(&vars.omega[1][0], 1, dim::T+1);
  new (&new_phi_) Eigen::Map<tp1Stack>(&vars.new_EF[1][0], 1, dim::T+1);

  new (&cos_x1_) Eigen::Map<tp1Stack>(&params.cos_xk[0][0], 1, dim::T+1);
  new (&abs_x2_) Eigen::Map<tp1Stack>(&params.abs_xk[0][0], 1, dim::T+1);

  new (&disturbance_) Eigen::Map<tp1Stack>(&params.disturbance[0][0], 1, dim::T+1);
  new (&old_phi_) Eigen::Map<tp1Stack>(&params.old_EF[0][0], 1, dim::T+1);
  new (&alpha_) Eigen::Map<tp1Stack>(&params.alpha[0][0], 1, dim::T+1);

  set_MPC_params();

  // std::cout << params.Cd_max[0] << "\t" << params.I[0] << "\t" << params.lambda[0] << "\t" << params.disturbance_0[0] << "\t" << params.disturbance_1[0] << "\n\n\n\n";

  // std::cout << &params.new_EF_0[0] << "\t" << &vars.new_EF_1[0] << "\t" << &vars.new_EF_2[0] << "\n";
  // std::cout << params.new_EF_0[0] << "\t" << vars.new_EF_1[0] << "\t" << vars.new_EF_2[0] << "\n";
  // std::cout << &params.new_EF[0][0] << "\t" << &vars.new_EF[1][0] << "\t" << &vars.new_EF[2][0] << "\n";
  // std::cout << params.new_EF[0][0] << "\t" << vars.new_EF[1][0] << "\t" << vars.new_EF[1][0] << "\n";
  // std::cout << new_phi_(0,0) << "\t" << new_phi_(0,1) << "\n";
}

void DTMPC_Pendulum::set_MPC_params(){
  /*
  one time MPC parameter initalizations
  */
  d0_(0) = 0.0; d0_(1) = 0.0;

  params.c1[0] = pq_.L_cm * pq_.m * pq_.g * mpc_.delta_t / pq_.I; //x1 coef
  params.c2[0] = 2 * pq_.Cd * mpc_.delta_t / pq_.I; // x2 coef
  params.Bd[0] = pq_.L * mpc_.delta_t / pq_.I; //input coef

  // state cost
  Q_(0,0) = mpc_.Q1;
  Q_(1,1) = mpc_.Q2;
  // terminal state cost
  Qfinal_(0,0) = mpc_.Q_final1;
  Qfinal_(1,1) = mpc_.Q_final2;
  // input cost
  R_(0,0) = mpc_.R;
  M_(0,0) = mpc_.M;
  //final desired point
  //initial d; always zero
  d0_(0) = 0.0;
  d0_(1) = 0.0;


  params.u_max[0] = pq_.umax; // N, max applied thrust
  // params.w_max[0] = mpc_.w_max_coarse; // N maximum of the w input variable
  params.delta_u_max[0] = mpc_.delta_u_max; //slew rate of u: delta u less than S
  params.x_dot_max[0] = mpc_.x_dot_max; // rads/s, max angular speed

  x_final_(0) = mpc_.x_final; x_final_(1) = 0;
  xf_ = x_final_;

  params.omega_max[0] = mpc_.omega_max;
  params.alpha_min[0] = mpc_.alpha_min;
  params.alpha_max[0] = mpc_.alpha_max;
  params.delta_alpha_max[0] = mpc_.delta_alpha_max;

  params.Cd_max[0] = mpc_.Cd_max;
  params.lambda[0] = mpc_.lambda;
  params.I[0] = pq_.I;
  params.delta_t[0] = mpc_.delta_t;
  params.u_optimal[0] = pq_.m * pq_.g * pq_.L_cm * sin(mpc_.x_final) / pq_.L;

  // added after state tightening:
  params.Cd[0] = pq_.Cd;
  params.L[0] = pq_.L;
  params.grav_term[0] = mpc_.grav_term;
  // params.delta_alpha_slew[0] = mpc_.delta_alpha_slew;

}

void DTMPC_Pendulum::DTMPC_cost2(){
  // state cost
  Q_(0,0) = mpc_.Q1;
  Q_(1,1) = mpc_.Q2;
  // terminal state cost
  Qfinal_(0,0) = mpc_.Q_final1;
  Qfinal_(1,1) = mpc_.Q_final2;
  // input cost
  R_(0,0) = mpc_.R;
  M_(0,0) = mpc_.M;
}

void DTMPC_Pendulum::setDTMPCinits(){
  if (inits_.req_id == 0){
    set_MPC_params();
    // DTMPC_cost2();
  }
  // if (inits_.req_id > 5){
  //   // params.w_max[0] = mpc_.w_max_fine;
  // }


  // after this t_traj doesn't need to be touched.
  for(int i = 0; i < dim::traj * (dim::T+1)+1; i++){
    t_traj_(0, i) = i * mpc_.delta_t_ms + inits_.t_0;
  }
  params.last_u[0] = inits_.last_u;
  params.last_alpha[0] = inits_.last_alpha;
  params.omega_0[0] = inits_.omega_0;

  x_0_(0) = inits_.theta_0; x_0_(1) = inits_.d_theta_0;
  x0_ = x_0_;

  // std::cout << "\n\nLAST U IS " << inits_.last_u << "\n\n";

  if (inits_.req_id == 0){
    for(int i = 0; i < dim::T+1; i++){
      uk_(0, i) = inits_.last_u;
      alpha_(0, i) = inits_.last_alpha;
      disturbance_(0, i) = mpc_.D + mpc_.eta;
      if (xk_(0,i) >= M_PI && xk_(0,i) <= 2*M_PI || xk_(0,i) >= 3*M_PI && xk_(0,i) <= 4*M_PI){
        disturbance_(0, i) += mpc_.disturb;
      }
    }
  }
  else{
    for(int i = 0; i < dim::T+1-int( (inits_.t_0 - last_t_0)/mpc_.delta_t_ms ); i++){
      // warm starting the trajectory
      uk_(0,i) = u_traj_(i + int( (inits_.t_0 - last_t_0)/mpc_.delta_t_ms ));
      // old_phi_(0,i) = phi_traj_(i + int( (inits_.t_0 - last_t_0)/mpc_.delta_t_ms ));
      alpha_(0,i) = alpha_traj_(i + int( (inits_.t_0 - last_t_0)/mpc_.delta_t_ms ));

      disturbance_(0,i) = mpc_.D + mpc_.eta;
      if (xk_(0,i) >= M_PI && xk_(0,i) <= 2*M_PI || xk_(0,i) >= 3*M_PI && xk_(0,i) <= 4*M_PI){
        disturbance_(0, i) += mpc_.disturb;
      }

    }
    for(int i = dim::T+1-int( (inits_.t_0 - last_t_0)/mpc_.delta_t_ms ); i < dim::T+1; i++){
      uk_(0,i) = u_traj_(dim::T);
      // old_phi_(0,i) = phi_traj_(dim::T);
      alpha_(0,i) = alpha_traj_(dim::T);

      disturbance_(0,i) = mpc_.D + mpc_.eta;
      if (xk_(0,i) >= M_PI && xk_(0,i) <= 2*M_PI || xk_(0,i) >= 3*M_PI && xk_(0,i) <= 4*M_PI){
        disturbance_(0, i) += mpc_.disturb;
      }

    }
  }
  last_t_0 = inits_.t_0;
  // old_phi_(0,dim::T+1) = phi_traj_(dim::T);
  params.new_EF_0[0] = inits_.phi_0;
  old_phi_(0,0) = inits_.phi_0;

  cos_x1_(0,0) = cos(x0_(0));
  abs_x2_(0,0) = fabs(x0_(1));

  //generate all other values based on trajectory
  for (int i = 1; i < dim::T+2; i++){
    // initialize states 1 through T+1
    xk_(0, i) = xk_(0, i-1) + mpc_.delta_t * xk_(1, i-1);
    xk_(1, i) = (-pq_.Cd *fabs(xk_(1, i-1))*xk_(1, i-1) - pq_.L_cm * pq_.m*pq_.g * sin( xk_(0, i-1) ) + pq_.L*uk_(0, i-1) ) * mpc_.delta_t/pq_.I + xk_(1, i-1);
    //initialize abs and cos 1 through T
    if (i < dim::T + 1){
      old_phi_(0,i) = old_phi_(0,i-1) + mpc_.delta_t * (-alpha_(0,i-1) * old_phi_(0,i-1) + mpc_.D +mpc_.eta + mpc_.Cd_max * fabs(xk_(1,i-1) * xk_(1,i-1))/pq_.I );
      cos_x1_(0, i) = cos(xk_(0, i));
      abs_x2_(0, i) = fabs(xk_(1, i));
    }
  }
  settings.max_iters = 25;
  settings.verbose = 0;
}

void DTMPC_Pendulum::DTMPC(){
  //run the SQP loop 10 times, see what happens, just see what happens

  int k = 0;
  int converged = 1;

  while (k < 20) {
    // std::cout << params.Cd_max[0] << "\t" << params.I[0] << "\t" << params.lambda[0] << "\t" << params.disturbance_0[0] << "\t" << params.disturbance_1[0] << "\n\n\n\n";
    solve();
    if (work.converged == 0) {
      std::cout << "\n---------ERROR DID NOT CONVERGE-----------\n";
      converged = 0;
    }
    else{
      converged = 1;
    }
    for(int i = 0; i < dim::T+1; i++){
      //for u, 0 through T, add up
      uk_(0, i) = uk_(0, i) + w_(0, i);
      //for x, 1 through T+1, add D; rememeber that arrays start at different points
      // we don't increment x0 because d0 is 0
      xk_(0, i+1) = xk_(0, i+1) + d_(0, i);
      xk_(1, i+1) = xk_(1, i+1) + d_(1, i);
      //for cos and abs, 0 through T, get abs and cos values
      cos_x1_(0, i) = cos(xk_(0, i)); abs_x2_(0, i) = fabs(xk_(1, i));


      disturbance_(0, i) = 0;
      disturbance_(0,i) += mpc_.D + mpc_.eta; // + mpc_.disturb * pq_.L / pq_.I;
      // disturbance_(0,i) += mpc_.delta_l_cm * pq_.m * pq_.g / pq_.I * fabs( sin( xk_(0,i) ) );
      //apply disturbance if necessary
      // if (xk_(0,i) >= M_PI && xk_(0,i) <= 2*M_PI || xk_(0,i) >= 3*M_PI && xk_(0,i) <= 4*M_PI){
      //   disturbance_(0, i) += mpc_.disturb * pq_.L / pq_.I * fabs( sin(xk_(0,i)) ) ;
      // }
      if (xk_(0,i) >= 0.5 * M_PI && xk_(0,i) <= 1.5*M_PI || xk_(0,i) >= 2.5*M_PI && xk_(0,i) <= 3.5*M_PI){
        disturbance_(0, i) += mpc_.disturb * pq_.L / pq_.I * fabs( cos(xk_(0,i)) ) ;
      }

      alpha_(0, i) = alpha_(0, i) + delta_alpha_(0, i);
      if (i != dim::T){
          old_phi_(0, i+1) = new_phi_(0, i);
      }
    }
    if (work.converged == 0 && (mpc_.verbose > 1.5) ){
      std::cout <<"w\n" << w_ << "\n\nu\n" << uk_ << "\n\nd\n" << d_ << "\n\nx\n" << xk_ << "\n\n";
      std::cout <<"delta_alpha\n" << delta_alpha_ << "\n\nalpha\n" << alpha_ << "\n\nphi\n" << new_phi_ << "\n\nomega\n" << omega_ << "\n\n";
    }
    k += 1;
    if (k >= 5 && std::max(fabs(delta_alpha_.maxCoeff()), fabs(delta_alpha_.minCoeff())) < 0.005 && std::max(fabs(d_.maxCoeff()), fabs(d_.minCoeff())) < 0.001 && std::max(fabs(w_.maxCoeff()), fabs(w_.minCoeff())) < 0.002){
      k = 20;
    }
  }
  if (converged == 0)
    std::cout << "\n $$$$$$$$$$$$$   NOT CONVERGED AT END   $$$$$$$$$$$$$ \n";
  // std::cout <<"w\n" << w_ << "\n\nu\n" << uk_ << "\n\nd\n" << d_ << "\n\nx\n" << xk_ << "\n\n";
  // std::cout <<"delta_alpha\n" << delta_alpha_ << "\n\nalpha\n" << alpha_ << "\n\nphi\n" << new_phi_ << "\n\nomega\n" << omega_ << "\n\n";
  // -----------------------------------------------------------------------------------------------------
  // MPC finished
  // reassign values to the trajectory values
  for(int i = 0; i < dim::T+1; i++){
    u_traj_(0, i) = uk_(0, i);
    x_traj_(0, i) = xk_(0, i);
    x_traj_(1, i) = xk_(1, i);
    if (i == 0){
      omega_traj_(0, i) = inits_.omega_0;
    }
    else {
      omega_traj_(0, i) = omega_(0, i);
    }
    phi_traj_(0, i) = new_phi_(0, i);
    alpha_traj_(0, i) = alpha_(0, i);
  }
}

// setter methods


void DTMPC_Pendulum::setParameters(const Params& params, const MPC_params& mpcs, const Physical_Quantities& pqs, const Finals& fins) {
  params_ = params;
  mpc_ = mpcs;
  pq_ = pqs;
  fin_ = fins;
}

// ----------------------------------------------------------------------------

void DTMPC_Pendulum::computeTrajectory() {

  // priorToFirstDTMPC();
  // auto start = std::chrono::system_clock::now();
  // auto end = std::chrono::system_clock::now();
  // std::chrono::duration<double> diff = end-start;
  // start = std::chrono::system_clock::now();

  setDTMPCinits();
  DTMPC();

  // end = std::chrono::system_clock::now();
  // diff = end-start;
  // std::cout << "\njust math: " << diff.count() << " s\n\n";
}
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------


float DTMPC_Pendulum::sat(float a){
  //saturation function
  if (a >= 1)
    return 1.0;
  else if (a <= -1)
    return -1.0;
  else
    return a;
}


} // ns pendulum
} // ns acl
