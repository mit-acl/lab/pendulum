/**
 * @file pendulum.cpp
 * @ Pendulum BLSC control and estimation
 * @author Savva Morozov <savva@mit.edu>
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 Dec 2019
 */

#include "dtmpc_pendulum/pendulum.h"
#include <chrono>
#include <iostream>
#include <algorithm>




namespace acl {
namespace pendulum {

BLSC_Pendulum::BLSC_Pendulum()

{
  quat_ = Eigen::Quaterniond::Identity();
  u_ = 0;
  std::srand((unsigned)std::time(0));
  DTMPC_period_ms = DTMPC_period * mpc_.delta_t*1000;
}
//
void BLSC_Pendulum::doBLSC(){
  int index = 0;
  u_ = 0;
  // determine the right index
  if (first_time_flag){
    first_time_flag = false;
    time_start_ += tnow_;
    tnow_ = 0;
  }

  while (index < dim::T+1 && tnow_ >= t_traj_use_(0, index) ){ //WARNING check the length of array
    index += 1;
  }
  // std::cout << "tnow\t" << tnow_ << "\tfirst\t" << t_traj_use_(0,0) << "\t" << t_traj_use_(0,dim::T) << "\t\t" << t_traj_future_(0,0) << "\t" << t_traj_future_(0,dim::T) << "\n";

  //case one: we are within the timehorizon of the MPC
  if (index < dim::T+1 ){
      index -= 1;
      // feedforward from the MPC
      u_mpc_ =  u_traj_use_(0, index);
      x_des = x_traj_use_(0,index) + (x_traj_use_(0,index+1) - x_traj_use_(0,index)) / mpc_.delta_t_ms * (tnow_ - t_traj_use_(0,index) );
      d_x_des = x_traj_use_(1,index) + (x_traj_use_(1,index+1) - x_traj_use_(1,index)) / mpc_.delta_t_ms * (tnow_ - t_traj_use_(0, index));
      a_ = alpha_traj_use_(0,index);
      phi_ = phi_traj_use_(0,index);
      omega_ = omega_traj_use_(0,index);
  }
  // else{
  //   u_mpc_ = pq_.m * pq_.g * pq_.L_cm *sin(mpc_.)
  // }
  // std::cout << "index\t" << index << "\tu\t" << u_mpc_ << "\tx\t" << x_des << "\t" << d_x_des << "\n\n";

  s_ = (d_theta_ - d_x_des ) + mpc_.lambda * ( theta_ - x_des );
  k_ = a_ * phi_ * pq_.I; // gain

  u_ = 0.0;
  u_ += u_mpc_;
  u_ += -k_/pq_.L * sat(s_ / phi_);
  u_sliding_ = -k_/pq_.L * sat(s_ / phi_);
  u_ += 1/pq_.L * (pq_.L_cm * pq_.m * pq_.g * (sin(theta_) - sin(x_des) ) );
  u_ += 1/pq_.L * pq_.Cd * (fabs( d_theta_ )* d_theta_ - fabs(d_x_des)*d_x_des );
  u_ += -1/pq_.L * pq_.I * mpc_.lambda * (d_theta_ - d_x_des) ;
  u_rec_ = u_;
  // std::cout << "mpc\t" << u_mpc_;
  // std::cout << "\ts_\t" << -k_/pq_.L * sat(s_ / phi_);
  // std::cout << "\tt1\t" << 1/pq_.L * (pq_.L_cm * pq_.m * pq_.g * (sin(theta_) - sin(x_des) ) );
  // std::cout << "\tt2\t" << 1/pq_.L * pq_.Cd * (fabs( d_theta_ )* d_theta_ - fabs(d_x_des)*d_x_des );
  // std::cout << "\tt3\t" << -1/pq_.L * pq_.I * mpc_.lambda * (d_theta_ - d_x_des);
  // std::cout << "\n";

  // std::cout << mpc_.grav_term / pq_.L << "\t" << pq_.Cd * (phi_ + mpc_.lambda * omega_) * 2 * mpc_.x_dot_max/pq_.L << "\t"  << pq_.I * mpc_.lambda * (phi_ + mpc_.lambda * omega_)/pq_.L  << "\t" << pq_.I * phi_*a_ /pq_.L << "\t" << "\ttru\t" << x_des *180/M_PI << "\tu_mpc\t" << u_mpc_  << "\n";

  addUp();
  // if (theta_ >= M_PI && theta_ <= 2*M_PI || theta_ >= 3*M_PI && theta_ <= 4*M_PI || theta_ >= 5*M_PI && theta_ <= 6*M_PI){
  //   if (mpc_.disturb_type < 0.5){
  //       u_ = mpc_.disturb * mpc_.disturbance_factor * max( double(rand())/RAND_MAX, 0.8 ) * fabs(sin(theta_));
  //   }
  //   else if (mpc_.disturb_type < 1.5){
  //     u_ = -mpc_.disturb * mpc_.disturbance_factor * max( double(rand())/RAND_MAX, 0.8 ) * fabs(sin(theta_));
  //   }
  //   else{
  //     u_ = mpc_.disturb * mpc_.disturbance_factor * max( double(rand())/RAND_MAX, 0.8 ) * pow(-1, rand()%2 ) * fabs(sin(theta_));
  //   }
  // }

  if (theta_ >= 0.5* M_PI && theta_ <= 1.5*M_PI || theta_ >= 2.5*M_PI && theta_ <= 3.5*M_PI || theta_ >= 4.5*M_PI && theta_ <= 5.5*M_PI){
    if (mpc_.disturb_type < 0.5){
        u_ = mpc_.disturb * mpc_.disturbance_factor * max( double(rand())/RAND_MAX, 0.5 ) * fabs(cos(theta_));
    }
    else if (mpc_.disturb_type < 1.5){
      u_ = -mpc_.disturb * mpc_.disturbance_factor * max( double(rand())/RAND_MAX, 0.5 ) * fabs(cos(theta_));
    }
    else{
      u_ = mpc_.disturb * mpc_.disturbance_factor * max( double(rand())/RAND_MAX, 0.5 ) * pow(-1, rand()%2 ) * fabs(cos(theta_));
    }
  }

  u_rec_ += u_;
  addUp();
  u_ = 0.05;
  addUp();
  u_ = -0.05;
  addUp();
}

void BLSC_Pendulum::processNewTrajectory(){
  future_available_ = true; //we got a new trajectory; be default it's a future trajcetory

  // if (mpc_.verbose > 0.5)
    // std::cout << "RECEIVING new trajectory; index: " << inits_.req_id << "\n";
  end = std::chrono::system_clock::now();
  diff = end-start;
  if (mpc_.verbose > 0.5)
    std::cout << "TIME: " << diff.count() << " s\n";

  if (!use_available_){ //it must be the very first generated trajectory
    // std::cout <<"FIRST TIME\n";
    useFutureTrajectory();
    running_ = true;
  }
}

void BLSC_Pendulum::useFutureTrajectory(){
  t_traj_use_ = t_traj_future_;
  u_traj_use_ = u_traj_future_;
  phi_traj_use_ = phi_traj_future_;
  omega_traj_use_ = omega_traj_future_;
  alpha_traj_use_ = alpha_traj_future_;
  x_traj_use_ = x_traj_future_;
  use_available_ = true;
  future_available_ = false;

  start = std::chrono::system_clock::now();
  setNextDTMPCinits();
  request_available_ = true;
  if (mpc_.verbose > 0.5)
    std::cout << "requesting new trajectory; index: " << inits_.req_id << "\n";
}

void BLSC_Pendulum::setFirstDTMPCinits(){
  // mpc_.D = 0.2 * pq_.L/pq_.I - mpc_.eta;
  // mpc_.last_alpha =
  // mpc_.phi_0 = (mpc_.D + mpc_.eta ) / mpc_.last_alpha;
  // mpc_.lambda = (mpc_.D + mpc_.eta ) / (mpc_.last_alpha * mpc_.omega_max );
  // std::cout << "-------------\nD\t" << mpc_.D << "\tlambda\t" << mpc_lambda;

  inits_.last_u = mpc_.last_u;
  inits_.last_alpha = mpc_.last_alpha;
  inits_.omega_0 = mpc_.omega_0;
  inits_.phi_0 = mpc_.phi_0;
  inits_.theta_0 = theta_;
  inits_.d_theta_0 = d_theta_;
  inits_.t_0 = 0;
  inits_.req_id = 0;
}

void BLSC_Pendulum::setNextDTMPCinits(){
  inits_.last_u = u_traj_use_(0, DTMPC_period-1);
  inits_.last_alpha = alpha_traj_use_(0, DTMPC_period-1);
  inits_.omega_0 = omega_traj_use_(0, DTMPC_period);
  inits_.phi_0 = phi_traj_use_(0, DTMPC_period);
  inits_.theta_0 = x_traj_use_(0, DTMPC_period);
  inits_.d_theta_0 = x_traj_use_(1, DTMPC_period);
  inits_.t_0 = t_traj_use_(0, DTMPC_period);
  inits_.req_id += 1;
}
//
void BLSC_Pendulum::checkToSwitchTrajectory(){
  if (future_available_ && use_available_){
    // both trajectories are available
    if (double(tnow_) - double(t_traj_future_(0,0)) >= 0){
      // std::cout << "next_traj" << tnow_ << "\t" <<  t_traj_future_(0,0) << "\t" << t_traj_future_(0,1) << "\n";
      // we should be using future trajectory now.
      useFutureTrajectory();
      if (mpc_.verbose > 0.5)
        std::cout << "USE_NEXT_TRAJ\n";
    }
  }
  if (!future_available_ && tnow_ > t_traj_use_(0,dim::T)){
    time_start_ += tnow_-t_traj_use_(0,0);
    tnow_ = t_traj_use_(0,0);
    if (mpc_.verbose > 0.5)
      std::cout << "-----------------\nTOO AHEAD\treq_id\t" << inits_.req_id << "\n";
  }

  if (tnow_ < t_traj_use_(0,0)){
    time_start_ += tnow_-t_traj_use_(0,0);
    tnow_ = t_traj_use_(0,0);
    if (mpc_.verbose > 0.5)
      std::cout << "-----------------\nTOO BEHIND\treq_id\t" << inits_.req_id << "\n";
  }
}

void BLSC_Pendulum::setParameters(const Params& params, const MPC_params& mpcs, const Physical_Quantities& pqs) {
  params_ = params;
  mpc_ = mpcs;
  pq_ = pqs;
}

void BLSC_Pendulum::setPID(const PID_params& pids) {
  pid_ = pids;
}

// ----------------------------------------------------------------------------


void BLSC_Pendulum::runController(){
  start = std::chrono::system_clock::now();
  setFirstDTMPCinits();
  request_available_ = true;
  if (mpc_.verbose > 0.5)
    std::cout << "requesting new trajectory; index: " << inits_.req_id << "\n\n";
}

void BLSC_Pendulum::stopController(){
  running_ = false;
}

int BLSC_Pendulum::lookup(int m_num, float thrust){
  // std::cout << m_num << "\t" << thrust << std::endl;
  int i = 0;
  if (m_num == 6){thrust *= params_.m6;}
  if (m_num == 7){thrust *= params_.m7;}

  if (thrust >= pq_.umax){ thrust = pq_.umax; }

  while (thrust_[i] < thrust){i+=1;}
  if (i == 0){
    if (m_num == 6) {return esc_m6_[0]-3;}
    if (m_num == 7) {return esc_m7_[0]-3;}
  }
  if (m_num == 6) {
    return esc_m6_[i-1] + (esc_m6_[i] - esc_m6_[i-1]) * (thrust-thrust_[i-1]) / (thrust_[i] - thrust_[i-1]);
  }
  if (m_num == 7) {
    return esc_m7_[i-1] + (esc_m7_[i] - esc_m7_[i-1]) * (thrust-thrust_[i-1]) / (thrust_[i] - thrust_[i-1]);
  }
}

void BLSC_Pendulum::addUp() {
  if (u_ < 0) {
    u_neg_ += -u_;
  }
  else if (u_ > 0) {
    u_pos_ += u_;
  }
  // u_last_ = u_;
  u_ = 0.0;
}

void BLSC_Pendulum::idlePID(){
  u_ = pid_.onset;
  addUp();
  u_ = -pid_.onset;
  addUp();
  u_ = -pid_.Kp * theta_ - pid_.Kd * d_theta_;
  // std::cout << "kp" << u_ << "\n";
  addUp();
}

void BLSC_Pendulum::findPWM(uint16_t *pwm){
  pwm[6] = lookup(6, u_pos_); // assuming that first motor is in positivve theta direction, second motor in negative
  pwm[7] = lookup(7, u_neg_);
}

Eigen::Vector3d BLSC_Pendulum::computeControl(uint16_t *pwm) {

  u_ = 0.0; u_neg_ = 0.0; u_pos_ = 0.0;
  // priorToFirstDTMPC();
  // auto start = std::chrono::system_clock::now();
  // auto end = std::chrono::system_clock::now();
  // std::chrono::duration<double> diff = end-start;
  // start = std::chrono::system_clock::now();

  if (use_available_){
      checkToSwitchTrajectory();
      doBLSC();
  }
  else {
    idlePID();
  }

  // end = std::chrono::system_clock::now();
  // diff = end-start;
  // std::cout << "DTMPC: " << diff.count() << " s\n\n";
  findPWM(pwm);

  Eigen::Vector3d torq; torq << -u_neg_, u_pos_, 0.0;
  return torq;
}


// ----------------------------------------------------------------------------

void BLSC_Pendulum::estimateAngle(uint64_t time_us, const Eigen::Vector3f& acc, const Eigen::Vector3f& gyr)
{
  static uint64_t last_time_us = time_us;

  // initialize filter with first measurement
  if (!lpf_initialized_) {
    acc_ = acc;
    gyr_ = gyr;
    lpf_initialized_ = true;
  }

  // lowpass filter input measurements
  LPF(params_.lpf_acc_alpha, acc, acc_);
  LPF(params_.lpf_gyr_alpha, gyr, gyr_);

  //
  // Estimate orientation from gyro
  //

  const double dt = (time_us - last_time_us)*1e-6;

  quat_.w() -= 0.5*(quat_.x()*gyr.x() + quat_.y()*gyr.y() + quat_.z()*gyr.z())*dt;
  quat_.x() += 0.5*(quat_.w()*gyr.x() - quat_.z()*gyr.y() + quat_.y()*gyr.z())*dt;
  quat_.y() += 0.5*(quat_.z()*gyr.x() + quat_.w()*gyr.y() - quat_.x()*gyr.z())*dt;
  quat_.z() += 0.5*(quat_.x()*gyr.y() - quat_.y()*gyr.x() + quat_.w()*gyr.z())*dt;
  quat_.normalize();

  // TODO: use accelerometer in complementary fashion
  last_time_us = time_us;
  if (running_ && time_start_ == 0){ //start recorded
    time_start_ = uint64_t(time_us/1000);
  }
  tnow_ = uint64_t((time_us)/1000) - time_start_; //time in ms since start
  t_ = (float) tnow_ / 1000; // set tiem in seconds since start


  //
  // Extract angle
  //

  // for convenience, extract the angle about the axis of rotation (x-axis)
  // Eigen::Vector3d euler = quat_.toRotationMatrix().eulerAngles(2, 1, 0);
  //angle conversion
  float t1, t2;
  t1 = 2.0 * (quat_.w() * quat_.x() + quat_.y() * quat_.z());
  t2 = 1.0 - 2.0 * (quat_.x() * quat_.x() + quat_.y() * quat_.y());
  float ex = atan2(t1, t2);
  t2 = 2.0 * (quat_.w() * quat_.y() - quat_.z() * quat_.x());
  if (t2>1){t2 = 1;}
  if (t2<-1){t2 = -1;}
  float ey = asin(t2);
  t1 = 2.0 * (quat_.w() * quat_.z() + quat_.x() * quat_.y());
  t2 = 1.0 - 2.0 * (quat_.y()*quat_.y() + quat_.z()*quat_.z());
  float ez = atan2(t1, t2);
  if (ez > M_PI){
    ez = ez - 2*M_PI;
  }

  d_theta_ = gyr.x();
  if (ex - theta_ < -5*M_PI){
    theta_ = ex + 6 * M_PI;
  }
  else if (ex - theta_ < -3*M_PI) {
    theta_ = ex + 4 * M_PI;
  }
  else if (ex - theta_ < -1*M_PI) {
    theta_ = ex + 2 * M_PI;
  }
  else if (ex - theta_ > M_PI) {
    theta_ = ex - 2 * M_PI;
  }
  else if (ex - theta_ > 3*M_PI) {
    theta_ = ex - 4 * M_PI;
  }
  else if (ex - theta_ > 5*M_PI) {
    theta_ = ex - 6 * M_PI;
  }
  else {
    theta_ = ex;
  }
}

// ----------------------------------------------------------------------------


float BLSC_Pendulum::sat(float a){
  //saturation function
  if (a >= 1)
    return 1.0;
  else if (a <= -1)
    return -1.0;
  else
    return a;
}


} // ns pendulum
} // ns acl
