/**
 * @file pendulum_ros.cpp
 * @brief ROS wrapper for pendulum components
 * @author Savva Morozov <savva@mit.edu>
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 Dec 2019
 */

#include "dtmpc_pendulum/dtmpc_ros.h"
#include <chrono>


namespace acl {
namespace pendulum {

DTMPC_ROS::DTMPC_ROS(const ros::NodeHandle nh, const ros::NodeHandle nhp)
: nh_(nh), nhp_(nhp)
{
  init();
  pub_trajectory_ = nh_.advertise<pendulum_msgs::trajectory>("trajectory", 1);
  // sub_request_ = nh_.subscribe<pendulum_msgs::traj_req>("/Pend01/traj_req", 1, &DTMPC_ROS::processRequest, this);
  // sub_request_ = nh_.subscribe("traj_rec", 1, processRequest);
  sub_request_ = nh_.subscribe("/Pend01/traj_req", 1, &DTMPC_ROS::processRequest, this);

}

// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

void DTMPC_ROS::processRequest(const pendulum_msgs::traj_req& msg) {
  // auto start = std::chrono::system_clock::now();
  // auto end = std::chrono::system_clock::now();
  // std::chrono::duration<double> diff = start-end;
  // start = std::chrono::system_clock::now();
  // map raw buffer to Eigen datatype

  // WARNING do some magic to determine inputs.
  dtmpc_pendulum_.inits_.last_u = msg.last_u;
  dtmpc_pendulum_.inits_.last_alpha = msg.last_alpha;
  dtmpc_pendulum_.inits_.omega_0 = msg.omega_0;
  dtmpc_pendulum_.inits_.phi_0 = msg.phi_0;
  dtmpc_pendulum_.inits_.theta_0 = msg.theta_0;
  dtmpc_pendulum_.inits_.d_theta_0 = msg.d_theta_0;
  dtmpc_pendulum_.inits_.t_0 = msg.t_0;
  dtmpc_pendulum_.inits_.req_id = msg.req_id;

  // calculate the desired torque and mix down to PWM signals
  dtmpc_pendulum_.computeTrajectory();
  // end = std::chrono::system_clock::now();
  // diff = end-start;
  // std::cout << "\nDTMPC, before publishing: " << diff.count() << " s\n\n";
  // publish the trajectory
  { // trajectory
    pendulum_msgs::trajectory msg;
    msg.header.stamp = ros::Time::now(); // TODO: use time_us instead?
    msg.tnow = dtmpc_pendulum_.tnow_;

    std_msgs::Float64MultiArray array; array.data.clear();
    for (int i = 0; i < dim::T+1; i++){ array.data.push_back(dtmpc_pendulum_.x_traj_(0,i) );}
    msg.theta_traj = array;

    array.data.clear();
    for (int i = 0; i < dim::T+1; i++){ array.data.push_back(dtmpc_pendulum_.x_traj_(1,i));}
    msg.d_theta_traj = array;

    array.data.clear();
    for (int i = 0; i < dim::T+1; i++){ array.data.push_back(dtmpc_pendulum_.u_traj_(0,i));}
    msg.u_traj = array;

    array.data.clear();
    for (int i = 0; i < dim::T+1; i++){ array.data.push_back(dtmpc_pendulum_.omega_traj_(0,i));}
    msg.omega_traj = array;

    array.data.clear();
    for (int i = 0; i < dim::T+1; i++){ array.data.push_back(dtmpc_pendulum_.phi_traj_(0,i));}
    msg.phi_traj = array;

    array.data.clear();
    for (int i = 0; i < dim::T+1; i++){ array.data.push_back(dtmpc_pendulum_.alpha_traj_(0,i));}
    msg.alpha_traj = array;

    std_msgs::UInt64MultiArray uint_array; uint_array.data.clear();
    for (int i = 0; i < dim::T+1; i++){ uint_array.data.push_back(dtmpc_pendulum_.t_traj_(0,i));}
    msg.t_traj = uint_array;

    pub_trajectory_.publish(msg);
  }
  // end = std::chrono::system_clock::now();
  // diff = end-start;
  // std::cout << "\nDTMPC, full: " << diff.count() << " s\n\n";
  // end = std::chrono::system_clock::now();
  // diff = end-start;
  // std::cout << "everything: " << diff.count() << " s\n\n";
}

// ----------------------------------------------------------------------------

void DTMPC_ROS::init()
{
  DTMPC_Pendulum::Params params;
  nhp_.param<float>("params/lpf/acc/alpha", params.lpf_acc_alpha, 0.6);
  nhp_.param<float>("params/lpf/gyr/alpha", params.lpf_gyr_alpha, 0.6);
  nhp_.param<float>("params/m6", params.m6, 1.5);
  nhp_.param<float>("params/m7", params.m7, 1.3);

  DTMPC_Pendulum::MPC_params mpcs;
  nhp_.param<double>("mpc/x_final", mpcs.x_final, 1.5708);
  nhp_.param<double>("mpc/delta_t", mpcs.delta_t, 0.005);
  nhp_.param<int>("mpc/delta_t_ms", mpcs.delta_t_ms, 5);
  nhp_.param<double>("mpc/Q1", mpcs.Q1, 1.0);
  nhp_.param<double>("mpc/Q2", mpcs.Q2, 0.1);
  nhp_.param<double>("mpc/R", mpcs.R, 0.1);
  nhp_.param<double>("mpc/M", mpcs.M, 0.1);
  nhp_.param<double>("mpc/Q_final1", mpcs.Q_final1, 10.0);
  nhp_.param<double>("mpc/Q_final2", mpcs.Q_final2, 10.0);

  // nhp_.param<double>("mpc/w_max_coarse", mpcs.w_max_coarse, 0.5);
  // nhp_.param<double>("mpc/w_max_fine", mpcs.w_max_fine, 0.2);
  nhp_.param<double>("mpc/delta_u_max", mpcs.delta_u_max, 2.0);
  // nhp_.param<double>("mpc/delta_u_max_fine", mpcs.delta_u_max_fine, 2.0);
  // nhp_.param<double>("mpc/u_cutoff", mpcs.u_cutoff, 2.0);
  nhp_.param<double>("mpc/u_max", mpcs.u_max, 1.8);
  nhp_.param<double>("mpc/omega_max", mpcs.omega_max, 0.0873);
  nhp_.param<double>("mpc/alpha_min", mpcs.alpha_min, 4.0);
  nhp_.param<double>("mpc/alpha_max", mpcs.alpha_max, 25.0);
  nhp_.param<double>("mpc/delta_alpha_max", mpcs.delta_alpha_max, 3.0);
  nhp_.param<double>("mpc/x_dot_max", mpcs.x_dot_max, 31.415);

  nhp_.param<double>("mpc/last_u", mpcs.last_u, 0.0);
  nhp_.param<double>("mpc/last_alpha", mpcs.last_alpha, 0.0);

  nhp_.param<double>("mpc/phi_0", mpcs.phi_0, 6.0);
  nhp_.param<double>("mpc/omega_0", mpcs.omega_0, 6.0);

  nhp_.param<double>("mpc/D", mpcs.D, 5.0);
  nhp_.param<double>("mpc/lambda", mpcs.lambda, 60.0);
  nhp_.param<double>("mpc/eta", mpcs.eta, 1.0);

  nhp_.param<double>("mpc/Cd_max", mpcs.Cd_max, 0.001);
  nhp_.param<double>("mpc/disturb", mpcs.disturb, 0.0);
  nhp_.param<double>("mpc/disturb_type", mpcs.disturb_type, 0);

  nhp_.param<double>("mpc/delta_l_cm", mpcs.delta_l_cm, 0);

  nhp_.param<double>("mpc/grav_term", mpcs.grav_term, 0);
  nhp_.param<double>("mpc/delta_alpha_slew", mpcs.delta_alpha_slew, 0);
  nhp_.param<double>("mpc/verbose", mpcs.verbose, 0);

  DTMPC_Pendulum::Finals fins;
  nhp_.param<double>("fin/num_stops", fins.num_stops, 0);
  // if (mpcs.num_stops > 0.5){
  //   // double temp = 0.0;
  //   for(int i = 0; i < mpcs.num_stops; i++){
  //     char st[10] = {'m', 'p', 'c', '/', 's', 't', 'o', 'p', char(i), '\0'};
  //     nhp_.param<double>(st, mpcs.stops[i], 0);
  //   }
  // }


  DTMPC_Pendulum::Physical_Quantities pqs;
  nhp_.param<float>("pq/g", pqs.g, 9.81);
  nhp_.param<float>("pq/m", pqs.m, 0.214);
  nhp_.param<float>("pq/L_cm", pqs.L_cm, 0.0921);
  nhp_.param<float>("pq/L", pqs.L, 0.229);
  nhp_.param<float>("pq/I", pqs.I, 0.00417);
  nhp_.param<float>("pq/Cd", pqs.Cd, 0.000077);
  nhp_.param<float>("pq/umax", pqs.umax, 1.8);

  dtmpc_pendulum_.setParameters(params, mpcs, pqs, fins);

}

// ----------------------------------------------------------------------------
// ROS Callbacks
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------



} // ns pendulum
} // ns acl
