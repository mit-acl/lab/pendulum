/**
 * @file pendulum_ros.cpp
 * @brief ROS wrapper for pendulum components
 * @author Savva Morozov <savva@mit.edu>
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 Dec 2019
 */

#include "dtmpc_pendulum/pendulum_ros.h"
#include <chrono>




namespace acl {
namespace pendulum {

PendulumROS::PendulumROS(const ros::NodeHandle nh, const ros::NodeHandle nhp)
: nh_(nh), nhp_(nhp)
{

  init();

  //
  // Setup ROS communication
  //

  pub_imu_ = nh_.advertise<sensor_msgs::Imu>("imu", 1);
  pub_imu_filtered_ = nh_.advertise<sensor_msgs::Imu>("imu_filtered", 1);
  pub_torque_ = nh_.advertise<geometry_msgs::Vector3Stamped>("torque", 1);
  pub_motors_ = nh_.advertise<snapstack_msgs::Motors>("motors", 1);
  pub_pose_ = nh_.advertise<geometry_msgs::PoseStamped>("pose", 1);
  pub_angle_ = nh_.advertise<geometry_msgs::Vector3Stamped>("angle_deg", 1);
  pub_pend_ = nh_.advertise<pendulum_msgs::pendulum_state>("pendulum", 1);
  pub_traj_req_ = nh_.advertise<pendulum_msgs::traj_req>("traj_req", 1);

  sub_trajectory_ = nh_.subscribe("/Pend01/trajectory", 1, &PendulumROS::processTrajectory, this);

  //arming
  srv_arm_ = nh_.advertiseService("arm", &PendulumROS::armCB, this);
  srv_isarmed_ = nh_.advertiseService("is_armed", &PendulumROS::isarmedCB, this);
  //start the controller service
  srv_runController_ = nh_.advertiseService("runController", &PendulumROS::runController, this);

  //
  // Setup the IMU communication
  //

  // When IMU data is available, let this class know via its handler
  imuman_.AddHandler(this);

  // Start handling IMU data asynchronously --- kicks off est/cntrl process
  imuman_.Initialize();
  imuman_.Start();
}




void PendulumROS::processTrajectory(const pendulum_msgs::trajectory& msg) {
  auto start = std::chrono::system_clock::now();
  auto end = std::chrono::system_clock::now();
  std::chrono::duration<double> diff = start-end;
  start = std::chrono::system_clock::now();
  // map raw buffer to Eigen datatype

  //update the future use trajectory

  for(int i = 0; i < dim::T+1; i++){
    pendulum_.t_traj_future_(0,i) = msg.t_traj.data[i];
    pendulum_.u_traj_future_(0,i) = msg.u_traj.data[i];
    pendulum_.omega_traj_future_(0,i) = msg.omega_traj.data[i];
    pendulum_.alpha_traj_future_(0,i) = msg.alpha_traj.data[i];
    pendulum_.phi_traj_future_(0,i) = msg.phi_traj.data[i];
    pendulum_.x_traj_future_(0,i) = msg.theta_traj.data[i];
    pendulum_.x_traj_future_(1,i) = msg.d_theta_traj.data[i];
  };

  pendulum_.processNewTrajectory();

  // end = std::chrono::system_clock::now();
  // diff = end-start;

  // std::cout << "BLSC everything: " << diff.count() << " s\n\n";
  // end = std::chrono::system_clock::now();
  // diff = end-start;
  // std::cout << "everything: " << diff.count() << " s\n\n";
}


// ----------------------------------------------------------------------------

bool PendulumROS::Imu_IEventListener_ProcessSamples(sensor_imu* samples,
                                                          uint32_t count)
{
  constexpr float kNormG = 9.80665f;

  // biases for correcting first-order IMU inaccuracies (captured at startup)
  static float bacc_x = 0.0f, bacc_y = 0.0f, bacc_z = 0.0f;
  static float bgyr_x = 0.0f, bgyr_y = 0.0f, bgyr_z = 0.0f;
  constexpr size_t CALIB_COUNT = 300;

  // NOTE: we will just use the last buffered IMU sample ¯\_(ツ)_/¯
  //      (we probably won't be dropping samples anyways)
  const size_t s = count-1;

  // unpack IMU data
  uint64_t time_us = samples[s].timestamp_in_us;
  float acc[3] = { 0.0f };
  float gyr[3] = { 0.0f };

  // Using original IMU orientation. See ATLFlight docs for image.
  // (https://github.com/ATLFlight/ros-examples#coordinate-frame-conventions)
  acc[0] = samples[s].linear_acceleration[0] * kNormG;
  acc[1] = samples[s].linear_acceleration[1] * kNormG;
  acc[2] = samples[s].linear_acceleration[2] * kNormG;
  gyr[0] = samples[s].angular_velocity[0];
  gyr[1] = samples[s].angular_velocity[1];
  gyr[2] = samples[s].angular_velocity[2];

  // TODO: calibrate IMU on startup
  static size_t n = 0;
  if (!is_imu_calibrated_) {

    // TODO: +gravity is removed from the y accel value since y is pointing up.
    // Instead of hardcoding this into the code, we could be smarter.

    n++;
    bacc_x += acc[0];
    bacc_y += acc[1] - kNormG;
    bacc_z += acc[2];
    bgyr_x += gyr[0];
    bgyr_y += gyr[1];
    bgyr_z += gyr[2];

    if (n >= CALIB_COUNT) {
      // average the noise to calculate a constant bias term
      bacc_x /= static_cast<float>(n);
      bacc_y /= static_cast<float>(n);
      bacc_z /= static_cast<float>(n);
      bgyr_x /= static_cast<float>(n);
      bgyr_y /= static_cast<float>(n);
      bgyr_z /= static_cast<float>(n);
      is_imu_calibrated_ = true;
      ROS_INFO("IMU calibration complete");
    }
  } else {
    // remove constant biases
    acc[0] -= bacc_x;
    acc[1] -= bacc_y;
    acc[2] -= bacc_z;
    gyr[0] -= bgyr_x;
    gyr[1] -= bgyr_y;
    gyr[2] -= bgyr_z;

    process(time_us, acc, gyr);
  }

  return true;
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

void PendulumROS::process(uint64_t time_us, float acc[3], float gyr[3])
{
  // auto start = std::chrono::system_clock::now();
  // auto end = std::chrono::system_clock::now();
  // std::chrono::duration<double> diff = start-end;
  // start = std::chrono::system_clock::now();
  // map raw buffer to Eigen datatype
  Eigen::Map<Eigen::Vector3f> accVec(acc);
  Eigen::Map<Eigen::Vector3f> gyrVec(gyr);
  uint16_t pwm[EscMan::NUM_PWMS] = {0,0,0,0, 0,0,0,0};

  //
  // Main control loop
  //

  // allow pendulum to estimate its current angle given IMU data
  pendulum_.estimateAngle(time_us, accVec, gyrVec);
  // calculate the desired torque and mix down to PWM signals
  const auto torque = pendulum_.computeControl(pwm);
  // achieve the desired torque by actuating motors via desired pwm
  escman_.update(pwm);

  //
  // Publish signals for logging

  if (pendulum_.request_available_){
    pendulum_.request_available_ = false; // drop the request flag
    pendulum_msgs::traj_req msg;
    msg.header.stamp = ros::Time::now(); // TODO: use time_us instead?
    msg.last_u = pendulum_.inits_.last_u;
    msg.last_alpha = pendulum_.inits_.last_alpha;
    msg.omega_0 = pendulum_.inits_.omega_0;
    msg.phi_0 = pendulum_.inits_.phi_0;
    msg.theta_0 = pendulum_.inits_.theta_0;
    msg.d_theta_0 = pendulum_.inits_.d_theta_0;
    msg.t_0 = pendulum_.inits_.t_0;
    msg.req_id = pendulum_.inits_.req_id;
    pub_traj_req_.publish(msg);
  }

  if (pendulum_.running_)
  { // motor pwm commands
    pendulum_msgs::pendulum_state msg;
    msg.header.stamp = ros::Time::now(); // TODO: use time_us instead?
    msg.tnow = pendulum_.tnow_;

    msg.theta = pendulum_.theta_;
    msg.d_theta = pendulum_.d_theta_;
    msg.theta_des = pendulum_.x_des;
    msg.d_theta_des = pendulum_.d_x_des;

    msg.s = pendulum_.s_;
    msg.omega = pendulum_.omega_;
    msg.alpha = pendulum_.a_;
    msg.phi = pendulum_.phi_;

    msg.u = pendulum_.u_rec_;
    msg.u_mpc = pendulum_.u_mpc_;
    msg.u_sliding = pendulum_.u_sliding_;

    pub_pend_.publish(msg);
  }

  { // raw IMU
    sensor_msgs::Imu msg;
    msg.header.stamp = ros::Time::now(); // TODO: use time_us instead?
    msg.header.frame_id = "imu_link";
    msg.angular_velocity.x = gyrVec.x();
    msg.angular_velocity.y = gyrVec.y();
    msg.angular_velocity.z = gyrVec.z();
    msg.linear_acceleration.x = accVec.x();
    msg.linear_acceleration.y = accVec.y();
    msg.linear_acceleration.z = accVec.z();
    pub_imu_.publish(msg);
  }

  { // filtered IMU
    sensor_msgs::Imu msg;
    msg.header.stamp = ros::Time::now(); // TODO: use time_us instead?
    msg.header.frame_id = "imu_link";
    msg.angular_velocity.x = pendulum_.getFilteredGyro().x();
    msg.angular_velocity.y = pendulum_.getFilteredGyro().y();
    msg.angular_velocity.z = pendulum_.getFilteredGyro().z();
    msg.linear_acceleration.x = pendulum_.getFilteredAccel().x();
    msg.linear_acceleration.y = pendulum_.getFilteredAccel().y();
    msg.linear_acceleration.z = pendulum_.getFilteredAccel().z();
    pub_imu_filtered_.publish(msg);
  }

  { // estimated pose
    geometry_msgs::PoseStamped msg;
    msg.header.stamp = ros::Time::now(); // TODO: use time_us instead?
    msg.header.frame_id = "base_link";
    msg.pose.position.x = 0.0;
    msg.pose.position.y = 0.0;
    msg.pose.position.z = 0.0;
    msg.pose.orientation.w = pendulum_.getQuaternion().w();
    msg.pose.orientation.x = pendulum_.getQuaternion().x();
    msg.pose.orientation.y = pendulum_.getQuaternion().y();
    msg.pose.orientation.z = pendulum_.getQuaternion().z();
    pub_pose_.publish(msg);
  }

  { // broadcast estimated pose on tf tree
    geometry_msgs::TransformStamped msg;
    msg.header.stamp = ros::Time::now(); // TODO: use time_us instead?
    msg.header.frame_id = "base_link";
    msg.child_frame_id = "arm";
    msg.transform.translation.x = 0.0;
    msg.transform.translation.y = 0.0;
    msg.transform.translation.z = 0.0;
    msg.transform.rotation.w = pendulum_.getQuaternion().w();
    msg.transform.rotation.x = pendulum_.getQuaternion().x();
    msg.transform.rotation.y = pendulum_.getQuaternion().y();
    msg.transform.rotation.z = pendulum_.getQuaternion().z();
    br_.sendTransform(msg);
  }

    { // extracted estimated angle for convenience
    geometry_msgs::Vector3Stamped msg;
    msg.header.stamp = ros::Time::now(); // TODO: use time_us instead?
    msg.header.frame_id = "base_link";
    msg.vector.x = pendulum_.getAngle() * 180.0/M_PI;
    msg.vector.y = 0.0;
    msg.vector.z = 0.0;
    pub_angle_.publish(msg);
  }

  { // desired torque
    geometry_msgs::Vector3Stamped msg;
    msg.header.stamp = ros::Time::now(); // TODO: use time_us instead?
    msg.header.frame_id = "base_link";
    msg.vector.x = torque.x();
    msg.vector.y = torque.y();
    msg.vector.z = torque.z();
    pub_torque_.publish(msg);
  }

  { // motor pwm commands
    snapstack_msgs::Motors msg;
    msg.header.stamp = ros::Time::now(); // TODO: use time_us instead?
    msg.m7 = pwm[6];
    msg.m8 = pwm[7];
    pub_motors_.publish(msg);
  }
  // end = std::chrono::system_clock::now();
  // diff = end-start;
  // std::cout << "everything: " << diff.count() << " s\n\n";
}

// ----------------------------------------------------------------------------

void PendulumROS::init() {
  BLSC_Pendulum::Params params;
  nhp_.param<float>("params/lpf/acc/alpha", params.lpf_acc_alpha, 0.6);
  nhp_.param<float>("params/lpf/gyr/alpha", params.lpf_gyr_alpha, 0.6);
  nhp_.param<float>("params/m6", params.m6, 1.5);
  nhp_.param<float>("params/m7", params.m7, 1.3);

  BLSC_Pendulum::MPC_params mpcs;
  nhp_.param<double>("mpc/x_final", mpcs.x_final, 1.5708);
  nhp_.param<double>("mpc/delta_t", mpcs.delta_t, 0.005);
  nhp_.param<int>("mpc/delta_t_ms", mpcs.delta_t_ms, 5);
  nhp_.param<double>("mpc/Q1", mpcs.Q1, 1.0);
  nhp_.param<double>("mpc/Q2", mpcs.Q2, 0.1);
  nhp_.param<double>("mpc/R", mpcs.R, 0.1);
  nhp_.param<double>("mpc/M", mpcs.M, 0.1);
  nhp_.param<double>("mpc/Q_final1", mpcs.Q_final1, 10.0);
  nhp_.param<double>("mpc/Q_final2", mpcs.Q_final2, 10.0);

  // nhp_.param<double>("mpc/w_max_coarse", mpcs.w_max_coarse, 0.5);
  // nhp_.param<double>("mpc/w_max_fine", mpcs.w_max_fine, 0.2);
  nhp_.param<double>("mpc/delta_u_max", mpcs.delta_u_max, 2.0);
  nhp_.param<double>("mpc/u_max", mpcs.u_max, 1.8);
  nhp_.param<double>("mpc/omega_max", mpcs.omega_max, 0.0873);
  nhp_.param<double>("mpc/alpha_min", mpcs.alpha_min, 4.0);
  nhp_.param<double>("mpc/alpha_max", mpcs.alpha_max, 25.0);
  nhp_.param<double>("mpc/delta_alpha_max", mpcs.delta_alpha_max, 3.0);
  nhp_.param<double>("mpc/x_dot_max", mpcs.x_dot_max, 31.415);

  nhp_.param<double>("mpc/last_u", mpcs.last_u, 0.0);
  nhp_.param<double>("mpc/last_alpha", mpcs.last_alpha, 5.0);

  nhp_.param<double>("mpc/phi_0", mpcs.phi_0, 6.0);
  nhp_.param<double>("mpc/omega_0", mpcs.omega_0, 6.0);

  nhp_.param<double>("mpc/D", mpcs.D, 5.0);
  nhp_.param<double>("mpc/lambda", mpcs.lambda, 60.0);
  nhp_.param<double>("mpc/eta", mpcs.eta, 1.0);

  nhp_.param<double>("mpc/Cd_max", mpcs.Cd_max, 0.001);
  nhp_.param<double>("mpc/disturb", mpcs.disturb, 0.0);
  nhp_.param<double>("mpc/disturb_type", mpcs.disturb_type, 0);
  nhp_.param<double>("mpc/disturbance_factor", mpcs.disturbance_factor, 0);

  nhp_.param<double>("mpc/grav_term", mpcs.grav_term, 0);
  nhp_.param<double>("mpc/verbose", mpcs.verbose, 0);


  BLSC_Pendulum::PID_params pids;
  nhp_.param<float>("pid/Kp", pids.Kp, 60.0);
  nhp_.param<float>("pid/Kd", pids.Kd, 0.0);
  nhp_.param<float>("pid/onset", pids.onset, 60.0);
  pendulum_.setPID(pids);

  BLSC_Pendulum::Physical_Quantities pqs;
  nhp_.param<float>("pq/g", pqs.g, 9.81);
  nhp_.param<float>("pq/m", pqs.m, 0.214);
  nhp_.param<float>("pq/L_cm", pqs.L_cm, 0.0921);
  nhp_.param<float>("pq/L", pqs.L, 0.229);
  nhp_.param<float>("pq/I", pqs.I, 0.00417);
  nhp_.param<float>("pq/Cd", pqs.Cd, 0.000077);
  nhp_.param<float>("pq/umax", pqs.umax, 1.8);

  pendulum_.setParameters(params, mpcs, pqs);
}

// ----------------------------------------------------------------------------
// ROS Callbacks
// ----------------------------------------------------------------------------

bool PendulumROS::runController(std_srvs::SetBool::Request &req,
                        std_srvs::SetBool::Response &res) {
  bool start_requested = req.data;
  // true - start
  if (start_requested && escman_.isArmed()) { //start controller
    pendulum_.runController();
    res.success = true;
    res.message = "Controller started.";
  } else if (start_requested && !escman_.isArmed()) { //start controller and escs
    bool success = escman_.arm();
    if (!success) { res.message = "Failed to arm ESCs, controller not started"; res.success = false; }
    else {
      res.message = "ESCs armed, controller started";
      res.success = true;
      pendulum_.runController();
    }
  } else {
    pendulum_.stopController();
    bool success = escman_.disarm();
    if (!success) {res.message = "Failed to disarm, controller reset"; res.success = false;}
    else {res.message = "ESCs off, controller reset"; res.success = true;}
  }
  return true;
}

// ----------------------------------------------------------------------------

bool PendulumROS::armCB(std_srvs::SetBool::Request &req,
                        std_srvs::SetBool::Response &res)
{
  bool arm_requested = req.data;
  if (arm_requested && is_imu_calibrated_) {
    bool success = escman_.arm();
    if (!success) res.message = "Failed to arm";
    else res.message = "ARMED";
  } else {
    bool success = escman_.disarm();
    if (!success) res.message = "Failed to disarm";
    else res.message = "DISARMED";
  }

  res.success = escman_.isArmed();
  return true;
}

// ----------------------------------------------------------------------------

bool PendulumROS::isarmedCB(std_srvs::Trigger::Request &req,
                            std_srvs::Trigger::Response &res)
{
  res.success = escman_.isArmed();
  return true;
}

// ----------------------------------------------------------------------------
//
// bool PendulumROS::setThetaDes(pendulum_msgs::srv_float::Request &req,
//                               pendulum_msgs::srv_float::Response &res) {
//   if (!pendulum_.running_) { // [endulum not running just go at it]
//     pendulum_.setThetaDes(req.data * M_PI/180);
//     res.success = true;
//     return true;
//   }
//   else{ //pendulum already running let's switch to next
//     pendulum_.stopController();
//     pendulum_.setThetaDes(req.data * M_PI/180);
//     pendulum_.runController();
//     res.success = true;
//     return true;
//   }
//
// }
//


} // ns pendulum
} // ns acl
