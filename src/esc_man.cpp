/**
 * @file esc_man.h
 * @brief Handles ESC signaling for motors
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 Dec 2019
 */

#include "dtmpc_pendulum/esc_man.h"

namespace acl {
namespace pendulum {

EscMan::EscMan()
{
  escs_.reset(new acl::ESCInterface(NUM_PWMS));
  escs_->init();
}

// ----------------------------------------------------------------------------

EscMan::~EscMan()
{
  escs_->close();
}

// ----------------------------------------------------------------------------

void EscMan::update(uint16_t f[NUM_PWMS])
{
  // static constexpr uint16_t PWM_MAX = acl::ESCInterface::PWM_MAX_PULSE_WIDTH;
  // static constexpr uint16_t PWM_MIN = acl::ESCInterface::PWM_MIN_PULSE_WIDTH;
  //
  // // Convert motor force to PWM
  // uint16_t f_pwm[NUM_PWMS];
  // for (uint8_t i=0; i<NUM_PWMS; ++i) {
  //     f_pwm[i] = (uint16_t) (PWM_MAX - PWM_MIN)*f[i] + PWM_MIN;
  // }

  std::lock_guard<std::mutex> lock(mutex_);
  escs_->update(f, NUM_PWMS);
}

// ----------------------------------------------------------------------------

bool EscMan::arm()
{
  std::lock_guard<std::mutex> lock(mutex_);
  return escs_->arm();
}

// ----------------------------------------------------------------------------

bool EscMan::disarm()
{
  std::lock_guard<std::mutex> lock(mutex_);
  return escs_->disarm();
}

// ----------------------------------------------------------------------------

bool EscMan::isArmed()
{
  std::lock_guard<std::mutex> lock(mutex_);
  return escs_->is_armed();
}

} // ns pendulum
} // ns acl
