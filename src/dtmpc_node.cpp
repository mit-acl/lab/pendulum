/**
 * @file dtmpc_node.cpp
 * @brief Entry point for the dtmpc ROS node
 * @author Savva Morozov <savva@mit.edu>
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 Dec 2019
 */

#include <ros/ros.h>

#include "dtmpc_pendulum/dtmpc_ros.h"

int main(int argc, char *argv[])
{
  ros::init(argc, argv, "dtmpc");
  ros::NodeHandle nhtopics("");
  ros::NodeHandle nhparams("~");
  acl::pendulum::DTMPC_ROS node(nhtopics, nhparams);
  ros::spin();
  return 0;
}
