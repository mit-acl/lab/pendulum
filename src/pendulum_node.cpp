/**
 * @file pendulum_node.cpp
 * @brief Entry point for pendulum ROS node
 * @author Savva Morozov <savva@mit.edu>
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 Dec 2019
 */

#include <ros/ros.h>

#include "dtmpc_pendulum/pendulum_ros.h"

int main(int argc, char *argv[])
{
  ros::init(argc, argv, "pendulum");
  ros::NodeHandle nhtopics("");
  ros::NodeHandle nhparams("~");
  acl::pendulum::PendulumROS node(nhtopics, nhparams);
  ros::spin();
  return 0;
}
