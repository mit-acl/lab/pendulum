/* Produced by CVXGEN, 2020-05-26 13:14:48 -0400.  */
/* CVXGEN is Copyright (C) 2006-2017 Jacob Mattingley, jem@cvxgen.com. */
/* The code in this file is Copyright (C) 2006-2017 Jacob Mattingley. */
/* CVXGEN, or solvers produced by CVXGEN, cannot be used for commercial */
/* applications without prior written permission from Jacob Mattingley. */

/* Filename: solver.c. */
/* Description: Main solver file. */
#include "solver.h"
double eval_gap(void) {
  int i;
  double gap;
  gap = 0;
  for (i = 0; i < 407; i++)
    gap += work.z[i]*work.s[i];
  return gap;
}
void set_defaults(void) {
  settings.resid_tol = 1e-6;
  settings.eps = 1e-4;
  settings.max_iters = 25;
  settings.refine_steps = 1;
  settings.s_init = 1;
  settings.z_init = 1;
  settings.debug = 0;
  settings.verbose = 1;
  settings.verbose_refinement = 0;
  settings.better_start = 1;
  settings.kkt_reg = 1e-7;
}
void setup_pointers(void) {
  work.y = work.x + 222;
  work.s = work.x + 370;
  work.z = work.x + 777;
  vars.d_1 = work.x + 0;
  vars.d_2 = work.x + 2;
  vars.d_3 = work.x + 4;
  vars.d_4 = work.x + 6;
  vars.d_5 = work.x + 8;
  vars.d_6 = work.x + 10;
  vars.d_7 = work.x + 12;
  vars.d_8 = work.x + 14;
  vars.d_9 = work.x + 16;
  vars.d_10 = work.x + 18;
  vars.d_11 = work.x + 20;
  vars.d_12 = work.x + 22;
  vars.d_13 = work.x + 24;
  vars.d_14 = work.x + 26;
  vars.d_15 = work.x + 28;
  vars.d_16 = work.x + 30;
  vars.d_17 = work.x + 32;
  vars.d_18 = work.x + 34;
  vars.d_19 = work.x + 36;
  vars.d_20 = work.x + 38;
  vars.d_21 = work.x + 40;
  vars.d_22 = work.x + 42;
  vars.d_23 = work.x + 44;
  vars.d_24 = work.x + 46;
  vars.d_25 = work.x + 48;
  vars.d_26 = work.x + 50;
  vars.d_27 = work.x + 52;
  vars.d_28 = work.x + 54;
  vars.d_29 = work.x + 56;
  vars.d_30 = work.x + 58;
  vars.d_31 = work.x + 60;
  vars.d_32 = work.x + 62;
  vars.d_33 = work.x + 64;
  vars.d_34 = work.x + 66;
  vars.d_35 = work.x + 68;
  vars.d_36 = work.x + 70;
  vars.d_37 = work.x + 72;
  vars.delta_alpha_0 = work.x + 74;
  vars.delta_alpha_1 = work.x + 75;
  vars.delta_alpha_2 = work.x + 76;
  vars.delta_alpha_3 = work.x + 77;
  vars.delta_alpha_4 = work.x + 78;
  vars.delta_alpha_5 = work.x + 79;
  vars.delta_alpha_6 = work.x + 80;
  vars.delta_alpha_7 = work.x + 81;
  vars.delta_alpha_8 = work.x + 82;
  vars.delta_alpha_9 = work.x + 83;
  vars.delta_alpha_10 = work.x + 84;
  vars.delta_alpha_11 = work.x + 85;
  vars.delta_alpha_12 = work.x + 86;
  vars.delta_alpha_13 = work.x + 87;
  vars.delta_alpha_14 = work.x + 88;
  vars.delta_alpha_15 = work.x + 89;
  vars.delta_alpha_16 = work.x + 90;
  vars.delta_alpha_17 = work.x + 91;
  vars.delta_alpha_18 = work.x + 92;
  vars.delta_alpha_19 = work.x + 93;
  vars.delta_alpha_20 = work.x + 94;
  vars.delta_alpha_21 = work.x + 95;
  vars.delta_alpha_22 = work.x + 96;
  vars.delta_alpha_23 = work.x + 97;
  vars.delta_alpha_24 = work.x + 98;
  vars.delta_alpha_25 = work.x + 99;
  vars.delta_alpha_26 = work.x + 100;
  vars.delta_alpha_27 = work.x + 101;
  vars.delta_alpha_28 = work.x + 102;
  vars.delta_alpha_29 = work.x + 103;
  vars.delta_alpha_30 = work.x + 104;
  vars.delta_alpha_31 = work.x + 105;
  vars.delta_alpha_32 = work.x + 106;
  vars.delta_alpha_33 = work.x + 107;
  vars.delta_alpha_34 = work.x + 108;
  vars.delta_alpha_35 = work.x + 109;
  vars.delta_alpha_36 = work.x + 110;
  vars.new_EF_1 = work.x + 111;
  vars.new_EF_2 = work.x + 112;
  vars.new_EF_3 = work.x + 113;
  vars.new_EF_4 = work.x + 114;
  vars.new_EF_5 = work.x + 115;
  vars.new_EF_6 = work.x + 116;
  vars.new_EF_7 = work.x + 117;
  vars.new_EF_8 = work.x + 118;
  vars.new_EF_9 = work.x + 119;
  vars.new_EF_10 = work.x + 120;
  vars.new_EF_11 = work.x + 121;
  vars.new_EF_12 = work.x + 122;
  vars.new_EF_13 = work.x + 123;
  vars.new_EF_14 = work.x + 124;
  vars.new_EF_15 = work.x + 125;
  vars.new_EF_16 = work.x + 126;
  vars.new_EF_17 = work.x + 127;
  vars.new_EF_18 = work.x + 128;
  vars.new_EF_19 = work.x + 129;
  vars.new_EF_20 = work.x + 130;
  vars.new_EF_21 = work.x + 131;
  vars.new_EF_22 = work.x + 132;
  vars.new_EF_23 = work.x + 133;
  vars.new_EF_24 = work.x + 134;
  vars.new_EF_25 = work.x + 135;
  vars.new_EF_26 = work.x + 136;
  vars.new_EF_27 = work.x + 137;
  vars.new_EF_28 = work.x + 138;
  vars.new_EF_29 = work.x + 139;
  vars.new_EF_30 = work.x + 140;
  vars.new_EF_31 = work.x + 141;
  vars.new_EF_32 = work.x + 142;
  vars.new_EF_33 = work.x + 143;
  vars.new_EF_34 = work.x + 144;
  vars.new_EF_35 = work.x + 145;
  vars.new_EF_36 = work.x + 146;
  vars.new_EF_37 = work.x + 147;
  vars.omega_1 = work.x + 148;
  vars.omega_2 = work.x + 149;
  vars.omega_3 = work.x + 150;
  vars.omega_4 = work.x + 151;
  vars.omega_5 = work.x + 152;
  vars.omega_6 = work.x + 153;
  vars.omega_7 = work.x + 154;
  vars.omega_8 = work.x + 155;
  vars.omega_9 = work.x + 156;
  vars.omega_10 = work.x + 157;
  vars.omega_11 = work.x + 158;
  vars.omega_12 = work.x + 159;
  vars.omega_13 = work.x + 160;
  vars.omega_14 = work.x + 161;
  vars.omega_15 = work.x + 162;
  vars.omega_16 = work.x + 163;
  vars.omega_17 = work.x + 164;
  vars.omega_18 = work.x + 165;
  vars.omega_19 = work.x + 166;
  vars.omega_20 = work.x + 167;
  vars.omega_21 = work.x + 168;
  vars.omega_22 = work.x + 169;
  vars.omega_23 = work.x + 170;
  vars.omega_24 = work.x + 171;
  vars.omega_25 = work.x + 172;
  vars.omega_26 = work.x + 173;
  vars.omega_27 = work.x + 174;
  vars.omega_28 = work.x + 175;
  vars.omega_29 = work.x + 176;
  vars.omega_30 = work.x + 177;
  vars.omega_31 = work.x + 178;
  vars.omega_32 = work.x + 179;
  vars.omega_33 = work.x + 180;
  vars.omega_34 = work.x + 181;
  vars.omega_35 = work.x + 182;
  vars.omega_36 = work.x + 183;
  vars.omega_37 = work.x + 184;
  vars.w_0 = work.x + 185;
  vars.w_1 = work.x + 186;
  vars.w_2 = work.x + 187;
  vars.w_3 = work.x + 188;
  vars.w_4 = work.x + 189;
  vars.w_5 = work.x + 190;
  vars.w_6 = work.x + 191;
  vars.w_7 = work.x + 192;
  vars.w_8 = work.x + 193;
  vars.w_9 = work.x + 194;
  vars.w_10 = work.x + 195;
  vars.w_11 = work.x + 196;
  vars.w_12 = work.x + 197;
  vars.w_13 = work.x + 198;
  vars.w_14 = work.x + 199;
  vars.w_15 = work.x + 200;
  vars.w_16 = work.x + 201;
  vars.w_17 = work.x + 202;
  vars.w_18 = work.x + 203;
  vars.w_19 = work.x + 204;
  vars.w_20 = work.x + 205;
  vars.w_21 = work.x + 206;
  vars.w_22 = work.x + 207;
  vars.w_23 = work.x + 208;
  vars.w_24 = work.x + 209;
  vars.w_25 = work.x + 210;
  vars.w_26 = work.x + 211;
  vars.w_27 = work.x + 212;
  vars.w_28 = work.x + 213;
  vars.w_29 = work.x + 214;
  vars.w_30 = work.x + 215;
  vars.w_31 = work.x + 216;
  vars.w_32 = work.x + 217;
  vars.w_33 = work.x + 218;
  vars.w_34 = work.x + 219;
  vars.w_35 = work.x + 220;
  vars.w_36 = work.x + 221;
}
void setup_indexed_params(void) {
  /* In CVXGEN, you can say */
  /*   parameters */
  /*     A[i] (5,3), i=1..4 */
  /*   end */
  /* This function sets up A[2] to be a pointer to A_2, which is a length-15 */
  /* vector of doubles. */
  /* If you access parameters that you haven't defined in CVXGEN, the result */
  /* is undefined. */
  params.xk[0] = params.xk_0;
  params.d[0] = params.d_0;
  params.uk[0] = params.uk_0;
  params.alpha[0] = params.alpha_0;
  params.xk[1] = params.xk_1;
  params.uk[1] = params.uk_1;
  params.alpha[1] = params.alpha_1;
  params.xk[2] = params.xk_2;
  params.uk[2] = params.uk_2;
  params.alpha[2] = params.alpha_2;
  params.xk[3] = params.xk_3;
  params.uk[3] = params.uk_3;
  params.alpha[3] = params.alpha_3;
  params.xk[4] = params.xk_4;
  params.uk[4] = params.uk_4;
  params.alpha[4] = params.alpha_4;
  params.xk[5] = params.xk_5;
  params.uk[5] = params.uk_5;
  params.alpha[5] = params.alpha_5;
  params.xk[6] = params.xk_6;
  params.uk[6] = params.uk_6;
  params.alpha[6] = params.alpha_6;
  params.xk[7] = params.xk_7;
  params.uk[7] = params.uk_7;
  params.alpha[7] = params.alpha_7;
  params.xk[8] = params.xk_8;
  params.uk[8] = params.uk_8;
  params.alpha[8] = params.alpha_8;
  params.xk[9] = params.xk_9;
  params.uk[9] = params.uk_9;
  params.alpha[9] = params.alpha_9;
  params.xk[10] = params.xk_10;
  params.uk[10] = params.uk_10;
  params.alpha[10] = params.alpha_10;
  params.xk[11] = params.xk_11;
  params.uk[11] = params.uk_11;
  params.alpha[11] = params.alpha_11;
  params.xk[12] = params.xk_12;
  params.uk[12] = params.uk_12;
  params.alpha[12] = params.alpha_12;
  params.xk[13] = params.xk_13;
  params.uk[13] = params.uk_13;
  params.alpha[13] = params.alpha_13;
  params.xk[14] = params.xk_14;
  params.uk[14] = params.uk_14;
  params.alpha[14] = params.alpha_14;
  params.xk[15] = params.xk_15;
  params.uk[15] = params.uk_15;
  params.alpha[15] = params.alpha_15;
  params.xk[16] = params.xk_16;
  params.uk[16] = params.uk_16;
  params.alpha[16] = params.alpha_16;
  params.xk[17] = params.xk_17;
  params.uk[17] = params.uk_17;
  params.alpha[17] = params.alpha_17;
  params.xk[18] = params.xk_18;
  params.uk[18] = params.uk_18;
  params.alpha[18] = params.alpha_18;
  params.xk[19] = params.xk_19;
  params.uk[19] = params.uk_19;
  params.alpha[19] = params.alpha_19;
  params.xk[20] = params.xk_20;
  params.uk[20] = params.uk_20;
  params.alpha[20] = params.alpha_20;
  params.xk[21] = params.xk_21;
  params.uk[21] = params.uk_21;
  params.alpha[21] = params.alpha_21;
  params.xk[22] = params.xk_22;
  params.uk[22] = params.uk_22;
  params.alpha[22] = params.alpha_22;
  params.xk[23] = params.xk_23;
  params.uk[23] = params.uk_23;
  params.alpha[23] = params.alpha_23;
  params.xk[24] = params.xk_24;
  params.uk[24] = params.uk_24;
  params.alpha[24] = params.alpha_24;
  params.xk[25] = params.xk_25;
  params.uk[25] = params.uk_25;
  params.alpha[25] = params.alpha_25;
  params.xk[26] = params.xk_26;
  params.uk[26] = params.uk_26;
  params.alpha[26] = params.alpha_26;
  params.xk[27] = params.xk_27;
  params.uk[27] = params.uk_27;
  params.alpha[27] = params.alpha_27;
  params.xk[28] = params.xk_28;
  params.uk[28] = params.uk_28;
  params.alpha[28] = params.alpha_28;
  params.xk[29] = params.xk_29;
  params.uk[29] = params.uk_29;
  params.alpha[29] = params.alpha_29;
  params.xk[30] = params.xk_30;
  params.uk[30] = params.uk_30;
  params.alpha[30] = params.alpha_30;
  params.xk[31] = params.xk_31;
  params.uk[31] = params.uk_31;
  params.alpha[31] = params.alpha_31;
  params.xk[32] = params.xk_32;
  params.uk[32] = params.uk_32;
  params.alpha[32] = params.alpha_32;
  params.xk[33] = params.xk_33;
  params.uk[33] = params.uk_33;
  params.alpha[33] = params.alpha_33;
  params.xk[34] = params.xk_34;
  params.uk[34] = params.uk_34;
  params.alpha[34] = params.alpha_34;
  params.xk[35] = params.xk_35;
  params.uk[35] = params.uk_35;
  params.alpha[35] = params.alpha_35;
  params.xk[36] = params.xk_36;
  params.uk[36] = params.uk_36;
  params.alpha[36] = params.alpha_36;
  params.xk[37] = params.xk_37;
  params.cos_xk[0] = params.cos_xk_0;
  params.abs_xk[0] = params.abs_xk_0;
  params.cos_xk[1] = params.cos_xk_1;
  params.abs_xk[1] = params.abs_xk_1;
  params.cos_xk[2] = params.cos_xk_2;
  params.abs_xk[2] = params.abs_xk_2;
  params.cos_xk[3] = params.cos_xk_3;
  params.abs_xk[3] = params.abs_xk_3;
  params.cos_xk[4] = params.cos_xk_4;
  params.abs_xk[4] = params.abs_xk_4;
  params.cos_xk[5] = params.cos_xk_5;
  params.abs_xk[5] = params.abs_xk_5;
  params.cos_xk[6] = params.cos_xk_6;
  params.abs_xk[6] = params.abs_xk_6;
  params.cos_xk[7] = params.cos_xk_7;
  params.abs_xk[7] = params.abs_xk_7;
  params.cos_xk[8] = params.cos_xk_8;
  params.abs_xk[8] = params.abs_xk_8;
  params.cos_xk[9] = params.cos_xk_9;
  params.abs_xk[9] = params.abs_xk_9;
  params.cos_xk[10] = params.cos_xk_10;
  params.abs_xk[10] = params.abs_xk_10;
  params.cos_xk[11] = params.cos_xk_11;
  params.abs_xk[11] = params.abs_xk_11;
  params.cos_xk[12] = params.cos_xk_12;
  params.abs_xk[12] = params.abs_xk_12;
  params.cos_xk[13] = params.cos_xk_13;
  params.abs_xk[13] = params.abs_xk_13;
  params.cos_xk[14] = params.cos_xk_14;
  params.abs_xk[14] = params.abs_xk_14;
  params.cos_xk[15] = params.cos_xk_15;
  params.abs_xk[15] = params.abs_xk_15;
  params.cos_xk[16] = params.cos_xk_16;
  params.abs_xk[16] = params.abs_xk_16;
  params.cos_xk[17] = params.cos_xk_17;
  params.abs_xk[17] = params.abs_xk_17;
  params.cos_xk[18] = params.cos_xk_18;
  params.abs_xk[18] = params.abs_xk_18;
  params.cos_xk[19] = params.cos_xk_19;
  params.abs_xk[19] = params.abs_xk_19;
  params.cos_xk[20] = params.cos_xk_20;
  params.abs_xk[20] = params.abs_xk_20;
  params.cos_xk[21] = params.cos_xk_21;
  params.abs_xk[21] = params.abs_xk_21;
  params.cos_xk[22] = params.cos_xk_22;
  params.abs_xk[22] = params.abs_xk_22;
  params.cos_xk[23] = params.cos_xk_23;
  params.abs_xk[23] = params.abs_xk_23;
  params.cos_xk[24] = params.cos_xk_24;
  params.abs_xk[24] = params.abs_xk_24;
  params.cos_xk[25] = params.cos_xk_25;
  params.abs_xk[25] = params.abs_xk_25;
  params.cos_xk[26] = params.cos_xk_26;
  params.abs_xk[26] = params.abs_xk_26;
  params.cos_xk[27] = params.cos_xk_27;
  params.abs_xk[27] = params.abs_xk_27;
  params.cos_xk[28] = params.cos_xk_28;
  params.abs_xk[28] = params.abs_xk_28;
  params.cos_xk[29] = params.cos_xk_29;
  params.abs_xk[29] = params.abs_xk_29;
  params.cos_xk[30] = params.cos_xk_30;
  params.abs_xk[30] = params.abs_xk_30;
  params.cos_xk[31] = params.cos_xk_31;
  params.abs_xk[31] = params.abs_xk_31;
  params.cos_xk[32] = params.cos_xk_32;
  params.abs_xk[32] = params.abs_xk_32;
  params.cos_xk[33] = params.cos_xk_33;
  params.abs_xk[33] = params.abs_xk_33;
  params.cos_xk[34] = params.cos_xk_34;
  params.abs_xk[34] = params.abs_xk_34;
  params.cos_xk[35] = params.cos_xk_35;
  params.abs_xk[35] = params.abs_xk_35;
  params.cos_xk[36] = params.cos_xk_36;
  params.abs_xk[36] = params.abs_xk_36;
  params.new_EF[0] = params.new_EF_0;
  params.old_EF[0] = params.old_EF_0;
  params.disturbance[0] = params.disturbance_0;
  params.old_EF[1] = params.old_EF_1;
  params.disturbance[1] = params.disturbance_1;
  params.old_EF[2] = params.old_EF_2;
  params.disturbance[2] = params.disturbance_2;
  params.old_EF[3] = params.old_EF_3;
  params.disturbance[3] = params.disturbance_3;
  params.old_EF[4] = params.old_EF_4;
  params.disturbance[4] = params.disturbance_4;
  params.old_EF[5] = params.old_EF_5;
  params.disturbance[5] = params.disturbance_5;
  params.old_EF[6] = params.old_EF_6;
  params.disturbance[6] = params.disturbance_6;
  params.old_EF[7] = params.old_EF_7;
  params.disturbance[7] = params.disturbance_7;
  params.old_EF[8] = params.old_EF_8;
  params.disturbance[8] = params.disturbance_8;
  params.old_EF[9] = params.old_EF_9;
  params.disturbance[9] = params.disturbance_9;
  params.old_EF[10] = params.old_EF_10;
  params.disturbance[10] = params.disturbance_10;
  params.old_EF[11] = params.old_EF_11;
  params.disturbance[11] = params.disturbance_11;
  params.old_EF[12] = params.old_EF_12;
  params.disturbance[12] = params.disturbance_12;
  params.old_EF[13] = params.old_EF_13;
  params.disturbance[13] = params.disturbance_13;
  params.old_EF[14] = params.old_EF_14;
  params.disturbance[14] = params.disturbance_14;
  params.old_EF[15] = params.old_EF_15;
  params.disturbance[15] = params.disturbance_15;
  params.old_EF[16] = params.old_EF_16;
  params.disturbance[16] = params.disturbance_16;
  params.old_EF[17] = params.old_EF_17;
  params.disturbance[17] = params.disturbance_17;
  params.old_EF[18] = params.old_EF_18;
  params.disturbance[18] = params.disturbance_18;
  params.old_EF[19] = params.old_EF_19;
  params.disturbance[19] = params.disturbance_19;
  params.old_EF[20] = params.old_EF_20;
  params.disturbance[20] = params.disturbance_20;
  params.old_EF[21] = params.old_EF_21;
  params.disturbance[21] = params.disturbance_21;
  params.old_EF[22] = params.old_EF_22;
  params.disturbance[22] = params.disturbance_22;
  params.old_EF[23] = params.old_EF_23;
  params.disturbance[23] = params.disturbance_23;
  params.old_EF[24] = params.old_EF_24;
  params.disturbance[24] = params.disturbance_24;
  params.old_EF[25] = params.old_EF_25;
  params.disturbance[25] = params.disturbance_25;
  params.old_EF[26] = params.old_EF_26;
  params.disturbance[26] = params.disturbance_26;
  params.old_EF[27] = params.old_EF_27;
  params.disturbance[27] = params.disturbance_27;
  params.old_EF[28] = params.old_EF_28;
  params.disturbance[28] = params.disturbance_28;
  params.old_EF[29] = params.old_EF_29;
  params.disturbance[29] = params.disturbance_29;
  params.old_EF[30] = params.old_EF_30;
  params.disturbance[30] = params.disturbance_30;
  params.old_EF[31] = params.old_EF_31;
  params.disturbance[31] = params.disturbance_31;
  params.old_EF[32] = params.old_EF_32;
  params.disturbance[32] = params.disturbance_32;
  params.old_EF[33] = params.old_EF_33;
  params.disturbance[33] = params.disturbance_33;
  params.old_EF[34] = params.old_EF_34;
  params.disturbance[34] = params.disturbance_34;
  params.old_EF[35] = params.old_EF_35;
  params.disturbance[35] = params.disturbance_35;
  params.old_EF[36] = params.old_EF_36;
  params.disturbance[36] = params.disturbance_36;
  params.omega[0] = params.omega_0;
}
void setup_indexed_optvars(void) {
  /* In CVXGEN, you can say */
  /*   variables */
  /*     x[i] (5), i=2..4 */
  /*   end */
  /* This function sets up x[3] to be a pointer to x_3, which is a length-5 */
  /* vector of doubles. */
  /* If you access variables that you haven't defined in CVXGEN, the result */
  /* is undefined. */
  vars.w[0] = vars.w_0;
  vars.delta_alpha[0] = vars.delta_alpha_0;
  vars.d[1] = vars.d_1;
  vars.w[1] = vars.w_1;
  vars.delta_alpha[1] = vars.delta_alpha_1;
  vars.d[2] = vars.d_2;
  vars.w[2] = vars.w_2;
  vars.delta_alpha[2] = vars.delta_alpha_2;
  vars.d[3] = vars.d_3;
  vars.w[3] = vars.w_3;
  vars.delta_alpha[3] = vars.delta_alpha_3;
  vars.d[4] = vars.d_4;
  vars.w[4] = vars.w_4;
  vars.delta_alpha[4] = vars.delta_alpha_4;
  vars.d[5] = vars.d_5;
  vars.w[5] = vars.w_5;
  vars.delta_alpha[5] = vars.delta_alpha_5;
  vars.d[6] = vars.d_6;
  vars.w[6] = vars.w_6;
  vars.delta_alpha[6] = vars.delta_alpha_6;
  vars.d[7] = vars.d_7;
  vars.w[7] = vars.w_7;
  vars.delta_alpha[7] = vars.delta_alpha_7;
  vars.d[8] = vars.d_8;
  vars.w[8] = vars.w_8;
  vars.delta_alpha[8] = vars.delta_alpha_8;
  vars.d[9] = vars.d_9;
  vars.w[9] = vars.w_9;
  vars.delta_alpha[9] = vars.delta_alpha_9;
  vars.d[10] = vars.d_10;
  vars.w[10] = vars.w_10;
  vars.delta_alpha[10] = vars.delta_alpha_10;
  vars.d[11] = vars.d_11;
  vars.w[11] = vars.w_11;
  vars.delta_alpha[11] = vars.delta_alpha_11;
  vars.d[12] = vars.d_12;
  vars.w[12] = vars.w_12;
  vars.delta_alpha[12] = vars.delta_alpha_12;
  vars.d[13] = vars.d_13;
  vars.w[13] = vars.w_13;
  vars.delta_alpha[13] = vars.delta_alpha_13;
  vars.d[14] = vars.d_14;
  vars.w[14] = vars.w_14;
  vars.delta_alpha[14] = vars.delta_alpha_14;
  vars.d[15] = vars.d_15;
  vars.w[15] = vars.w_15;
  vars.delta_alpha[15] = vars.delta_alpha_15;
  vars.d[16] = vars.d_16;
  vars.w[16] = vars.w_16;
  vars.delta_alpha[16] = vars.delta_alpha_16;
  vars.d[17] = vars.d_17;
  vars.w[17] = vars.w_17;
  vars.delta_alpha[17] = vars.delta_alpha_17;
  vars.d[18] = vars.d_18;
  vars.w[18] = vars.w_18;
  vars.delta_alpha[18] = vars.delta_alpha_18;
  vars.d[19] = vars.d_19;
  vars.w[19] = vars.w_19;
  vars.delta_alpha[19] = vars.delta_alpha_19;
  vars.d[20] = vars.d_20;
  vars.w[20] = vars.w_20;
  vars.delta_alpha[20] = vars.delta_alpha_20;
  vars.d[21] = vars.d_21;
  vars.w[21] = vars.w_21;
  vars.delta_alpha[21] = vars.delta_alpha_21;
  vars.d[22] = vars.d_22;
  vars.w[22] = vars.w_22;
  vars.delta_alpha[22] = vars.delta_alpha_22;
  vars.d[23] = vars.d_23;
  vars.w[23] = vars.w_23;
  vars.delta_alpha[23] = vars.delta_alpha_23;
  vars.d[24] = vars.d_24;
  vars.w[24] = vars.w_24;
  vars.delta_alpha[24] = vars.delta_alpha_24;
  vars.d[25] = vars.d_25;
  vars.w[25] = vars.w_25;
  vars.delta_alpha[25] = vars.delta_alpha_25;
  vars.d[26] = vars.d_26;
  vars.w[26] = vars.w_26;
  vars.delta_alpha[26] = vars.delta_alpha_26;
  vars.d[27] = vars.d_27;
  vars.w[27] = vars.w_27;
  vars.delta_alpha[27] = vars.delta_alpha_27;
  vars.d[28] = vars.d_28;
  vars.w[28] = vars.w_28;
  vars.delta_alpha[28] = vars.delta_alpha_28;
  vars.d[29] = vars.d_29;
  vars.w[29] = vars.w_29;
  vars.delta_alpha[29] = vars.delta_alpha_29;
  vars.d[30] = vars.d_30;
  vars.w[30] = vars.w_30;
  vars.delta_alpha[30] = vars.delta_alpha_30;
  vars.d[31] = vars.d_31;
  vars.w[31] = vars.w_31;
  vars.delta_alpha[31] = vars.delta_alpha_31;
  vars.d[32] = vars.d_32;
  vars.w[32] = vars.w_32;
  vars.delta_alpha[32] = vars.delta_alpha_32;
  vars.d[33] = vars.d_33;
  vars.w[33] = vars.w_33;
  vars.delta_alpha[33] = vars.delta_alpha_33;
  vars.d[34] = vars.d_34;
  vars.w[34] = vars.w_34;
  vars.delta_alpha[34] = vars.delta_alpha_34;
  vars.d[35] = vars.d_35;
  vars.w[35] = vars.w_35;
  vars.delta_alpha[35] = vars.delta_alpha_35;
  vars.d[36] = vars.d_36;
  vars.w[36] = vars.w_36;
  vars.delta_alpha[36] = vars.delta_alpha_36;
  vars.d[37] = vars.d_37;
  vars.new_EF[1] = vars.new_EF_1;
  vars.new_EF[2] = vars.new_EF_2;
  vars.new_EF[3] = vars.new_EF_3;
  vars.new_EF[4] = vars.new_EF_4;
  vars.new_EF[5] = vars.new_EF_5;
  vars.new_EF[6] = vars.new_EF_6;
  vars.new_EF[7] = vars.new_EF_7;
  vars.new_EF[8] = vars.new_EF_8;
  vars.new_EF[9] = vars.new_EF_9;
  vars.new_EF[10] = vars.new_EF_10;
  vars.new_EF[11] = vars.new_EF_11;
  vars.new_EF[12] = vars.new_EF_12;
  vars.new_EF[13] = vars.new_EF_13;
  vars.new_EF[14] = vars.new_EF_14;
  vars.new_EF[15] = vars.new_EF_15;
  vars.new_EF[16] = vars.new_EF_16;
  vars.new_EF[17] = vars.new_EF_17;
  vars.new_EF[18] = vars.new_EF_18;
  vars.new_EF[19] = vars.new_EF_19;
  vars.new_EF[20] = vars.new_EF_20;
  vars.new_EF[21] = vars.new_EF_21;
  vars.new_EF[22] = vars.new_EF_22;
  vars.new_EF[23] = vars.new_EF_23;
  vars.new_EF[24] = vars.new_EF_24;
  vars.new_EF[25] = vars.new_EF_25;
  vars.new_EF[26] = vars.new_EF_26;
  vars.new_EF[27] = vars.new_EF_27;
  vars.new_EF[28] = vars.new_EF_28;
  vars.new_EF[29] = vars.new_EF_29;
  vars.new_EF[30] = vars.new_EF_30;
  vars.new_EF[31] = vars.new_EF_31;
  vars.new_EF[32] = vars.new_EF_32;
  vars.new_EF[33] = vars.new_EF_33;
  vars.new_EF[34] = vars.new_EF_34;
  vars.new_EF[35] = vars.new_EF_35;
  vars.new_EF[36] = vars.new_EF_36;
  vars.new_EF[37] = vars.new_EF_37;
  vars.omega[1] = vars.omega_1;
  vars.omega[2] = vars.omega_2;
  vars.omega[3] = vars.omega_3;
  vars.omega[4] = vars.omega_4;
  vars.omega[5] = vars.omega_5;
  vars.omega[6] = vars.omega_6;
  vars.omega[7] = vars.omega_7;
  vars.omega[8] = vars.omega_8;
  vars.omega[9] = vars.omega_9;
  vars.omega[10] = vars.omega_10;
  vars.omega[11] = vars.omega_11;
  vars.omega[12] = vars.omega_12;
  vars.omega[13] = vars.omega_13;
  vars.omega[14] = vars.omega_14;
  vars.omega[15] = vars.omega_15;
  vars.omega[16] = vars.omega_16;
  vars.omega[17] = vars.omega_17;
  vars.omega[18] = vars.omega_18;
  vars.omega[19] = vars.omega_19;
  vars.omega[20] = vars.omega_20;
  vars.omega[21] = vars.omega_21;
  vars.omega[22] = vars.omega_22;
  vars.omega[23] = vars.omega_23;
  vars.omega[24] = vars.omega_24;
  vars.omega[25] = vars.omega_25;
  vars.omega[26] = vars.omega_26;
  vars.omega[27] = vars.omega_27;
  vars.omega[28] = vars.omega_28;
  vars.omega[29] = vars.omega_29;
  vars.omega[30] = vars.omega_30;
  vars.omega[31] = vars.omega_31;
  vars.omega[32] = vars.omega_32;
  vars.omega[33] = vars.omega_33;
  vars.omega[34] = vars.omega_34;
  vars.omega[35] = vars.omega_35;
  vars.omega[36] = vars.omega_36;
  vars.omega[37] = vars.omega_37;
}
void setup_indexing(void) {
  setup_pointers();
  setup_indexed_params();
  setup_indexed_optvars();
}
void set_start(void) {
  int i;
  for (i = 0; i < 222; i++)
    work.x[i] = 0;
  for (i = 0; i < 148; i++)
    work.y[i] = 0;
  for (i = 0; i < 407; i++)
    work.s[i] = (work.h[i] > 0) ? work.h[i] : settings.s_init;
  for (i = 0; i < 407; i++)
    work.z[i] = settings.z_init;
}
double eval_objv(void) {
  int i;
  double objv;
  /* Borrow space in work.rhs. */
  multbyP(work.rhs, work.x);
  objv = 0;
  for (i = 0; i < 222; i++)
    objv += work.x[i]*work.rhs[i];
  objv *= 0.5;
  for (i = 0; i < 222; i++)
    objv += work.q[i]*work.x[i];
  objv += work.quad_270107893760[0]+work.quad_307730403328[0]+work.quad_445352677376[0]+work.quad_702230208512[0]+work.quad_876608462848[0]+work.quad_175950630912[0]+work.quad_332149305344[0]+work.quad_59389796352[0]+work.quad_302248755200[0]+work.quad_977458573312[0]+work.quad_981646897152[0]+work.quad_487442513920[0]+work.quad_648024653824[0]+work.quad_691904045056[0]+work.quad_341291667456[0]+work.quad_453679702016[0]+work.quad_370898460672[0]+work.quad_610273832960[0]+work.quad_929545818112[0]+work.quad_574610743296[0]+work.quad_566947319808[0]+work.quad_126117388288[0]+work.quad_589364629504[0]+work.quad_565576192000[0]+work.quad_839768125440[0]+work.quad_72106487808[0]+work.quad_338226507776[0]+work.quad_881072074752[0]+work.quad_159865249792[0]+work.quad_409310203904[0]+work.quad_748685393920[0]+work.quad_572116054016[0]+work.quad_46587228160[0]+work.quad_266521808896[0]+work.quad_654511644672[0]+work.quad_585389805568[0]+work.quad_660241698816[0]+work.quad_81845256192[0]+work.quad_749055442944[0]+work.quad_348638896128[0]+work.quad_228862201856[0]+work.quad_782724800512[0]+work.quad_906353664000[0]+work.quad_758131437568[0]+work.quad_232271880192[0]+work.quad_786671919104[0]+work.quad_559026388992[0]+work.quad_326244257792[0]+work.quad_388870012928[0]+work.quad_190065135616[0]+work.quad_607032025088[0]+work.quad_588796252160[0]+work.quad_242214543360[0]+work.quad_46308270080[0]+work.quad_74921062400[0]+work.quad_166489407488[0]+work.quad_271313948672[0]+work.quad_274730606592[0]+work.quad_574739918848[0]+work.quad_223001780224[0]+work.quad_548109938688[0]+work.quad_849945169920[0]+work.quad_662255759360[0]+work.quad_859892768768[0]+work.quad_619977998336[0]+work.quad_645643497472[0]+work.quad_827707936768[0]+work.quad_312808595456[0]+work.quad_111384309760[0]+work.quad_891445882880[0]+work.quad_600455548928[0]+work.quad_971924062208[0]+work.quad_930083958784[0]+work.quad_892859551744[0]+work.quad_595768565760[0]+work.quad_182585827328[0]+work.quad_330017021952[0]+work.quad_885751197696[0]+work.quad_111311339520[0]+work.quad_347651670016[0]+work.quad_533513490432[0]+work.quad_790321422336[0]+work.quad_784062255104[0]+work.quad_852730523648[0]+work.quad_98378784768[0]+work.quad_471133708288[0]+work.quad_987156705280[0]+work.quad_581413453824[0]+work.quad_280367702016[0]+work.quad_847507894272[0]+work.quad_771595739136[0]+work.quad_290273087488[0]+work.quad_638189170688[0]+work.quad_365765689344[0]+work.quad_154452013056[0]+work.quad_933579579392[0]+work.quad_586980057088[0]+work.quad_454122455040[0]+work.quad_362807410688[0]+work.quad_422286278656[0]+work.quad_5651824640[0]+work.quad_141227048960[0]+work.quad_951767871488[0]+work.quad_501586014208[0]+work.quad_674208681984[0]+work.quad_169035161600[0]+work.quad_526743453696[0]+work.quad_681618362368[0]+work.quad_397319585792[0]+work.quad_440407265280[0]+work.quad_592808808448[0]+work.quad_68052533248[0];
  return objv;
}
void fillrhs_aff(void) {
  int i;
  double *r1, *r2, *r3, *r4;
  r1 = work.rhs;
  r2 = work.rhs + 222;
  r3 = work.rhs + 629;
  r4 = work.rhs + 1036;
  /* r1 = -A^Ty - G^Tz - Px - q. */
  multbymAT(r1, work.y);
  multbymGT(work.buffer, work.z);
  for (i = 0; i < 222; i++)
    r1[i] += work.buffer[i];
  multbyP(work.buffer, work.x);
  for (i = 0; i < 222; i++)
    r1[i] -= work.buffer[i] + work.q[i];
  /* r2 = -z. */
  for (i = 0; i < 407; i++)
    r2[i] = -work.z[i];
  /* r3 = -Gx - s + h. */
  multbymG(r3, work.x);
  for (i = 0; i < 407; i++)
    r3[i] += -work.s[i] + work.h[i];
  /* r4 = -Ax + b. */
  multbymA(r4, work.x);
  for (i = 0; i < 148; i++)
    r4[i] += work.b[i];
}
void fillrhs_cc(void) {
  int i;
  double *r2;
  double *ds_aff, *dz_aff;
  double mu;
  double alpha;
  double sigma;
  double smu;
  double minval;
  r2 = work.rhs + 222;
  ds_aff = work.lhs_aff + 222;
  dz_aff = work.lhs_aff + 629;
  mu = 0;
  for (i = 0; i < 407; i++)
    mu += work.s[i]*work.z[i];
  /* Don't finish calculating mu quite yet. */
  /* Find min(min(ds./s), min(dz./z)). */
  minval = 0;
  for (i = 0; i < 407; i++)
    if (ds_aff[i] < minval*work.s[i])
      minval = ds_aff[i]/work.s[i];
  for (i = 0; i < 407; i++)
    if (dz_aff[i] < minval*work.z[i])
      minval = dz_aff[i]/work.z[i];
  /* Find alpha. */
  if (-1 < minval)
      alpha = 1;
  else
      alpha = -1/minval;
  sigma = 0;
  for (i = 0; i < 407; i++)
    sigma += (work.s[i] + alpha*ds_aff[i])*
      (work.z[i] + alpha*dz_aff[i]);
  sigma /= mu;
  sigma = sigma*sigma*sigma;
  /* Finish calculating mu now. */
  mu *= 0.002457002457002457;
  smu = sigma*mu;
  /* Fill-in the rhs. */
  for (i = 0; i < 222; i++)
    work.rhs[i] = 0;
  for (i = 629; i < 1184; i++)
    work.rhs[i] = 0;
  for (i = 0; i < 407; i++)
    r2[i] = work.s_inv[i]*(smu - ds_aff[i]*dz_aff[i]);
}
void refine(double *target, double *var) {
  int i, j;
  double *residual = work.buffer;
  double norm2;
  double *new_var = work.buffer2;
  for (j = 0; j < settings.refine_steps; j++) {
    norm2 = 0;
    matrix_multiply(residual, var);
    for (i = 0; i < 1184; i++) {
      residual[i] = residual[i] - target[i];
      norm2 += residual[i]*residual[i];
    }
#ifndef ZERO_LIBRARY_MODE
    if (settings.verbose_refinement) {
      if (j == 0)
        printf("Initial residual before refinement has norm squared %.6g.\n", norm2);
      else
        printf("After refinement we get squared norm %.6g.\n", norm2);
    }
#endif
    /* Solve to find new_var = KKT \ (target - A*var). */
    ldl_solve(residual, new_var);
    /* Update var += new_var, or var += KKT \ (target - A*var). */
    for (i = 0; i < 1184; i++) {
      var[i] -= new_var[i];
    }
  }
#ifndef ZERO_LIBRARY_MODE
  if (settings.verbose_refinement) {
    /* Check the residual once more, but only if we're reporting it, since */
    /* it's expensive. */
    norm2 = 0;
    matrix_multiply(residual, var);
    for (i = 0; i < 1184; i++) {
      residual[i] = residual[i] - target[i];
      norm2 += residual[i]*residual[i];
    }
    if (j == 0)
      printf("Initial residual before refinement has norm squared %.6g.\n", norm2);
    else
      printf("After refinement we get squared norm %.6g.\n", norm2);
  }
#endif
}
double calc_ineq_resid_squared(void) {
  /* Calculates the norm ||-Gx - s + h||. */
  double norm2_squared;
  int i;
  /* Find -Gx. */
  multbymG(work.buffer, work.x);
  /* Add -s + h. */
  for (i = 0; i < 407; i++)
    work.buffer[i] += -work.s[i] + work.h[i];
  /* Now find the squared norm. */
  norm2_squared = 0;
  for (i = 0; i < 407; i++)
    norm2_squared += work.buffer[i]*work.buffer[i];
  return norm2_squared;
}
double calc_eq_resid_squared(void) {
  /* Calculates the norm ||-Ax + b||. */
  double norm2_squared;
  int i;
  /* Find -Ax. */
  multbymA(work.buffer, work.x);
  /* Add +b. */
  for (i = 0; i < 148; i++)
    work.buffer[i] += work.b[i];
  /* Now find the squared norm. */
  norm2_squared = 0;
  for (i = 0; i < 148; i++)
    norm2_squared += work.buffer[i]*work.buffer[i];
  return norm2_squared;
}
void better_start(void) {
  /* Calculates a better starting point, using a similar approach to CVXOPT. */
  /* Not yet speed optimized. */
  int i;
  double *x, *s, *z, *y;
  double alpha;
  work.block_33[0] = -1;
  /* Make sure sinvz is 1 to make hijacked KKT system ok. */
  for (i = 0; i < 407; i++)
    work.s_inv_z[i] = 1;
  fill_KKT();
  ldl_factor();
  fillrhs_start();
  /* Borrow work.lhs_aff for the solution. */
  ldl_solve(work.rhs, work.lhs_aff);
  /* Don't do any refinement for now. Precision doesn't matter too much. */
  x = work.lhs_aff;
  s = work.lhs_aff + 222;
  z = work.lhs_aff + 629;
  y = work.lhs_aff + 1036;
  /* Just set x and y as is. */
  for (i = 0; i < 222; i++)
    work.x[i] = x[i];
  for (i = 0; i < 148; i++)
    work.y[i] = y[i];
  /* Now complete the initialization. Start with s. */
  /* Must have alpha > max(z). */
  alpha = -1e99;
  for (i = 0; i < 407; i++)
    if (alpha < z[i])
      alpha = z[i];
  if (alpha < 0) {
    for (i = 0; i < 407; i++)
      work.s[i] = -z[i];
  } else {
    alpha += 1;
    for (i = 0; i < 407; i++)
      work.s[i] = -z[i] + alpha;
  }
  /* Now initialize z. */
  /* Now must have alpha > max(-z). */
  alpha = -1e99;
  for (i = 0; i < 407; i++)
    if (alpha < -z[i])
      alpha = -z[i];
  if (alpha < 0) {
    for (i = 0; i < 407; i++)
      work.z[i] = z[i];
  } else {
    alpha += 1;
    for (i = 0; i < 407; i++)
      work.z[i] = z[i] + alpha;
  }
}
void fillrhs_start(void) {
  /* Fill rhs with (-q, 0, h, b). */
  int i;
  double *r1, *r2, *r3, *r4;
  r1 = work.rhs;
  r2 = work.rhs + 222;
  r3 = work.rhs + 629;
  r4 = work.rhs + 1036;
  for (i = 0; i < 222; i++)
    r1[i] = -work.q[i];
  for (i = 0; i < 407; i++)
    r2[i] = 0;
  for (i = 0; i < 407; i++)
    r3[i] = work.h[i];
  for (i = 0; i < 148; i++)
    r4[i] = work.b[i];
}
long solve(void) {
  int i;
  int iter;
  double *dx, *ds, *dy, *dz;
  double minval;
  double alpha;
  work.converged = 0;
  setup_pointers();
  pre_ops();
#ifndef ZERO_LIBRARY_MODE
  if (settings.verbose)
    printf("iter     objv        gap       |Ax-b|    |Gx+s-h|    step\n");
#endif
  fillq();
  fillh();
  fillb();
  if (settings.better_start)
    better_start();
  else
    set_start();
  for (iter = 0; iter < settings.max_iters; iter++) {
    for (i = 0; i < 407; i++) {
      work.s_inv[i] = 1.0 / work.s[i];
      work.s_inv_z[i] = work.s_inv[i]*work.z[i];
    }
    work.block_33[0] = 0;
    fill_KKT();
    ldl_factor();
    /* Affine scaling directions. */
    fillrhs_aff();
    ldl_solve(work.rhs, work.lhs_aff);
    refine(work.rhs, work.lhs_aff);
    /* Centering plus corrector directions. */
    fillrhs_cc();
    ldl_solve(work.rhs, work.lhs_cc);
    refine(work.rhs, work.lhs_cc);
    /* Add the two together and store in aff. */
    for (i = 0; i < 1184; i++)
      work.lhs_aff[i] += work.lhs_cc[i];
    /* Rename aff to reflect its new meaning. */
    dx = work.lhs_aff;
    ds = work.lhs_aff + 222;
    dz = work.lhs_aff + 629;
    dy = work.lhs_aff + 1036;
    /* Find min(min(ds./s), min(dz./z)). */
    minval = 0;
    for (i = 0; i < 407; i++)
      if (ds[i] < minval*work.s[i])
        minval = ds[i]/work.s[i];
    for (i = 0; i < 407; i++)
      if (dz[i] < minval*work.z[i])
        minval = dz[i]/work.z[i];
    /* Find alpha. */
    if (-0.99 < minval)
      alpha = 1;
    else
      alpha = -0.99/minval;
    /* Update the primal and dual variables. */
    for (i = 0; i < 222; i++)
      work.x[i] += alpha*dx[i];
    for (i = 0; i < 407; i++)
      work.s[i] += alpha*ds[i];
    for (i = 0; i < 407; i++)
      work.z[i] += alpha*dz[i];
    for (i = 0; i < 148; i++)
      work.y[i] += alpha*dy[i];
    work.gap = eval_gap();
    work.eq_resid_squared = calc_eq_resid_squared();
    work.ineq_resid_squared = calc_ineq_resid_squared();
#ifndef ZERO_LIBRARY_MODE
    if (settings.verbose) {
      work.optval = eval_objv();
      printf("%3d   %10.3e  %9.2e  %9.2e  %9.2e  % 6.4f\n",
          iter+1, work.optval, work.gap, sqrt(work.eq_resid_squared),
          sqrt(work.ineq_resid_squared), alpha);
    }
#endif
    /* Test termination conditions. Requires optimality, and satisfied */
    /* constraints. */
    if (   (work.gap < settings.eps)
        && (work.eq_resid_squared <= settings.resid_tol*settings.resid_tol)
        && (work.ineq_resid_squared <= settings.resid_tol*settings.resid_tol)
       ) {
      work.converged = 1;
      work.optval = eval_objv();
      return iter+1;
    }
  }
  return iter;
}
