%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     one time initialization
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all;


m = 1; % number of inputs
n = 2; % number of states
T = 10; % time horizon

xnow = [0; 0]; %current global state, angle and angular speed
d0 = [0; 0]; % initial d state, which is always zero cause it's x - xk
delta_t = 0.010; % time between consecutive steps
k = 0; %number of times we repeated sqp

uk = zeros(m, T+1); % input uk[i] i from 0 to T
xk = zeros(n, T+2); % states xk[i] i from 0 to T+1
abs_xk_2 = abs(xk(2, :)); % absolute value of x2s
cos_xk_1 = cos(xk(1, :)); % cosines of x1s

% initial guess - all zeros, nothing is  happening


u_max= 1.8; % N, maximum applied thrust
x_dot_bound = 3*pi; % rad/s, max angualr speed

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Arbitrarily chosen values        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
w_max = 1; % maximum w sqp can use
S = 1; % slew rate; maximum delta w
Q = 1 * [1 0; 0 0]; % state cost
R = 10 * eye(1); % input cost
Q_final = 1*eye(2); %terminal state cost
x_final = [pi; 0];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

g = 9.81; % m/s2
m = 0.214; % kg
L_cm = 0.0921; % m, cm of pendulum
L = 0.229; % m, where torque is applied
I = 0.00417; % kgm2 inertia; assuming that all mass is at the tip
Cd = 0.000077; % estimated drag

c = delta_t;
c1 = L_cm * m * g * delta_t / I;
c2 = 2 * Cd * delta_t / I;
Bd = L * delta_t / I;

x_world = 0.0;
x_dot_world = 0.0;
delta_t_world = 0.010;

% one time parameter intializations
params.c = c;
params.c1 = c1;
params.c2 = c2;
params.Bd = Bd;
params.Q = Q;
params.Q_final = Q_final;
params.R = R;
params.S = S;
params.d_0 = d0;
params.u_max = u_max;
params.w_max = w_max;
params.x_dot_bound = x_dot_bound;
params.x_final = x_final;


l = 0; % mpc iterator
time_array = zeros(1,T+2);
for i=1:T+2,
  time_array(1,i) = (i-1) * delta_t;
end
x_true = zeros(2, T+2);
u_true = zeros(1, T+2);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     MPC Loop
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
while l < T+1,

  xnow = [x_world; x_dot_world];


  if l > 0, % recalculate the trajectory based on new location
    uk = circshift(uk, [0 -1]); uk(1, T+1) = 0;
    xk = zeros(n, T+2); % states xk[i] i from 0 to T+1
    xk(:,1) = xnow; % initialize the first value
    %calculate the rest
    for i = 2:T+2,
      xk(1, i) = xk(1, i-1) + delta_t * xk(2, i-1);
      xk(2, i) = (-Cd *abs(xk(2, i-1))*xk(2, i-1) - L_cm * m*g * sin( xk(1, i-1) ) + L*uk(i-1) ) * delta_t/I + xk(2, i-1);
    end
    abs_xk_2 = abs(xk(2, :)); % absolute value of x2s
    cos_xk_1 = cos(xk(1, :)); % cosines of x1s
  end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % SQP Loop
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  k = 0;
  settings.verbose = 0;  % disable output of solver progress.
  while k < 1
    % iitialize values of the matrix

    params.uk_0 = uk(:,1);
    params.xk_0 = xk(:,1);
    params.abs_xk_2_0 = abs_xk_2(1);
    params.cos_xk_1_0 = cos_xk_1(1);
    for i = 1:T,
        params.uk{i} = uk(:, i+1);
        params.xk{i} = xk(:, i+1);
        params.abs_xk_2{i} = abs_xk_2(i+1);
        params.cos_xk_1{i} = cos_xk_1(i+1);
    end
    params.xk{T+1} = xk(:, T+2);
    params.abs_xk_2{T+1} = abs_xk_2(T+2);
    params.cos_xk_1{T+1} = cos_xk_1(T+2);

    % Exercise the high-speed solver.
    [vars, status] = csolve(params, settings);  % solve, saving results.

    % Check convergence, and display the optimal variable value.
    if ~status.converged, error 'failed to converge'; end

    % update teh results for xk uk
    xk(:,2:T+2) = xk(:,2:T+2) + [vars.d{:}];
    uk(:, 1) = uk(:, 1) + vars.w_0;
    uk(:,2:T+1) = uk(:, 2:T+1) + [vars.w{:}];

    abs_xk_2 = abs(xk(2, :)); % absolute value of x2s
    cos_xk_1 = cos(xk(1, :)); % cosines of x1s
    k = k + 1;
    % xk
    % uk
    % figure(1); subplot(3,1,1); hold on; plot(uk); ylabel('u');
    % figure(1); subplot(3,1,2); hold on; plot(xk(1, :)); ylabel('theta');
    % figure(1); subplot(3,1,3); hold on; plot(xk(2, :)); ylabel('theta dot');
  end
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % apply the first step in the world
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  figure(1); subplot(3,1,1); hold on; plot(time_array(1:T+1) + delta_t * l, uk); ylabel('u');
  figure(1); subplot(3,1,2); hold on; plot(time_array + delta_t * l, xk(1, :)); ylabel('theta');
  figure(1); subplot(3,1,3); hold on; plot(time_array + delta_t * l, xk(2, :)); ylabel('theta dot');

  u_world = uk(1); % take the first offered step and run it
  % 50 ms passed = 5 * 10 ms, while the controller is run every 50ms
  % i m just using a more fine version of the Newton's method here
  for i = 1:5,
    x_world = x_world + delta_t_world * x_dot_world;
    x_dot_world = (-Cd *abs(x_dot_world)*x_dot_world - L_cm * m*g * sin(x_world) + L*u_world ) * delta_t_world/I + x_dot_world; % + 2*(rand()-0.5);
  end
  % go to the next mpc step
  l = l+1;
  u_true(l+1) = u_world;
  x_true(:, l+1) = [x_world; x_dot_world];
end

figure(1); subplot(3,1,1); hold on; plot(time_array(1:T), u_true(2:T+1), 'LineWidth', 3); ylabel('u'); xlim([0 0.7]);
figure(1); subplot(3,1,2); hold on; plot(time_array, x_true(1, :), 'LineWidth', 3 ); ylabel('theta');xlim([0 0.7]);
figure(1); subplot(3,1,3); hold on; plot(time_array, x_true(2, :), 'LineWidth', 3); ylabel('theta dot');xlim([0 0.7]);


clear all;
