float Pendulum::dd_theta_des(){
  //return desired angular acceleration based on currnt time tnow_
  // desired trajectory - go from zero to pi in time_lim_,
  // half time accelerate, half time dececlerate
  if (tnow_ > time_limit_ || !running_){
    return 0.0;
  }
  else if (tnow_ <= time_limit_ / 2){
    return 4.0 * M_PI / time_lim_;
  }
  else{
    return -4.0 * M_PI / time_lim_;
  }
}


float d_theta_des(){
  //return desired angular speed based on currnt time tnow_
  // desired trajectory - go from zero to pi in time_lim_,
  // half time accelerate, half time dececlerate
  if (tnow_ > time_limit_ || !running_){
    return 0.0;
  }
  else if (tnow_ <= time_limit_ / 2){
    return 4.0 * M_PI * ( (float) tnow_/1000000 ) / pow(time_lim_, 2) ;
  }
  else{
    return 4.0*M_PI/time_lim_ - 4.0*M_PI*( (float) tnow_/1000000 ) / pow(time_lim_, 2) ;
  }
}

float theta_des(){
  //return desired angle based on currnt time tnow_
  // desired trajectory - go from zero to pi in time_lim_,
  // half time accelerate, half time dececlerate
  if (tnow_ > time_limit_ || !running_){
    return M_PI;
  }
  else if (tnow_ <= time_limit_ / 2){
    return 2.0 * M_PI * ( pow( (float) tnow_/1000000, 2 ) / pow(time_lim_, 2));
  }
  else{
    return 4.0*M_PI * ((float) tnow_/1000000) / time_lim_ - 2*M_PI * pow((float) tnow_/1000000,2)/pow(time_lim_,2) - M_PI;
  }
}
