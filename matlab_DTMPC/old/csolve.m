% csolve  Solves a custom quadratic program very rapidly.
%
% [vars, status] = csolve(params, settings)
%
% solves the convex optimization problem
%
%   minimize(quad_form(x_final - (xk_0 + d_0), Q) + quad_form(uk_0 + w_0, R) + quad_form(I_over_L*(alpha_0*new_EF_0 + old_EF_0*delta_alpha_0), R_delta) + quad_form(alpha_0 + delta_alpha_0 - alpha_min, M) + quad_form(x_final - (xk_1 + d_1), Q) + quad_form(uk_1 + w_1, R) + quad_form(I_over_L*(alpha_1*new_EF_1 + old_EF_1*delta_alpha_1), R_delta) + quad_form(alpha_1 + delta_alpha_1 - alpha_min, M) + quad_form(x_final - (xk_2 + d_2), Q) + quad_form(uk_2 + w_2, R) + quad_form(I_over_L*(alpha_2*new_EF_2 + old_EF_2*delta_alpha_2), R_delta) + quad_form(alpha_2 + delta_alpha_2 - alpha_min, M) + quad_form(x_final - (xk_3 + d_3), Q) + quad_form(uk_3 + w_3, R) + quad_form(I_over_L*(alpha_3*new_EF_3 + old_EF_3*delta_alpha_3), R_delta) + quad_form(alpha_3 + delta_alpha_3 - alpha_min, M) + quad_form(x_final - (xk_4 + d_4), Q) + quad_form(uk_4 + w_4, R) + quad_form(I_over_L*(alpha_4*new_EF_4 + old_EF_4*delta_alpha_4), R_delta) + quad_form(alpha_4 + delta_alpha_4 - alpha_min, M) + quad_form(x_final - (xk_5 + d_5), Q) + quad_form(uk_5 + w_5, R) + quad_form(I_over_L*(alpha_5*new_EF_5 + old_EF_5*delta_alpha_5), R_delta) + quad_form(alpha_5 + delta_alpha_5 - alpha_min, M) + quad_form(x_final - (xk_6 + d_6), Q) + quad_form(uk_6 + w_6, R) + quad_form(I_over_L*(alpha_6*new_EF_6 + old_EF_6*delta_alpha_6), R_delta) + quad_form(alpha_6 + delta_alpha_6 - alpha_min, M) + quad_form(x_final - (xk_7 + d_7), Q) + quad_form(uk_7 + w_7, R) + quad_form(I_over_L*(alpha_7*new_EF_7 + old_EF_7*delta_alpha_7), R_delta) + quad_form(alpha_7 + delta_alpha_7 - alpha_min, M) + quad_form(x_final - (xk_8 + d_8), Q) + quad_form(uk_8 + w_8, R) + quad_form(I_over_L*(alpha_8*new_EF_8 + old_EF_8*delta_alpha_8), R_delta) + quad_form(alpha_8 + delta_alpha_8 - alpha_min, M) + quad_form(x_final - (xk_9 + d_9), Q) + quad_form(uk_9 + w_9, R) + quad_form(I_over_L*(alpha_9*new_EF_9 + old_EF_9*delta_alpha_9), R_delta) + quad_form(alpha_9 + delta_alpha_9 - alpha_min, M) + quad_form(x_final - (xk_10 + d_10), Q) + quad_form(uk_10 + w_10, R) + quad_form(I_over_L*(alpha_10*new_EF_10 + old_EF_10*delta_alpha_10), R_delta) + quad_form(alpha_10 + delta_alpha_10 - alpha_min, M) + quad_form(x_final - (xk_11 + d_11), Q) + quad_form(uk_11 + w_11, R) + quad_form(I_over_L*(alpha_11*new_EF_11 + old_EF_11*delta_alpha_11), R_delta) + quad_form(alpha_11 + delta_alpha_11 - alpha_min, M) + quad_form(x_final - (xk_12 + d_12), Q) + quad_form(uk_12 + w_12, R) + quad_form(I_over_L*(alpha_12*new_EF_12 + old_EF_12*delta_alpha_12), R_delta) + quad_form(alpha_12 + delta_alpha_12 - alpha_min, M) + quad_form(x_final - (xk_13 + d_13), Q) + quad_form(uk_13 + w_13, R) + quad_form(I_over_L*(alpha_13*new_EF_13 + old_EF_13*delta_alpha_13), R_delta) + quad_form(alpha_13 + delta_alpha_13 - alpha_min, M) + quad_form(x_final - (xk_14 + d_14), Q) + quad_form(uk_14 + w_14, R) + quad_form(I_over_L*(alpha_14*new_EF_14 + old_EF_14*delta_alpha_14), R_delta) + quad_form(alpha_14 + delta_alpha_14 - alpha_min, M) + quad_form(x_final - (xk_15 + d_15), Q) + quad_form(uk_15 + w_15, R) + quad_form(I_over_L*(alpha_15*new_EF_15 + old_EF_15*delta_alpha_15), R_delta) + quad_form(alpha_15 + delta_alpha_15 - alpha_min, M) + quad_form(x_final - (xk_16 + d_16), Q) + quad_form(uk_16 + w_16, R) + quad_form(I_over_L*(alpha_16*new_EF_16 + old_EF_16*delta_alpha_16), R_delta) + quad_form(alpha_16 + delta_alpha_16 - alpha_min, M) + quad_form(x_final - (xk_17 + d_17), Q) + quad_form(uk_17 + w_17, R) + quad_form(I_over_L*(alpha_17*new_EF_17 + old_EF_17*delta_alpha_17), R_delta) + quad_form(alpha_17 + delta_alpha_17 - alpha_min, M) + quad_form(x_final - (xk_18 + d_18), Q) + quad_form(uk_18 + w_18, R) + quad_form(I_over_L*(alpha_18*new_EF_18 + old_EF_18*delta_alpha_18), R_delta) + quad_form(alpha_18 + delta_alpha_18 - alpha_min, M) + quad_form(x_final - (xk_19 + d_19), Q) + quad_form(uk_19 + w_19, R) + quad_form(I_over_L*(alpha_19*new_EF_19 + old_EF_19*delta_alpha_19), R_delta) + quad_form(alpha_19 + delta_alpha_19 - alpha_min, M) + quad_form(x_final - (xk_20 + d_20), Q) + quad_form(uk_20 + w_20, R) + quad_form(I_over_L*(alpha_20*new_EF_20 + old_EF_20*delta_alpha_20), R_delta) + quad_form(alpha_20 + delta_alpha_20 - alpha_min, M) + quad_form(x_final - (xk_21 + d_21), Q) + quad_form(uk_21 + w_21, R) + quad_form(I_over_L*(alpha_21*new_EF_21 + old_EF_21*delta_alpha_21), R_delta) + quad_form(alpha_21 + delta_alpha_21 - alpha_min, M) + quad_form(x_final - (xk_22 + d_22), Q) + quad_form(uk_22 + w_22, R) + quad_form(I_over_L*(alpha_22*new_EF_22 + old_EF_22*delta_alpha_22), R_delta) + quad_form(alpha_22 + delta_alpha_22 - alpha_min, M) + quad_form(x_final - (xk_23 + d_23), Q) + quad_form(uk_23 + w_23, R) + quad_form(I_over_L*(alpha_23*new_EF_23 + old_EF_23*delta_alpha_23), R_delta) + quad_form(alpha_23 + delta_alpha_23 - alpha_min, M) + quad_form(x_final - (xk_24 + d_24), Q) + quad_form(uk_24 + w_24, R) + quad_form(I_over_L*(alpha_24*new_EF_24 + old_EF_24*delta_alpha_24), R_delta) + quad_form(alpha_24 + delta_alpha_24 - alpha_min, M) + quad_form(x_final - (xk_25 + d_25), Q) + quad_form(uk_25 + w_25, R) + quad_form(I_over_L*(alpha_25*new_EF_25 + old_EF_25*delta_alpha_25), R_delta) + quad_form(alpha_25 + delta_alpha_25 - alpha_min, M) + quad_form(x_final - (xk_26 + d_26), Q_final))
%   subject to
%     d_1(1) == delta_t*d_0(2) + d_0(1)
%     d_2(1) == delta_t*d_1(2) + d_1(1)
%     d_3(1) == delta_t*d_2(2) + d_2(1)
%     d_4(1) == delta_t*d_3(2) + d_3(1)
%     d_5(1) == delta_t*d_4(2) + d_4(1)
%     d_6(1) == delta_t*d_5(2) + d_5(1)
%     d_7(1) == delta_t*d_6(2) + d_6(1)
%     d_8(1) == delta_t*d_7(2) + d_7(1)
%     d_9(1) == delta_t*d_8(2) + d_8(1)
%     d_10(1) == delta_t*d_9(2) + d_9(1)
%     d_11(1) == delta_t*d_10(2) + d_10(1)
%     d_12(1) == delta_t*d_11(2) + d_11(1)
%     d_13(1) == delta_t*d_12(2) + d_12(1)
%     d_14(1) == delta_t*d_13(2) + d_13(1)
%     d_15(1) == delta_t*d_14(2) + d_14(1)
%     d_16(1) == delta_t*d_15(2) + d_15(1)
%     d_17(1) == delta_t*d_16(2) + d_16(1)
%     d_18(1) == delta_t*d_17(2) + d_17(1)
%     d_19(1) == delta_t*d_18(2) + d_18(1)
%     d_20(1) == delta_t*d_19(2) + d_19(1)
%     d_21(1) == delta_t*d_20(2) + d_20(1)
%     d_22(1) == delta_t*d_21(2) + d_21(1)
%     d_23(1) == delta_t*d_22(2) + d_22(1)
%     d_24(1) == delta_t*d_23(2) + d_23(1)
%     d_25(1) == delta_t*d_24(2) + d_24(1)
%     d_26(1) == delta_t*d_25(2) + d_25(1)
%     d_1(2) ==  - c1*cos_xk_0*d_0(1) + (1 - abs_xk_0*c2)*d_0(2) + Bd*w_0
%     d_2(2) ==  - c1*cos_xk_1*d_1(1) + (1 - abs_xk_1*c2)*d_1(2) + Bd*w_1
%     d_3(2) ==  - c1*cos_xk_2*d_2(1) + (1 - abs_xk_2*c2)*d_2(2) + Bd*w_2
%     d_4(2) ==  - c1*cos_xk_3*d_3(1) + (1 - abs_xk_3*c2)*d_3(2) + Bd*w_3
%     d_5(2) ==  - c1*cos_xk_4*d_4(1) + (1 - abs_xk_4*c2)*d_4(2) + Bd*w_4
%     d_6(2) ==  - c1*cos_xk_5*d_5(1) + (1 - abs_xk_5*c2)*d_5(2) + Bd*w_5
%     d_7(2) ==  - c1*cos_xk_6*d_6(1) + (1 - abs_xk_6*c2)*d_6(2) + Bd*w_6
%     d_8(2) ==  - c1*cos_xk_7*d_7(1) + (1 - abs_xk_7*c2)*d_7(2) + Bd*w_7
%     d_9(2) ==  - c1*cos_xk_8*d_8(1) + (1 - abs_xk_8*c2)*d_8(2) + Bd*w_8
%     d_10(2) ==  - c1*cos_xk_9*d_9(1) + (1 - abs_xk_9*c2)*d_9(2) + Bd*w_9
%     d_11(2) ==  - c1*cos_xk_10*d_10(1) + (1 - abs_xk_10*c2)*d_10(2) + Bd*w_10
%     d_12(2) ==  - c1*cos_xk_11*d_11(1) + (1 - abs_xk_11*c2)*d_11(2) + Bd*w_11
%     d_13(2) ==  - c1*cos_xk_12*d_12(1) + (1 - abs_xk_12*c2)*d_12(2) + Bd*w_12
%     d_14(2) ==  - c1*cos_xk_13*d_13(1) + (1 - abs_xk_13*c2)*d_13(2) + Bd*w_13
%     d_15(2) ==  - c1*cos_xk_14*d_14(1) + (1 - abs_xk_14*c2)*d_14(2) + Bd*w_14
%     d_16(2) ==  - c1*cos_xk_15*d_15(1) + (1 - abs_xk_15*c2)*d_15(2) + Bd*w_15
%     d_17(2) ==  - c1*cos_xk_16*d_16(1) + (1 - abs_xk_16*c2)*d_16(2) + Bd*w_16
%     d_18(2) ==  - c1*cos_xk_17*d_17(1) + (1 - abs_xk_17*c2)*d_17(2) + Bd*w_17
%     d_19(2) ==  - c1*cos_xk_18*d_18(1) + (1 - abs_xk_18*c2)*d_18(2) + Bd*w_18
%     d_20(2) ==  - c1*cos_xk_19*d_19(1) + (1 - abs_xk_19*c2)*d_19(2) + Bd*w_19
%     d_21(2) ==  - c1*cos_xk_20*d_20(1) + (1 - abs_xk_20*c2)*d_20(2) + Bd*w_20
%     d_22(2) ==  - c1*cos_xk_21*d_21(1) + (1 - abs_xk_21*c2)*d_21(2) + Bd*w_21
%     d_23(2) ==  - c1*cos_xk_22*d_22(1) + (1 - abs_xk_22*c2)*d_22(2) + Bd*w_22
%     d_24(2) ==  - c1*cos_xk_23*d_23(1) + (1 - abs_xk_23*c2)*d_23(2) + Bd*w_23
%     d_25(2) ==  - c1*cos_xk_24*d_24(1) + (1 - abs_xk_24*c2)*d_24(2) + Bd*w_24
%     d_26(2) ==  - c1*cos_xk_25*d_25(1) + (1 - abs_xk_25*c2)*d_25(2) + Bd*w_25
%     abs(w_0) <= w_max
%     abs(w_1) <= w_max
%     abs(w_2) <= w_max
%     abs(w_3) <= w_max
%     abs(w_4) <= w_max
%     abs(w_5) <= w_max
%     abs(w_6) <= w_max
%     abs(w_7) <= w_max
%     abs(w_8) <= w_max
%     abs(w_9) <= w_max
%     abs(w_10) <= w_max
%     abs(w_11) <= w_max
%     abs(w_12) <= w_max
%     abs(w_13) <= w_max
%     abs(w_14) <= w_max
%     abs(w_15) <= w_max
%     abs(w_16) <= w_max
%     abs(w_17) <= w_max
%     abs(w_18) <= w_max
%     abs(w_19) <= w_max
%     abs(w_20) <= w_max
%     abs(w_21) <= w_max
%     abs(w_22) <= w_max
%     abs(w_23) <= w_max
%     abs(w_24) <= w_max
%     abs(w_25) <= w_max
%     abs(uk_0 + w_0) <= u_max
%     abs(uk_1 + w_1) <= u_max
%     abs(uk_2 + w_2) <= u_max
%     abs(uk_3 + w_3) <= u_max
%     abs(uk_4 + w_4) <= u_max
%     abs(uk_5 + w_5) <= u_max
%     abs(uk_6 + w_6) <= u_max
%     abs(uk_7 + w_7) <= u_max
%     abs(uk_8 + w_8) <= u_max
%     abs(uk_9 + w_9) <= u_max
%     abs(uk_10 + w_10) <= u_max
%     abs(uk_11 + w_11) <= u_max
%     abs(uk_12 + w_12) <= u_max
%     abs(uk_13 + w_13) <= u_max
%     abs(uk_14 + w_14) <= u_max
%     abs(uk_15 + w_15) <= u_max
%     abs(uk_16 + w_16) <= u_max
%     abs(uk_17 + w_17) <= u_max
%     abs(uk_18 + w_18) <= u_max
%     abs(uk_19 + w_19) <= u_max
%     abs(uk_20 + w_20) <= u_max
%     abs(uk_21 + w_21) <= u_max
%     abs(uk_22 + w_22) <= u_max
%     abs(uk_23 + w_23) <= u_max
%     abs(uk_24 + w_24) <= u_max
%     abs(uk_25 + w_25) <= u_max
%     abs(xk_1(2) + d_1(2)) <= x_dot_bound
%     abs(xk_2(2) + d_2(2)) <= x_dot_bound
%     abs(xk_3(2) + d_3(2)) <= x_dot_bound
%     abs(xk_4(2) + d_4(2)) <= x_dot_bound
%     abs(xk_5(2) + d_5(2)) <= x_dot_bound
%     abs(xk_6(2) + d_6(2)) <= x_dot_bound
%     abs(xk_7(2) + d_7(2)) <= x_dot_bound
%     abs(xk_8(2) + d_8(2)) <= x_dot_bound
%     abs(xk_9(2) + d_9(2)) <= x_dot_bound
%     abs(xk_10(2) + d_10(2)) <= x_dot_bound
%     abs(xk_11(2) + d_11(2)) <= x_dot_bound
%     abs(xk_12(2) + d_12(2)) <= x_dot_bound
%     abs(xk_13(2) + d_13(2)) <= x_dot_bound
%     abs(xk_14(2) + d_14(2)) <= x_dot_bound
%     abs(xk_15(2) + d_15(2)) <= x_dot_bound
%     abs(xk_16(2) + d_16(2)) <= x_dot_bound
%     abs(xk_17(2) + d_17(2)) <= x_dot_bound
%     abs(xk_18(2) + d_18(2)) <= x_dot_bound
%     abs(xk_19(2) + d_19(2)) <= x_dot_bound
%     abs(xk_20(2) + d_20(2)) <= x_dot_bound
%     abs(xk_21(2) + d_21(2)) <= x_dot_bound
%     abs(xk_22(2) + d_22(2)) <= x_dot_bound
%     abs(xk_23(2) + d_23(2)) <= x_dot_bound
%     abs(xk_24(2) + d_24(2)) <= x_dot_bound
%     abs(xk_25(2) + d_25(2)) <= x_dot_bound
%     abs(xk_26(2) + d_26(2)) <= x_dot_bound
%     norm(uk_1 + w_1 - uk_0 - w_0, inf) <= S
%     norm(uk_2 + w_2 - uk_1 - w_1, inf) <= S
%     norm(uk_3 + w_3 - uk_2 - w_2, inf) <= S
%     norm(uk_4 + w_4 - uk_3 - w_3, inf) <= S
%     norm(uk_5 + w_5 - uk_4 - w_4, inf) <= S
%     norm(uk_6 + w_6 - uk_5 - w_5, inf) <= S
%     norm(uk_7 + w_7 - uk_6 - w_6, inf) <= S
%     norm(uk_8 + w_8 - uk_7 - w_7, inf) <= S
%     norm(uk_9 + w_9 - uk_8 - w_8, inf) <= S
%     norm(uk_10 + w_10 - uk_9 - w_9, inf) <= S
%     norm(uk_11 + w_11 - uk_10 - w_10, inf) <= S
%     norm(uk_12 + w_12 - uk_11 - w_11, inf) <= S
%     norm(uk_13 + w_13 - uk_12 - w_12, inf) <= S
%     norm(uk_14 + w_14 - uk_13 - w_13, inf) <= S
%     norm(uk_15 + w_15 - uk_14 - w_14, inf) <= S
%     norm(uk_16 + w_16 - uk_15 - w_15, inf) <= S
%     norm(uk_17 + w_17 - uk_16 - w_16, inf) <= S
%     norm(uk_18 + w_18 - uk_17 - w_17, inf) <= S
%     norm(uk_19 + w_19 - uk_18 - w_18, inf) <= S
%     norm(uk_20 + w_20 - uk_19 - w_19, inf) <= S
%     norm(uk_21 + w_21 - uk_20 - w_20, inf) <= S
%     norm(uk_22 + w_22 - uk_21 - w_21, inf) <= S
%     norm(uk_23 + w_23 - uk_22 - w_22, inf) <= S
%     norm(uk_24 + w_24 - uk_23 - w_23, inf) <= S
%     norm(uk_25 + w_25 - uk_24 - w_24, inf) <= S
%     norm(uk_0 + w_0 - last_u, inf) <= S
%     new_EF_1 == new_EF_0*(1 - alpha_0*delta_t) - delta_t*old_EF_0*delta_alpha_0 + delta_t*(nu + D + disturbance_0 + Cd_max*abs_xk_0*(abs_xk_0/I))
%     new_EF_2 == (1 - alpha_1*delta_t)*new_EF_1 - delta_t*old_EF_1*delta_alpha_1 + delta_t*(nu + D + disturbance_1 + Cd_max*abs_xk_1*(abs_xk_1/I))
%     new_EF_3 == (1 - alpha_2*delta_t)*new_EF_2 - delta_t*old_EF_2*delta_alpha_2 + delta_t*(nu + D + disturbance_2 + Cd_max*abs_xk_2*(abs_xk_2/I))
%     new_EF_4 == (1 - alpha_3*delta_t)*new_EF_3 - delta_t*old_EF_3*delta_alpha_3 + delta_t*(nu + D + disturbance_3 + Cd_max*abs_xk_3*(abs_xk_3/I))
%     new_EF_5 == (1 - alpha_4*delta_t)*new_EF_4 - delta_t*old_EF_4*delta_alpha_4 + delta_t*(nu + D + disturbance_4 + Cd_max*abs_xk_4*(abs_xk_4/I))
%     new_EF_6 == (1 - alpha_5*delta_t)*new_EF_5 - delta_t*old_EF_5*delta_alpha_5 + delta_t*(nu + D + disturbance_5 + Cd_max*abs_xk_5*(abs_xk_5/I))
%     new_EF_7 == (1 - alpha_6*delta_t)*new_EF_6 - delta_t*old_EF_6*delta_alpha_6 + delta_t*(nu + D + disturbance_6 + Cd_max*abs_xk_6*(abs_xk_6/I))
%     new_EF_8 == (1 - alpha_7*delta_t)*new_EF_7 - delta_t*old_EF_7*delta_alpha_7 + delta_t*(nu + D + disturbance_7 + Cd_max*abs_xk_7*(abs_xk_7/I))
%     new_EF_9 == (1 - alpha_8*delta_t)*new_EF_8 - delta_t*old_EF_8*delta_alpha_8 + delta_t*(nu + D + disturbance_8 + Cd_max*abs_xk_8*(abs_xk_8/I))
%     new_EF_10 == (1 - alpha_9*delta_t)*new_EF_9 - delta_t*old_EF_9*delta_alpha_9 + delta_t*(nu + D + disturbance_9 + Cd_max*abs_xk_9*(abs_xk_9/I))
%     new_EF_11 == (1 - alpha_10*delta_t)*new_EF_10 - delta_t*old_EF_10*delta_alpha_10 + delta_t*(nu + D + disturbance_10 + Cd_max*abs_xk_10*(abs_xk_10/I))
%     new_EF_12 == (1 - alpha_11*delta_t)*new_EF_11 - delta_t*old_EF_11*delta_alpha_11 + delta_t*(nu + D + disturbance_11 + Cd_max*abs_xk_11*(abs_xk_11/I))
%     new_EF_13 == (1 - alpha_12*delta_t)*new_EF_12 - delta_t*old_EF_12*delta_alpha_12 + delta_t*(nu + D + disturbance_12 + Cd_max*abs_xk_12*(abs_xk_12/I))
%     new_EF_14 == (1 - alpha_13*delta_t)*new_EF_13 - delta_t*old_EF_13*delta_alpha_13 + delta_t*(nu + D + disturbance_13 + Cd_max*abs_xk_13*(abs_xk_13/I))
%     new_EF_15 == (1 - alpha_14*delta_t)*new_EF_14 - delta_t*old_EF_14*delta_alpha_14 + delta_t*(nu + D + disturbance_14 + Cd_max*abs_xk_14*(abs_xk_14/I))
%     new_EF_16 == (1 - alpha_15*delta_t)*new_EF_15 - delta_t*old_EF_15*delta_alpha_15 + delta_t*(nu + D + disturbance_15 + Cd_max*abs_xk_15*(abs_xk_15/I))
%     new_EF_17 == (1 - alpha_16*delta_t)*new_EF_16 - delta_t*old_EF_16*delta_alpha_16 + delta_t*(nu + D + disturbance_16 + Cd_max*abs_xk_16*(abs_xk_16/I))
%     new_EF_18 == (1 - alpha_17*delta_t)*new_EF_17 - delta_t*old_EF_17*delta_alpha_17 + delta_t*(nu + D + disturbance_17 + Cd_max*abs_xk_17*(abs_xk_17/I))
%     new_EF_19 == (1 - alpha_18*delta_t)*new_EF_18 - delta_t*old_EF_18*delta_alpha_18 + delta_t*(nu + D + disturbance_18 + Cd_max*abs_xk_18*(abs_xk_18/I))
%     new_EF_20 == (1 - alpha_19*delta_t)*new_EF_19 - delta_t*old_EF_19*delta_alpha_19 + delta_t*(nu + D + disturbance_19 + Cd_max*abs_xk_19*(abs_xk_19/I))
%     new_EF_21 == (1 - alpha_20*delta_t)*new_EF_20 - delta_t*old_EF_20*delta_alpha_20 + delta_t*(nu + D + disturbance_20 + Cd_max*abs_xk_20*(abs_xk_20/I))
%     new_EF_22 == (1 - alpha_21*delta_t)*new_EF_21 - delta_t*old_EF_21*delta_alpha_21 + delta_t*(nu + D + disturbance_21 + Cd_max*abs_xk_21*(abs_xk_21/I))
%     new_EF_23 == (1 - alpha_22*delta_t)*new_EF_22 - delta_t*old_EF_22*delta_alpha_22 + delta_t*(nu + D + disturbance_22 + Cd_max*abs_xk_22*(abs_xk_22/I))
%     new_EF_24 == (1 - alpha_23*delta_t)*new_EF_23 - delta_t*old_EF_23*delta_alpha_23 + delta_t*(nu + D + disturbance_23 + Cd_max*abs_xk_23*(abs_xk_23/I))
%     new_EF_25 == (1 - alpha_24*delta_t)*new_EF_24 - delta_t*old_EF_24*delta_alpha_24 + delta_t*(nu + D + disturbance_24 + Cd_max*abs_xk_24*(abs_xk_24/I))
%     new_EF_26 == (1 - alpha_25*delta_t)*new_EF_25 - delta_t*old_EF_25*delta_alpha_25 + delta_t*(nu + D + disturbance_25 + Cd_max*abs_xk_25*(abs_xk_25/I))
%     alpha_0*new_EF_0 + old_EF_0*delta_alpha_0 <= gain_max
%     alpha_1*new_EF_1 + old_EF_1*delta_alpha_1 <= gain_max
%     alpha_2*new_EF_2 + old_EF_2*delta_alpha_2 <= gain_max
%     alpha_3*new_EF_3 + old_EF_3*delta_alpha_3 <= gain_max
%     alpha_4*new_EF_4 + old_EF_4*delta_alpha_4 <= gain_max
%     alpha_5*new_EF_5 + old_EF_5*delta_alpha_5 <= gain_max
%     alpha_6*new_EF_6 + old_EF_6*delta_alpha_6 <= gain_max
%     alpha_7*new_EF_7 + old_EF_7*delta_alpha_7 <= gain_max
%     alpha_8*new_EF_8 + old_EF_8*delta_alpha_8 <= gain_max
%     alpha_9*new_EF_9 + old_EF_9*delta_alpha_9 <= gain_max
%     alpha_10*new_EF_10 + old_EF_10*delta_alpha_10 <= gain_max
%     alpha_11*new_EF_11 + old_EF_11*delta_alpha_11 <= gain_max
%     alpha_12*new_EF_12 + old_EF_12*delta_alpha_12 <= gain_max
%     alpha_13*new_EF_13 + old_EF_13*delta_alpha_13 <= gain_max
%     alpha_14*new_EF_14 + old_EF_14*delta_alpha_14 <= gain_max
%     alpha_15*new_EF_15 + old_EF_15*delta_alpha_15 <= gain_max
%     alpha_16*new_EF_16 + old_EF_16*delta_alpha_16 <= gain_max
%     alpha_17*new_EF_17 + old_EF_17*delta_alpha_17 <= gain_max
%     alpha_18*new_EF_18 + old_EF_18*delta_alpha_18 <= gain_max
%     alpha_19*new_EF_19 + old_EF_19*delta_alpha_19 <= gain_max
%     alpha_20*new_EF_20 + old_EF_20*delta_alpha_20 <= gain_max
%     alpha_21*new_EF_21 + old_EF_21*delta_alpha_21 <= gain_max
%     alpha_22*new_EF_22 + old_EF_22*delta_alpha_22 <= gain_max
%     alpha_23*new_EF_23 + old_EF_23*delta_alpha_23 <= gain_max
%     alpha_24*new_EF_24 + old_EF_24*delta_alpha_24 <= gain_max
%     alpha_25*new_EF_25 + old_EF_25*delta_alpha_25 <= gain_max
%     new_EF_1 >= 0
%     new_EF_2 >= 0
%     new_EF_3 >= 0
%     new_EF_4 >= 0
%     new_EF_5 >= 0
%     new_EF_6 >= 0
%     new_EF_7 >= 0
%     new_EF_8 >= 0
%     new_EF_9 >= 0
%     new_EF_10 >= 0
%     new_EF_11 >= 0
%     new_EF_12 >= 0
%     new_EF_13 >= 0
%     new_EF_14 >= 0
%     new_EF_15 >= 0
%     new_EF_16 >= 0
%     new_EF_17 >= 0
%     new_EF_18 >= 0
%     new_EF_19 >= 0
%     new_EF_20 >= 0
%     new_EF_21 >= 0
%     new_EF_22 >= 0
%     new_EF_23 >= 0
%     new_EF_24 >= 0
%     new_EF_25 >= 0
%     new_EF_26 >= 0
%     omega_1 == delta_t*( - lambda*omega_0 + new_EF_0) + omega_0
%     omega_2 == delta_t*( - lambda*omega_1 + new_EF_1) + omega_1
%     omega_3 == delta_t*( - lambda*omega_2 + new_EF_2) + omega_2
%     omega_4 == delta_t*( - lambda*omega_3 + new_EF_3) + omega_3
%     omega_5 == delta_t*( - lambda*omega_4 + new_EF_4) + omega_4
%     omega_6 == delta_t*( - lambda*omega_5 + new_EF_5) + omega_5
%     omega_7 == delta_t*( - lambda*omega_6 + new_EF_6) + omega_6
%     omega_8 == delta_t*( - lambda*omega_7 + new_EF_7) + omega_7
%     omega_9 == delta_t*( - lambda*omega_8 + new_EF_8) + omega_8
%     omega_10 == delta_t*( - lambda*omega_9 + new_EF_9) + omega_9
%     omega_11 == delta_t*( - lambda*omega_10 + new_EF_10) + omega_10
%     omega_12 == delta_t*( - lambda*omega_11 + new_EF_11) + omega_11
%     omega_13 == delta_t*( - lambda*omega_12 + new_EF_12) + omega_12
%     omega_14 == delta_t*( - lambda*omega_13 + new_EF_13) + omega_13
%     omega_15 == delta_t*( - lambda*omega_14 + new_EF_14) + omega_14
%     omega_16 == delta_t*( - lambda*omega_15 + new_EF_15) + omega_15
%     omega_17 == delta_t*( - lambda*omega_16 + new_EF_16) + omega_16
%     omega_18 == delta_t*( - lambda*omega_17 + new_EF_17) + omega_17
%     omega_19 == delta_t*( - lambda*omega_18 + new_EF_18) + omega_18
%     omega_20 == delta_t*( - lambda*omega_19 + new_EF_19) + omega_19
%     omega_21 == delta_t*( - lambda*omega_20 + new_EF_20) + omega_20
%     omega_22 == delta_t*( - lambda*omega_21 + new_EF_21) + omega_21
%     omega_23 == delta_t*( - lambda*omega_22 + new_EF_22) + omega_22
%     omega_24 == delta_t*( - lambda*omega_23 + new_EF_23) + omega_23
%     omega_25 == delta_t*( - lambda*omega_24 + new_EF_24) + omega_24
%     omega_26 == delta_t*( - lambda*omega_25 + new_EF_25) + omega_25
%     norm(alpha_5 + delta_alpha_5 - alpha_final_0, inf) <= delta_alpha_final
%     norm(alpha_10 + delta_alpha_10 - alpha_final_1, inf) <= delta_alpha_final
%     norm(alpha_15 + delta_alpha_15 - alpha_final_2, inf) <= delta_alpha_final
%     alpha_0 + delta_alpha_0 >= alpha_min
%     alpha_1 + delta_alpha_1 >= alpha_min
%     alpha_2 + delta_alpha_2 >= alpha_min
%     alpha_3 + delta_alpha_3 >= alpha_min
%     alpha_4 + delta_alpha_4 >= alpha_min
%     alpha_5 + delta_alpha_5 >= alpha_min
%     alpha_6 + delta_alpha_6 >= alpha_min
%     alpha_7 + delta_alpha_7 >= alpha_min
%     alpha_8 + delta_alpha_8 >= alpha_min
%     alpha_9 + delta_alpha_9 >= alpha_min
%     alpha_10 + delta_alpha_10 >= alpha_min
%     alpha_11 + delta_alpha_11 >= alpha_min
%     alpha_12 + delta_alpha_12 >= alpha_min
%     alpha_13 + delta_alpha_13 >= alpha_min
%     alpha_14 + delta_alpha_14 >= alpha_min
%     alpha_15 + delta_alpha_15 >= alpha_min
%     alpha_16 + delta_alpha_16 >= alpha_min
%     alpha_17 + delta_alpha_17 >= alpha_min
%     alpha_18 + delta_alpha_18 >= alpha_min
%     alpha_19 + delta_alpha_19 >= alpha_min
%     alpha_20 + delta_alpha_20 >= alpha_min
%     alpha_21 + delta_alpha_21 >= alpha_min
%     alpha_22 + delta_alpha_22 >= alpha_min
%     alpha_23 + delta_alpha_23 >= alpha_min
%     alpha_24 + delta_alpha_24 >= alpha_min
%     alpha_25 + delta_alpha_25 >= alpha_min
%     alpha_0 + delta_alpha_0 <= alpha_max
%     alpha_1 + delta_alpha_1 <= alpha_max
%     alpha_2 + delta_alpha_2 <= alpha_max
%     alpha_3 + delta_alpha_3 <= alpha_max
%     alpha_4 + delta_alpha_4 <= alpha_max
%     alpha_5 + delta_alpha_5 <= alpha_max
%     alpha_6 + delta_alpha_6 <= alpha_max
%     alpha_7 + delta_alpha_7 <= alpha_max
%     alpha_8 + delta_alpha_8 <= alpha_max
%     alpha_9 + delta_alpha_9 <= alpha_max
%     alpha_10 + delta_alpha_10 <= alpha_max
%     alpha_11 + delta_alpha_11 <= alpha_max
%     alpha_12 + delta_alpha_12 <= alpha_max
%     alpha_13 + delta_alpha_13 <= alpha_max
%     alpha_14 + delta_alpha_14 <= alpha_max
%     alpha_15 + delta_alpha_15 <= alpha_max
%     alpha_16 + delta_alpha_16 <= alpha_max
%     alpha_17 + delta_alpha_17 <= alpha_max
%     alpha_18 + delta_alpha_18 <= alpha_max
%     alpha_19 + delta_alpha_19 <= alpha_max
%     alpha_20 + delta_alpha_20 <= alpha_max
%     alpha_21 + delta_alpha_21 <= alpha_max
%     alpha_22 + delta_alpha_22 <= alpha_max
%     alpha_23 + delta_alpha_23 <= alpha_max
%     alpha_24 + delta_alpha_24 <= alpha_max
%     alpha_25 + delta_alpha_25 <= alpha_max
%     abs(alpha_1 + delta_alpha_1 - alpha_0 - delta_alpha_0) <= delta_alpha_max
%     abs(alpha_2 + delta_alpha_2 - alpha_1 - delta_alpha_1) <= delta_alpha_max
%     abs(alpha_3 + delta_alpha_3 - alpha_2 - delta_alpha_2) <= delta_alpha_max
%     abs(alpha_4 + delta_alpha_4 - alpha_3 - delta_alpha_3) <= delta_alpha_max
%     abs(alpha_5 + delta_alpha_5 - alpha_4 - delta_alpha_4) <= delta_alpha_max
%     abs(alpha_6 + delta_alpha_6 - alpha_5 - delta_alpha_5) <= delta_alpha_max
%     abs(alpha_7 + delta_alpha_7 - alpha_6 - delta_alpha_6) <= delta_alpha_max
%     abs(alpha_8 + delta_alpha_8 - alpha_7 - delta_alpha_7) <= delta_alpha_max
%     abs(alpha_9 + delta_alpha_9 - alpha_8 - delta_alpha_8) <= delta_alpha_max
%     abs(alpha_10 + delta_alpha_10 - alpha_9 - delta_alpha_9) <= delta_alpha_max
%     abs(alpha_11 + delta_alpha_11 - alpha_10 - delta_alpha_10) <= delta_alpha_max
%     abs(alpha_12 + delta_alpha_12 - alpha_11 - delta_alpha_11) <= delta_alpha_max
%     abs(alpha_13 + delta_alpha_13 - alpha_12 - delta_alpha_12) <= delta_alpha_max
%     abs(alpha_14 + delta_alpha_14 - alpha_13 - delta_alpha_13) <= delta_alpha_max
%     abs(alpha_15 + delta_alpha_15 - alpha_14 - delta_alpha_14) <= delta_alpha_max
%     abs(alpha_16 + delta_alpha_16 - alpha_15 - delta_alpha_15) <= delta_alpha_max
%     abs(alpha_17 + delta_alpha_17 - alpha_16 - delta_alpha_16) <= delta_alpha_max
%     abs(alpha_18 + delta_alpha_18 - alpha_17 - delta_alpha_17) <= delta_alpha_max
%     abs(alpha_19 + delta_alpha_19 - alpha_18 - delta_alpha_18) <= delta_alpha_max
%     abs(alpha_20 + delta_alpha_20 - alpha_19 - delta_alpha_19) <= delta_alpha_max
%     abs(alpha_21 + delta_alpha_21 - alpha_20 - delta_alpha_20) <= delta_alpha_max
%     abs(alpha_22 + delta_alpha_22 - alpha_21 - delta_alpha_21) <= delta_alpha_max
%     abs(alpha_23 + delta_alpha_23 - alpha_22 - delta_alpha_22) <= delta_alpha_max
%     abs(alpha_24 + delta_alpha_24 - alpha_23 - delta_alpha_23) <= delta_alpha_max
%     abs(alpha_25 + delta_alpha_25 - alpha_24 - delta_alpha_24) <= delta_alpha_max
%     abs(alpha_0 + delta_alpha_0 - last_alpha) <= delta_alpha_max
%     omega_1 >= 0
%     omega_2 >= 0
%     omega_3 >= 0
%     omega_4 >= 0
%     omega_5 >= 0
%     omega_6 >= 0
%     omega_7 >= 0
%     omega_8 >= 0
%     omega_9 >= 0
%     omega_10 >= 0
%     omega_11 >= 0
%     omega_12 >= 0
%     omega_13 >= 0
%     omega_14 >= 0
%     omega_15 >= 0
%     omega_16 >= 0
%     omega_17 >= 0
%     omega_18 >= 0
%     omega_19 >= 0
%     omega_20 >= 0
%     omega_21 >= 0
%     omega_22 >= 0
%     omega_23 >= 0
%     omega_24 >= 0
%     omega_25 >= 0
%     omega_26 >= 0
%     omega_1 <= omega_max
%     omega_2 <= omega_max
%     omega_3 <= omega_max
%     omega_4 <= omega_max
%     omega_5 <= omega_max
%     omega_6 <= omega_max
%     omega_7 <= omega_max
%     omega_8 <= omega_max
%     omega_9 <= omega_max
%     omega_10 <= omega_max
%     omega_11 <= omega_max
%     omega_12 <= omega_max
%     omega_13 <= omega_max
%     omega_14 <= omega_max
%     omega_15 <= omega_max
%     omega_16 <= omega_max
%     omega_17 <= omega_max
%     omega_18 <= omega_max
%     omega_19 <= omega_max
%     omega_20 <= omega_max
%     omega_21 <= omega_max
%     omega_22 <= omega_max
%     omega_23 <= omega_max
%     omega_24 <= omega_max
%     omega_25 <= omega_max
%     omega_26 <= omega_max
%
% with variables
%      d_1   2 x 1
%      d_2   2 x 1
%      d_3   2 x 1
%      d_4   2 x 1
%      d_5   2 x 1
%      d_6   2 x 1
%      d_7   2 x 1
%      d_8   2 x 1
%      d_9   2 x 1
%     d_10   2 x 1
%     d_11   2 x 1
%     d_12   2 x 1
%     d_13   2 x 1
%     d_14   2 x 1
%     d_15   2 x 1
%     d_16   2 x 1
%     d_17   2 x 1
%     d_18   2 x 1
%     d_19   2 x 1
%     d_20   2 x 1
%     d_21   2 x 1
%     d_22   2 x 1
%     d_23   2 x 1
%     d_24   2 x 1
%     d_25   2 x 1
%     d_26   2 x 1
% delta_alpha_0   1 x 1
% delta_alpha_1   1 x 1
% delta_alpha_2   1 x 1
% delta_alpha_3   1 x 1
% delta_alpha_4   1 x 1
% delta_alpha_5   1 x 1
% delta_alpha_6   1 x 1
% delta_alpha_7   1 x 1
% delta_alpha_8   1 x 1
% delta_alpha_9   1 x 1
% delta_alpha_10   1 x 1
% delta_alpha_11   1 x 1
% delta_alpha_12   1 x 1
% delta_alpha_13   1 x 1
% delta_alpha_14   1 x 1
% delta_alpha_15   1 x 1
% delta_alpha_16   1 x 1
% delta_alpha_17   1 x 1
% delta_alpha_18   1 x 1
% delta_alpha_19   1 x 1
% delta_alpha_20   1 x 1
% delta_alpha_21   1 x 1
% delta_alpha_22   1 x 1
% delta_alpha_23   1 x 1
% delta_alpha_24   1 x 1
% delta_alpha_25   1 x 1
% new_EF_1   1 x 1
% new_EF_2   1 x 1
% new_EF_3   1 x 1
% new_EF_4   1 x 1
% new_EF_5   1 x 1
% new_EF_6   1 x 1
% new_EF_7   1 x 1
% new_EF_8   1 x 1
% new_EF_9   1 x 1
% new_EF_10   1 x 1
% new_EF_11   1 x 1
% new_EF_12   1 x 1
% new_EF_13   1 x 1
% new_EF_14   1 x 1
% new_EF_15   1 x 1
% new_EF_16   1 x 1
% new_EF_17   1 x 1
% new_EF_18   1 x 1
% new_EF_19   1 x 1
% new_EF_20   1 x 1
% new_EF_21   1 x 1
% new_EF_22   1 x 1
% new_EF_23   1 x 1
% new_EF_24   1 x 1
% new_EF_25   1 x 1
% new_EF_26   1 x 1
%  omega_1   1 x 1
%  omega_2   1 x 1
%  omega_3   1 x 1
%  omega_4   1 x 1
%  omega_5   1 x 1
%  omega_6   1 x 1
%  omega_7   1 x 1
%  omega_8   1 x 1
%  omega_9   1 x 1
% omega_10   1 x 1
% omega_11   1 x 1
% omega_12   1 x 1
% omega_13   1 x 1
% omega_14   1 x 1
% omega_15   1 x 1
% omega_16   1 x 1
% omega_17   1 x 1
% omega_18   1 x 1
% omega_19   1 x 1
% omega_20   1 x 1
% omega_21   1 x 1
% omega_22   1 x 1
% omega_23   1 x 1
% omega_24   1 x 1
% omega_25   1 x 1
% omega_26   1 x 1
%      w_0   1 x 1
%      w_1   1 x 1
%      w_2   1 x 1
%      w_3   1 x 1
%      w_4   1 x 1
%      w_5   1 x 1
%      w_6   1 x 1
%      w_7   1 x 1
%      w_8   1 x 1
%      w_9   1 x 1
%     w_10   1 x 1
%     w_11   1 x 1
%     w_12   1 x 1
%     w_13   1 x 1
%     w_14   1 x 1
%     w_15   1 x 1
%     w_16   1 x 1
%     w_17   1 x 1
%     w_18   1 x 1
%     w_19   1 x 1
%     w_20   1 x 1
%     w_21   1 x 1
%     w_22   1 x 1
%     w_23   1 x 1
%     w_24   1 x 1
%     w_25   1 x 1
%
% and parameters
%       Bd   1 x 1    positive
%   Cd_max   1 x 1    positive
%        D   1 x 1    positive
%        I   1 x 1    positive
% I_over_L   1 x 1    positive
%        M   1 x 1    PSD
%        Q   2 x 2    PSD
%  Q_final   2 x 2    PSD
%        R   1 x 1    PSD
%  R_delta   1 x 1    PSD
%        S   1 x 1    positive
% abs_xk_0   1 x 1    positive
% abs_xk_1   1 x 1    positive
% abs_xk_2   1 x 1    positive
% abs_xk_3   1 x 1    positive
% abs_xk_4   1 x 1    positive
% abs_xk_5   1 x 1    positive
% abs_xk_6   1 x 1    positive
% abs_xk_7   1 x 1    positive
% abs_xk_8   1 x 1    positive
% abs_xk_9   1 x 1    positive
% abs_xk_10   1 x 1    positive
% abs_xk_11   1 x 1    positive
% abs_xk_12   1 x 1    positive
% abs_xk_13   1 x 1    positive
% abs_xk_14   1 x 1    positive
% abs_xk_15   1 x 1    positive
% abs_xk_16   1 x 1    positive
% abs_xk_17   1 x 1    positive
% abs_xk_18   1 x 1    positive
% abs_xk_19   1 x 1    positive
% abs_xk_20   1 x 1    positive
% abs_xk_21   1 x 1    positive
% abs_xk_22   1 x 1    positive
% abs_xk_23   1 x 1    positive
% abs_xk_24   1 x 1    positive
% abs_xk_25   1 x 1    positive
%  alpha_0   1 x 1
%  alpha_1   1 x 1
%  alpha_2   1 x 1
%  alpha_3   1 x 1
%  alpha_4   1 x 1
%  alpha_5   1 x 1
%  alpha_6   1 x 1
%  alpha_7   1 x 1
%  alpha_8   1 x 1
%  alpha_9   1 x 1
% alpha_10   1 x 1
% alpha_11   1 x 1
% alpha_12   1 x 1
% alpha_13   1 x 1
% alpha_14   1 x 1
% alpha_15   1 x 1
% alpha_16   1 x 1
% alpha_17   1 x 1
% alpha_18   1 x 1
% alpha_19   1 x 1
% alpha_20   1 x 1
% alpha_21   1 x 1
% alpha_22   1 x 1
% alpha_23   1 x 1
% alpha_24   1 x 1
% alpha_25   1 x 1
% alpha_final_0   1 x 1    positive
% alpha_final_1   1 x 1    positive
% alpha_final_2   1 x 1    positive
% alpha_max   1 x 1    positive
% alpha_min   1 x 1    positive
%       c1   1 x 1    positive
%       c2   1 x 1    positive
% cos_xk_0   1 x 1
% cos_xk_1   1 x 1
% cos_xk_2   1 x 1
% cos_xk_3   1 x 1
% cos_xk_4   1 x 1
% cos_xk_5   1 x 1
% cos_xk_6   1 x 1
% cos_xk_7   1 x 1
% cos_xk_8   1 x 1
% cos_xk_9   1 x 1
% cos_xk_10   1 x 1
% cos_xk_11   1 x 1
% cos_xk_12   1 x 1
% cos_xk_13   1 x 1
% cos_xk_14   1 x 1
% cos_xk_15   1 x 1
% cos_xk_16   1 x 1
% cos_xk_17   1 x 1
% cos_xk_18   1 x 1
% cos_xk_19   1 x 1
% cos_xk_20   1 x 1
% cos_xk_21   1 x 1
% cos_xk_22   1 x 1
% cos_xk_23   1 x 1
% cos_xk_24   1 x 1
% cos_xk_25   1 x 1
%      d_0   2 x 1
% delta_alpha_final   1 x 1    positive
% delta_alpha_max   1 x 1    positive
%  delta_t   1 x 1    positive
% disturbance_0   1 x 1
% disturbance_1   1 x 1
% disturbance_2   1 x 1
% disturbance_3   1 x 1
% disturbance_4   1 x 1
% disturbance_5   1 x 1
% disturbance_6   1 x 1
% disturbance_7   1 x 1
% disturbance_8   1 x 1
% disturbance_9   1 x 1
% disturbance_10   1 x 1
% disturbance_11   1 x 1
% disturbance_12   1 x 1
% disturbance_13   1 x 1
% disturbance_14   1 x 1
% disturbance_15   1 x 1
% disturbance_16   1 x 1
% disturbance_17   1 x 1
% disturbance_18   1 x 1
% disturbance_19   1 x 1
% disturbance_20   1 x 1
% disturbance_21   1 x 1
% disturbance_22   1 x 1
% disturbance_23   1 x 1
% disturbance_24   1 x 1
% disturbance_25   1 x 1
% gain_max   1 x 1    positive
%   lambda   1 x 1    positive
% last_alpha   1 x 1    positive
%   last_u   1 x 1
% new_EF_0   1 x 1    positive
%       nu   1 x 1    positive
% old_EF_0   1 x 1
% old_EF_1   1 x 1
% old_EF_2   1 x 1
% old_EF_3   1 x 1
% old_EF_4   1 x 1
% old_EF_5   1 x 1
% old_EF_6   1 x 1
% old_EF_7   1 x 1
% old_EF_8   1 x 1
% old_EF_9   1 x 1
% old_EF_10   1 x 1
% old_EF_11   1 x 1
% old_EF_12   1 x 1
% old_EF_13   1 x 1
% old_EF_14   1 x 1
% old_EF_15   1 x 1
% old_EF_16   1 x 1
% old_EF_17   1 x 1
% old_EF_18   1 x 1
% old_EF_19   1 x 1
% old_EF_20   1 x 1
% old_EF_21   1 x 1
% old_EF_22   1 x 1
% old_EF_23   1 x 1
% old_EF_24   1 x 1
% old_EF_25   1 x 1
%  omega_0   1 x 1    positive
% omega_max   1 x 1    positive
%    u_max   1 x 1    positive
%     uk_0   1 x 1
%     uk_1   1 x 1
%     uk_2   1 x 1
%     uk_3   1 x 1
%     uk_4   1 x 1
%     uk_5   1 x 1
%     uk_6   1 x 1
%     uk_7   1 x 1
%     uk_8   1 x 1
%     uk_9   1 x 1
%    uk_10   1 x 1
%    uk_11   1 x 1
%    uk_12   1 x 1
%    uk_13   1 x 1
%    uk_14   1 x 1
%    uk_15   1 x 1
%    uk_16   1 x 1
%    uk_17   1 x 1
%    uk_18   1 x 1
%    uk_19   1 x 1
%    uk_20   1 x 1
%    uk_21   1 x 1
%    uk_22   1 x 1
%    uk_23   1 x 1
%    uk_24   1 x 1
%    uk_25   1 x 1
%    w_max   1 x 1    positive
% x_dot_bound   1 x 1    positive
%  x_final   2 x 1
%     xk_0   2 x 1
%     xk_1   2 x 1
%     xk_2   2 x 1
%     xk_3   2 x 1
%     xk_4   2 x 1
%     xk_5   2 x 1
%     xk_6   2 x 1
%     xk_7   2 x 1
%     xk_8   2 x 1
%     xk_9   2 x 1
%    xk_10   2 x 1
%    xk_11   2 x 1
%    xk_12   2 x 1
%    xk_13   2 x 1
%    xk_14   2 x 1
%    xk_15   2 x 1
%    xk_16   2 x 1
%    xk_17   2 x 1
%    xk_18   2 x 1
%    xk_19   2 x 1
%    xk_20   2 x 1
%    xk_21   2 x 1
%    xk_22   2 x 1
%    xk_23   2 x 1
%    xk_24   2 x 1
%    xk_25   2 x 1
%    xk_26   2 x 1
%
% Note:
%   - Check status.converged, which will be 1 if optimization succeeded.
%   - You don't have to specify settings if you don't want to.
%   - To hide output, use settings.verbose = 0.
%   - To change iterations, use settings.max_iters = 20.
%   - You may wish to compare with cvxsolve to check the solver is correct.
%
% Specify params.Bd, ..., params.xk_26, then run
%   [vars, status] = csolve(params, settings)
% Produced by CVXGEN, 2020-02-27 17:11:42 -0500.
% CVXGEN is Copyright (C) 2006-2017 Jacob Mattingley, jem@cvxgen.com.
% The code in this file is Copyright (C) 2006-2017 Jacob Mattingley.
% CVXGEN, or solvers produced by CVXGEN, cannot be used for commercial
% applications without prior written permission from Jacob Mattingley.

% Filename: csolve.m.
% Description: Help file for the Matlab solver interface.
