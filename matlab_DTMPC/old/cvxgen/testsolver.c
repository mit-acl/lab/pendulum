/* Produced by CVXGEN, 2020-02-27 17:13:27 -0500.  */
/* CVXGEN is Copyright (C) 2006-2017 Jacob Mattingley, jem@cvxgen.com. */
/* The code in this file is Copyright (C) 2006-2017 Jacob Mattingley. */
/* CVXGEN, or solvers produced by CVXGEN, cannot be used for commercial */
/* applications without prior written permission from Jacob Mattingley. */

/* Filename: testsolver.c. */
/* Description: Basic test harness for solver.c. */
#include "solver.h"
Vars vars;
Params params;
Workspace work;
Settings settings;
#define NUMTESTS 0
int main(int argc, char **argv) {
  int num_iters;
#if (NUMTESTS > 0)
  int i;
  double time;
  double time_per;
#endif
  set_defaults();
  setup_indexing();
  load_default_data();
  /* Solve problem instance for the record. */
  settings.verbose = 1;
  num_iters = solve();
#ifndef ZERO_LIBRARY_MODE
#if (NUMTESTS > 0)
  /* Now solve multiple problem instances for timing purposes. */
  settings.verbose = 0;
  tic();
  for (i = 0; i < NUMTESTS; i++) {
    solve();
  }
  time = tocq();
  printf("Timed %d solves over %.3f seconds.\n", NUMTESTS, time);
  time_per = time / NUMTESTS;
  if (time_per > 1) {
    printf("Actual time taken per solve: %.3g s.\n", time_per);
  } else if (time_per > 1e-3) {
    printf("Actual time taken per solve: %.3g ms.\n", 1e3*time_per);
  } else {
    printf("Actual time taken per solve: %.3g us.\n", 1e6*time_per);
  }
#endif
#endif
  return 0;
}
void load_default_data(void) {
  params.x_final[0] = 0.20319161029830202;
  params.x_final[1] = 0.8325912904724193;
  params.xk_0[0] = -0.8363810443482227;
  params.xk_0[1] = 0.04331042079065206;
  params.d_0[0] = 1.5717878173906188;
  params.d_0[1] = 1.5851723557337523;
  /* Make this a diagonal PSD matrix, even though it's not diagonal. */
  params.Q[0] = 1.1255853104638363;
  params.Q[2] = 0;
  params.Q[1] = 0;
  params.Q[3] = 1.2072428781381868;
  params.uk_0[0] = -1.7941311867966805;
  /* Make this a diagonal PSD matrix, even though it's not diagonal. */
  params.R[0] = 1.4408098436506365;
  params.alpha_0[0] = -1.8804951564857322;
  params.new_EF_0[0] = 0.9136664487894222;
  params.old_EF_0[0] = 0.596576190459043;
  params.I_over_L[0] = 0.5569745652959506;
  /* Make this a diagonal PSD matrix, even though it's not diagonal. */
  params.R_delta[0] = 1.6762549019801312;
  params.alpha_min[0] = 1.1817256348327017;
  /* Make this a diagonal PSD matrix, even though it's not diagonal. */
  params.M[0] = 1.0239818823771654;
  params.xk_1[0] = 0.23541635196352795;
  params.xk_1[1] = -0.9629902123701384;
  params.uk_1[0] = -0.3395952119597214;
  params.alpha_1[0] = -0.865899672914725;
  params.old_EF_1[0] = 0.7725516732519853;
  params.xk_2[0] = -0.23818512931704205;
  params.xk_2[1] = -1.372529046100147;
  params.uk_2[0] = 0.17859607212737894;
  params.alpha_2[0] = 1.1212590580454682;
  params.old_EF_2[0] = -0.774545870495281;
  params.xk_3[0] = -1.1121684642712744;
  params.xk_3[1] = -0.44811496977740495;
  params.uk_3[0] = 1.7455345994417217;
  params.alpha_3[0] = 1.9039816898917352;
  params.old_EF_3[0] = 0.6895347036512547;
  params.xk_4[0] = 1.6113364341535923;
  params.xk_4[1] = 1.383003485172717;
  params.uk_4[0] = -0.48802383468444344;
  params.alpha_4[0] = -1.631131964513103;
  params.old_EF_4[0] = 0.6136436100941447;
  params.xk_5[0] = 0.2313630495538037;
  params.xk_5[1] = -0.5537409477496875;
  params.uk_5[0] = -1.0997819806406723;
  params.alpha_5[0] = -0.3739203344950055;
  params.old_EF_5[0] = -0.12423900520332376;
  params.xk_6[0] = -0.923057686995755;
  params.xk_6[1] = -0.8328289030982696;
  params.uk_6[0] = -0.16925440270808823;
  params.alpha_6[0] = 1.442135651787706;
  params.old_EF_6[0] = 0.34501161787128565;
  params.xk_7[0] = -0.8660485502711608;
  params.xk_7[1] = -0.8880899735055947;
  params.uk_7[0] = -0.1815116979122129;
  params.alpha_7[0] = -1.17835862158005;
  params.old_EF_7[0] = -1.1944851558277074;
  params.xk_8[0] = 0.05614023926976763;
  params.xk_8[1] = -1.6510825248767813;
  params.uk_8[0] = -0.06565787059365391;
  params.alpha_8[0] = -0.5512951504486665;
  params.old_EF_8[0] = 0.8307464872626844;
  params.xk_9[0] = 0.9869848924080182;
  params.xk_9[1] = 0.7643716874230573;
  params.uk_9[0] = 0.7567216550196565;
  params.alpha_9[0] = -0.5055995034042868;
  params.old_EF_9[0] = 0.6725392189410702;
  params.xk_10[0] = -0.6406053441727284;
  params.xk_10[1] = 0.29117547947550015;
  params.uk_10[0] = -0.6967713677405021;
  params.alpha_10[0] = -0.21941980294587182;
  params.old_EF_10[0] = -1.753884276680243;
  params.xk_11[0] = -1.0292983112626475;
  params.xk_11[1] = 1.8864104246942706;
  params.uk_11[0] = -1.077663182579704;
  params.alpha_11[0] = 0.7659100437893209;
  params.old_EF_11[0] = 0.6019074328549583;
  params.xk_12[0] = 0.8957565577499285;
  params.xk_12[1] = -0.09964555746227477;
  params.uk_12[0] = 0.38665509840745127;
  params.alpha_12[0] = -1.7321223042686946;
  params.old_EF_12[0] = -1.7097514487110663;
  params.xk_13[0] = -1.2040958948116867;
  params.xk_13[1] = -1.3925560119658358;
  params.uk_13[0] = -1.5995826216742213;
  params.alpha_13[0] = -1.4828245415645833;
  params.old_EF_13[0] = 0.21311092723061398;
  params.xk_14[0] = -1.248740700304487;
  params.xk_14[1] = 1.808404972124833;
  params.uk_14[0] = 0.7264471152297065;
  params.alpha_14[0] = 0.16407869343908477;
  params.old_EF_14[0] = 0.8287224032315907;
  params.xk_15[0] = -0.9444533161899464;
  params.xk_15[1] = 1.7069027370149112;
  params.uk_15[0] = 1.3567722311998827;
  params.alpha_15[0] = 0.9052779937121489;
  params.old_EF_15[0] = -0.07904017565835986;
  params.xk_16[0] = 1.3684127435065871;
  params.xk_16[1] = 0.979009293697437;
  params.uk_16[0] = 0.6413036255984501;
  params.alpha_16[0] = 1.6559010680237511;
  params.old_EF_16[0] = 0.5346622551502991;
  params.xk_17[0] = -0.5362376605895625;
  params.xk_17[1] = 0.2113782926017822;
  params.uk_17[0] = -1.2144776931994525;
  params.alpha_17[0] = -1.2317108144255875;
  params.old_EF_17[0] = 0.9026784957312834;
  params.xk_18[0] = 1.1397468137245244;
  params.xk_18[1] = 1.8883934547350631;
  params.uk_18[0] = 1.4038856681660068;
  params.alpha_18[0] = 0.17437730638329096;
  params.old_EF_18[0] = -1.6408365219077408;
  params.xk_19[0] = -0.04450702153554875;
  params.xk_19[1] = 1.7117453902485025;
  params.uk_19[0] = 1.1504727980139053;
  params.alpha_19[0] = -0.05962309578364744;
  params.old_EF_19[0] = -0.1788825540764547;
  params.xk_20[0] = -1.1280569263625857;
  params.xk_20[1] = -1.2911464767927057;
  params.uk_20[0] = -1.7055053231225696;
  params.alpha_20[0] = 1.56957275034837;
  params.old_EF_20[0] = 0.5607064675962357;
  params.xk_21[0] = -1.4266707301147146;
  params.xk_21[1] = -0.3434923211351708;
  params.uk_21[0] = -1.8035643024085055;
  params.alpha_21[0] = -1.1625066019105454;
  params.old_EF_21[0] = 0.9228324965161532;
  params.xk_22[0] = 0.6044910817663975;
  params.xk_22[1] = -0.0840868104920891;
  params.uk_22[0] = -0.900877978017443;
  params.alpha_22[0] = 0.608892500264739;
  params.old_EF_22[0] = 1.8257980452695217;
  params.xk_23[0] = -0.25791777529922877;
  params.xk_23[1] = -1.7194699796493191;
  params.uk_23[0] = -1.7690740487081298;
  params.alpha_23[0] = -1.6685159248097703;
  params.old_EF_23[0] = 1.8388287490128845;
  params.xk_24[0] = 0.16304334474597537;
  params.xk_24[1] = 1.3498497306788897;
  params.uk_24[0] = -1.3198658230514613;
  params.alpha_24[0] = -0.9586197090843394;
  params.old_EF_24[0] = 0.7679100474913709;
  params.xk_25[0] = 1.5822813125679343;
  params.xk_25[1] = -0.6372460621593619;
  params.uk_25[0] = -1.741307208038867;
  params.alpha_25[0] = 1.456478677642575;
  params.old_EF_25[0] = -0.8365102166820959;
  params.xk_26[0] = 0.9643296255982503;
  params.xk_26[1] = -1.367865381194024;
  /* Make this a diagonal PSD matrix, even though it's not diagonal. */
  params.Q_final[0] = 1.6949634351408758;
  params.Q_final[2] = 0;
  params.Q_final[1] = 0;
  params.Q_final[3] = 1.8414196190311483;
  params.delta_t[0] = 1.4543041574934186;
  params.c1[0] = 0.7182150497269828;
  params.cos_xk_0[0] = 0.9067590059607915;
  params.c2[0] = 0.27893424836492065;
  params.abs_xk_0[0] = 0.6276382304664441;
  params.Bd[0] = 0.8391655133658891;
  params.cos_xk_1[0] = 1.5088481557772684;
  params.abs_xk_1[0] = 0.307480417142286;
  params.cos_xk_2[0] = 1.5204991609972622;
  params.abs_xk_2[0] = 1.5979286384416078;
  params.cos_xk_3[0] = 1.8864971883119228;
  params.abs_xk_3[0] = 0.7354059666069208;
  params.cos_xk_4[0] = -1.1802409243688836;
  params.abs_xk_4[0] = 0.48114064066919804;
  params.cos_xk_5[0] = 1.3114512056856835;
  params.abs_xk_5[0] = 1.9304562971878307;
  params.cos_xk_6[0] = 0.7952399935216938;
  params.abs_xk_6[0] = 0.9649940835476598;
  params.cos_xk_7[0] = -0.8518009412754686;
  params.abs_xk_7[0] = 1.6673757686863193;
  params.cos_xk_8[0] = 1.4887180335977037;
  params.abs_xk_8[0] = 0.1842631836011832;
  params.cos_xk_9[0] = -1.1362021159208933;
  params.abs_xk_9[0] = 1.663522180915733;
  params.cos_xk_10[0] = 1.3932155883179842;
  params.abs_xk_10[0] = 0.6293059975279947;
  params.cos_xk_11[0] = -0.8828216126125747;
  params.abs_xk_11[0] = 0.86163004403692;
  params.cos_xk_12[0] = 0.15778600105866714;
  params.abs_xk_12[0] = 0.19113363001322714;
  params.cos_xk_13[0] = 1.3476485548544606;
  params.abs_xk_13[0] = 1.069469740702642;
  params.cos_xk_14[0] = 1.0998712601636944;
  params.abs_xk_14[0] = 0.4616725311526537;
  params.cos_xk_15[0] = 1.8611734044254629;
  params.abs_xk_15[0] = 1.5020546146367586;
  params.cos_xk_16[0] = -0.6276245424321543;
  params.abs_xk_16[0] = 1.8970552939199095;
  params.cos_xk_17[0] = 0.8020471158650913;
  params.abs_xk_17[0] = 1.681122170972474;
  params.cos_xk_18[0] = -1.8180107765765245;
  params.abs_xk_18[0] = 0.11128308210337634;
  params.cos_xk_19[0] = 0.9709490941985153;
  params.abs_xk_19[0] = 0.6093728658967841;
  params.cos_xk_20[0] = 0.0671374633729811;
  params.abs_xk_20[0] = 0.31252484734254704;
  params.cos_xk_21[0] = 1.9118096386279388;
  params.abs_xk_21[0] = 1.005502095348839;
  params.cos_xk_22[0] = 1.3160043138989015;
  params.abs_xk_22[0] = 0.1480755925599928;
  params.cos_xk_23[0] = -0.08433819112864738;
  params.abs_xk_23[0] = 0.12455896081155182;
  params.cos_xk_24[0] = 1.536965724350949;
  params.abs_xk_24[0] = 0.8916203574259176;
  params.cos_xk_25[0] = -1.725800326952653;
  params.abs_xk_25[0] = 0.15299256463191413;
  params.w_max[0] = 1.07758531600634;
  params.u_max[0] = 0.15113280901046156;
  params.x_dot_bound[0] = 0.3675446360248855;
  params.S[0] = 0.872714168333028;
  params.last_u[0] = -0.008868675926170244;
  params.nu[0] = 1.1666238304835148;
  params.D[0] = 1.2410253628098147;
  params.disturbance_0[0] = -0.5087540014293261;
  params.Cd_max[0] = 1.2374731659611597;
  params.I[0] = 0.3144893167702725;
  params.disturbance_1[0] = -0.8979660982652256;
  params.disturbance_2[0] = 1.194873082385242;
  params.disturbance_3[0] = -1.3876427970939353;
  params.disturbance_4[0] = -1.106708108457053;
  params.disturbance_5[0] = -1.0280872812241797;
  params.disturbance_6[0] = -0.08197078070773234;
  params.disturbance_7[0] = -1.9970179118324083;
  params.disturbance_8[0] = -1.878754557910134;
  params.disturbance_9[0] = -0.15380739340877803;
  params.disturbance_10[0] = -1.349917260533923;
  params.disturbance_11[0] = 0.7180072150931407;
  params.disturbance_12[0] = 1.1808183487065538;
  params.disturbance_13[0] = 0.31265343495084075;
  params.disturbance_14[0] = 0.7790599086928229;
  params.disturbance_15[0] = -0.4361679370644853;
  params.disturbance_16[0] = -1.8148151880282066;
  params.disturbance_17[0] = -0.24231386948140266;
  params.disturbance_18[0] = -0.5120787511622411;
  params.disturbance_19[0] = 0.3880129688013203;
  params.disturbance_20[0] = -1.4631273212038676;
  params.disturbance_21[0] = -1.0891484131126563;
  params.disturbance_22[0] = 1.2591296661091191;
  params.disturbance_23[0] = -0.9426978934391474;
  params.disturbance_24[0] = -0.358719180371347;
  params.disturbance_25[0] = 1.7438887059831263;
  params.gain_max[0] = 0.5511049260417091;
  params.lambda[0] = 0.29057991770712777;
  params.omega_0[0] = 1.4040402586629046;
  params.alpha_final_0[0] = 1.1341331008825493;
  params.delta_alpha_final[0] = 1.223187671093194;
  params.alpha_final_1[0] = 0.08406170198714724;
  params.alpha_final_2[0] = 0.8345337895144536;
  params.alpha_max[0] = 0.008532868334318877;
  params.delta_alpha_max[0] = 0.493070937721779;
  params.last_alpha[0] = 1.4121123671680127;
  params.omega_max[0] = 0.12308143184139952;
}
