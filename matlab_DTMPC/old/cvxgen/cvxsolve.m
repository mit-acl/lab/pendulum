% Produced by CVXGEN, 2020-02-27 17:11:42 -0500.
% CVXGEN is Copyright (C) 2006-2017 Jacob Mattingley, jem@cvxgen.com.
% The code in this file is Copyright (C) 2006-2017 Jacob Mattingley.
% CVXGEN, or solvers produced by CVXGEN, cannot be used for commercial
% applications without prior written permission from Jacob Mattingley.

% Filename: cvxsolve.m.
% Description: Solution file, via cvx, for use with sample.m.
function [vars, status] = cvxsolve(params, settings)
Bd = params.Bd;
Cd_max = params.Cd_max;
D = params.D;
I = params.I;
I_over_L = params.I_over_L;
M = params.M;
Q = params.Q;
Q_final = params.Q_final;
R = params.R;
R_delta = params.R_delta;
S = params.S;
abs_xk_0 = params.abs_xk_0;
if isfield(params, 'abs_xk_1')
  abs_xk_1 = params.abs_xk_1;
elseif isfield(params, 'abs_xk')
  abs_xk_1 = params.abs_xk{1};
else
  error 'could not find abs_xk_1'
end
if isfield(params, 'abs_xk_2')
  abs_xk_2 = params.abs_xk_2;
elseif isfield(params, 'abs_xk')
  abs_xk_2 = params.abs_xk{2};
else
  error 'could not find abs_xk_2'
end
if isfield(params, 'abs_xk_3')
  abs_xk_3 = params.abs_xk_3;
elseif isfield(params, 'abs_xk')
  abs_xk_3 = params.abs_xk{3};
else
  error 'could not find abs_xk_3'
end
if isfield(params, 'abs_xk_4')
  abs_xk_4 = params.abs_xk_4;
elseif isfield(params, 'abs_xk')
  abs_xk_4 = params.abs_xk{4};
else
  error 'could not find abs_xk_4'
end
if isfield(params, 'abs_xk_5')
  abs_xk_5 = params.abs_xk_5;
elseif isfield(params, 'abs_xk')
  abs_xk_5 = params.abs_xk{5};
else
  error 'could not find abs_xk_5'
end
if isfield(params, 'abs_xk_6')
  abs_xk_6 = params.abs_xk_6;
elseif isfield(params, 'abs_xk')
  abs_xk_6 = params.abs_xk{6};
else
  error 'could not find abs_xk_6'
end
if isfield(params, 'abs_xk_7')
  abs_xk_7 = params.abs_xk_7;
elseif isfield(params, 'abs_xk')
  abs_xk_7 = params.abs_xk{7};
else
  error 'could not find abs_xk_7'
end
if isfield(params, 'abs_xk_8')
  abs_xk_8 = params.abs_xk_8;
elseif isfield(params, 'abs_xk')
  abs_xk_8 = params.abs_xk{8};
else
  error 'could not find abs_xk_8'
end
if isfield(params, 'abs_xk_9')
  abs_xk_9 = params.abs_xk_9;
elseif isfield(params, 'abs_xk')
  abs_xk_9 = params.abs_xk{9};
else
  error 'could not find abs_xk_9'
end
if isfield(params, 'abs_xk_10')
  abs_xk_10 = params.abs_xk_10;
elseif isfield(params, 'abs_xk')
  abs_xk_10 = params.abs_xk{10};
else
  error 'could not find abs_xk_10'
end
if isfield(params, 'abs_xk_11')
  abs_xk_11 = params.abs_xk_11;
elseif isfield(params, 'abs_xk')
  abs_xk_11 = params.abs_xk{11};
else
  error 'could not find abs_xk_11'
end
if isfield(params, 'abs_xk_12')
  abs_xk_12 = params.abs_xk_12;
elseif isfield(params, 'abs_xk')
  abs_xk_12 = params.abs_xk{12};
else
  error 'could not find abs_xk_12'
end
if isfield(params, 'abs_xk_13')
  abs_xk_13 = params.abs_xk_13;
elseif isfield(params, 'abs_xk')
  abs_xk_13 = params.abs_xk{13};
else
  error 'could not find abs_xk_13'
end
if isfield(params, 'abs_xk_14')
  abs_xk_14 = params.abs_xk_14;
elseif isfield(params, 'abs_xk')
  abs_xk_14 = params.abs_xk{14};
else
  error 'could not find abs_xk_14'
end
if isfield(params, 'abs_xk_15')
  abs_xk_15 = params.abs_xk_15;
elseif isfield(params, 'abs_xk')
  abs_xk_15 = params.abs_xk{15};
else
  error 'could not find abs_xk_15'
end
if isfield(params, 'abs_xk_16')
  abs_xk_16 = params.abs_xk_16;
elseif isfield(params, 'abs_xk')
  abs_xk_16 = params.abs_xk{16};
else
  error 'could not find abs_xk_16'
end
if isfield(params, 'abs_xk_17')
  abs_xk_17 = params.abs_xk_17;
elseif isfield(params, 'abs_xk')
  abs_xk_17 = params.abs_xk{17};
else
  error 'could not find abs_xk_17'
end
if isfield(params, 'abs_xk_18')
  abs_xk_18 = params.abs_xk_18;
elseif isfield(params, 'abs_xk')
  abs_xk_18 = params.abs_xk{18};
else
  error 'could not find abs_xk_18'
end
if isfield(params, 'abs_xk_19')
  abs_xk_19 = params.abs_xk_19;
elseif isfield(params, 'abs_xk')
  abs_xk_19 = params.abs_xk{19};
else
  error 'could not find abs_xk_19'
end
if isfield(params, 'abs_xk_20')
  abs_xk_20 = params.abs_xk_20;
elseif isfield(params, 'abs_xk')
  abs_xk_20 = params.abs_xk{20};
else
  error 'could not find abs_xk_20'
end
if isfield(params, 'abs_xk_21')
  abs_xk_21 = params.abs_xk_21;
elseif isfield(params, 'abs_xk')
  abs_xk_21 = params.abs_xk{21};
else
  error 'could not find abs_xk_21'
end
if isfield(params, 'abs_xk_22')
  abs_xk_22 = params.abs_xk_22;
elseif isfield(params, 'abs_xk')
  abs_xk_22 = params.abs_xk{22};
else
  error 'could not find abs_xk_22'
end
if isfield(params, 'abs_xk_23')
  abs_xk_23 = params.abs_xk_23;
elseif isfield(params, 'abs_xk')
  abs_xk_23 = params.abs_xk{23};
else
  error 'could not find abs_xk_23'
end
if isfield(params, 'abs_xk_24')
  abs_xk_24 = params.abs_xk_24;
elseif isfield(params, 'abs_xk')
  abs_xk_24 = params.abs_xk{24};
else
  error 'could not find abs_xk_24'
end
if isfield(params, 'abs_xk_25')
  abs_xk_25 = params.abs_xk_25;
elseif isfield(params, 'abs_xk')
  abs_xk_25 = params.abs_xk{25};
else
  error 'could not find abs_xk_25'
end
alpha_0 = params.alpha_0;
if isfield(params, 'alpha_1')
  alpha_1 = params.alpha_1;
elseif isfield(params, 'alpha')
  alpha_1 = params.alpha{1};
else
  error 'could not find alpha_1'
end
if isfield(params, 'alpha_2')
  alpha_2 = params.alpha_2;
elseif isfield(params, 'alpha')
  alpha_2 = params.alpha{2};
else
  error 'could not find alpha_2'
end
if isfield(params, 'alpha_3')
  alpha_3 = params.alpha_3;
elseif isfield(params, 'alpha')
  alpha_3 = params.alpha{3};
else
  error 'could not find alpha_3'
end
if isfield(params, 'alpha_4')
  alpha_4 = params.alpha_4;
elseif isfield(params, 'alpha')
  alpha_4 = params.alpha{4};
else
  error 'could not find alpha_4'
end
if isfield(params, 'alpha_5')
  alpha_5 = params.alpha_5;
elseif isfield(params, 'alpha')
  alpha_5 = params.alpha{5};
else
  error 'could not find alpha_5'
end
if isfield(params, 'alpha_6')
  alpha_6 = params.alpha_6;
elseif isfield(params, 'alpha')
  alpha_6 = params.alpha{6};
else
  error 'could not find alpha_6'
end
if isfield(params, 'alpha_7')
  alpha_7 = params.alpha_7;
elseif isfield(params, 'alpha')
  alpha_7 = params.alpha{7};
else
  error 'could not find alpha_7'
end
if isfield(params, 'alpha_8')
  alpha_8 = params.alpha_8;
elseif isfield(params, 'alpha')
  alpha_8 = params.alpha{8};
else
  error 'could not find alpha_8'
end
if isfield(params, 'alpha_9')
  alpha_9 = params.alpha_9;
elseif isfield(params, 'alpha')
  alpha_9 = params.alpha{9};
else
  error 'could not find alpha_9'
end
if isfield(params, 'alpha_10')
  alpha_10 = params.alpha_10;
elseif isfield(params, 'alpha')
  alpha_10 = params.alpha{10};
else
  error 'could not find alpha_10'
end
if isfield(params, 'alpha_11')
  alpha_11 = params.alpha_11;
elseif isfield(params, 'alpha')
  alpha_11 = params.alpha{11};
else
  error 'could not find alpha_11'
end
if isfield(params, 'alpha_12')
  alpha_12 = params.alpha_12;
elseif isfield(params, 'alpha')
  alpha_12 = params.alpha{12};
else
  error 'could not find alpha_12'
end
if isfield(params, 'alpha_13')
  alpha_13 = params.alpha_13;
elseif isfield(params, 'alpha')
  alpha_13 = params.alpha{13};
else
  error 'could not find alpha_13'
end
if isfield(params, 'alpha_14')
  alpha_14 = params.alpha_14;
elseif isfield(params, 'alpha')
  alpha_14 = params.alpha{14};
else
  error 'could not find alpha_14'
end
if isfield(params, 'alpha_15')
  alpha_15 = params.alpha_15;
elseif isfield(params, 'alpha')
  alpha_15 = params.alpha{15};
else
  error 'could not find alpha_15'
end
if isfield(params, 'alpha_16')
  alpha_16 = params.alpha_16;
elseif isfield(params, 'alpha')
  alpha_16 = params.alpha{16};
else
  error 'could not find alpha_16'
end
if isfield(params, 'alpha_17')
  alpha_17 = params.alpha_17;
elseif isfield(params, 'alpha')
  alpha_17 = params.alpha{17};
else
  error 'could not find alpha_17'
end
if isfield(params, 'alpha_18')
  alpha_18 = params.alpha_18;
elseif isfield(params, 'alpha')
  alpha_18 = params.alpha{18};
else
  error 'could not find alpha_18'
end
if isfield(params, 'alpha_19')
  alpha_19 = params.alpha_19;
elseif isfield(params, 'alpha')
  alpha_19 = params.alpha{19};
else
  error 'could not find alpha_19'
end
if isfield(params, 'alpha_20')
  alpha_20 = params.alpha_20;
elseif isfield(params, 'alpha')
  alpha_20 = params.alpha{20};
else
  error 'could not find alpha_20'
end
if isfield(params, 'alpha_21')
  alpha_21 = params.alpha_21;
elseif isfield(params, 'alpha')
  alpha_21 = params.alpha{21};
else
  error 'could not find alpha_21'
end
if isfield(params, 'alpha_22')
  alpha_22 = params.alpha_22;
elseif isfield(params, 'alpha')
  alpha_22 = params.alpha{22};
else
  error 'could not find alpha_22'
end
if isfield(params, 'alpha_23')
  alpha_23 = params.alpha_23;
elseif isfield(params, 'alpha')
  alpha_23 = params.alpha{23};
else
  error 'could not find alpha_23'
end
if isfield(params, 'alpha_24')
  alpha_24 = params.alpha_24;
elseif isfield(params, 'alpha')
  alpha_24 = params.alpha{24};
else
  error 'could not find alpha_24'
end
if isfield(params, 'alpha_25')
  alpha_25 = params.alpha_25;
elseif isfield(params, 'alpha')
  alpha_25 = params.alpha{25};
else
  error 'could not find alpha_25'
end
alpha_final_0 = params.alpha_final_0;
if isfield(params, 'alpha_final_1')
  alpha_final_1 = params.alpha_final_1;
elseif isfield(params, 'alpha_final')
  alpha_final_1 = params.alpha_final{1};
else
  error 'could not find alpha_final_1'
end
if isfield(params, 'alpha_final_2')
  alpha_final_2 = params.alpha_final_2;
elseif isfield(params, 'alpha_final')
  alpha_final_2 = params.alpha_final{2};
else
  error 'could not find alpha_final_2'
end
alpha_max = params.alpha_max;
alpha_min = params.alpha_min;
c1 = params.c1;
c2 = params.c2;
cos_xk_0 = params.cos_xk_0;
if isfield(params, 'cos_xk_1')
  cos_xk_1 = params.cos_xk_1;
elseif isfield(params, 'cos_xk')
  cos_xk_1 = params.cos_xk{1};
else
  error 'could not find cos_xk_1'
end
if isfield(params, 'cos_xk_2')
  cos_xk_2 = params.cos_xk_2;
elseif isfield(params, 'cos_xk')
  cos_xk_2 = params.cos_xk{2};
else
  error 'could not find cos_xk_2'
end
if isfield(params, 'cos_xk_3')
  cos_xk_3 = params.cos_xk_3;
elseif isfield(params, 'cos_xk')
  cos_xk_3 = params.cos_xk{3};
else
  error 'could not find cos_xk_3'
end
if isfield(params, 'cos_xk_4')
  cos_xk_4 = params.cos_xk_4;
elseif isfield(params, 'cos_xk')
  cos_xk_4 = params.cos_xk{4};
else
  error 'could not find cos_xk_4'
end
if isfield(params, 'cos_xk_5')
  cos_xk_5 = params.cos_xk_5;
elseif isfield(params, 'cos_xk')
  cos_xk_5 = params.cos_xk{5};
else
  error 'could not find cos_xk_5'
end
if isfield(params, 'cos_xk_6')
  cos_xk_6 = params.cos_xk_6;
elseif isfield(params, 'cos_xk')
  cos_xk_6 = params.cos_xk{6};
else
  error 'could not find cos_xk_6'
end
if isfield(params, 'cos_xk_7')
  cos_xk_7 = params.cos_xk_7;
elseif isfield(params, 'cos_xk')
  cos_xk_7 = params.cos_xk{7};
else
  error 'could not find cos_xk_7'
end
if isfield(params, 'cos_xk_8')
  cos_xk_8 = params.cos_xk_8;
elseif isfield(params, 'cos_xk')
  cos_xk_8 = params.cos_xk{8};
else
  error 'could not find cos_xk_8'
end
if isfield(params, 'cos_xk_9')
  cos_xk_9 = params.cos_xk_9;
elseif isfield(params, 'cos_xk')
  cos_xk_9 = params.cos_xk{9};
else
  error 'could not find cos_xk_9'
end
if isfield(params, 'cos_xk_10')
  cos_xk_10 = params.cos_xk_10;
elseif isfield(params, 'cos_xk')
  cos_xk_10 = params.cos_xk{10};
else
  error 'could not find cos_xk_10'
end
if isfield(params, 'cos_xk_11')
  cos_xk_11 = params.cos_xk_11;
elseif isfield(params, 'cos_xk')
  cos_xk_11 = params.cos_xk{11};
else
  error 'could not find cos_xk_11'
end
if isfield(params, 'cos_xk_12')
  cos_xk_12 = params.cos_xk_12;
elseif isfield(params, 'cos_xk')
  cos_xk_12 = params.cos_xk{12};
else
  error 'could not find cos_xk_12'
end
if isfield(params, 'cos_xk_13')
  cos_xk_13 = params.cos_xk_13;
elseif isfield(params, 'cos_xk')
  cos_xk_13 = params.cos_xk{13};
else
  error 'could not find cos_xk_13'
end
if isfield(params, 'cos_xk_14')
  cos_xk_14 = params.cos_xk_14;
elseif isfield(params, 'cos_xk')
  cos_xk_14 = params.cos_xk{14};
else
  error 'could not find cos_xk_14'
end
if isfield(params, 'cos_xk_15')
  cos_xk_15 = params.cos_xk_15;
elseif isfield(params, 'cos_xk')
  cos_xk_15 = params.cos_xk{15};
else
  error 'could not find cos_xk_15'
end
if isfield(params, 'cos_xk_16')
  cos_xk_16 = params.cos_xk_16;
elseif isfield(params, 'cos_xk')
  cos_xk_16 = params.cos_xk{16};
else
  error 'could not find cos_xk_16'
end
if isfield(params, 'cos_xk_17')
  cos_xk_17 = params.cos_xk_17;
elseif isfield(params, 'cos_xk')
  cos_xk_17 = params.cos_xk{17};
else
  error 'could not find cos_xk_17'
end
if isfield(params, 'cos_xk_18')
  cos_xk_18 = params.cos_xk_18;
elseif isfield(params, 'cos_xk')
  cos_xk_18 = params.cos_xk{18};
else
  error 'could not find cos_xk_18'
end
if isfield(params, 'cos_xk_19')
  cos_xk_19 = params.cos_xk_19;
elseif isfield(params, 'cos_xk')
  cos_xk_19 = params.cos_xk{19};
else
  error 'could not find cos_xk_19'
end
if isfield(params, 'cos_xk_20')
  cos_xk_20 = params.cos_xk_20;
elseif isfield(params, 'cos_xk')
  cos_xk_20 = params.cos_xk{20};
else
  error 'could not find cos_xk_20'
end
if isfield(params, 'cos_xk_21')
  cos_xk_21 = params.cos_xk_21;
elseif isfield(params, 'cos_xk')
  cos_xk_21 = params.cos_xk{21};
else
  error 'could not find cos_xk_21'
end
if isfield(params, 'cos_xk_22')
  cos_xk_22 = params.cos_xk_22;
elseif isfield(params, 'cos_xk')
  cos_xk_22 = params.cos_xk{22};
else
  error 'could not find cos_xk_22'
end
if isfield(params, 'cos_xk_23')
  cos_xk_23 = params.cos_xk_23;
elseif isfield(params, 'cos_xk')
  cos_xk_23 = params.cos_xk{23};
else
  error 'could not find cos_xk_23'
end
if isfield(params, 'cos_xk_24')
  cos_xk_24 = params.cos_xk_24;
elseif isfield(params, 'cos_xk')
  cos_xk_24 = params.cos_xk{24};
else
  error 'could not find cos_xk_24'
end
if isfield(params, 'cos_xk_25')
  cos_xk_25 = params.cos_xk_25;
elseif isfield(params, 'cos_xk')
  cos_xk_25 = params.cos_xk{25};
else
  error 'could not find cos_xk_25'
end
d_0 = params.d_0;
delta_alpha_final = params.delta_alpha_final;
delta_alpha_max = params.delta_alpha_max;
delta_t = params.delta_t;
disturbance_0 = params.disturbance_0;
if isfield(params, 'disturbance_1')
  disturbance_1 = params.disturbance_1;
elseif isfield(params, 'disturbance')
  disturbance_1 = params.disturbance{1};
else
  error 'could not find disturbance_1'
end
if isfield(params, 'disturbance_2')
  disturbance_2 = params.disturbance_2;
elseif isfield(params, 'disturbance')
  disturbance_2 = params.disturbance{2};
else
  error 'could not find disturbance_2'
end
if isfield(params, 'disturbance_3')
  disturbance_3 = params.disturbance_3;
elseif isfield(params, 'disturbance')
  disturbance_3 = params.disturbance{3};
else
  error 'could not find disturbance_3'
end
if isfield(params, 'disturbance_4')
  disturbance_4 = params.disturbance_4;
elseif isfield(params, 'disturbance')
  disturbance_4 = params.disturbance{4};
else
  error 'could not find disturbance_4'
end
if isfield(params, 'disturbance_5')
  disturbance_5 = params.disturbance_5;
elseif isfield(params, 'disturbance')
  disturbance_5 = params.disturbance{5};
else
  error 'could not find disturbance_5'
end
if isfield(params, 'disturbance_6')
  disturbance_6 = params.disturbance_6;
elseif isfield(params, 'disturbance')
  disturbance_6 = params.disturbance{6};
else
  error 'could not find disturbance_6'
end
if isfield(params, 'disturbance_7')
  disturbance_7 = params.disturbance_7;
elseif isfield(params, 'disturbance')
  disturbance_7 = params.disturbance{7};
else
  error 'could not find disturbance_7'
end
if isfield(params, 'disturbance_8')
  disturbance_8 = params.disturbance_8;
elseif isfield(params, 'disturbance')
  disturbance_8 = params.disturbance{8};
else
  error 'could not find disturbance_8'
end
if isfield(params, 'disturbance_9')
  disturbance_9 = params.disturbance_9;
elseif isfield(params, 'disturbance')
  disturbance_9 = params.disturbance{9};
else
  error 'could not find disturbance_9'
end
if isfield(params, 'disturbance_10')
  disturbance_10 = params.disturbance_10;
elseif isfield(params, 'disturbance')
  disturbance_10 = params.disturbance{10};
else
  error 'could not find disturbance_10'
end
if isfield(params, 'disturbance_11')
  disturbance_11 = params.disturbance_11;
elseif isfield(params, 'disturbance')
  disturbance_11 = params.disturbance{11};
else
  error 'could not find disturbance_11'
end
if isfield(params, 'disturbance_12')
  disturbance_12 = params.disturbance_12;
elseif isfield(params, 'disturbance')
  disturbance_12 = params.disturbance{12};
else
  error 'could not find disturbance_12'
end
if isfield(params, 'disturbance_13')
  disturbance_13 = params.disturbance_13;
elseif isfield(params, 'disturbance')
  disturbance_13 = params.disturbance{13};
else
  error 'could not find disturbance_13'
end
if isfield(params, 'disturbance_14')
  disturbance_14 = params.disturbance_14;
elseif isfield(params, 'disturbance')
  disturbance_14 = params.disturbance{14};
else
  error 'could not find disturbance_14'
end
if isfield(params, 'disturbance_15')
  disturbance_15 = params.disturbance_15;
elseif isfield(params, 'disturbance')
  disturbance_15 = params.disturbance{15};
else
  error 'could not find disturbance_15'
end
if isfield(params, 'disturbance_16')
  disturbance_16 = params.disturbance_16;
elseif isfield(params, 'disturbance')
  disturbance_16 = params.disturbance{16};
else
  error 'could not find disturbance_16'
end
if isfield(params, 'disturbance_17')
  disturbance_17 = params.disturbance_17;
elseif isfield(params, 'disturbance')
  disturbance_17 = params.disturbance{17};
else
  error 'could not find disturbance_17'
end
if isfield(params, 'disturbance_18')
  disturbance_18 = params.disturbance_18;
elseif isfield(params, 'disturbance')
  disturbance_18 = params.disturbance{18};
else
  error 'could not find disturbance_18'
end
if isfield(params, 'disturbance_19')
  disturbance_19 = params.disturbance_19;
elseif isfield(params, 'disturbance')
  disturbance_19 = params.disturbance{19};
else
  error 'could not find disturbance_19'
end
if isfield(params, 'disturbance_20')
  disturbance_20 = params.disturbance_20;
elseif isfield(params, 'disturbance')
  disturbance_20 = params.disturbance{20};
else
  error 'could not find disturbance_20'
end
if isfield(params, 'disturbance_21')
  disturbance_21 = params.disturbance_21;
elseif isfield(params, 'disturbance')
  disturbance_21 = params.disturbance{21};
else
  error 'could not find disturbance_21'
end
if isfield(params, 'disturbance_22')
  disturbance_22 = params.disturbance_22;
elseif isfield(params, 'disturbance')
  disturbance_22 = params.disturbance{22};
else
  error 'could not find disturbance_22'
end
if isfield(params, 'disturbance_23')
  disturbance_23 = params.disturbance_23;
elseif isfield(params, 'disturbance')
  disturbance_23 = params.disturbance{23};
else
  error 'could not find disturbance_23'
end
if isfield(params, 'disturbance_24')
  disturbance_24 = params.disturbance_24;
elseif isfield(params, 'disturbance')
  disturbance_24 = params.disturbance{24};
else
  error 'could not find disturbance_24'
end
if isfield(params, 'disturbance_25')
  disturbance_25 = params.disturbance_25;
elseif isfield(params, 'disturbance')
  disturbance_25 = params.disturbance{25};
else
  error 'could not find disturbance_25'
end
gain_max = params.gain_max;
lambda = params.lambda;
last_alpha = params.last_alpha;
last_u = params.last_u;
new_EF_0 = params.new_EF_0;
nu = params.nu;
old_EF_0 = params.old_EF_0;
if isfield(params, 'old_EF_1')
  old_EF_1 = params.old_EF_1;
elseif isfield(params, 'old_EF')
  old_EF_1 = params.old_EF{1};
else
  error 'could not find old_EF_1'
end
if isfield(params, 'old_EF_2')
  old_EF_2 = params.old_EF_2;
elseif isfield(params, 'old_EF')
  old_EF_2 = params.old_EF{2};
else
  error 'could not find old_EF_2'
end
if isfield(params, 'old_EF_3')
  old_EF_3 = params.old_EF_3;
elseif isfield(params, 'old_EF')
  old_EF_3 = params.old_EF{3};
else
  error 'could not find old_EF_3'
end
if isfield(params, 'old_EF_4')
  old_EF_4 = params.old_EF_4;
elseif isfield(params, 'old_EF')
  old_EF_4 = params.old_EF{4};
else
  error 'could not find old_EF_4'
end
if isfield(params, 'old_EF_5')
  old_EF_5 = params.old_EF_5;
elseif isfield(params, 'old_EF')
  old_EF_5 = params.old_EF{5};
else
  error 'could not find old_EF_5'
end
if isfield(params, 'old_EF_6')
  old_EF_6 = params.old_EF_6;
elseif isfield(params, 'old_EF')
  old_EF_6 = params.old_EF{6};
else
  error 'could not find old_EF_6'
end
if isfield(params, 'old_EF_7')
  old_EF_7 = params.old_EF_7;
elseif isfield(params, 'old_EF')
  old_EF_7 = params.old_EF{7};
else
  error 'could not find old_EF_7'
end
if isfield(params, 'old_EF_8')
  old_EF_8 = params.old_EF_8;
elseif isfield(params, 'old_EF')
  old_EF_8 = params.old_EF{8};
else
  error 'could not find old_EF_8'
end
if isfield(params, 'old_EF_9')
  old_EF_9 = params.old_EF_9;
elseif isfield(params, 'old_EF')
  old_EF_9 = params.old_EF{9};
else
  error 'could not find old_EF_9'
end
if isfield(params, 'old_EF_10')
  old_EF_10 = params.old_EF_10;
elseif isfield(params, 'old_EF')
  old_EF_10 = params.old_EF{10};
else
  error 'could not find old_EF_10'
end
if isfield(params, 'old_EF_11')
  old_EF_11 = params.old_EF_11;
elseif isfield(params, 'old_EF')
  old_EF_11 = params.old_EF{11};
else
  error 'could not find old_EF_11'
end
if isfield(params, 'old_EF_12')
  old_EF_12 = params.old_EF_12;
elseif isfield(params, 'old_EF')
  old_EF_12 = params.old_EF{12};
else
  error 'could not find old_EF_12'
end
if isfield(params, 'old_EF_13')
  old_EF_13 = params.old_EF_13;
elseif isfield(params, 'old_EF')
  old_EF_13 = params.old_EF{13};
else
  error 'could not find old_EF_13'
end
if isfield(params, 'old_EF_14')
  old_EF_14 = params.old_EF_14;
elseif isfield(params, 'old_EF')
  old_EF_14 = params.old_EF{14};
else
  error 'could not find old_EF_14'
end
if isfield(params, 'old_EF_15')
  old_EF_15 = params.old_EF_15;
elseif isfield(params, 'old_EF')
  old_EF_15 = params.old_EF{15};
else
  error 'could not find old_EF_15'
end
if isfield(params, 'old_EF_16')
  old_EF_16 = params.old_EF_16;
elseif isfield(params, 'old_EF')
  old_EF_16 = params.old_EF{16};
else
  error 'could not find old_EF_16'
end
if isfield(params, 'old_EF_17')
  old_EF_17 = params.old_EF_17;
elseif isfield(params, 'old_EF')
  old_EF_17 = params.old_EF{17};
else
  error 'could not find old_EF_17'
end
if isfield(params, 'old_EF_18')
  old_EF_18 = params.old_EF_18;
elseif isfield(params, 'old_EF')
  old_EF_18 = params.old_EF{18};
else
  error 'could not find old_EF_18'
end
if isfield(params, 'old_EF_19')
  old_EF_19 = params.old_EF_19;
elseif isfield(params, 'old_EF')
  old_EF_19 = params.old_EF{19};
else
  error 'could not find old_EF_19'
end
if isfield(params, 'old_EF_20')
  old_EF_20 = params.old_EF_20;
elseif isfield(params, 'old_EF')
  old_EF_20 = params.old_EF{20};
else
  error 'could not find old_EF_20'
end
if isfield(params, 'old_EF_21')
  old_EF_21 = params.old_EF_21;
elseif isfield(params, 'old_EF')
  old_EF_21 = params.old_EF{21};
else
  error 'could not find old_EF_21'
end
if isfield(params, 'old_EF_22')
  old_EF_22 = params.old_EF_22;
elseif isfield(params, 'old_EF')
  old_EF_22 = params.old_EF{22};
else
  error 'could not find old_EF_22'
end
if isfield(params, 'old_EF_23')
  old_EF_23 = params.old_EF_23;
elseif isfield(params, 'old_EF')
  old_EF_23 = params.old_EF{23};
else
  error 'could not find old_EF_23'
end
if isfield(params, 'old_EF_24')
  old_EF_24 = params.old_EF_24;
elseif isfield(params, 'old_EF')
  old_EF_24 = params.old_EF{24};
else
  error 'could not find old_EF_24'
end
if isfield(params, 'old_EF_25')
  old_EF_25 = params.old_EF_25;
elseif isfield(params, 'old_EF')
  old_EF_25 = params.old_EF{25};
else
  error 'could not find old_EF_25'
end
omega_0 = params.omega_0;
omega_max = params.omega_max;
u_max = params.u_max;
uk_0 = params.uk_0;
if isfield(params, 'uk_1')
  uk_1 = params.uk_1;
elseif isfield(params, 'uk')
  uk_1 = params.uk{1};
else
  error 'could not find uk_1'
end
if isfield(params, 'uk_2')
  uk_2 = params.uk_2;
elseif isfield(params, 'uk')
  uk_2 = params.uk{2};
else
  error 'could not find uk_2'
end
if isfield(params, 'uk_3')
  uk_3 = params.uk_3;
elseif isfield(params, 'uk')
  uk_3 = params.uk{3};
else
  error 'could not find uk_3'
end
if isfield(params, 'uk_4')
  uk_4 = params.uk_4;
elseif isfield(params, 'uk')
  uk_4 = params.uk{4};
else
  error 'could not find uk_4'
end
if isfield(params, 'uk_5')
  uk_5 = params.uk_5;
elseif isfield(params, 'uk')
  uk_5 = params.uk{5};
else
  error 'could not find uk_5'
end
if isfield(params, 'uk_6')
  uk_6 = params.uk_6;
elseif isfield(params, 'uk')
  uk_6 = params.uk{6};
else
  error 'could not find uk_6'
end
if isfield(params, 'uk_7')
  uk_7 = params.uk_7;
elseif isfield(params, 'uk')
  uk_7 = params.uk{7};
else
  error 'could not find uk_7'
end
if isfield(params, 'uk_8')
  uk_8 = params.uk_8;
elseif isfield(params, 'uk')
  uk_8 = params.uk{8};
else
  error 'could not find uk_8'
end
if isfield(params, 'uk_9')
  uk_9 = params.uk_9;
elseif isfield(params, 'uk')
  uk_9 = params.uk{9};
else
  error 'could not find uk_9'
end
if isfield(params, 'uk_10')
  uk_10 = params.uk_10;
elseif isfield(params, 'uk')
  uk_10 = params.uk{10};
else
  error 'could not find uk_10'
end
if isfield(params, 'uk_11')
  uk_11 = params.uk_11;
elseif isfield(params, 'uk')
  uk_11 = params.uk{11};
else
  error 'could not find uk_11'
end
if isfield(params, 'uk_12')
  uk_12 = params.uk_12;
elseif isfield(params, 'uk')
  uk_12 = params.uk{12};
else
  error 'could not find uk_12'
end
if isfield(params, 'uk_13')
  uk_13 = params.uk_13;
elseif isfield(params, 'uk')
  uk_13 = params.uk{13};
else
  error 'could not find uk_13'
end
if isfield(params, 'uk_14')
  uk_14 = params.uk_14;
elseif isfield(params, 'uk')
  uk_14 = params.uk{14};
else
  error 'could not find uk_14'
end
if isfield(params, 'uk_15')
  uk_15 = params.uk_15;
elseif isfield(params, 'uk')
  uk_15 = params.uk{15};
else
  error 'could not find uk_15'
end
if isfield(params, 'uk_16')
  uk_16 = params.uk_16;
elseif isfield(params, 'uk')
  uk_16 = params.uk{16};
else
  error 'could not find uk_16'
end
if isfield(params, 'uk_17')
  uk_17 = params.uk_17;
elseif isfield(params, 'uk')
  uk_17 = params.uk{17};
else
  error 'could not find uk_17'
end
if isfield(params, 'uk_18')
  uk_18 = params.uk_18;
elseif isfield(params, 'uk')
  uk_18 = params.uk{18};
else
  error 'could not find uk_18'
end
if isfield(params, 'uk_19')
  uk_19 = params.uk_19;
elseif isfield(params, 'uk')
  uk_19 = params.uk{19};
else
  error 'could not find uk_19'
end
if isfield(params, 'uk_20')
  uk_20 = params.uk_20;
elseif isfield(params, 'uk')
  uk_20 = params.uk{20};
else
  error 'could not find uk_20'
end
if isfield(params, 'uk_21')
  uk_21 = params.uk_21;
elseif isfield(params, 'uk')
  uk_21 = params.uk{21};
else
  error 'could not find uk_21'
end
if isfield(params, 'uk_22')
  uk_22 = params.uk_22;
elseif isfield(params, 'uk')
  uk_22 = params.uk{22};
else
  error 'could not find uk_22'
end
if isfield(params, 'uk_23')
  uk_23 = params.uk_23;
elseif isfield(params, 'uk')
  uk_23 = params.uk{23};
else
  error 'could not find uk_23'
end
if isfield(params, 'uk_24')
  uk_24 = params.uk_24;
elseif isfield(params, 'uk')
  uk_24 = params.uk{24};
else
  error 'could not find uk_24'
end
if isfield(params, 'uk_25')
  uk_25 = params.uk_25;
elseif isfield(params, 'uk')
  uk_25 = params.uk{25};
else
  error 'could not find uk_25'
end
w_max = params.w_max;
x_dot_bound = params.x_dot_bound;
x_final = params.x_final;
xk_0 = params.xk_0;
if isfield(params, 'xk_1')
  xk_1 = params.xk_1;
elseif isfield(params, 'xk')
  xk_1 = params.xk{1};
else
  error 'could not find xk_1'
end
if isfield(params, 'xk_2')
  xk_2 = params.xk_2;
elseif isfield(params, 'xk')
  xk_2 = params.xk{2};
else
  error 'could not find xk_2'
end
if isfield(params, 'xk_3')
  xk_3 = params.xk_3;
elseif isfield(params, 'xk')
  xk_3 = params.xk{3};
else
  error 'could not find xk_3'
end
if isfield(params, 'xk_4')
  xk_4 = params.xk_4;
elseif isfield(params, 'xk')
  xk_4 = params.xk{4};
else
  error 'could not find xk_4'
end
if isfield(params, 'xk_5')
  xk_5 = params.xk_5;
elseif isfield(params, 'xk')
  xk_5 = params.xk{5};
else
  error 'could not find xk_5'
end
if isfield(params, 'xk_6')
  xk_6 = params.xk_6;
elseif isfield(params, 'xk')
  xk_6 = params.xk{6};
else
  error 'could not find xk_6'
end
if isfield(params, 'xk_7')
  xk_7 = params.xk_7;
elseif isfield(params, 'xk')
  xk_7 = params.xk{7};
else
  error 'could not find xk_7'
end
if isfield(params, 'xk_8')
  xk_8 = params.xk_8;
elseif isfield(params, 'xk')
  xk_8 = params.xk{8};
else
  error 'could not find xk_8'
end
if isfield(params, 'xk_9')
  xk_9 = params.xk_9;
elseif isfield(params, 'xk')
  xk_9 = params.xk{9};
else
  error 'could not find xk_9'
end
if isfield(params, 'xk_10')
  xk_10 = params.xk_10;
elseif isfield(params, 'xk')
  xk_10 = params.xk{10};
else
  error 'could not find xk_10'
end
if isfield(params, 'xk_11')
  xk_11 = params.xk_11;
elseif isfield(params, 'xk')
  xk_11 = params.xk{11};
else
  error 'could not find xk_11'
end
if isfield(params, 'xk_12')
  xk_12 = params.xk_12;
elseif isfield(params, 'xk')
  xk_12 = params.xk{12};
else
  error 'could not find xk_12'
end
if isfield(params, 'xk_13')
  xk_13 = params.xk_13;
elseif isfield(params, 'xk')
  xk_13 = params.xk{13};
else
  error 'could not find xk_13'
end
if isfield(params, 'xk_14')
  xk_14 = params.xk_14;
elseif isfield(params, 'xk')
  xk_14 = params.xk{14};
else
  error 'could not find xk_14'
end
if isfield(params, 'xk_15')
  xk_15 = params.xk_15;
elseif isfield(params, 'xk')
  xk_15 = params.xk{15};
else
  error 'could not find xk_15'
end
if isfield(params, 'xk_16')
  xk_16 = params.xk_16;
elseif isfield(params, 'xk')
  xk_16 = params.xk{16};
else
  error 'could not find xk_16'
end
if isfield(params, 'xk_17')
  xk_17 = params.xk_17;
elseif isfield(params, 'xk')
  xk_17 = params.xk{17};
else
  error 'could not find xk_17'
end
if isfield(params, 'xk_18')
  xk_18 = params.xk_18;
elseif isfield(params, 'xk')
  xk_18 = params.xk{18};
else
  error 'could not find xk_18'
end
if isfield(params, 'xk_19')
  xk_19 = params.xk_19;
elseif isfield(params, 'xk')
  xk_19 = params.xk{19};
else
  error 'could not find xk_19'
end
if isfield(params, 'xk_20')
  xk_20 = params.xk_20;
elseif isfield(params, 'xk')
  xk_20 = params.xk{20};
else
  error 'could not find xk_20'
end
if isfield(params, 'xk_21')
  xk_21 = params.xk_21;
elseif isfield(params, 'xk')
  xk_21 = params.xk{21};
else
  error 'could not find xk_21'
end
if isfield(params, 'xk_22')
  xk_22 = params.xk_22;
elseif isfield(params, 'xk')
  xk_22 = params.xk{22};
else
  error 'could not find xk_22'
end
if isfield(params, 'xk_23')
  xk_23 = params.xk_23;
elseif isfield(params, 'xk')
  xk_23 = params.xk{23};
else
  error 'could not find xk_23'
end
if isfield(params, 'xk_24')
  xk_24 = params.xk_24;
elseif isfield(params, 'xk')
  xk_24 = params.xk{24};
else
  error 'could not find xk_24'
end
if isfield(params, 'xk_25')
  xk_25 = params.xk_25;
elseif isfield(params, 'xk')
  xk_25 = params.xk{25};
else
  error 'could not find xk_25'
end
if isfield(params, 'xk_26')
  xk_26 = params.xk_26;
elseif isfield(params, 'xk')
  xk_26 = params.xk{26};
else
  error 'could not find xk_26'
end
cvx_begin
  % Caution: automatically generated by cvxgen. May be incorrect.
  variable w_0;
  variable delta_alpha_0;
  variable d_1(2, 1);
  variable w_1;
  variable new_EF_1;
  variable delta_alpha_1;
  variable d_2(2, 1);
  variable w_2;
  variable new_EF_2;
  variable delta_alpha_2;
  variable d_3(2, 1);
  variable w_3;
  variable new_EF_3;
  variable delta_alpha_3;
  variable d_4(2, 1);
  variable w_4;
  variable new_EF_4;
  variable delta_alpha_4;
  variable d_5(2, 1);
  variable w_5;
  variable new_EF_5;
  variable delta_alpha_5;
  variable d_6(2, 1);
  variable w_6;
  variable new_EF_6;
  variable delta_alpha_6;
  variable d_7(2, 1);
  variable w_7;
  variable new_EF_7;
  variable delta_alpha_7;
  variable d_8(2, 1);
  variable w_8;
  variable new_EF_8;
  variable delta_alpha_8;
  variable d_9(2, 1);
  variable w_9;
  variable new_EF_9;
  variable delta_alpha_9;
  variable d_10(2, 1);
  variable w_10;
  variable new_EF_10;
  variable delta_alpha_10;
  variable d_11(2, 1);
  variable w_11;
  variable new_EF_11;
  variable delta_alpha_11;
  variable d_12(2, 1);
  variable w_12;
  variable new_EF_12;
  variable delta_alpha_12;
  variable d_13(2, 1);
  variable w_13;
  variable new_EF_13;
  variable delta_alpha_13;
  variable d_14(2, 1);
  variable w_14;
  variable new_EF_14;
  variable delta_alpha_14;
  variable d_15(2, 1);
  variable w_15;
  variable new_EF_15;
  variable delta_alpha_15;
  variable d_16(2, 1);
  variable w_16;
  variable new_EF_16;
  variable delta_alpha_16;
  variable d_17(2, 1);
  variable w_17;
  variable new_EF_17;
  variable delta_alpha_17;
  variable d_18(2, 1);
  variable w_18;
  variable new_EF_18;
  variable delta_alpha_18;
  variable d_19(2, 1);
  variable w_19;
  variable new_EF_19;
  variable delta_alpha_19;
  variable d_20(2, 1);
  variable w_20;
  variable new_EF_20;
  variable delta_alpha_20;
  variable d_21(2, 1);
  variable w_21;
  variable new_EF_21;
  variable delta_alpha_21;
  variable d_22(2, 1);
  variable w_22;
  variable new_EF_22;
  variable delta_alpha_22;
  variable d_23(2, 1);
  variable w_23;
  variable new_EF_23;
  variable delta_alpha_23;
  variable d_24(2, 1);
  variable w_24;
  variable new_EF_24;
  variable delta_alpha_24;
  variable d_25(2, 1);
  variable w_25;
  variable new_EF_25;
  variable delta_alpha_25;
  variable d_26(2, 1);
  variable new_EF_26;
  variable omega_1;
  variable omega_2;
  variable omega_3;
  variable omega_4;
  variable omega_5;
  variable omega_6;
  variable omega_7;
  variable omega_8;
  variable omega_9;
  variable omega_10;
  variable omega_11;
  variable omega_12;
  variable omega_13;
  variable omega_14;
  variable omega_15;
  variable omega_16;
  variable omega_17;
  variable omega_18;
  variable omega_19;
  variable omega_20;
  variable omega_21;
  variable omega_22;
  variable omega_23;
  variable omega_24;
  variable omega_25;
  variable omega_26;

  minimize(quad_form(x_final - (xk_0 + d_0), Q) + quad_form(uk_0 + w_0, R) + quad_form(I_over_L*(alpha_0*new_EF_0 + old_EF_0*delta_alpha_0), R_delta) + quad_form(alpha_0 + delta_alpha_0 - alpha_min, M) + quad_form(x_final - (xk_1 + d_1), Q) + quad_form(uk_1 + w_1, R) + quad_form(I_over_L*(alpha_1*new_EF_1 + old_EF_1*delta_alpha_1), R_delta) + quad_form(alpha_1 + delta_alpha_1 - alpha_min, M) + quad_form(x_final - (xk_2 + d_2), Q) + quad_form(uk_2 + w_2, R) + quad_form(I_over_L*(alpha_2*new_EF_2 + old_EF_2*delta_alpha_2), R_delta) + quad_form(alpha_2 + delta_alpha_2 - alpha_min, M) + quad_form(x_final - (xk_3 + d_3), Q) + quad_form(uk_3 + w_3, R) + quad_form(I_over_L*(alpha_3*new_EF_3 + old_EF_3*delta_alpha_3), R_delta) + quad_form(alpha_3 + delta_alpha_3 - alpha_min, M) + quad_form(x_final - (xk_4 + d_4), Q) + quad_form(uk_4 + w_4, R) + quad_form(I_over_L*(alpha_4*new_EF_4 + old_EF_4*delta_alpha_4), R_delta) + quad_form(alpha_4 + delta_alpha_4 - alpha_min, M) + quad_form(x_final - (xk_5 + d_5), Q) + quad_form(uk_5 + w_5, R) + quad_form(I_over_L*(alpha_5*new_EF_5 + old_EF_5*delta_alpha_5), R_delta) + quad_form(alpha_5 + delta_alpha_5 - alpha_min, M) + quad_form(x_final - (xk_6 + d_6), Q) + quad_form(uk_6 + w_6, R) + quad_form(I_over_L*(alpha_6*new_EF_6 + old_EF_6*delta_alpha_6), R_delta) + quad_form(alpha_6 + delta_alpha_6 - alpha_min, M) + quad_form(x_final - (xk_7 + d_7), Q) + quad_form(uk_7 + w_7, R) + quad_form(I_over_L*(alpha_7*new_EF_7 + old_EF_7*delta_alpha_7), R_delta) + quad_form(alpha_7 + delta_alpha_7 - alpha_min, M) + quad_form(x_final - (xk_8 + d_8), Q) + quad_form(uk_8 + w_8, R) + quad_form(I_over_L*(alpha_8*new_EF_8 + old_EF_8*delta_alpha_8), R_delta) + quad_form(alpha_8 + delta_alpha_8 - alpha_min, M) + quad_form(x_final - (xk_9 + d_9), Q) + quad_form(uk_9 + w_9, R) + quad_form(I_over_L*(alpha_9*new_EF_9 + old_EF_9*delta_alpha_9), R_delta) + quad_form(alpha_9 + delta_alpha_9 - alpha_min, M) + quad_form(x_final - (xk_10 + d_10), Q) + quad_form(uk_10 + w_10, R) + quad_form(I_over_L*(alpha_10*new_EF_10 + old_EF_10*delta_alpha_10), R_delta) + quad_form(alpha_10 + delta_alpha_10 - alpha_min, M) + quad_form(x_final - (xk_11 + d_11), Q) + quad_form(uk_11 + w_11, R) + quad_form(I_over_L*(alpha_11*new_EF_11 + old_EF_11*delta_alpha_11), R_delta) + quad_form(alpha_11 + delta_alpha_11 - alpha_min, M) + quad_form(x_final - (xk_12 + d_12), Q) + quad_form(uk_12 + w_12, R) + quad_form(I_over_L*(alpha_12*new_EF_12 + old_EF_12*delta_alpha_12), R_delta) + quad_form(alpha_12 + delta_alpha_12 - alpha_min, M) + quad_form(x_final - (xk_13 + d_13), Q) + quad_form(uk_13 + w_13, R) + quad_form(I_over_L*(alpha_13*new_EF_13 + old_EF_13*delta_alpha_13), R_delta) + quad_form(alpha_13 + delta_alpha_13 - alpha_min, M) + quad_form(x_final - (xk_14 + d_14), Q) + quad_form(uk_14 + w_14, R) + quad_form(I_over_L*(alpha_14*new_EF_14 + old_EF_14*delta_alpha_14), R_delta) + quad_form(alpha_14 + delta_alpha_14 - alpha_min, M) + quad_form(x_final - (xk_15 + d_15), Q) + quad_form(uk_15 + w_15, R) + quad_form(I_over_L*(alpha_15*new_EF_15 + old_EF_15*delta_alpha_15), R_delta) + quad_form(alpha_15 + delta_alpha_15 - alpha_min, M) + quad_form(x_final - (xk_16 + d_16), Q) + quad_form(uk_16 + w_16, R) + quad_form(I_over_L*(alpha_16*new_EF_16 + old_EF_16*delta_alpha_16), R_delta) + quad_form(alpha_16 + delta_alpha_16 - alpha_min, M) + quad_form(x_final - (xk_17 + d_17), Q) + quad_form(uk_17 + w_17, R) + quad_form(I_over_L*(alpha_17*new_EF_17 + old_EF_17*delta_alpha_17), R_delta) + quad_form(alpha_17 + delta_alpha_17 - alpha_min, M) + quad_form(x_final - (xk_18 + d_18), Q) + quad_form(uk_18 + w_18, R) + quad_form(I_over_L*(alpha_18*new_EF_18 + old_EF_18*delta_alpha_18), R_delta) + quad_form(alpha_18 + delta_alpha_18 - alpha_min, M) + quad_form(x_final - (xk_19 + d_19), Q) + quad_form(uk_19 + w_19, R) + quad_form(I_over_L*(alpha_19*new_EF_19 + old_EF_19*delta_alpha_19), R_delta) + quad_form(alpha_19 + delta_alpha_19 - alpha_min, M) + quad_form(x_final - (xk_20 + d_20), Q) + quad_form(uk_20 + w_20, R) + quad_form(I_over_L*(alpha_20*new_EF_20 + old_EF_20*delta_alpha_20), R_delta) + quad_form(alpha_20 + delta_alpha_20 - alpha_min, M) + quad_form(x_final - (xk_21 + d_21), Q) + quad_form(uk_21 + w_21, R) + quad_form(I_over_L*(alpha_21*new_EF_21 + old_EF_21*delta_alpha_21), R_delta) + quad_form(alpha_21 + delta_alpha_21 - alpha_min, M) + quad_form(x_final - (xk_22 + d_22), Q) + quad_form(uk_22 + w_22, R) + quad_form(I_over_L*(alpha_22*new_EF_22 + old_EF_22*delta_alpha_22), R_delta) + quad_form(alpha_22 + delta_alpha_22 - alpha_min, M) + quad_form(x_final - (xk_23 + d_23), Q) + quad_form(uk_23 + w_23, R) + quad_form(I_over_L*(alpha_23*new_EF_23 + old_EF_23*delta_alpha_23), R_delta) + quad_form(alpha_23 + delta_alpha_23 - alpha_min, M) + quad_form(x_final - (xk_24 + d_24), Q) + quad_form(uk_24 + w_24, R) + quad_form(I_over_L*(alpha_24*new_EF_24 + old_EF_24*delta_alpha_24), R_delta) + quad_form(alpha_24 + delta_alpha_24 - alpha_min, M) + quad_form(x_final - (xk_25 + d_25), Q) + quad_form(uk_25 + w_25, R) + quad_form(I_over_L*(alpha_25*new_EF_25 + old_EF_25*delta_alpha_25), R_delta) + quad_form(alpha_25 + delta_alpha_25 - alpha_min, M) + quad_form(x_final - (xk_26 + d_26), Q_final));
  subject to
    d_1(1) == delta_t*d_0(2) + d_0(1);
    d_2(1) == delta_t*d_1(2) + d_1(1);
    d_3(1) == delta_t*d_2(2) + d_2(1);
    d_4(1) == delta_t*d_3(2) + d_3(1);
    d_5(1) == delta_t*d_4(2) + d_4(1);
    d_6(1) == delta_t*d_5(2) + d_5(1);
    d_7(1) == delta_t*d_6(2) + d_6(1);
    d_8(1) == delta_t*d_7(2) + d_7(1);
    d_9(1) == delta_t*d_8(2) + d_8(1);
    d_10(1) == delta_t*d_9(2) + d_9(1);
    d_11(1) == delta_t*d_10(2) + d_10(1);
    d_12(1) == delta_t*d_11(2) + d_11(1);
    d_13(1) == delta_t*d_12(2) + d_12(1);
    d_14(1) == delta_t*d_13(2) + d_13(1);
    d_15(1) == delta_t*d_14(2) + d_14(1);
    d_16(1) == delta_t*d_15(2) + d_15(1);
    d_17(1) == delta_t*d_16(2) + d_16(1);
    d_18(1) == delta_t*d_17(2) + d_17(1);
    d_19(1) == delta_t*d_18(2) + d_18(1);
    d_20(1) == delta_t*d_19(2) + d_19(1);
    d_21(1) == delta_t*d_20(2) + d_20(1);
    d_22(1) == delta_t*d_21(2) + d_21(1);
    d_23(1) == delta_t*d_22(2) + d_22(1);
    d_24(1) == delta_t*d_23(2) + d_23(1);
    d_25(1) == delta_t*d_24(2) + d_24(1);
    d_26(1) == delta_t*d_25(2) + d_25(1);
    d_1(2) ==  - c1*cos_xk_0*d_0(1) + (1 - abs_xk_0*c2)*d_0(2) + Bd*w_0;
    d_2(2) ==  - c1*cos_xk_1*d_1(1) + (1 - abs_xk_1*c2)*d_1(2) + Bd*w_1;
    d_3(2) ==  - c1*cos_xk_2*d_2(1) + (1 - abs_xk_2*c2)*d_2(2) + Bd*w_2;
    d_4(2) ==  - c1*cos_xk_3*d_3(1) + (1 - abs_xk_3*c2)*d_3(2) + Bd*w_3;
    d_5(2) ==  - c1*cos_xk_4*d_4(1) + (1 - abs_xk_4*c2)*d_4(2) + Bd*w_4;
    d_6(2) ==  - c1*cos_xk_5*d_5(1) + (1 - abs_xk_5*c2)*d_5(2) + Bd*w_5;
    d_7(2) ==  - c1*cos_xk_6*d_6(1) + (1 - abs_xk_6*c2)*d_6(2) + Bd*w_6;
    d_8(2) ==  - c1*cos_xk_7*d_7(1) + (1 - abs_xk_7*c2)*d_7(2) + Bd*w_7;
    d_9(2) ==  - c1*cos_xk_8*d_8(1) + (1 - abs_xk_8*c2)*d_8(2) + Bd*w_8;
    d_10(2) ==  - c1*cos_xk_9*d_9(1) + (1 - abs_xk_9*c2)*d_9(2) + Bd*w_9;
    d_11(2) ==  - c1*cos_xk_10*d_10(1) + (1 - abs_xk_10*c2)*d_10(2) + Bd*w_10;
    d_12(2) ==  - c1*cos_xk_11*d_11(1) + (1 - abs_xk_11*c2)*d_11(2) + Bd*w_11;
    d_13(2) ==  - c1*cos_xk_12*d_12(1) + (1 - abs_xk_12*c2)*d_12(2) + Bd*w_12;
    d_14(2) ==  - c1*cos_xk_13*d_13(1) + (1 - abs_xk_13*c2)*d_13(2) + Bd*w_13;
    d_15(2) ==  - c1*cos_xk_14*d_14(1) + (1 - abs_xk_14*c2)*d_14(2) + Bd*w_14;
    d_16(2) ==  - c1*cos_xk_15*d_15(1) + (1 - abs_xk_15*c2)*d_15(2) + Bd*w_15;
    d_17(2) ==  - c1*cos_xk_16*d_16(1) + (1 - abs_xk_16*c2)*d_16(2) + Bd*w_16;
    d_18(2) ==  - c1*cos_xk_17*d_17(1) + (1 - abs_xk_17*c2)*d_17(2) + Bd*w_17;
    d_19(2) ==  - c1*cos_xk_18*d_18(1) + (1 - abs_xk_18*c2)*d_18(2) + Bd*w_18;
    d_20(2) ==  - c1*cos_xk_19*d_19(1) + (1 - abs_xk_19*c2)*d_19(2) + Bd*w_19;
    d_21(2) ==  - c1*cos_xk_20*d_20(1) + (1 - abs_xk_20*c2)*d_20(2) + Bd*w_20;
    d_22(2) ==  - c1*cos_xk_21*d_21(1) + (1 - abs_xk_21*c2)*d_21(2) + Bd*w_21;
    d_23(2) ==  - c1*cos_xk_22*d_22(1) + (1 - abs_xk_22*c2)*d_22(2) + Bd*w_22;
    d_24(2) ==  - c1*cos_xk_23*d_23(1) + (1 - abs_xk_23*c2)*d_23(2) + Bd*w_23;
    d_25(2) ==  - c1*cos_xk_24*d_24(1) + (1 - abs_xk_24*c2)*d_24(2) + Bd*w_24;
    d_26(2) ==  - c1*cos_xk_25*d_25(1) + (1 - abs_xk_25*c2)*d_25(2) + Bd*w_25;
    abs(w_0) <= w_max;
    abs(w_1) <= w_max;
    abs(w_2) <= w_max;
    abs(w_3) <= w_max;
    abs(w_4) <= w_max;
    abs(w_5) <= w_max;
    abs(w_6) <= w_max;
    abs(w_7) <= w_max;
    abs(w_8) <= w_max;
    abs(w_9) <= w_max;
    abs(w_10) <= w_max;
    abs(w_11) <= w_max;
    abs(w_12) <= w_max;
    abs(w_13) <= w_max;
    abs(w_14) <= w_max;
    abs(w_15) <= w_max;
    abs(w_16) <= w_max;
    abs(w_17) <= w_max;
    abs(w_18) <= w_max;
    abs(w_19) <= w_max;
    abs(w_20) <= w_max;
    abs(w_21) <= w_max;
    abs(w_22) <= w_max;
    abs(w_23) <= w_max;
    abs(w_24) <= w_max;
    abs(w_25) <= w_max;
    abs(uk_0 + w_0) <= u_max;
    abs(uk_1 + w_1) <= u_max;
    abs(uk_2 + w_2) <= u_max;
    abs(uk_3 + w_3) <= u_max;
    abs(uk_4 + w_4) <= u_max;
    abs(uk_5 + w_5) <= u_max;
    abs(uk_6 + w_6) <= u_max;
    abs(uk_7 + w_7) <= u_max;
    abs(uk_8 + w_8) <= u_max;
    abs(uk_9 + w_9) <= u_max;
    abs(uk_10 + w_10) <= u_max;
    abs(uk_11 + w_11) <= u_max;
    abs(uk_12 + w_12) <= u_max;
    abs(uk_13 + w_13) <= u_max;
    abs(uk_14 + w_14) <= u_max;
    abs(uk_15 + w_15) <= u_max;
    abs(uk_16 + w_16) <= u_max;
    abs(uk_17 + w_17) <= u_max;
    abs(uk_18 + w_18) <= u_max;
    abs(uk_19 + w_19) <= u_max;
    abs(uk_20 + w_20) <= u_max;
    abs(uk_21 + w_21) <= u_max;
    abs(uk_22 + w_22) <= u_max;
    abs(uk_23 + w_23) <= u_max;
    abs(uk_24 + w_24) <= u_max;
    abs(uk_25 + w_25) <= u_max;
    abs(xk_1(2) + d_1(2)) <= x_dot_bound;
    abs(xk_2(2) + d_2(2)) <= x_dot_bound;
    abs(xk_3(2) + d_3(2)) <= x_dot_bound;
    abs(xk_4(2) + d_4(2)) <= x_dot_bound;
    abs(xk_5(2) + d_5(2)) <= x_dot_bound;
    abs(xk_6(2) + d_6(2)) <= x_dot_bound;
    abs(xk_7(2) + d_7(2)) <= x_dot_bound;
    abs(xk_8(2) + d_8(2)) <= x_dot_bound;
    abs(xk_9(2) + d_9(2)) <= x_dot_bound;
    abs(xk_10(2) + d_10(2)) <= x_dot_bound;
    abs(xk_11(2) + d_11(2)) <= x_dot_bound;
    abs(xk_12(2) + d_12(2)) <= x_dot_bound;
    abs(xk_13(2) + d_13(2)) <= x_dot_bound;
    abs(xk_14(2) + d_14(2)) <= x_dot_bound;
    abs(xk_15(2) + d_15(2)) <= x_dot_bound;
    abs(xk_16(2) + d_16(2)) <= x_dot_bound;
    abs(xk_17(2) + d_17(2)) <= x_dot_bound;
    abs(xk_18(2) + d_18(2)) <= x_dot_bound;
    abs(xk_19(2) + d_19(2)) <= x_dot_bound;
    abs(xk_20(2) + d_20(2)) <= x_dot_bound;
    abs(xk_21(2) + d_21(2)) <= x_dot_bound;
    abs(xk_22(2) + d_22(2)) <= x_dot_bound;
    abs(xk_23(2) + d_23(2)) <= x_dot_bound;
    abs(xk_24(2) + d_24(2)) <= x_dot_bound;
    abs(xk_25(2) + d_25(2)) <= x_dot_bound;
    abs(xk_26(2) + d_26(2)) <= x_dot_bound;
    norm(uk_1 + w_1 - uk_0 - w_0, inf) <= S;
    norm(uk_2 + w_2 - uk_1 - w_1, inf) <= S;
    norm(uk_3 + w_3 - uk_2 - w_2, inf) <= S;
    norm(uk_4 + w_4 - uk_3 - w_3, inf) <= S;
    norm(uk_5 + w_5 - uk_4 - w_4, inf) <= S;
    norm(uk_6 + w_6 - uk_5 - w_5, inf) <= S;
    norm(uk_7 + w_7 - uk_6 - w_6, inf) <= S;
    norm(uk_8 + w_8 - uk_7 - w_7, inf) <= S;
    norm(uk_9 + w_9 - uk_8 - w_8, inf) <= S;
    norm(uk_10 + w_10 - uk_9 - w_9, inf) <= S;
    norm(uk_11 + w_11 - uk_10 - w_10, inf) <= S;
    norm(uk_12 + w_12 - uk_11 - w_11, inf) <= S;
    norm(uk_13 + w_13 - uk_12 - w_12, inf) <= S;
    norm(uk_14 + w_14 - uk_13 - w_13, inf) <= S;
    norm(uk_15 + w_15 - uk_14 - w_14, inf) <= S;
    norm(uk_16 + w_16 - uk_15 - w_15, inf) <= S;
    norm(uk_17 + w_17 - uk_16 - w_16, inf) <= S;
    norm(uk_18 + w_18 - uk_17 - w_17, inf) <= S;
    norm(uk_19 + w_19 - uk_18 - w_18, inf) <= S;
    norm(uk_20 + w_20 - uk_19 - w_19, inf) <= S;
    norm(uk_21 + w_21 - uk_20 - w_20, inf) <= S;
    norm(uk_22 + w_22 - uk_21 - w_21, inf) <= S;
    norm(uk_23 + w_23 - uk_22 - w_22, inf) <= S;
    norm(uk_24 + w_24 - uk_23 - w_23, inf) <= S;
    norm(uk_25 + w_25 - uk_24 - w_24, inf) <= S;
    norm(uk_0 + w_0 - last_u, inf) <= S;
    new_EF_1 == new_EF_0*(1 - alpha_0*delta_t) - delta_t*old_EF_0*delta_alpha_0 + delta_t*(nu + D + disturbance_0 + Cd_max*abs_xk_0*(abs_xk_0/I));
    new_EF_2 == (1 - alpha_1*delta_t)*new_EF_1 - delta_t*old_EF_1*delta_alpha_1 + delta_t*(nu + D + disturbance_1 + Cd_max*abs_xk_1*(abs_xk_1/I));
    new_EF_3 == (1 - alpha_2*delta_t)*new_EF_2 - delta_t*old_EF_2*delta_alpha_2 + delta_t*(nu + D + disturbance_2 + Cd_max*abs_xk_2*(abs_xk_2/I));
    new_EF_4 == (1 - alpha_3*delta_t)*new_EF_3 - delta_t*old_EF_3*delta_alpha_3 + delta_t*(nu + D + disturbance_3 + Cd_max*abs_xk_3*(abs_xk_3/I));
    new_EF_5 == (1 - alpha_4*delta_t)*new_EF_4 - delta_t*old_EF_4*delta_alpha_4 + delta_t*(nu + D + disturbance_4 + Cd_max*abs_xk_4*(abs_xk_4/I));
    new_EF_6 == (1 - alpha_5*delta_t)*new_EF_5 - delta_t*old_EF_5*delta_alpha_5 + delta_t*(nu + D + disturbance_5 + Cd_max*abs_xk_5*(abs_xk_5/I));
    new_EF_7 == (1 - alpha_6*delta_t)*new_EF_6 - delta_t*old_EF_6*delta_alpha_6 + delta_t*(nu + D + disturbance_6 + Cd_max*abs_xk_6*(abs_xk_6/I));
    new_EF_8 == (1 - alpha_7*delta_t)*new_EF_7 - delta_t*old_EF_7*delta_alpha_7 + delta_t*(nu + D + disturbance_7 + Cd_max*abs_xk_7*(abs_xk_7/I));
    new_EF_9 == (1 - alpha_8*delta_t)*new_EF_8 - delta_t*old_EF_8*delta_alpha_8 + delta_t*(nu + D + disturbance_8 + Cd_max*abs_xk_8*(abs_xk_8/I));
    new_EF_10 == (1 - alpha_9*delta_t)*new_EF_9 - delta_t*old_EF_9*delta_alpha_9 + delta_t*(nu + D + disturbance_9 + Cd_max*abs_xk_9*(abs_xk_9/I));
    new_EF_11 == (1 - alpha_10*delta_t)*new_EF_10 - delta_t*old_EF_10*delta_alpha_10 + delta_t*(nu + D + disturbance_10 + Cd_max*abs_xk_10*(abs_xk_10/I));
    new_EF_12 == (1 - alpha_11*delta_t)*new_EF_11 - delta_t*old_EF_11*delta_alpha_11 + delta_t*(nu + D + disturbance_11 + Cd_max*abs_xk_11*(abs_xk_11/I));
    new_EF_13 == (1 - alpha_12*delta_t)*new_EF_12 - delta_t*old_EF_12*delta_alpha_12 + delta_t*(nu + D + disturbance_12 + Cd_max*abs_xk_12*(abs_xk_12/I));
    new_EF_14 == (1 - alpha_13*delta_t)*new_EF_13 - delta_t*old_EF_13*delta_alpha_13 + delta_t*(nu + D + disturbance_13 + Cd_max*abs_xk_13*(abs_xk_13/I));
    new_EF_15 == (1 - alpha_14*delta_t)*new_EF_14 - delta_t*old_EF_14*delta_alpha_14 + delta_t*(nu + D + disturbance_14 + Cd_max*abs_xk_14*(abs_xk_14/I));
    new_EF_16 == (1 - alpha_15*delta_t)*new_EF_15 - delta_t*old_EF_15*delta_alpha_15 + delta_t*(nu + D + disturbance_15 + Cd_max*abs_xk_15*(abs_xk_15/I));
    new_EF_17 == (1 - alpha_16*delta_t)*new_EF_16 - delta_t*old_EF_16*delta_alpha_16 + delta_t*(nu + D + disturbance_16 + Cd_max*abs_xk_16*(abs_xk_16/I));
    new_EF_18 == (1 - alpha_17*delta_t)*new_EF_17 - delta_t*old_EF_17*delta_alpha_17 + delta_t*(nu + D + disturbance_17 + Cd_max*abs_xk_17*(abs_xk_17/I));
    new_EF_19 == (1 - alpha_18*delta_t)*new_EF_18 - delta_t*old_EF_18*delta_alpha_18 + delta_t*(nu + D + disturbance_18 + Cd_max*abs_xk_18*(abs_xk_18/I));
    new_EF_20 == (1 - alpha_19*delta_t)*new_EF_19 - delta_t*old_EF_19*delta_alpha_19 + delta_t*(nu + D + disturbance_19 + Cd_max*abs_xk_19*(abs_xk_19/I));
    new_EF_21 == (1 - alpha_20*delta_t)*new_EF_20 - delta_t*old_EF_20*delta_alpha_20 + delta_t*(nu + D + disturbance_20 + Cd_max*abs_xk_20*(abs_xk_20/I));
    new_EF_22 == (1 - alpha_21*delta_t)*new_EF_21 - delta_t*old_EF_21*delta_alpha_21 + delta_t*(nu + D + disturbance_21 + Cd_max*abs_xk_21*(abs_xk_21/I));
    new_EF_23 == (1 - alpha_22*delta_t)*new_EF_22 - delta_t*old_EF_22*delta_alpha_22 + delta_t*(nu + D + disturbance_22 + Cd_max*abs_xk_22*(abs_xk_22/I));
    new_EF_24 == (1 - alpha_23*delta_t)*new_EF_23 - delta_t*old_EF_23*delta_alpha_23 + delta_t*(nu + D + disturbance_23 + Cd_max*abs_xk_23*(abs_xk_23/I));
    new_EF_25 == (1 - alpha_24*delta_t)*new_EF_24 - delta_t*old_EF_24*delta_alpha_24 + delta_t*(nu + D + disturbance_24 + Cd_max*abs_xk_24*(abs_xk_24/I));
    new_EF_26 == (1 - alpha_25*delta_t)*new_EF_25 - delta_t*old_EF_25*delta_alpha_25 + delta_t*(nu + D + disturbance_25 + Cd_max*abs_xk_25*(abs_xk_25/I));
    alpha_0*new_EF_0 + old_EF_0*delta_alpha_0 <= gain_max;
    alpha_1*new_EF_1 + old_EF_1*delta_alpha_1 <= gain_max;
    alpha_2*new_EF_2 + old_EF_2*delta_alpha_2 <= gain_max;
    alpha_3*new_EF_3 + old_EF_3*delta_alpha_3 <= gain_max;
    alpha_4*new_EF_4 + old_EF_4*delta_alpha_4 <= gain_max;
    alpha_5*new_EF_5 + old_EF_5*delta_alpha_5 <= gain_max;
    alpha_6*new_EF_6 + old_EF_6*delta_alpha_6 <= gain_max;
    alpha_7*new_EF_7 + old_EF_7*delta_alpha_7 <= gain_max;
    alpha_8*new_EF_8 + old_EF_8*delta_alpha_8 <= gain_max;
    alpha_9*new_EF_9 + old_EF_9*delta_alpha_9 <= gain_max;
    alpha_10*new_EF_10 + old_EF_10*delta_alpha_10 <= gain_max;
    alpha_11*new_EF_11 + old_EF_11*delta_alpha_11 <= gain_max;
    alpha_12*new_EF_12 + old_EF_12*delta_alpha_12 <= gain_max;
    alpha_13*new_EF_13 + old_EF_13*delta_alpha_13 <= gain_max;
    alpha_14*new_EF_14 + old_EF_14*delta_alpha_14 <= gain_max;
    alpha_15*new_EF_15 + old_EF_15*delta_alpha_15 <= gain_max;
    alpha_16*new_EF_16 + old_EF_16*delta_alpha_16 <= gain_max;
    alpha_17*new_EF_17 + old_EF_17*delta_alpha_17 <= gain_max;
    alpha_18*new_EF_18 + old_EF_18*delta_alpha_18 <= gain_max;
    alpha_19*new_EF_19 + old_EF_19*delta_alpha_19 <= gain_max;
    alpha_20*new_EF_20 + old_EF_20*delta_alpha_20 <= gain_max;
    alpha_21*new_EF_21 + old_EF_21*delta_alpha_21 <= gain_max;
    alpha_22*new_EF_22 + old_EF_22*delta_alpha_22 <= gain_max;
    alpha_23*new_EF_23 + old_EF_23*delta_alpha_23 <= gain_max;
    alpha_24*new_EF_24 + old_EF_24*delta_alpha_24 <= gain_max;
    alpha_25*new_EF_25 + old_EF_25*delta_alpha_25 <= gain_max;
    new_EF_1 >= 0;
    new_EF_2 >= 0;
    new_EF_3 >= 0;
    new_EF_4 >= 0;
    new_EF_5 >= 0;
    new_EF_6 >= 0;
    new_EF_7 >= 0;
    new_EF_8 >= 0;
    new_EF_9 >= 0;
    new_EF_10 >= 0;
    new_EF_11 >= 0;
    new_EF_12 >= 0;
    new_EF_13 >= 0;
    new_EF_14 >= 0;
    new_EF_15 >= 0;
    new_EF_16 >= 0;
    new_EF_17 >= 0;
    new_EF_18 >= 0;
    new_EF_19 >= 0;
    new_EF_20 >= 0;
    new_EF_21 >= 0;
    new_EF_22 >= 0;
    new_EF_23 >= 0;
    new_EF_24 >= 0;
    new_EF_25 >= 0;
    new_EF_26 >= 0;
    omega_1 == delta_t*( - lambda*omega_0 + new_EF_0) + omega_0;
    omega_2 == delta_t*( - lambda*omega_1 + new_EF_1) + omega_1;
    omega_3 == delta_t*( - lambda*omega_2 + new_EF_2) + omega_2;
    omega_4 == delta_t*( - lambda*omega_3 + new_EF_3) + omega_3;
    omega_5 == delta_t*( - lambda*omega_4 + new_EF_4) + omega_4;
    omega_6 == delta_t*( - lambda*omega_5 + new_EF_5) + omega_5;
    omega_7 == delta_t*( - lambda*omega_6 + new_EF_6) + omega_6;
    omega_8 == delta_t*( - lambda*omega_7 + new_EF_7) + omega_7;
    omega_9 == delta_t*( - lambda*omega_8 + new_EF_8) + omega_8;
    omega_10 == delta_t*( - lambda*omega_9 + new_EF_9) + omega_9;
    omega_11 == delta_t*( - lambda*omega_10 + new_EF_10) + omega_10;
    omega_12 == delta_t*( - lambda*omega_11 + new_EF_11) + omega_11;
    omega_13 == delta_t*( - lambda*omega_12 + new_EF_12) + omega_12;
    omega_14 == delta_t*( - lambda*omega_13 + new_EF_13) + omega_13;
    omega_15 == delta_t*( - lambda*omega_14 + new_EF_14) + omega_14;
    omega_16 == delta_t*( - lambda*omega_15 + new_EF_15) + omega_15;
    omega_17 == delta_t*( - lambda*omega_16 + new_EF_16) + omega_16;
    omega_18 == delta_t*( - lambda*omega_17 + new_EF_17) + omega_17;
    omega_19 == delta_t*( - lambda*omega_18 + new_EF_18) + omega_18;
    omega_20 == delta_t*( - lambda*omega_19 + new_EF_19) + omega_19;
    omega_21 == delta_t*( - lambda*omega_20 + new_EF_20) + omega_20;
    omega_22 == delta_t*( - lambda*omega_21 + new_EF_21) + omega_21;
    omega_23 == delta_t*( - lambda*omega_22 + new_EF_22) + omega_22;
    omega_24 == delta_t*( - lambda*omega_23 + new_EF_23) + omega_23;
    omega_25 == delta_t*( - lambda*omega_24 + new_EF_24) + omega_24;
    omega_26 == delta_t*( - lambda*omega_25 + new_EF_25) + omega_25;
    norm(alpha_5 + delta_alpha_5 - alpha_final_0, inf) <= delta_alpha_final;
    norm(alpha_10 + delta_alpha_10 - alpha_final_1, inf) <= delta_alpha_final;
    norm(alpha_15 + delta_alpha_15 - alpha_final_2, inf) <= delta_alpha_final;
    alpha_0 + delta_alpha_0 >= alpha_min;
    alpha_1 + delta_alpha_1 >= alpha_min;
    alpha_2 + delta_alpha_2 >= alpha_min;
    alpha_3 + delta_alpha_3 >= alpha_min;
    alpha_4 + delta_alpha_4 >= alpha_min;
    alpha_5 + delta_alpha_5 >= alpha_min;
    alpha_6 + delta_alpha_6 >= alpha_min;
    alpha_7 + delta_alpha_7 >= alpha_min;
    alpha_8 + delta_alpha_8 >= alpha_min;
    alpha_9 + delta_alpha_9 >= alpha_min;
    alpha_10 + delta_alpha_10 >= alpha_min;
    alpha_11 + delta_alpha_11 >= alpha_min;
    alpha_12 + delta_alpha_12 >= alpha_min;
    alpha_13 + delta_alpha_13 >= alpha_min;
    alpha_14 + delta_alpha_14 >= alpha_min;
    alpha_15 + delta_alpha_15 >= alpha_min;
    alpha_16 + delta_alpha_16 >= alpha_min;
    alpha_17 + delta_alpha_17 >= alpha_min;
    alpha_18 + delta_alpha_18 >= alpha_min;
    alpha_19 + delta_alpha_19 >= alpha_min;
    alpha_20 + delta_alpha_20 >= alpha_min;
    alpha_21 + delta_alpha_21 >= alpha_min;
    alpha_22 + delta_alpha_22 >= alpha_min;
    alpha_23 + delta_alpha_23 >= alpha_min;
    alpha_24 + delta_alpha_24 >= alpha_min;
    alpha_25 + delta_alpha_25 >= alpha_min;
    alpha_0 + delta_alpha_0 <= alpha_max;
    alpha_1 + delta_alpha_1 <= alpha_max;
    alpha_2 + delta_alpha_2 <= alpha_max;
    alpha_3 + delta_alpha_3 <= alpha_max;
    alpha_4 + delta_alpha_4 <= alpha_max;
    alpha_5 + delta_alpha_5 <= alpha_max;
    alpha_6 + delta_alpha_6 <= alpha_max;
    alpha_7 + delta_alpha_7 <= alpha_max;
    alpha_8 + delta_alpha_8 <= alpha_max;
    alpha_9 + delta_alpha_9 <= alpha_max;
    alpha_10 + delta_alpha_10 <= alpha_max;
    alpha_11 + delta_alpha_11 <= alpha_max;
    alpha_12 + delta_alpha_12 <= alpha_max;
    alpha_13 + delta_alpha_13 <= alpha_max;
    alpha_14 + delta_alpha_14 <= alpha_max;
    alpha_15 + delta_alpha_15 <= alpha_max;
    alpha_16 + delta_alpha_16 <= alpha_max;
    alpha_17 + delta_alpha_17 <= alpha_max;
    alpha_18 + delta_alpha_18 <= alpha_max;
    alpha_19 + delta_alpha_19 <= alpha_max;
    alpha_20 + delta_alpha_20 <= alpha_max;
    alpha_21 + delta_alpha_21 <= alpha_max;
    alpha_22 + delta_alpha_22 <= alpha_max;
    alpha_23 + delta_alpha_23 <= alpha_max;
    alpha_24 + delta_alpha_24 <= alpha_max;
    alpha_25 + delta_alpha_25 <= alpha_max;
    abs(alpha_1 + delta_alpha_1 - alpha_0 - delta_alpha_0) <= delta_alpha_max;
    abs(alpha_2 + delta_alpha_2 - alpha_1 - delta_alpha_1) <= delta_alpha_max;
    abs(alpha_3 + delta_alpha_3 - alpha_2 - delta_alpha_2) <= delta_alpha_max;
    abs(alpha_4 + delta_alpha_4 - alpha_3 - delta_alpha_3) <= delta_alpha_max;
    abs(alpha_5 + delta_alpha_5 - alpha_4 - delta_alpha_4) <= delta_alpha_max;
    abs(alpha_6 + delta_alpha_6 - alpha_5 - delta_alpha_5) <= delta_alpha_max;
    abs(alpha_7 + delta_alpha_7 - alpha_6 - delta_alpha_6) <= delta_alpha_max;
    abs(alpha_8 + delta_alpha_8 - alpha_7 - delta_alpha_7) <= delta_alpha_max;
    abs(alpha_9 + delta_alpha_9 - alpha_8 - delta_alpha_8) <= delta_alpha_max;
    abs(alpha_10 + delta_alpha_10 - alpha_9 - delta_alpha_9) <= delta_alpha_max;
    abs(alpha_11 + delta_alpha_11 - alpha_10 - delta_alpha_10) <= delta_alpha_max;
    abs(alpha_12 + delta_alpha_12 - alpha_11 - delta_alpha_11) <= delta_alpha_max;
    abs(alpha_13 + delta_alpha_13 - alpha_12 - delta_alpha_12) <= delta_alpha_max;
    abs(alpha_14 + delta_alpha_14 - alpha_13 - delta_alpha_13) <= delta_alpha_max;
    abs(alpha_15 + delta_alpha_15 - alpha_14 - delta_alpha_14) <= delta_alpha_max;
    abs(alpha_16 + delta_alpha_16 - alpha_15 - delta_alpha_15) <= delta_alpha_max;
    abs(alpha_17 + delta_alpha_17 - alpha_16 - delta_alpha_16) <= delta_alpha_max;
    abs(alpha_18 + delta_alpha_18 - alpha_17 - delta_alpha_17) <= delta_alpha_max;
    abs(alpha_19 + delta_alpha_19 - alpha_18 - delta_alpha_18) <= delta_alpha_max;
    abs(alpha_20 + delta_alpha_20 - alpha_19 - delta_alpha_19) <= delta_alpha_max;
    abs(alpha_21 + delta_alpha_21 - alpha_20 - delta_alpha_20) <= delta_alpha_max;
    abs(alpha_22 + delta_alpha_22 - alpha_21 - delta_alpha_21) <= delta_alpha_max;
    abs(alpha_23 + delta_alpha_23 - alpha_22 - delta_alpha_22) <= delta_alpha_max;
    abs(alpha_24 + delta_alpha_24 - alpha_23 - delta_alpha_23) <= delta_alpha_max;
    abs(alpha_25 + delta_alpha_25 - alpha_24 - delta_alpha_24) <= delta_alpha_max;
    abs(alpha_0 + delta_alpha_0 - last_alpha) <= delta_alpha_max;
    omega_1 >= 0;
    omega_2 >= 0;
    omega_3 >= 0;
    omega_4 >= 0;
    omega_5 >= 0;
    omega_6 >= 0;
    omega_7 >= 0;
    omega_8 >= 0;
    omega_9 >= 0;
    omega_10 >= 0;
    omega_11 >= 0;
    omega_12 >= 0;
    omega_13 >= 0;
    omega_14 >= 0;
    omega_15 >= 0;
    omega_16 >= 0;
    omega_17 >= 0;
    omega_18 >= 0;
    omega_19 >= 0;
    omega_20 >= 0;
    omega_21 >= 0;
    omega_22 >= 0;
    omega_23 >= 0;
    omega_24 >= 0;
    omega_25 >= 0;
    omega_26 >= 0;
    omega_1 <= omega_max;
    omega_2 <= omega_max;
    omega_3 <= omega_max;
    omega_4 <= omega_max;
    omega_5 <= omega_max;
    omega_6 <= omega_max;
    omega_7 <= omega_max;
    omega_8 <= omega_max;
    omega_9 <= omega_max;
    omega_10 <= omega_max;
    omega_11 <= omega_max;
    omega_12 <= omega_max;
    omega_13 <= omega_max;
    omega_14 <= omega_max;
    omega_15 <= omega_max;
    omega_16 <= omega_max;
    omega_17 <= omega_max;
    omega_18 <= omega_max;
    omega_19 <= omega_max;
    omega_20 <= omega_max;
    omega_21 <= omega_max;
    omega_22 <= omega_max;
    omega_23 <= omega_max;
    omega_24 <= omega_max;
    omega_25 <= omega_max;
    omega_26 <= omega_max;
cvx_end
vars.d_1 = d_1;
vars.d{1} = d_1;
vars.d_2 = d_2;
vars.d{2} = d_2;
vars.d_3 = d_3;
vars.d{3} = d_3;
vars.d_4 = d_4;
vars.d{4} = d_4;
vars.d_5 = d_5;
vars.d{5} = d_5;
vars.d_6 = d_6;
vars.d{6} = d_6;
vars.d_7 = d_7;
vars.d{7} = d_7;
vars.d_8 = d_8;
vars.d{8} = d_8;
vars.d_9 = d_9;
vars.d{9} = d_9;
vars.d_10 = d_10;
vars.d{10} = d_10;
vars.d_11 = d_11;
vars.d{11} = d_11;
vars.d_12 = d_12;
vars.d{12} = d_12;
vars.d_13 = d_13;
vars.d{13} = d_13;
vars.d_14 = d_14;
vars.d{14} = d_14;
vars.d_15 = d_15;
vars.d{15} = d_15;
vars.d_16 = d_16;
vars.d{16} = d_16;
vars.d_17 = d_17;
vars.d{17} = d_17;
vars.d_18 = d_18;
vars.d{18} = d_18;
vars.d_19 = d_19;
vars.d{19} = d_19;
vars.d_20 = d_20;
vars.d{20} = d_20;
vars.d_21 = d_21;
vars.d{21} = d_21;
vars.d_22 = d_22;
vars.d{22} = d_22;
vars.d_23 = d_23;
vars.d{23} = d_23;
vars.d_24 = d_24;
vars.d{24} = d_24;
vars.d_25 = d_25;
vars.d{25} = d_25;
vars.d_26 = d_26;
vars.d{26} = d_26;
vars.delta_alpha_0 = delta_alpha_0;
vars.delta_alpha_1 = delta_alpha_1;
vars.delta_alpha{1} = delta_alpha_1;
vars.delta_alpha_2 = delta_alpha_2;
vars.delta_alpha{2} = delta_alpha_2;
vars.delta_alpha_3 = delta_alpha_3;
vars.delta_alpha{3} = delta_alpha_3;
vars.delta_alpha_4 = delta_alpha_4;
vars.delta_alpha{4} = delta_alpha_4;
vars.delta_alpha_5 = delta_alpha_5;
vars.delta_alpha{5} = delta_alpha_5;
vars.delta_alpha_6 = delta_alpha_6;
vars.delta_alpha{6} = delta_alpha_6;
vars.delta_alpha_7 = delta_alpha_7;
vars.delta_alpha{7} = delta_alpha_7;
vars.delta_alpha_8 = delta_alpha_8;
vars.delta_alpha{8} = delta_alpha_8;
vars.delta_alpha_9 = delta_alpha_9;
vars.delta_alpha{9} = delta_alpha_9;
vars.delta_alpha_10 = delta_alpha_10;
vars.delta_alpha{10} = delta_alpha_10;
vars.delta_alpha_11 = delta_alpha_11;
vars.delta_alpha{11} = delta_alpha_11;
vars.delta_alpha_12 = delta_alpha_12;
vars.delta_alpha{12} = delta_alpha_12;
vars.delta_alpha_13 = delta_alpha_13;
vars.delta_alpha{13} = delta_alpha_13;
vars.delta_alpha_14 = delta_alpha_14;
vars.delta_alpha{14} = delta_alpha_14;
vars.delta_alpha_15 = delta_alpha_15;
vars.delta_alpha{15} = delta_alpha_15;
vars.delta_alpha_16 = delta_alpha_16;
vars.delta_alpha{16} = delta_alpha_16;
vars.delta_alpha_17 = delta_alpha_17;
vars.delta_alpha{17} = delta_alpha_17;
vars.delta_alpha_18 = delta_alpha_18;
vars.delta_alpha{18} = delta_alpha_18;
vars.delta_alpha_19 = delta_alpha_19;
vars.delta_alpha{19} = delta_alpha_19;
vars.delta_alpha_20 = delta_alpha_20;
vars.delta_alpha{20} = delta_alpha_20;
vars.delta_alpha_21 = delta_alpha_21;
vars.delta_alpha{21} = delta_alpha_21;
vars.delta_alpha_22 = delta_alpha_22;
vars.delta_alpha{22} = delta_alpha_22;
vars.delta_alpha_23 = delta_alpha_23;
vars.delta_alpha{23} = delta_alpha_23;
vars.delta_alpha_24 = delta_alpha_24;
vars.delta_alpha{24} = delta_alpha_24;
vars.delta_alpha_25 = delta_alpha_25;
vars.delta_alpha{25} = delta_alpha_25;
vars.new_EF_1 = new_EF_1;
vars.new_EF{1} = new_EF_1;
vars.new_EF_2 = new_EF_2;
vars.new_EF{2} = new_EF_2;
vars.new_EF_3 = new_EF_3;
vars.new_EF{3} = new_EF_3;
vars.new_EF_4 = new_EF_4;
vars.new_EF{4} = new_EF_4;
vars.new_EF_5 = new_EF_5;
vars.new_EF{5} = new_EF_5;
vars.new_EF_6 = new_EF_6;
vars.new_EF{6} = new_EF_6;
vars.new_EF_7 = new_EF_7;
vars.new_EF{7} = new_EF_7;
vars.new_EF_8 = new_EF_8;
vars.new_EF{8} = new_EF_8;
vars.new_EF_9 = new_EF_9;
vars.new_EF{9} = new_EF_9;
vars.new_EF_10 = new_EF_10;
vars.new_EF{10} = new_EF_10;
vars.new_EF_11 = new_EF_11;
vars.new_EF{11} = new_EF_11;
vars.new_EF_12 = new_EF_12;
vars.new_EF{12} = new_EF_12;
vars.new_EF_13 = new_EF_13;
vars.new_EF{13} = new_EF_13;
vars.new_EF_14 = new_EF_14;
vars.new_EF{14} = new_EF_14;
vars.new_EF_15 = new_EF_15;
vars.new_EF{15} = new_EF_15;
vars.new_EF_16 = new_EF_16;
vars.new_EF{16} = new_EF_16;
vars.new_EF_17 = new_EF_17;
vars.new_EF{17} = new_EF_17;
vars.new_EF_18 = new_EF_18;
vars.new_EF{18} = new_EF_18;
vars.new_EF_19 = new_EF_19;
vars.new_EF{19} = new_EF_19;
vars.new_EF_20 = new_EF_20;
vars.new_EF{20} = new_EF_20;
vars.new_EF_21 = new_EF_21;
vars.new_EF{21} = new_EF_21;
vars.new_EF_22 = new_EF_22;
vars.new_EF{22} = new_EF_22;
vars.new_EF_23 = new_EF_23;
vars.new_EF{23} = new_EF_23;
vars.new_EF_24 = new_EF_24;
vars.new_EF{24} = new_EF_24;
vars.new_EF_25 = new_EF_25;
vars.new_EF{25} = new_EF_25;
vars.new_EF_26 = new_EF_26;
vars.new_EF{26} = new_EF_26;
vars.omega_1 = omega_1;
vars.omega{1} = omega_1;
vars.omega_2 = omega_2;
vars.omega{2} = omega_2;
vars.omega_3 = omega_3;
vars.omega{3} = omega_3;
vars.omega_4 = omega_4;
vars.omega{4} = omega_4;
vars.omega_5 = omega_5;
vars.omega{5} = omega_5;
vars.omega_6 = omega_6;
vars.omega{6} = omega_6;
vars.omega_7 = omega_7;
vars.omega{7} = omega_7;
vars.omega_8 = omega_8;
vars.omega{8} = omega_8;
vars.omega_9 = omega_9;
vars.omega{9} = omega_9;
vars.omega_10 = omega_10;
vars.omega{10} = omega_10;
vars.omega_11 = omega_11;
vars.omega{11} = omega_11;
vars.omega_12 = omega_12;
vars.omega{12} = omega_12;
vars.omega_13 = omega_13;
vars.omega{13} = omega_13;
vars.omega_14 = omega_14;
vars.omega{14} = omega_14;
vars.omega_15 = omega_15;
vars.omega{15} = omega_15;
vars.omega_16 = omega_16;
vars.omega{16} = omega_16;
vars.omega_17 = omega_17;
vars.omega{17} = omega_17;
vars.omega_18 = omega_18;
vars.omega{18} = omega_18;
vars.omega_19 = omega_19;
vars.omega{19} = omega_19;
vars.omega_20 = omega_20;
vars.omega{20} = omega_20;
vars.omega_21 = omega_21;
vars.omega{21} = omega_21;
vars.omega_22 = omega_22;
vars.omega{22} = omega_22;
vars.omega_23 = omega_23;
vars.omega{23} = omega_23;
vars.omega_24 = omega_24;
vars.omega{24} = omega_24;
vars.omega_25 = omega_25;
vars.omega{25} = omega_25;
vars.omega_26 = omega_26;
vars.omega{26} = omega_26;
vars.w_0 = w_0;
vars.w_1 = w_1;
vars.w{1} = w_1;
vars.w_2 = w_2;
vars.w{2} = w_2;
vars.w_3 = w_3;
vars.w{3} = w_3;
vars.w_4 = w_4;
vars.w{4} = w_4;
vars.w_5 = w_5;
vars.w{5} = w_5;
vars.w_6 = w_6;
vars.w{6} = w_6;
vars.w_7 = w_7;
vars.w{7} = w_7;
vars.w_8 = w_8;
vars.w{8} = w_8;
vars.w_9 = w_9;
vars.w{9} = w_9;
vars.w_10 = w_10;
vars.w{10} = w_10;
vars.w_11 = w_11;
vars.w{11} = w_11;
vars.w_12 = w_12;
vars.w{12} = w_12;
vars.w_13 = w_13;
vars.w{13} = w_13;
vars.w_14 = w_14;
vars.w{14} = w_14;
vars.w_15 = w_15;
vars.w{15} = w_15;
vars.w_16 = w_16;
vars.w{16} = w_16;
vars.w_17 = w_17;
vars.w{17} = w_17;
vars.w_18 = w_18;
vars.w{18} = w_18;
vars.w_19 = w_19;
vars.w{19} = w_19;
vars.w_20 = w_20;
vars.w{20} = w_20;
vars.w_21 = w_21;
vars.w{21} = w_21;
vars.w_22 = w_22;
vars.w{22} = w_22;
vars.w_23 = w_23;
vars.w{23} = w_23;
vars.w_24 = w_24;
vars.w{24} = w_24;
vars.w_25 = w_25;
vars.w{25} = w_25;
status.cvx_status = cvx_status;
% Provide a drop-in replacement for csolve.
status.optval = cvx_optval;
status.converged = strcmp(cvx_status, 'Solved');
