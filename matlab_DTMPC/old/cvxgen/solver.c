/* Produced by CVXGEN, 2020-02-27 17:12:48 -0500.  */
/* CVXGEN is Copyright (C) 2006-2017 Jacob Mattingley, jem@cvxgen.com. */
/* The code in this file is Copyright (C) 2006-2017 Jacob Mattingley. */
/* CVXGEN, or solvers produced by CVXGEN, cannot be used for commercial */
/* applications without prior written permission from Jacob Mattingley. */

/* Filename: solver.c. */
/* Description: Main solver file. */
#include "solver.h"
double eval_gap(void) {
  int i;
  double gap;
  gap = 0;
  for (i = 0; i < 555; i++)
    gap += work.z[i]*work.s[i];
  return gap;
}
void set_defaults(void) {
  settings.resid_tol = 1e-6;
  settings.eps = 1e-4;
  settings.max_iters = 25;
  settings.refine_steps = 1;
  settings.s_init = 1;
  settings.z_init = 1;
  settings.debug = 0;
  settings.verbose = 1;
  settings.verbose_refinement = 0;
  settings.better_start = 1;
  settings.kkt_reg = 1e-7;
}
void setup_pointers(void) {
  work.y = work.x + 314;
  work.s = work.x + 443;
  work.z = work.x + 998;
  vars.d_1 = work.x + 0;
  vars.d_2 = work.x + 2;
  vars.d_3 = work.x + 4;
  vars.d_4 = work.x + 6;
  vars.d_5 = work.x + 8;
  vars.d_6 = work.x + 10;
  vars.d_7 = work.x + 12;
  vars.d_8 = work.x + 14;
  vars.d_9 = work.x + 16;
  vars.d_10 = work.x + 18;
  vars.d_11 = work.x + 20;
  vars.d_12 = work.x + 22;
  vars.d_13 = work.x + 24;
  vars.d_14 = work.x + 26;
  vars.d_15 = work.x + 28;
  vars.d_16 = work.x + 30;
  vars.d_17 = work.x + 32;
  vars.d_18 = work.x + 34;
  vars.d_19 = work.x + 36;
  vars.d_20 = work.x + 38;
  vars.d_21 = work.x + 40;
  vars.d_22 = work.x + 42;
  vars.d_23 = work.x + 44;
  vars.d_24 = work.x + 46;
  vars.d_25 = work.x + 48;
  vars.d_26 = work.x + 50;
  vars.delta_alpha_0 = work.x + 52;
  vars.delta_alpha_1 = work.x + 53;
  vars.delta_alpha_2 = work.x + 54;
  vars.delta_alpha_3 = work.x + 55;
  vars.delta_alpha_4 = work.x + 56;
  vars.delta_alpha_5 = work.x + 57;
  vars.delta_alpha_6 = work.x + 58;
  vars.delta_alpha_7 = work.x + 59;
  vars.delta_alpha_8 = work.x + 60;
  vars.delta_alpha_9 = work.x + 61;
  vars.delta_alpha_10 = work.x + 62;
  vars.delta_alpha_11 = work.x + 63;
  vars.delta_alpha_12 = work.x + 64;
  vars.delta_alpha_13 = work.x + 65;
  vars.delta_alpha_14 = work.x + 66;
  vars.delta_alpha_15 = work.x + 67;
  vars.delta_alpha_16 = work.x + 68;
  vars.delta_alpha_17 = work.x + 69;
  vars.delta_alpha_18 = work.x + 70;
  vars.delta_alpha_19 = work.x + 71;
  vars.delta_alpha_20 = work.x + 72;
  vars.delta_alpha_21 = work.x + 73;
  vars.delta_alpha_22 = work.x + 74;
  vars.delta_alpha_23 = work.x + 75;
  vars.delta_alpha_24 = work.x + 76;
  vars.delta_alpha_25 = work.x + 77;
  vars.new_EF_1 = work.x + 78;
  vars.new_EF_2 = work.x + 79;
  vars.new_EF_3 = work.x + 80;
  vars.new_EF_4 = work.x + 81;
  vars.new_EF_5 = work.x + 82;
  vars.new_EF_6 = work.x + 83;
  vars.new_EF_7 = work.x + 84;
  vars.new_EF_8 = work.x + 85;
  vars.new_EF_9 = work.x + 86;
  vars.new_EF_10 = work.x + 87;
  vars.new_EF_11 = work.x + 88;
  vars.new_EF_12 = work.x + 89;
  vars.new_EF_13 = work.x + 90;
  vars.new_EF_14 = work.x + 91;
  vars.new_EF_15 = work.x + 92;
  vars.new_EF_16 = work.x + 93;
  vars.new_EF_17 = work.x + 94;
  vars.new_EF_18 = work.x + 95;
  vars.new_EF_19 = work.x + 96;
  vars.new_EF_20 = work.x + 97;
  vars.new_EF_21 = work.x + 98;
  vars.new_EF_22 = work.x + 99;
  vars.new_EF_23 = work.x + 100;
  vars.new_EF_24 = work.x + 101;
  vars.new_EF_25 = work.x + 102;
  vars.new_EF_26 = work.x + 103;
  vars.omega_1 = work.x + 104;
  vars.omega_2 = work.x + 105;
  vars.omega_3 = work.x + 106;
  vars.omega_4 = work.x + 107;
  vars.omega_5 = work.x + 108;
  vars.omega_6 = work.x + 109;
  vars.omega_7 = work.x + 110;
  vars.omega_8 = work.x + 111;
  vars.omega_9 = work.x + 112;
  vars.omega_10 = work.x + 113;
  vars.omega_11 = work.x + 114;
  vars.omega_12 = work.x + 115;
  vars.omega_13 = work.x + 116;
  vars.omega_14 = work.x + 117;
  vars.omega_15 = work.x + 118;
  vars.omega_16 = work.x + 119;
  vars.omega_17 = work.x + 120;
  vars.omega_18 = work.x + 121;
  vars.omega_19 = work.x + 122;
  vars.omega_20 = work.x + 123;
  vars.omega_21 = work.x + 124;
  vars.omega_22 = work.x + 125;
  vars.omega_23 = work.x + 126;
  vars.omega_24 = work.x + 127;
  vars.omega_25 = work.x + 128;
  vars.omega_26 = work.x + 129;
  vars.w_0 = work.x + 288;
  vars.w_1 = work.x + 289;
  vars.w_2 = work.x + 290;
  vars.w_3 = work.x + 291;
  vars.w_4 = work.x + 292;
  vars.w_5 = work.x + 293;
  vars.w_6 = work.x + 294;
  vars.w_7 = work.x + 295;
  vars.w_8 = work.x + 296;
  vars.w_9 = work.x + 297;
  vars.w_10 = work.x + 298;
  vars.w_11 = work.x + 299;
  vars.w_12 = work.x + 300;
  vars.w_13 = work.x + 301;
  vars.w_14 = work.x + 302;
  vars.w_15 = work.x + 303;
  vars.w_16 = work.x + 304;
  vars.w_17 = work.x + 305;
  vars.w_18 = work.x + 306;
  vars.w_19 = work.x + 307;
  vars.w_20 = work.x + 308;
  vars.w_21 = work.x + 309;
  vars.w_22 = work.x + 310;
  vars.w_23 = work.x + 311;
  vars.w_24 = work.x + 312;
  vars.w_25 = work.x + 313;
}
void setup_indexed_params(void) {
  /* In CVXGEN, you can say */
  /*   parameters */
  /*     A[i] (5,3), i=1..4 */
  /*   end */
  /* This function sets up A[2] to be a pointer to A_2, which is a length-15 */
  /* vector of doubles. */
  /* If you access parameters that you haven't defined in CVXGEN, the result */
  /* is undefined. */
  params.xk[0] = params.xk_0;
  params.d[0] = params.d_0;
  params.uk[0] = params.uk_0;
  params.alpha[0] = params.alpha_0;
  params.new_EF[0] = params.new_EF_0;
  params.old_EF[0] = params.old_EF_0;
  params.xk[1] = params.xk_1;
  params.uk[1] = params.uk_1;
  params.alpha[1] = params.alpha_1;
  params.old_EF[1] = params.old_EF_1;
  params.xk[2] = params.xk_2;
  params.uk[2] = params.uk_2;
  params.alpha[2] = params.alpha_2;
  params.old_EF[2] = params.old_EF_2;
  params.xk[3] = params.xk_3;
  params.uk[3] = params.uk_3;
  params.alpha[3] = params.alpha_3;
  params.old_EF[3] = params.old_EF_3;
  params.xk[4] = params.xk_4;
  params.uk[4] = params.uk_4;
  params.alpha[4] = params.alpha_4;
  params.old_EF[4] = params.old_EF_4;
  params.xk[5] = params.xk_5;
  params.uk[5] = params.uk_5;
  params.alpha[5] = params.alpha_5;
  params.old_EF[5] = params.old_EF_5;
  params.xk[6] = params.xk_6;
  params.uk[6] = params.uk_6;
  params.alpha[6] = params.alpha_6;
  params.old_EF[6] = params.old_EF_6;
  params.xk[7] = params.xk_7;
  params.uk[7] = params.uk_7;
  params.alpha[7] = params.alpha_7;
  params.old_EF[7] = params.old_EF_7;
  params.xk[8] = params.xk_8;
  params.uk[8] = params.uk_8;
  params.alpha[8] = params.alpha_8;
  params.old_EF[8] = params.old_EF_8;
  params.xk[9] = params.xk_9;
  params.uk[9] = params.uk_9;
  params.alpha[9] = params.alpha_9;
  params.old_EF[9] = params.old_EF_9;
  params.xk[10] = params.xk_10;
  params.uk[10] = params.uk_10;
  params.alpha[10] = params.alpha_10;
  params.old_EF[10] = params.old_EF_10;
  params.xk[11] = params.xk_11;
  params.uk[11] = params.uk_11;
  params.alpha[11] = params.alpha_11;
  params.old_EF[11] = params.old_EF_11;
  params.xk[12] = params.xk_12;
  params.uk[12] = params.uk_12;
  params.alpha[12] = params.alpha_12;
  params.old_EF[12] = params.old_EF_12;
  params.xk[13] = params.xk_13;
  params.uk[13] = params.uk_13;
  params.alpha[13] = params.alpha_13;
  params.old_EF[13] = params.old_EF_13;
  params.xk[14] = params.xk_14;
  params.uk[14] = params.uk_14;
  params.alpha[14] = params.alpha_14;
  params.old_EF[14] = params.old_EF_14;
  params.xk[15] = params.xk_15;
  params.uk[15] = params.uk_15;
  params.alpha[15] = params.alpha_15;
  params.old_EF[15] = params.old_EF_15;
  params.xk[16] = params.xk_16;
  params.uk[16] = params.uk_16;
  params.alpha[16] = params.alpha_16;
  params.old_EF[16] = params.old_EF_16;
  params.xk[17] = params.xk_17;
  params.uk[17] = params.uk_17;
  params.alpha[17] = params.alpha_17;
  params.old_EF[17] = params.old_EF_17;
  params.xk[18] = params.xk_18;
  params.uk[18] = params.uk_18;
  params.alpha[18] = params.alpha_18;
  params.old_EF[18] = params.old_EF_18;
  params.xk[19] = params.xk_19;
  params.uk[19] = params.uk_19;
  params.alpha[19] = params.alpha_19;
  params.old_EF[19] = params.old_EF_19;
  params.xk[20] = params.xk_20;
  params.uk[20] = params.uk_20;
  params.alpha[20] = params.alpha_20;
  params.old_EF[20] = params.old_EF_20;
  params.xk[21] = params.xk_21;
  params.uk[21] = params.uk_21;
  params.alpha[21] = params.alpha_21;
  params.old_EF[21] = params.old_EF_21;
  params.xk[22] = params.xk_22;
  params.uk[22] = params.uk_22;
  params.alpha[22] = params.alpha_22;
  params.old_EF[22] = params.old_EF_22;
  params.xk[23] = params.xk_23;
  params.uk[23] = params.uk_23;
  params.alpha[23] = params.alpha_23;
  params.old_EF[23] = params.old_EF_23;
  params.xk[24] = params.xk_24;
  params.uk[24] = params.uk_24;
  params.alpha[24] = params.alpha_24;
  params.old_EF[24] = params.old_EF_24;
  params.xk[25] = params.xk_25;
  params.uk[25] = params.uk_25;
  params.alpha[25] = params.alpha_25;
  params.old_EF[25] = params.old_EF_25;
  params.xk[26] = params.xk_26;
  params.cos_xk[0] = params.cos_xk_0;
  params.abs_xk[0] = params.abs_xk_0;
  params.cos_xk[1] = params.cos_xk_1;
  params.abs_xk[1] = params.abs_xk_1;
  params.cos_xk[2] = params.cos_xk_2;
  params.abs_xk[2] = params.abs_xk_2;
  params.cos_xk[3] = params.cos_xk_3;
  params.abs_xk[3] = params.abs_xk_3;
  params.cos_xk[4] = params.cos_xk_4;
  params.abs_xk[4] = params.abs_xk_4;
  params.cos_xk[5] = params.cos_xk_5;
  params.abs_xk[5] = params.abs_xk_5;
  params.cos_xk[6] = params.cos_xk_6;
  params.abs_xk[6] = params.abs_xk_6;
  params.cos_xk[7] = params.cos_xk_7;
  params.abs_xk[7] = params.abs_xk_7;
  params.cos_xk[8] = params.cos_xk_8;
  params.abs_xk[8] = params.abs_xk_8;
  params.cos_xk[9] = params.cos_xk_9;
  params.abs_xk[9] = params.abs_xk_9;
  params.cos_xk[10] = params.cos_xk_10;
  params.abs_xk[10] = params.abs_xk_10;
  params.cos_xk[11] = params.cos_xk_11;
  params.abs_xk[11] = params.abs_xk_11;
  params.cos_xk[12] = params.cos_xk_12;
  params.abs_xk[12] = params.abs_xk_12;
  params.cos_xk[13] = params.cos_xk_13;
  params.abs_xk[13] = params.abs_xk_13;
  params.cos_xk[14] = params.cos_xk_14;
  params.abs_xk[14] = params.abs_xk_14;
  params.cos_xk[15] = params.cos_xk_15;
  params.abs_xk[15] = params.abs_xk_15;
  params.cos_xk[16] = params.cos_xk_16;
  params.abs_xk[16] = params.abs_xk_16;
  params.cos_xk[17] = params.cos_xk_17;
  params.abs_xk[17] = params.abs_xk_17;
  params.cos_xk[18] = params.cos_xk_18;
  params.abs_xk[18] = params.abs_xk_18;
  params.cos_xk[19] = params.cos_xk_19;
  params.abs_xk[19] = params.abs_xk_19;
  params.cos_xk[20] = params.cos_xk_20;
  params.abs_xk[20] = params.abs_xk_20;
  params.cos_xk[21] = params.cos_xk_21;
  params.abs_xk[21] = params.abs_xk_21;
  params.cos_xk[22] = params.cos_xk_22;
  params.abs_xk[22] = params.abs_xk_22;
  params.cos_xk[23] = params.cos_xk_23;
  params.abs_xk[23] = params.abs_xk_23;
  params.cos_xk[24] = params.cos_xk_24;
  params.abs_xk[24] = params.abs_xk_24;
  params.cos_xk[25] = params.cos_xk_25;
  params.abs_xk[25] = params.abs_xk_25;
  params.disturbance[0] = params.disturbance_0;
  params.disturbance[1] = params.disturbance_1;
  params.disturbance[2] = params.disturbance_2;
  params.disturbance[3] = params.disturbance_3;
  params.disturbance[4] = params.disturbance_4;
  params.disturbance[5] = params.disturbance_5;
  params.disturbance[6] = params.disturbance_6;
  params.disturbance[7] = params.disturbance_7;
  params.disturbance[8] = params.disturbance_8;
  params.disturbance[9] = params.disturbance_9;
  params.disturbance[10] = params.disturbance_10;
  params.disturbance[11] = params.disturbance_11;
  params.disturbance[12] = params.disturbance_12;
  params.disturbance[13] = params.disturbance_13;
  params.disturbance[14] = params.disturbance_14;
  params.disturbance[15] = params.disturbance_15;
  params.disturbance[16] = params.disturbance_16;
  params.disturbance[17] = params.disturbance_17;
  params.disturbance[18] = params.disturbance_18;
  params.disturbance[19] = params.disturbance_19;
  params.disturbance[20] = params.disturbance_20;
  params.disturbance[21] = params.disturbance_21;
  params.disturbance[22] = params.disturbance_22;
  params.disturbance[23] = params.disturbance_23;
  params.disturbance[24] = params.disturbance_24;
  params.disturbance[25] = params.disturbance_25;
  params.omega[0] = params.omega_0;
  params.alpha_final[0] = params.alpha_final_0;
  params.alpha_final[1] = params.alpha_final_1;
  params.alpha_final[2] = params.alpha_final_2;
}
void setup_indexed_optvars(void) {
  /* In CVXGEN, you can say */
  /*   variables */
  /*     x[i] (5), i=2..4 */
  /*   end */
  /* This function sets up x[3] to be a pointer to x_3, which is a length-5 */
  /* vector of doubles. */
  /* If you access variables that you haven't defined in CVXGEN, the result */
  /* is undefined. */
  vars.w[0] = vars.w_0;
  vars.delta_alpha[0] = vars.delta_alpha_0;
  vars.d[1] = vars.d_1;
  vars.w[1] = vars.w_1;
  vars.new_EF[1] = vars.new_EF_1;
  vars.delta_alpha[1] = vars.delta_alpha_1;
  vars.d[2] = vars.d_2;
  vars.w[2] = vars.w_2;
  vars.new_EF[2] = vars.new_EF_2;
  vars.delta_alpha[2] = vars.delta_alpha_2;
  vars.d[3] = vars.d_3;
  vars.w[3] = vars.w_3;
  vars.new_EF[3] = vars.new_EF_3;
  vars.delta_alpha[3] = vars.delta_alpha_3;
  vars.d[4] = vars.d_4;
  vars.w[4] = vars.w_4;
  vars.new_EF[4] = vars.new_EF_4;
  vars.delta_alpha[4] = vars.delta_alpha_4;
  vars.d[5] = vars.d_5;
  vars.w[5] = vars.w_5;
  vars.new_EF[5] = vars.new_EF_5;
  vars.delta_alpha[5] = vars.delta_alpha_5;
  vars.d[6] = vars.d_6;
  vars.w[6] = vars.w_6;
  vars.new_EF[6] = vars.new_EF_6;
  vars.delta_alpha[6] = vars.delta_alpha_6;
  vars.d[7] = vars.d_7;
  vars.w[7] = vars.w_7;
  vars.new_EF[7] = vars.new_EF_7;
  vars.delta_alpha[7] = vars.delta_alpha_7;
  vars.d[8] = vars.d_8;
  vars.w[8] = vars.w_8;
  vars.new_EF[8] = vars.new_EF_8;
  vars.delta_alpha[8] = vars.delta_alpha_8;
  vars.d[9] = vars.d_9;
  vars.w[9] = vars.w_9;
  vars.new_EF[9] = vars.new_EF_9;
  vars.delta_alpha[9] = vars.delta_alpha_9;
  vars.d[10] = vars.d_10;
  vars.w[10] = vars.w_10;
  vars.new_EF[10] = vars.new_EF_10;
  vars.delta_alpha[10] = vars.delta_alpha_10;
  vars.d[11] = vars.d_11;
  vars.w[11] = vars.w_11;
  vars.new_EF[11] = vars.new_EF_11;
  vars.delta_alpha[11] = vars.delta_alpha_11;
  vars.d[12] = vars.d_12;
  vars.w[12] = vars.w_12;
  vars.new_EF[12] = vars.new_EF_12;
  vars.delta_alpha[12] = vars.delta_alpha_12;
  vars.d[13] = vars.d_13;
  vars.w[13] = vars.w_13;
  vars.new_EF[13] = vars.new_EF_13;
  vars.delta_alpha[13] = vars.delta_alpha_13;
  vars.d[14] = vars.d_14;
  vars.w[14] = vars.w_14;
  vars.new_EF[14] = vars.new_EF_14;
  vars.delta_alpha[14] = vars.delta_alpha_14;
  vars.d[15] = vars.d_15;
  vars.w[15] = vars.w_15;
  vars.new_EF[15] = vars.new_EF_15;
  vars.delta_alpha[15] = vars.delta_alpha_15;
  vars.d[16] = vars.d_16;
  vars.w[16] = vars.w_16;
  vars.new_EF[16] = vars.new_EF_16;
  vars.delta_alpha[16] = vars.delta_alpha_16;
  vars.d[17] = vars.d_17;
  vars.w[17] = vars.w_17;
  vars.new_EF[17] = vars.new_EF_17;
  vars.delta_alpha[17] = vars.delta_alpha_17;
  vars.d[18] = vars.d_18;
  vars.w[18] = vars.w_18;
  vars.new_EF[18] = vars.new_EF_18;
  vars.delta_alpha[18] = vars.delta_alpha_18;
  vars.d[19] = vars.d_19;
  vars.w[19] = vars.w_19;
  vars.new_EF[19] = vars.new_EF_19;
  vars.delta_alpha[19] = vars.delta_alpha_19;
  vars.d[20] = vars.d_20;
  vars.w[20] = vars.w_20;
  vars.new_EF[20] = vars.new_EF_20;
  vars.delta_alpha[20] = vars.delta_alpha_20;
  vars.d[21] = vars.d_21;
  vars.w[21] = vars.w_21;
  vars.new_EF[21] = vars.new_EF_21;
  vars.delta_alpha[21] = vars.delta_alpha_21;
  vars.d[22] = vars.d_22;
  vars.w[22] = vars.w_22;
  vars.new_EF[22] = vars.new_EF_22;
  vars.delta_alpha[22] = vars.delta_alpha_22;
  vars.d[23] = vars.d_23;
  vars.w[23] = vars.w_23;
  vars.new_EF[23] = vars.new_EF_23;
  vars.delta_alpha[23] = vars.delta_alpha_23;
  vars.d[24] = vars.d_24;
  vars.w[24] = vars.w_24;
  vars.new_EF[24] = vars.new_EF_24;
  vars.delta_alpha[24] = vars.delta_alpha_24;
  vars.d[25] = vars.d_25;
  vars.w[25] = vars.w_25;
  vars.new_EF[25] = vars.new_EF_25;
  vars.delta_alpha[25] = vars.delta_alpha_25;
  vars.d[26] = vars.d_26;
  vars.new_EF[26] = vars.new_EF_26;
  vars.omega[1] = vars.omega_1;
  vars.omega[2] = vars.omega_2;
  vars.omega[3] = vars.omega_3;
  vars.omega[4] = vars.omega_4;
  vars.omega[5] = vars.omega_5;
  vars.omega[6] = vars.omega_6;
  vars.omega[7] = vars.omega_7;
  vars.omega[8] = vars.omega_8;
  vars.omega[9] = vars.omega_9;
  vars.omega[10] = vars.omega_10;
  vars.omega[11] = vars.omega_11;
  vars.omega[12] = vars.omega_12;
  vars.omega[13] = vars.omega_13;
  vars.omega[14] = vars.omega_14;
  vars.omega[15] = vars.omega_15;
  vars.omega[16] = vars.omega_16;
  vars.omega[17] = vars.omega_17;
  vars.omega[18] = vars.omega_18;
  vars.omega[19] = vars.omega_19;
  vars.omega[20] = vars.omega_20;
  vars.omega[21] = vars.omega_21;
  vars.omega[22] = vars.omega_22;
  vars.omega[23] = vars.omega_23;
  vars.omega[24] = vars.omega_24;
  vars.omega[25] = vars.omega_25;
  vars.omega[26] = vars.omega_26;
}
void setup_indexing(void) {
  setup_pointers();
  setup_indexed_params();
  setup_indexed_optvars();
}
void set_start(void) {
  int i;
  for (i = 0; i < 314; i++)
    work.x[i] = 0;
  for (i = 0; i < 129; i++)
    work.y[i] = 0;
  for (i = 0; i < 555; i++)
    work.s[i] = (work.h[i] > 0) ? work.h[i] : settings.s_init;
  for (i = 0; i < 555; i++)
    work.z[i] = settings.z_init;
}
double eval_objv(void) {
  int i;
  double objv;
  /* Borrow space in work.rhs. */
  multbyP(work.rhs, work.x);
  objv = 0;
  for (i = 0; i < 314; i++)
    objv += work.x[i]*work.rhs[i];
  objv *= 0.5;
  for (i = 0; i < 314; i++)
    objv += work.q[i]*work.x[i];
  objv += work.quad_270107893760[0]+work.quad_392898023424[0]+work.quad_354214375424[0]+work.quad_445352677376[0]+work.quad_702230208512[0]+work.quad_335242391552[0]+work.quad_175950630912[0]+work.quad_332149305344[0]+work.quad_17263370240[0]+work.quad_302248755200[0]+work.quad_977458573312[0]+work.quad_579963990016[0]+work.quad_487442513920[0]+work.quad_648024653824[0]+work.quad_910448377856[0]+work.quad_341291667456[0]+work.quad_453679702016[0]+work.quad_588369764352[0]+work.quad_610273832960[0]+work.quad_929545818112[0]+work.quad_3726573568[0]+work.quad_566947319808[0]+work.quad_126117388288[0]+work.quad_442566197248[0]+work.quad_565576192000[0]+work.quad_839768125440[0]+work.quad_694648061952[0]+work.quad_338226507776[0]+work.quad_881072074752[0]+work.quad_815321604096[0]+work.quad_409310203904[0]+work.quad_748685393920[0]+work.quad_671950053376[0]+work.quad_46587228160[0]+work.quad_266521808896[0]+work.quad_986003238912[0]+work.quad_585389805568[0]+work.quad_660241698816[0]+work.quad_59129753600[0]+work.quad_749055442944[0]+work.quad_348638896128[0]+work.quad_628438679552[0]+work.quad_782724800512[0]+work.quad_906353664000[0]+work.quad_41312305152[0]+work.quad_232271880192[0]+work.quad_786671919104[0]+work.quad_352455172096[0]+work.quad_326244257792[0]+work.quad_388870012928[0]+work.quad_564407173120[0]+work.quad_607032025088[0]+work.quad_588796252160[0]+work.quad_631891841024[0]+work.quad_46308270080[0]+work.quad_74921062400[0]+work.quad_587445669888[0]+work.quad_271313948672[0]+work.quad_274730606592[0]+work.quad_496943005696[0]+work.quad_223001780224[0]+work.quad_548109938688[0]+work.quad_137830731776[0]+work.quad_662255759360[0]+work.quad_859892768768[0]+work.quad_772709756928[0]+work.quad_645643497472[0]+work.quad_827707936768[0]+work.quad_513757962240[0]+work.quad_111384309760[0]+work.quad_891445882880[0]+work.quad_521918545920[0]+work.quad_971924062208[0]+work.quad_930083958784[0]+work.quad_103559696384[0]+work.quad_595768565760[0]+work.quad_182585827328[0]+work.quad_261409124352[0]+work.quad_885751197696[0]+work.quad_310504296448[0];
  return objv;
}
void fillrhs_aff(void) {
  int i;
  double *r1, *r2, *r3, *r4;
  r1 = work.rhs;
  r2 = work.rhs + 314;
  r3 = work.rhs + 869;
  r4 = work.rhs + 1424;
  /* r1 = -A^Ty - G^Tz - Px - q. */
  multbymAT(r1, work.y);
  multbymGT(work.buffer, work.z);
  for (i = 0; i < 314; i++)
    r1[i] += work.buffer[i];
  multbyP(work.buffer, work.x);
  for (i = 0; i < 314; i++)
    r1[i] -= work.buffer[i] + work.q[i];
  /* r2 = -z. */
  for (i = 0; i < 555; i++)
    r2[i] = -work.z[i];
  /* r3 = -Gx - s + h. */
  multbymG(r3, work.x);
  for (i = 0; i < 555; i++)
    r3[i] += -work.s[i] + work.h[i];
  /* r4 = -Ax + b. */
  multbymA(r4, work.x);
  for (i = 0; i < 129; i++)
    r4[i] += work.b[i];
}
void fillrhs_cc(void) {
  int i;
  double *r2;
  double *ds_aff, *dz_aff;
  double mu;
  double alpha;
  double sigma;
  double smu;
  double minval;
  r2 = work.rhs + 314;
  ds_aff = work.lhs_aff + 314;
  dz_aff = work.lhs_aff + 869;
  mu = 0;
  for (i = 0; i < 555; i++)
    mu += work.s[i]*work.z[i];
  /* Don't finish calculating mu quite yet. */
  /* Find min(min(ds./s), min(dz./z)). */
  minval = 0;
  for (i = 0; i < 555; i++)
    if (ds_aff[i] < minval*work.s[i])
      minval = ds_aff[i]/work.s[i];
  for (i = 0; i < 555; i++)
    if (dz_aff[i] < minval*work.z[i])
      minval = dz_aff[i]/work.z[i];
  /* Find alpha. */
  if (-1 < minval)
      alpha = 1;
  else
      alpha = -1/minval;
  sigma = 0;
  for (i = 0; i < 555; i++)
    sigma += (work.s[i] + alpha*ds_aff[i])*
      (work.z[i] + alpha*dz_aff[i]);
  sigma /= mu;
  sigma = sigma*sigma*sigma;
  /* Finish calculating mu now. */
  mu *= 0.0018018018018018018;
  smu = sigma*mu;
  /* Fill-in the rhs. */
  for (i = 0; i < 314; i++)
    work.rhs[i] = 0;
  for (i = 869; i < 1553; i++)
    work.rhs[i] = 0;
  for (i = 0; i < 555; i++)
    r2[i] = work.s_inv[i]*(smu - ds_aff[i]*dz_aff[i]);
}
void refine(double *target, double *var) {
  int i, j;
  double *residual = work.buffer;
  double norm2;
  double *new_var = work.buffer2;
  for (j = 0; j < settings.refine_steps; j++) {
    norm2 = 0;
    matrix_multiply(residual, var);
    for (i = 0; i < 1553; i++) {
      residual[i] = residual[i] - target[i];
      norm2 += residual[i]*residual[i];
    }
#ifndef ZERO_LIBRARY_MODE
    if (settings.verbose_refinement) {
      if (j == 0)
        printf("Initial residual before refinement has norm squared %.6g.\n", norm2);
      else
        printf("After refinement we get squared norm %.6g.\n", norm2);
    }
#endif
    /* Solve to find new_var = KKT \ (target - A*var). */
    ldl_solve(residual, new_var);
    /* Update var += new_var, or var += KKT \ (target - A*var). */
    for (i = 0; i < 1553; i++) {
      var[i] -= new_var[i];
    }
  }
#ifndef ZERO_LIBRARY_MODE
  if (settings.verbose_refinement) {
    /* Check the residual once more, but only if we're reporting it, since */
    /* it's expensive. */
    norm2 = 0;
    matrix_multiply(residual, var);
    for (i = 0; i < 1553; i++) {
      residual[i] = residual[i] - target[i];
      norm2 += residual[i]*residual[i];
    }
    if (j == 0)
      printf("Initial residual before refinement has norm squared %.6g.\n", norm2);
    else
      printf("After refinement we get squared norm %.6g.\n", norm2);
  }
#endif
}
double calc_ineq_resid_squared(void) {
  /* Calculates the norm ||-Gx - s + h||. */
  double norm2_squared;
  int i;
  /* Find -Gx. */
  multbymG(work.buffer, work.x);
  /* Add -s + h. */
  for (i = 0; i < 555; i++)
    work.buffer[i] += -work.s[i] + work.h[i];
  /* Now find the squared norm. */
  norm2_squared = 0;
  for (i = 0; i < 555; i++)
    norm2_squared += work.buffer[i]*work.buffer[i];
  return norm2_squared;
}
double calc_eq_resid_squared(void) {
  /* Calculates the norm ||-Ax + b||. */
  double norm2_squared;
  int i;
  /* Find -Ax. */
  multbymA(work.buffer, work.x);
  /* Add +b. */
  for (i = 0; i < 129; i++)
    work.buffer[i] += work.b[i];
  /* Now find the squared norm. */
  norm2_squared = 0;
  for (i = 0; i < 129; i++)
    norm2_squared += work.buffer[i]*work.buffer[i];
  return norm2_squared;
}
void better_start(void) {
  /* Calculates a better starting point, using a similar approach to CVXOPT. */
  /* Not yet speed optimized. */
  int i;
  double *x, *s, *z, *y;
  double alpha;
  work.block_33[0] = -1;
  /* Make sure sinvz is 1 to make hijacked KKT system ok. */
  for (i = 0; i < 555; i++)
    work.s_inv_z[i] = 1;
  fill_KKT();
  ldl_factor();
  fillrhs_start();
  /* Borrow work.lhs_aff for the solution. */
  ldl_solve(work.rhs, work.lhs_aff);
  /* Don't do any refinement for now. Precision doesn't matter too much. */
  x = work.lhs_aff;
  s = work.lhs_aff + 314;
  z = work.lhs_aff + 869;
  y = work.lhs_aff + 1424;
  /* Just set x and y as is. */
  for (i = 0; i < 314; i++)
    work.x[i] = x[i];
  for (i = 0; i < 129; i++)
    work.y[i] = y[i];
  /* Now complete the initialization. Start with s. */
  /* Must have alpha > max(z). */
  alpha = -1e99;
  for (i = 0; i < 555; i++)
    if (alpha < z[i])
      alpha = z[i];
  if (alpha < 0) {
    for (i = 0; i < 555; i++)
      work.s[i] = -z[i];
  } else {
    alpha += 1;
    for (i = 0; i < 555; i++)
      work.s[i] = -z[i] + alpha;
  }
  /* Now initialize z. */
  /* Now must have alpha > max(-z). */
  alpha = -1e99;
  for (i = 0; i < 555; i++)
    if (alpha < -z[i])
      alpha = -z[i];
  if (alpha < 0) {
    for (i = 0; i < 555; i++)
      work.z[i] = z[i];
  } else {
    alpha += 1;
    for (i = 0; i < 555; i++)
      work.z[i] = z[i] + alpha;
  }
}
void fillrhs_start(void) {
  /* Fill rhs with (-q, 0, h, b). */
  int i;
  double *r1, *r2, *r3, *r4;
  r1 = work.rhs;
  r2 = work.rhs + 314;
  r3 = work.rhs + 869;
  r4 = work.rhs + 1424;
  for (i = 0; i < 314; i++)
    r1[i] = -work.q[i];
  for (i = 0; i < 555; i++)
    r2[i] = 0;
  for (i = 0; i < 555; i++)
    r3[i] = work.h[i];
  for (i = 0; i < 129; i++)
    r4[i] = work.b[i];
}
long solve(void) {
  int i;
  int iter;
  double *dx, *ds, *dy, *dz;
  double minval;
  double alpha;
  work.converged = 0;
  setup_pointers();
  pre_ops();
#ifndef ZERO_LIBRARY_MODE
  if (settings.verbose)
    printf("iter     objv        gap       |Ax-b|    |Gx+s-h|    step\n");
#endif
  fillq();
  fillh();
  fillb();
  if (settings.better_start)
    better_start();
  else
    set_start();
  for (iter = 0; iter < settings.max_iters; iter++) {
    for (i = 0; i < 555; i++) {
      work.s_inv[i] = 1.0 / work.s[i];
      work.s_inv_z[i] = work.s_inv[i]*work.z[i];
    }
    work.block_33[0] = 0;
    fill_KKT();
    ldl_factor();
    /* Affine scaling directions. */
    fillrhs_aff();
    ldl_solve(work.rhs, work.lhs_aff);
    refine(work.rhs, work.lhs_aff);
    /* Centering plus corrector directions. */
    fillrhs_cc();
    ldl_solve(work.rhs, work.lhs_cc);
    refine(work.rhs, work.lhs_cc);
    /* Add the two together and store in aff. */
    for (i = 0; i < 1553; i++)
      work.lhs_aff[i] += work.lhs_cc[i];
    /* Rename aff to reflect its new meaning. */
    dx = work.lhs_aff;
    ds = work.lhs_aff + 314;
    dz = work.lhs_aff + 869;
    dy = work.lhs_aff + 1424;
    /* Find min(min(ds./s), min(dz./z)). */
    minval = 0;
    for (i = 0; i < 555; i++)
      if (ds[i] < minval*work.s[i])
        minval = ds[i]/work.s[i];
    for (i = 0; i < 555; i++)
      if (dz[i] < minval*work.z[i])
        minval = dz[i]/work.z[i];
    /* Find alpha. */
    if (-0.99 < minval)
      alpha = 1;
    else
      alpha = -0.99/minval;
    /* Update the primal and dual variables. */
    for (i = 0; i < 314; i++)
      work.x[i] += alpha*dx[i];
    for (i = 0; i < 555; i++)
      work.s[i] += alpha*ds[i];
    for (i = 0; i < 555; i++)
      work.z[i] += alpha*dz[i];
    for (i = 0; i < 129; i++)
      work.y[i] += alpha*dy[i];
    work.gap = eval_gap();
    work.eq_resid_squared = calc_eq_resid_squared();
    work.ineq_resid_squared = calc_ineq_resid_squared();
#ifndef ZERO_LIBRARY_MODE
    if (settings.verbose) {
      work.optval = eval_objv();
      printf("%3d   %10.3e  %9.2e  %9.2e  %9.2e  % 6.4f\n",
          iter+1, work.optval, work.gap, sqrt(work.eq_resid_squared),
          sqrt(work.ineq_resid_squared), alpha);
    }
#endif
    /* Test termination conditions. Requires optimality, and satisfied */
    /* constraints. */
    if (   (work.gap < settings.eps)
        && (work.eq_resid_squared <= settings.resid_tol*settings.resid_tol)
        && (work.ineq_resid_squared <= settings.resid_tol*settings.resid_tol)
       ) {
      work.converged = 1;
      work.optval = eval_objv();
      return iter+1;
    }
  }
  return iter;
}
