/* Produced by CVXGEN, 2020-02-27 17:13:27 -0500.  */
/* CVXGEN is Copyright (C) 2006-2017 Jacob Mattingley, jem@cvxgen.com. */
/* The code in this file is Copyright (C) 2006-2017 Jacob Mattingley. */
/* CVXGEN, or solvers produced by CVXGEN, cannot be used for commercial */
/* applications without prior written permission from Jacob Mattingley. */

/* Filename: solver.h. */
/* Description: Header file with relevant definitions. */
#ifndef SOLVER_H
#define SOLVER_H
/* Uncomment the next line to remove all library dependencies. */
/*#define ZERO_LIBRARY_MODE */
#ifdef MATLAB_MEX_FILE
/* Matlab functions. MATLAB_MEX_FILE will be defined by the mex compiler. */
/* If you are not using the mex compiler, this functionality will not intrude, */
/* as it will be completely disabled at compile-time. */
#include "mex.h"
#else
#ifndef ZERO_LIBRARY_MODE
#include <stdio.h>
#endif
#endif
/* Space must be allocated somewhere (testsolver.c, csolve.c or your own */
/* program) for the global variables vars, params, work and settings. */
/* At the bottom of this file, they are externed. */
#ifndef ZERO_LIBRARY_MODE
#include <math.h>
#define pm(A, m, n) printmatrix(#A, A, m, n, 1)
#endif
typedef struct Params_t {
  double x_final[2];
  double xk_0[2];
  double d_0[2];
  double Q[4];
  double uk_0[1];
  double R[1];
  double alpha_0[1];
  double new_EF_0[1];
  double old_EF_0[1];
  double I_over_L[1];
  double R_delta[1];
  double alpha_min[1];
  double M[1];
  double xk_1[2];
  double uk_1[1];
  double alpha_1[1];
  double old_EF_1[1];
  double xk_2[2];
  double uk_2[1];
  double alpha_2[1];
  double old_EF_2[1];
  double xk_3[2];
  double uk_3[1];
  double alpha_3[1];
  double old_EF_3[1];
  double xk_4[2];
  double uk_4[1];
  double alpha_4[1];
  double old_EF_4[1];
  double xk_5[2];
  double uk_5[1];
  double alpha_5[1];
  double old_EF_5[1];
  double xk_6[2];
  double uk_6[1];
  double alpha_6[1];
  double old_EF_6[1];
  double xk_7[2];
  double uk_7[1];
  double alpha_7[1];
  double old_EF_7[1];
  double xk_8[2];
  double uk_8[1];
  double alpha_8[1];
  double old_EF_8[1];
  double xk_9[2];
  double uk_9[1];
  double alpha_9[1];
  double old_EF_9[1];
  double xk_10[2];
  double uk_10[1];
  double alpha_10[1];
  double old_EF_10[1];
  double xk_11[2];
  double uk_11[1];
  double alpha_11[1];
  double old_EF_11[1];
  double xk_12[2];
  double uk_12[1];
  double alpha_12[1];
  double old_EF_12[1];
  double xk_13[2];
  double uk_13[1];
  double alpha_13[1];
  double old_EF_13[1];
  double xk_14[2];
  double uk_14[1];
  double alpha_14[1];
  double old_EF_14[1];
  double xk_15[2];
  double uk_15[1];
  double alpha_15[1];
  double old_EF_15[1];
  double xk_16[2];
  double uk_16[1];
  double alpha_16[1];
  double old_EF_16[1];
  double xk_17[2];
  double uk_17[1];
  double alpha_17[1];
  double old_EF_17[1];
  double xk_18[2];
  double uk_18[1];
  double alpha_18[1];
  double old_EF_18[1];
  double xk_19[2];
  double uk_19[1];
  double alpha_19[1];
  double old_EF_19[1];
  double xk_20[2];
  double uk_20[1];
  double alpha_20[1];
  double old_EF_20[1];
  double xk_21[2];
  double uk_21[1];
  double alpha_21[1];
  double old_EF_21[1];
  double xk_22[2];
  double uk_22[1];
  double alpha_22[1];
  double old_EF_22[1];
  double xk_23[2];
  double uk_23[1];
  double alpha_23[1];
  double old_EF_23[1];
  double xk_24[2];
  double uk_24[1];
  double alpha_24[1];
  double old_EF_24[1];
  double xk_25[2];
  double uk_25[1];
  double alpha_25[1];
  double old_EF_25[1];
  double xk_26[2];
  double Q_final[4];
  double delta_t[1];
  double c1[1];
  double cos_xk_0[1];
  double c2[1];
  double abs_xk_0[1];
  double Bd[1];
  double cos_xk_1[1];
  double abs_xk_1[1];
  double cos_xk_2[1];
  double abs_xk_2[1];
  double cos_xk_3[1];
  double abs_xk_3[1];
  double cos_xk_4[1];
  double abs_xk_4[1];
  double cos_xk_5[1];
  double abs_xk_5[1];
  double cos_xk_6[1];
  double abs_xk_6[1];
  double cos_xk_7[1];
  double abs_xk_7[1];
  double cos_xk_8[1];
  double abs_xk_8[1];
  double cos_xk_9[1];
  double abs_xk_9[1];
  double cos_xk_10[1];
  double abs_xk_10[1];
  double cos_xk_11[1];
  double abs_xk_11[1];
  double cos_xk_12[1];
  double abs_xk_12[1];
  double cos_xk_13[1];
  double abs_xk_13[1];
  double cos_xk_14[1];
  double abs_xk_14[1];
  double cos_xk_15[1];
  double abs_xk_15[1];
  double cos_xk_16[1];
  double abs_xk_16[1];
  double cos_xk_17[1];
  double abs_xk_17[1];
  double cos_xk_18[1];
  double abs_xk_18[1];
  double cos_xk_19[1];
  double abs_xk_19[1];
  double cos_xk_20[1];
  double abs_xk_20[1];
  double cos_xk_21[1];
  double abs_xk_21[1];
  double cos_xk_22[1];
  double abs_xk_22[1];
  double cos_xk_23[1];
  double abs_xk_23[1];
  double cos_xk_24[1];
  double abs_xk_24[1];
  double cos_xk_25[1];
  double abs_xk_25[1];
  double w_max[1];
  double u_max[1];
  double x_dot_bound[1];
  double S[1];
  double last_u[1];
  double nu[1];
  double D[1];
  double disturbance_0[1];
  double Cd_max[1];
  double I[1];
  double disturbance_1[1];
  double disturbance_2[1];
  double disturbance_3[1];
  double disturbance_4[1];
  double disturbance_5[1];
  double disturbance_6[1];
  double disturbance_7[1];
  double disturbance_8[1];
  double disturbance_9[1];
  double disturbance_10[1];
  double disturbance_11[1];
  double disturbance_12[1];
  double disturbance_13[1];
  double disturbance_14[1];
  double disturbance_15[1];
  double disturbance_16[1];
  double disturbance_17[1];
  double disturbance_18[1];
  double disturbance_19[1];
  double disturbance_20[1];
  double disturbance_21[1];
  double disturbance_22[1];
  double disturbance_23[1];
  double disturbance_24[1];
  double disturbance_25[1];
  double gain_max[1];
  double lambda[1];
  double omega_0[1];
  double alpha_final_0[1];
  double delta_alpha_final[1];
  double alpha_final_1[1];
  double alpha_final_2[1];
  double alpha_max[1];
  double delta_alpha_max[1];
  double last_alpha[1];
  double omega_max[1];
  double *xk[27];
  double *d[1];
  double *uk[26];
  double *alpha[26];
  double *new_EF[1];
  double *old_EF[26];
  double *cos_xk[26];
  double *abs_xk[26];
  double *disturbance[26];
  double *omega[1];
  double *alpha_final[3];
} Params;
typedef struct Vars_t {
  double *w_0; /* 1 rows. */
  double *delta_alpha_0; /* 1 rows. */
  double *d_1; /* 2 rows. */
  double *w_1; /* 1 rows. */
  double *t_01; /* 1 rows. */
  double *delta_alpha_1; /* 1 rows. */
  double *d_2; /* 2 rows. */
  double *w_2; /* 1 rows. */
  double *t_02; /* 1 rows. */
  double *delta_alpha_2; /* 1 rows. */
  double *d_3; /* 2 rows. */
  double *w_3; /* 1 rows. */
  double *t_03; /* 1 rows. */
  double *delta_alpha_3; /* 1 rows. */
  double *d_4; /* 2 rows. */
  double *w_4; /* 1 rows. */
  double *t_04; /* 1 rows. */
  double *delta_alpha_4; /* 1 rows. */
  double *d_5; /* 2 rows. */
  double *w_5; /* 1 rows. */
  double *t_05; /* 1 rows. */
  double *delta_alpha_5; /* 1 rows. */
  double *d_6; /* 2 rows. */
  double *w_6; /* 1 rows. */
  double *t_06; /* 1 rows. */
  double *delta_alpha_6; /* 1 rows. */
  double *d_7; /* 2 rows. */
  double *w_7; /* 1 rows. */
  double *t_07; /* 1 rows. */
  double *delta_alpha_7; /* 1 rows. */
  double *d_8; /* 2 rows. */
  double *w_8; /* 1 rows. */
  double *t_08; /* 1 rows. */
  double *delta_alpha_8; /* 1 rows. */
  double *d_9; /* 2 rows. */
  double *w_9; /* 1 rows. */
  double *t_09; /* 1 rows. */
  double *delta_alpha_9; /* 1 rows. */
  double *d_10; /* 2 rows. */
  double *w_10; /* 1 rows. */
  double *t_10; /* 1 rows. */
  double *delta_alpha_10; /* 1 rows. */
  double *d_11; /* 2 rows. */
  double *w_11; /* 1 rows. */
  double *t_11; /* 1 rows. */
  double *delta_alpha_11; /* 1 rows. */
  double *d_12; /* 2 rows. */
  double *w_12; /* 1 rows. */
  double *t_12; /* 1 rows. */
  double *delta_alpha_12; /* 1 rows. */
  double *d_13; /* 2 rows. */
  double *w_13; /* 1 rows. */
  double *t_13; /* 1 rows. */
  double *delta_alpha_13; /* 1 rows. */
  double *d_14; /* 2 rows. */
  double *w_14; /* 1 rows. */
  double *t_14; /* 1 rows. */
  double *delta_alpha_14; /* 1 rows. */
  double *d_15; /* 2 rows. */
  double *w_15; /* 1 rows. */
  double *t_15; /* 1 rows. */
  double *delta_alpha_15; /* 1 rows. */
  double *d_16; /* 2 rows. */
  double *w_16; /* 1 rows. */
  double *t_16; /* 1 rows. */
  double *delta_alpha_16; /* 1 rows. */
  double *d_17; /* 2 rows. */
  double *w_17; /* 1 rows. */
  double *t_17; /* 1 rows. */
  double *delta_alpha_17; /* 1 rows. */
  double *d_18; /* 2 rows. */
  double *w_18; /* 1 rows. */
  double *t_18; /* 1 rows. */
  double *delta_alpha_18; /* 1 rows. */
  double *d_19; /* 2 rows. */
  double *w_19; /* 1 rows. */
  double *t_19; /* 1 rows. */
  double *delta_alpha_19; /* 1 rows. */
  double *d_20; /* 2 rows. */
  double *w_20; /* 1 rows. */
  double *t_20; /* 1 rows. */
  double *delta_alpha_20; /* 1 rows. */
  double *d_21; /* 2 rows. */
  double *w_21; /* 1 rows. */
  double *t_21; /* 1 rows. */
  double *delta_alpha_21; /* 1 rows. */
  double *d_22; /* 2 rows. */
  double *w_22; /* 1 rows. */
  double *t_22; /* 1 rows. */
  double *delta_alpha_22; /* 1 rows. */
  double *d_23; /* 2 rows. */
  double *w_23; /* 1 rows. */
  double *t_23; /* 1 rows. */
  double *delta_alpha_23; /* 1 rows. */
  double *d_24; /* 2 rows. */
  double *w_24; /* 1 rows. */
  double *t_24; /* 1 rows. */
  double *delta_alpha_24; /* 1 rows. */
  double *d_25; /* 2 rows. */
  double *w_25; /* 1 rows. */
  double *t_25; /* 1 rows. */
  double *delta_alpha_25; /* 1 rows. */
  double *d_26; /* 2 rows. */
  double *t_26; /* 1 rows. */
  double *t_27; /* 1 rows. */
  double *t_28; /* 1 rows. */
  double *t_29; /* 1 rows. */
  double *t_30; /* 1 rows. */
  double *t_31; /* 1 rows. */
  double *t_32; /* 1 rows. */
  double *t_33; /* 1 rows. */
  double *t_34; /* 1 rows. */
  double *t_35; /* 1 rows. */
  double *t_36; /* 1 rows. */
  double *t_37; /* 1 rows. */
  double *t_38; /* 1 rows. */
  double *t_39; /* 1 rows. */
  double *t_40; /* 1 rows. */
  double *t_41; /* 1 rows. */
  double *t_42; /* 1 rows. */
  double *t_43; /* 1 rows. */
  double *t_44; /* 1 rows. */
  double *t_45; /* 1 rows. */
  double *t_46; /* 1 rows. */
  double *t_47; /* 1 rows. */
  double *t_48; /* 1 rows. */
  double *t_49; /* 1 rows. */
  double *t_50; /* 1 rows. */
  double *t_51; /* 1 rows. */
  double *t_52; /* 1 rows. */
  double *t_53; /* 1 rows. */
  double *t_54; /* 1 rows. */
  double *t_55; /* 1 rows. */
  double *t_56; /* 1 rows. */
  double *t_57; /* 1 rows. */
  double *t_58; /* 1 rows. */
  double *t_59; /* 1 rows. */
  double *t_60; /* 1 rows. */
  double *t_61; /* 1 rows. */
  double *t_62; /* 1 rows. */
  double *t_63; /* 1 rows. */
  double *t_64; /* 1 rows. */
  double *t_65; /* 1 rows. */
  double *t_66; /* 1 rows. */
  double *t_67; /* 1 rows. */
  double *t_68; /* 1 rows. */
  double *t_69; /* 1 rows. */
  double *t_70; /* 1 rows. */
  double *t_71; /* 1 rows. */
  double *t_72; /* 1 rows. */
  double *t_73; /* 1 rows. */
  double *t_74; /* 1 rows. */
  double *t_75; /* 1 rows. */
  double *t_76; /* 1 rows. */
  double *t_77; /* 1 rows. */
  double *t_78; /* 1 rows. */
  double *t_79; /* 1 rows. */
  double *t_80; /* 1 rows. */
  double *t_81; /* 1 rows. */
  double *t_82; /* 1 rows. */
  double *t_83; /* 1 rows. */
  double *t_84; /* 1 rows. */
  double *t_85; /* 1 rows. */
  double *t_86; /* 1 rows. */
  double *t_87; /* 1 rows. */
  double *t_88; /* 1 rows. */
  double *t_89; /* 1 rows. */
  double *t_90; /* 1 rows. */
  double *t_91; /* 1 rows. */
  double *t_92; /* 1 rows. */
  double *t_93; /* 1 rows. */
  double *t_94; /* 1 rows. */
  double *t_95; /* 1 rows. */
  double *t_96; /* 1 rows. */
  double *t_97; /* 1 rows. */
  double *t_98; /* 1 rows. */
  double *t_99; /* 1 rows. */
  double *t_100; /* 1 rows. */
  double *t_101; /* 1 rows. */
  double *t_102; /* 1 rows. */
  double *t_103; /* 1 rows. */
  double *t_104; /* 1 rows. */
  double *t_105; /* 1 rows. */
  double *t_106; /* 1 rows. */
  double *t_107; /* 1 rows. */
  double *t_108; /* 1 rows. */
  double *t_109; /* 1 rows. */
  double *t_110; /* 1 rows. */
  double *t_111; /* 1 rows. */
  double *t_112; /* 1 rows. */
  double *t_113; /* 1 rows. */
  double *t_114; /* 1 rows. */
  double *t_115; /* 1 rows. */
  double *t_116; /* 1 rows. */
  double *t_117; /* 1 rows. */
  double *t_118; /* 1 rows. */
  double *t_119; /* 1 rows. */
  double *t_120; /* 1 rows. */
  double *t_121; /* 1 rows. */
  double *t_122; /* 1 rows. */
  double *t_123; /* 1 rows. */
  double *t_124; /* 1 rows. */
  double *t_125; /* 1 rows. */
  double *t_126; /* 1 rows. */
  double *t_127; /* 1 rows. */
  double *t_128; /* 1 rows. */
  double *t_129; /* 1 rows. */
  double *new_EF_1; /* 1 rows. */
  double *new_EF_2; /* 1 rows. */
  double *new_EF_3; /* 1 rows. */
  double *new_EF_4; /* 1 rows. */
  double *new_EF_5; /* 1 rows. */
  double *new_EF_6; /* 1 rows. */
  double *new_EF_7; /* 1 rows. */
  double *new_EF_8; /* 1 rows. */
  double *new_EF_9; /* 1 rows. */
  double *new_EF_10; /* 1 rows. */
  double *new_EF_11; /* 1 rows. */
  double *new_EF_12; /* 1 rows. */
  double *new_EF_13; /* 1 rows. */
  double *new_EF_14; /* 1 rows. */
  double *new_EF_15; /* 1 rows. */
  double *new_EF_16; /* 1 rows. */
  double *new_EF_17; /* 1 rows. */
  double *new_EF_18; /* 1 rows. */
  double *new_EF_19; /* 1 rows. */
  double *new_EF_20; /* 1 rows. */
  double *new_EF_21; /* 1 rows. */
  double *new_EF_22; /* 1 rows. */
  double *new_EF_23; /* 1 rows. */
  double *new_EF_24; /* 1 rows. */
  double *new_EF_25; /* 1 rows. */
  double *new_EF_26; /* 1 rows. */
  double *t_130; /* 1 rows. */
  double *t_131; /* 1 rows. */
  double *t_132; /* 1 rows. */
  double *t_133; /* 1 rows. */
  double *t_134; /* 1 rows. */
  double *t_135; /* 1 rows. */
  double *t_136; /* 1 rows. */
  double *t_137; /* 1 rows. */
  double *t_138; /* 1 rows. */
  double *t_139; /* 1 rows. */
  double *t_140; /* 1 rows. */
  double *t_141; /* 1 rows. */
  double *t_142; /* 1 rows. */
  double *t_143; /* 1 rows. */
  double *t_144; /* 1 rows. */
  double *t_145; /* 1 rows. */
  double *t_146; /* 1 rows. */
  double *t_147; /* 1 rows. */
  double *t_148; /* 1 rows. */
  double *t_149; /* 1 rows. */
  double *t_150; /* 1 rows. */
  double *t_151; /* 1 rows. */
  double *t_152; /* 1 rows. */
  double *t_153; /* 1 rows. */
  double *t_154; /* 1 rows. */
  double *t_155; /* 1 rows. */
  double *t_156; /* 1 rows. */
  double *t_157; /* 1 rows. */
  double *t_158; /* 1 rows. */
  double *omega_1; /* 1 rows. */
  double *omega_2; /* 1 rows. */
  double *omega_3; /* 1 rows. */
  double *omega_4; /* 1 rows. */
  double *omega_5; /* 1 rows. */
  double *omega_6; /* 1 rows. */
  double *omega_7; /* 1 rows. */
  double *omega_8; /* 1 rows. */
  double *omega_9; /* 1 rows. */
  double *omega_10; /* 1 rows. */
  double *omega_11; /* 1 rows. */
  double *omega_12; /* 1 rows. */
  double *omega_13; /* 1 rows. */
  double *omega_14; /* 1 rows. */
  double *omega_15; /* 1 rows. */
  double *omega_16; /* 1 rows. */
  double *omega_17; /* 1 rows. */
  double *omega_18; /* 1 rows. */
  double *omega_19; /* 1 rows. */
  double *omega_20; /* 1 rows. */
  double *omega_21; /* 1 rows. */
  double *omega_22; /* 1 rows. */
  double *omega_23; /* 1 rows. */
  double *omega_24; /* 1 rows. */
  double *omega_25; /* 1 rows. */
  double *omega_26; /* 1 rows. */
  double *w[26];
  double *delta_alpha[26];
  double *d[27];
  double *new_EF[27];
  double *omega[27];
} Vars;
typedef struct Workspace_t {
  double h[555];
  double s_inv[555];
  double s_inv_z[555];
  double b[129];
  double q[314];
  double rhs[1553];
  double x[1553];
  double *s;
  double *z;
  double *y;
  double lhs_aff[1553];
  double lhs_cc[1553];
  double buffer[1553];
  double buffer2[1553];
  double KKT[3172];
  double L[2456];
  double d[1553];
  double v[1553];
  double d_inv[1553];
  double gap;
  double optval;
  double ineq_resid_squared;
  double eq_resid_squared;
  double block_33[1];
  /* Pre-op symbols. */
  double quad_826163765248[1];
  double quad_270107893760[1];
  double quad_392898023424[1];
  double quad_354214375424[1];
  double quad_445352677376[1];
  double quad_702230208512[1];
  double quad_335242391552[1];
  double quad_175950630912[1];
  double quad_332149305344[1];
  double quad_17263370240[1];
  double quad_302248755200[1];
  double quad_977458573312[1];
  double quad_579963990016[1];
  double quad_487442513920[1];
  double quad_648024653824[1];
  double quad_910448377856[1];
  double quad_341291667456[1];
  double quad_453679702016[1];
  double quad_588369764352[1];
  double quad_610273832960[1];
  double quad_929545818112[1];
  double quad_3726573568[1];
  double quad_566947319808[1];
  double quad_126117388288[1];
  double quad_442566197248[1];
  double quad_565576192000[1];
  double quad_839768125440[1];
  double quad_694648061952[1];
  double quad_338226507776[1];
  double quad_881072074752[1];
  double quad_815321604096[1];
  double quad_409310203904[1];
  double quad_748685393920[1];
  double quad_671950053376[1];
  double quad_46587228160[1];
  double quad_266521808896[1];
  double quad_986003238912[1];
  double quad_585389805568[1];
  double quad_660241698816[1];
  double quad_59129753600[1];
  double quad_749055442944[1];
  double quad_348638896128[1];
  double quad_628438679552[1];
  double quad_782724800512[1];
  double quad_906353664000[1];
  double quad_41312305152[1];
  double quad_232271880192[1];
  double quad_786671919104[1];
  double quad_352455172096[1];
  double quad_326244257792[1];
  double quad_388870012928[1];
  double quad_564407173120[1];
  double quad_607032025088[1];
  double quad_588796252160[1];
  double quad_631891841024[1];
  double quad_46308270080[1];
  double quad_74921062400[1];
  double quad_587445669888[1];
  double quad_271313948672[1];
  double quad_274730606592[1];
  double quad_496943005696[1];
  double quad_223001780224[1];
  double quad_548109938688[1];
  double quad_137830731776[1];
  double quad_662255759360[1];
  double quad_859892768768[1];
  double quad_772709756928[1];
  double quad_645643497472[1];
  double quad_827707936768[1];
  double quad_513757962240[1];
  double quad_111384309760[1];
  double quad_891445882880[1];
  double quad_521918545920[1];
  double quad_971924062208[1];
  double quad_930083958784[1];
  double quad_103559696384[1];
  double quad_595768565760[1];
  double quad_182585827328[1];
  double quad_261409124352[1];
  double quad_885751197696[1];
  double quad_310504296448[1];
  double frac_440034369536;
  double frac_442821914624;
  double frac_74805100544;
  double frac_432494084096;
  double frac_286396239872;
  double frac_57120047104;
  double frac_184596529152;
  double frac_280580362240;
  double frac_268329107456;
  double frac_813883129856;
  double frac_716272693248;
  double frac_249440423936;
  double frac_258379026432;
  double frac_285372968960;
  double frac_230137634816;
  double frac_266644762624;
  double frac_453918560256;
  double frac_874774331392;
  double frac_378318163968;
  double frac_972859576320;
  double frac_531304456192;
  double frac_126340628480;
  double frac_148170981376;
  double frac_630636609536;
  double frac_730757550080;
  double frac_127025135616;
  int converged;
} Workspace;
typedef struct Settings_t {
  double resid_tol;
  double eps;
  int max_iters;
  int refine_steps;
  int better_start;
  /* Better start obviates the need for s_init and z_init. */
  double s_init;
  double z_init;
  int verbose;
  /* Show extra details of the iterative refinement steps. */
  int verbose_refinement;
  int debug;
  /* For regularization. Minimum value of abs(D_ii) in the kkt D factor. */
  double kkt_reg;
} Settings;
extern Vars vars;
extern Params params;
extern Workspace work;
extern Settings settings;
/* Function definitions in ldl.c: */
void ldl_solve(double *target, double *var);
void ldl_factor(void);
double check_factorization(void);
void matrix_multiply(double *result, double *source);
double check_residual(double *target, double *multiplicand);
void fill_KKT(void);

/* Function definitions in matrix_support.c: */
void multbymA(double *lhs, double *rhs);
void multbymAT(double *lhs, double *rhs);
void multbymG(double *lhs, double *rhs);
void multbymGT(double *lhs, double *rhs);
void multbyP(double *lhs, double *rhs);
void fillq(void);
void fillh(void);
void fillb(void);
void pre_ops(void);

/* Function definitions in solver.c: */
double eval_gap(void);
void set_defaults(void);
void setup_pointers(void);
void setup_indexed_params(void);
void setup_indexed_optvars(void);
void setup_indexing(void);
void set_start(void);
double eval_objv(void);
void fillrhs_aff(void);
void fillrhs_cc(void);
void refine(double *target, double *var);
double calc_ineq_resid_squared(void);
double calc_eq_resid_squared(void);
void better_start(void);
void fillrhs_start(void);
long solve(void);

/* Function definitions in testsolver.c: */
int main(int argc, char **argv);
void load_default_data(void);

/* Function definitions in util.c: */
void tic(void);
float toc(void);
float tocq(void);
void printmatrix(char *name, double *A, int m, int n, int sparse);
double unif(double lower, double upper);
float ran1(long*idum, int reset);
float randn_internal(long *idum, int reset);
double randn(void);
void reset_rand(void);

#endif
