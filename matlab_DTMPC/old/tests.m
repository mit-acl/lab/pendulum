%% Clear stuff
clear all;
try
    clf(1);
%     clf(2);
%     clf(3);
%     clf(4);
%     clf(5);
end

%% Initial Inputs
m = 1; % number of inputs
n = 2; % number of statesss
T = 25; % time horizon

d0 = [0; 0]; % initial d state, which is always zero cause it's x - xk
k = 0; %number of times we repeated sqp

% initial guess - all zeros, nothing is  happening
g = 9.81; % m/s2
mass = 0.214; % kg
L_cm = 0.0921; % m, cm of pendulum
L = 0.229; % m, where torque is applied
I = 0.00417; % kgm2 inertia; assuming that all mass is at the tip
Cd = 0.000077; % estimated drag

%% -------------------------------CHANGE ME
figure(5); sgtitle("From 0 to pi/2, disturbance applied always, low omega");
delta_t = 0.012; % time between consecutive steps
num_traj = 1;
apply_disturb = 0.0;
gain_multiplier = 20;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Changeable Values        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
w_max = 1; % maximum w sqp can use
S = 2; % slew rate; maximum delta w
Q = 1 * [100 0; 0 0.001]; % state cost
R = 0.01 * eye(1); % input cost
% R_delta = 0.1 * eye(1); % input cost
R_delta = 0.1 * eye(1); % input cost
M = 0.01*eye(1); % cost function for alpha
% Q_final = 4*eye(2); %terminal state cost
Q_final = 1*100 * [1 0; 0 1];
% Q_final = Q;

x_global_final = [pi/2; 0]; %final location
x_final = x_global_final; %final location
x_global_0 = [0; 0];
x_0 = x_global_0; %current global state, angle and angular speed


u_max= 1.8; % N, maximum applied thrust
x_dot_bound = 3*pi; % rad/s, max angualr speed
omega_max = 5*pi/180; % maximum tube radius
% omega_idle = 30*pi/180; % maximum tube radius
% alpha_min = 0.4; 
alpha_min = 0.4; 
alpha_max = alpha_min * 92;
delta_alpha_max = 5;
delta_alpha_final = 100;
alpha_final = [5, 5, 5];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DTMPC stuff
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disturbance = zeros(1, T+1); % disturbance i from 0 to T 
omega_0 = 0*pi/180; % initial radius, in radians
Cd_max = 0.005;
% Cd_max = 0.0;

% disturb  = 0;  %1/s2
disturb = 20; %  1/s2 
nu = 0.55;
% nu = 0;
D = 5; % 1/s2 max disturbance 
% EF_0 = 2; 
EF_0 = 3; 
lambda = 1*(nu + D)/(alpha_min* omega_max); % sliding variable lambda
disp(lambda)
gain_max = 1000;

%% Setting Permanent Parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

c = delta_t;
c1 = L_cm * mass * g * delta_t / I;
c2 = 2 * Cd * delta_t / I;
Bd = L * delta_t / I;
last_u = 0.0;
last_alpha = alpha_min;

% one time parameter intializations
% params.c = c;
params.delta_alpha_final = delta_alpha_final;
params.alpha_final_0 = alpha_final(1);
params.alpha_final_1 = alpha_final(2);
params.alpha_final_2 = alpha_final(3);
params.c1 = c1;
params.c2 = c2;
params.Bd = Bd;
params.Q = Q;
params.Q_final = Q_final;
params.R = R;
params.R_delta = R_delta;
params.S = S;
params.d_0 = d0;
params.u_max = u_max;
params.w_max = w_max;
params.x_dot_bound = x_dot_bound;
params.x_final = x_final;
% DTMPC
params.Cd_max = Cd_max;
params.nu = nu;
params.D = D;
params.lambda = lambda;
params.omega_max = omega_max;
params.I = I;
params.delta_t = delta_t;
params.alpha_min = alpha_min;
params.alpha_max = alpha_max;
params.delta_alpha_max = delta_alpha_max;
params.M = M;
params.I_over_L = I/ L;
params.gain_max = gain_max;

%% Creating trajectory vars 
% start the trajectories
uk = zeros(m, T+1); % input uk[i] i from 0 to T
xk = zeros(n, T+2); % states xk[i] i from 0 to T+1
abs_xk_2 = abs(xk(2, :)); % absolute value of x2s
cos_xk_1 = cos(xk(1, :)); % cosines of x1s
old_EF = zeros(1, T+2); % 0 to T+1
alpha = zeros(1, T+1); % 0 to T
omega = zeros(1, T+2); % 0 to T+1

world_length = num_traj*(T+1);

time_traj = zeros(1,world_length+1);
u_traj = zeros(1,world_length);
x_traj = zeros(2,world_length+1);
omega_traj = zeros(1,world_length+1);
EF_traj = zeros(1,world_length+1);
alpha_traj = zeros(1,world_length);

% initialize time trajectory
for i=1:(world_length + 1)
  time_traj(1,i) = (i-1) * delta_t;
end

%% DTMPC to create arch trajectory
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%            MPC started
l = 0;
while l < num_traj

  % if not at zeroth iteration - grab initial condition from last iteration
  % - that is, value T+1 of last iteration
  if l > 0
      x_0 = x_traj(:, 1 + l * (T+1));
      omega_0 = omega_traj(1, 1 + l * (T+1));
      alpha_0 = alpha_traj(1, 1 + l * (T+1));
      EF_0 = EF_traj(1, 1 + l * (T+1));
      last_u = u_traj(1, l * (T+1));
      last_alpha = alpha_traj(1, l * (T+1));
  end
  
  uk = zeros(m, T+1); % 0 to T; all generated
  
  xk = zeros(n, T+2); % 0 to T+1; 0 given, rest generated
  xk(:,1) = x_0; % first val
  
  old_EF = zeros(1, T+2); % 0 to T+1; 0 given, rest generated
  for i=1:T+2, old_EF(1,i) = EF_0; end % first val
  
  alpha = zeros(1, T+1); % 0 to T; all generated
  for i=1:T+1, alpha(1,i) = alpha_min; end % first val
  
  omega = zeros(1, T+2); % 0 to T+1; first given, rest generated
  omega(1,1) = omega_0; % first val
  
  
  % calculate the rest according to the model
  for i = 2:T+2
    xk(1, i) = xk(1, i-1) + delta_t * xk(2, i-1); 
    xk(2, i) = (-Cd *abs(xk(2, i-1))*xk(2, i-1) - L_cm * mass*g * sin( xk(1, i-1) ) + L*uk(i-1) ) * delta_t/I + xk(2, i-1);
  end
  abs_xk_2 = abs(xk(2, :)); % absolute value of x2s
  cos_xk_1 = cos(xk(1, :)); % cosines of x1s
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%            SQP started
    
  k = 0;
  settings.verbose = 0;  % disable output of solver progress.
  settings.max_iters = 1000;
  params.last_u = last_u;
  params.last_alpha = last_alpha;
  while k < 10
    % iitialize parameters
    params.uk_0 = uk(:,1);
    params.xk_0 = xk(:,1);
    params.abs_xk_0 = abs_xk_2(1);
    params.cos_xk_0 = cos_xk_1(1);
    params.omega_0 = omega(1, 1);
    params.old_EF_0 = old_EF(1, 1);
    params.new_EF_0 = old_EF(1, 1);
    params.disturbance_0 = disturbance(1, 1);
    params.alpha_0 = alpha(1,1);
    for i = 1:T
        params.uk{i} = uk(:, i+1);
        params.xk{i} = xk(:, i+1);
        params.abs_xk{i} = abs_xk_2(i+1);
        params.cos_xk{i} = cos_xk_1(i+1);
        params.disturbance{i} = disturbance(1, i+1);
        params.old_EF{i} = old_EF(1, i+1);
        params.alpha{i} = alpha(1, i+1);
    end
    params.xk{T+1} = xk(:, T+2);
    params.abs_xk{T+1} = abs_xk_2(T+2);
    params.cos_xk{T+1} = cos_xk_1(T+2);
    params.old_EF{T+1} = old_EF(T+2);

    % run the solver
    [vars, status] = csolve(params, settings); 

    % Check convergence, and display the optimal variable value.
%     if ~status.converged, clear all; error 'failed to converge'; end
    if ~status.converged, disp('failed to converge'); end

    % update teh results
    
    % SQP
    xk(:,2:T+2) = xk(:,2:T+2) + [vars.d{:}];
    uk(:, 1) = uk(:, 1) + vars.w_0;
    uk(:,2:T+1) = uk(:, 2:T+1) + [vars.w{:}];
    % recalculate 
    abs_xk_2 = abs(xk(2, :)); % absolute value of x2sw
    cos_xk_1 = cos(xk(1, :)); % cosines of x1s
    for i=1:T+1
        if xk(1, i) > pi/4
            if (i > T*apply_disturb)
                disturbance(1, i) = disturb * sin(xk(1,i)-pi/4)^2;
            else
                disturbance(1, i) = 0;
            end
            
        end
    end
    
    %%% DTMPC
    % sqp
    alpha(1,1) = alpha(1,1) + vars.delta_alpha_0;
    alpha(1, 2:T+1) = alpha(1, 2:T+1) + [vars.delta_alpha{:}];
    omega(1,1) = omega_0;
    omega(1, 2:T+2) = [vars.omega{:}];
    old_EF(1, 2:T+2) = [vars.new_EF{:}];
    
    %%% recalculate disturbance 
    
    k = k + 1;
  end
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%            SQP COMPLETED
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  u_traj(:, (1+(T+1)*l):(T+1+(T+1)*l) ) = uk;
  x_traj(:, (1+(T+1)*l):(T+2+(T+1)*l) ) = xk;
  alpha_traj(:, (1+(T+1)*l):(T+1+(T+1)*l) ) = alpha;
  omega_traj(:, (1+(T+1)*l):(T+2+(T+1)*l) ) = omega;
  EF_traj(:, (1+(T+1)*l):(T+2+(T+1)*l) ) = old_EF;
  
  
  
  
  
  l = l+1;
%   figure(1); subplot(6,1,1); hold on; plot(time_traj(1:T+1), uk(1:T+1), 'r', 'LineWidth', 3); xlabel('time, seconds'); ylabel('u, N'); xlim([0 0.7]);
%   figure(1); subplot(6,1,2); hold on; plot(time_traj(1:T+2), 180/pi*xk(1, 1:T+2),'r', 'LineWidth', 3 ); xlabel('time, seconds'); ylabel('theta, degrees');xlim([0 0.7]);
%   figure(1); subplot(6,1,3); hold on; plot(time_traj(1:T+2), 180/pi*xk(2, 1:T+2), 'LineWidth', 3); ylabel('theta dot');xlim([0 0.7]);
%   
%   figure(1); subplot(6,1,4); hold on; plot(time_traj(1:T+2), old_EF(1, 1:T+2), 'LineWidth', 3); ylabel('EF');xlim([0 0.7]);
%   figure(1); subplot(6,1,5); hold on; plot(time_traj(1:T+2), 180/pi*omega(1, 1:T+2), 'LineWidth', 3); ylabel('OMEGA');xlim([0 0.7]);
%   figure(1); subplot(6,1,6); hold on; plot(time_traj(1:T+1), alpha(1, 1:T+1), 'LineWidth', 3); ylabel('alpha');xlim([0 0.7]);

end

subplot(6,3,1); hold on; plot(time_traj(1:world_length), u_traj(1:world_length), 'r', 'LineWidth', 3); xlabel('time, seconds'); ylabel('u, N'); xlim([0 time_traj(world_length+1)]);
subplot(6,3,4); hold on; plot(time_traj(1:world_length+1), 180/pi*x_traj(1, 1:world_length+1),'r', 'LineWidth', 3 ); xlabel('time, seconds'); ylabel('theta, degrees');xlim([0 time_traj(world_length+1)]);
subplot(6,3,4); hold on; plot(time_traj(1:world_length+1), 180/pi*(x_traj(1, 1:world_length+1)+omega_traj(1:world_length+1)),'b', 'LineWidth', 2 ); xlabel('time, seconds'); ylabel('theta, degrees');xlim([0 time_traj(world_length+1)]);
subplot(6,3,4); hold on; plot(time_traj(1:world_length+1), 180/pi*(x_traj(1, 1:world_length+1)-omega_traj(1:world_length+1)),'b', 'LineWidth', 2 ); xlabel('time, seconds'); ylabel('theta, degrees');xlim([0 time_traj(world_length+1)]);

subplot(6,3,7); hold on; plot(time_traj(1:world_length+1), 180/pi*x_traj(2, 1:world_length+1), 'r','LineWidth', 3); ylabel('theta dot');xlim([0 time_traj(world_length+1)]);

subplot(6,3,10); hold on; plot(time_traj(1:world_length+1), EF_traj(1, 1:world_length+1), 'r','LineWidth', 3); ylabel('EF');xlim([0 time_traj(world_length+1)]);
subplot(6,3,13); hold on; plot(time_traj(1:world_length+1), 180/pi*omega_traj(1, 1:world_length+1), 'r','LineWidth', 3); ylabel('OMEGA');xlim([0 time_traj(world_length+1)]);
subplot(6,3,16); hold on; plot(time_traj(1:world_length), alpha_traj(1, 1:world_length),'r', 'LineWidth', 3); ylabel('alpha');xlim([0 time_traj(world_length+1)]);
 
%% Initial Inputs
% m = 1; % number of inputs
% n = 2; % number of statesss
% T = 25; % time horizon
d0 = [0; 0]; % initial d state, which is always zero cause it's x - xk
k = 0; %number of times we repeated sqp
% initial guess - all zeros, nothing is  happening
% g = 9.81; % m/s2
% mass = 0.214; % kg
% L_cm = 0.0921; % m, cm of pendulum
% L = 0.229; % m, where torque is applied
% I = 0.00417; % kgm2 inertia; assuming that all mass is at the tip
% Cd = 0.000077; % estimated drag

%% -------------------------------CHANGE ME
inner_steps = 5;
delta_t = delta_t/inner_steps; % time between consecutive steps
num_traj = 8;


c = delta_t;
c1 = L_cm * mass * g * delta_t / I;
c2 = 2 * Cd * delta_t / I;
Bd = L * delta_t / I;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Arbitrarily chosen values        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
w_max = 1; % maximum w sqp can use
S = 0.5; % slew rate; maximum delta w
% Q = 1 * [100 0; 0 0.001]; % state cost
% R = 0.01 * eye(1); % input cost
% R_delta = 0.01 * eye(1); % input cost
% M = 0.1*eye(1); % cost function for alpha
% Q_final = 4*eye(2); %terminal state cost
% Q_final = 1*100 * [1 0; 0 1];
% Q_final = Q;

% x_final = [pi/2; 0]; %final location
% x_0 = [0; 0]; %current global state, angle and angular speed


% u_max= 1.8; % N, maximum applied thrust
% x_dot_bound = 3*pi; % rad/s, max angualr speed
% omega_max = 15*pi/180; % maximum tube radius
% omega_idle = 30*pi/180; % maximum tube radius
% alpha_min = 1; 
% alpha_max = alpha_min * 92;
delta_alpha_max = 0.15;
delta_alpha_final = 0.001;
% alpha_final = alpha_min;
params.delta_alpha_final = delta_alpha_final;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DTMPC stuff
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disturbance = zeros(1, T+1); % disturbance i from 0 to T 
% omega_0 = 10*pi/180; % initial radius, in radians
% Cd_max = 0.005;
% Cd_max = 0.0;

%%%%%%%%%%%%%%%%%%
% disturb  = 0;  %1/s2
% disturb  = 20; %  1/s2 
% CHANGE ME
% nu = 0.55;
% nu = 0;
% D = 5; % 1/s2 max disturbance 
% EF_0 = 2; 
% lambda = (nu + D)/(alpha_min* omega_max); % sliding variable lambda
% gain_max = 1000;

%% Setting Permanent Params
last_u = 0.0;
last_alpha = alpha_min;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
uk = zeros(m, T+1); % input uk[i] i from 0 to T
xk = zeros(n, T+2); % states xk[i] i from 0 to T+1
abs_xk_2 = abs(xk(2, :)); % absolute value of x2s
cos_xk_1 = cos(xk(1, :)); % cosines of x1s
old_EF = zeros(1, T+2); % 0 to T+1
alpha = zeros(1, T+1); % 0 to T
omega = zeros(1, T+2); % 0 to T+1



% one time parameter intializations
% params.c = c;
params.c1 = c1;
params.c2 = c2;
params.Bd = Bd;
params.Q = Q;
params.Q_final = Q_final;
params.R = R;
params.R_delta = R_delta;
params.S = S;
params.d_0 = d0;
params.u_max = u_max;
params.w_max = w_max;
params.x_dot_bound = x_dot_bound;
params.x_final = x_final;
% DTMPC
params.Cd_max = Cd_max;
params.nu = nu;
params.D = D;
params.lambda = lambda;
params.omega_max = omega_max;
params.I = I;
params.delta_t = delta_t;
params.alpha_min = alpha_min;
params.alpha_max = alpha_max;
params.delta_alpha_max = delta_alpha_max;
params.M = M;
params.I_over_L = I/ L;
params.gain_max = gain_max;

%% Creating trajectory vars
% start the trajectories
long_world_length = num_traj*(T+1);
% long_world_length = world_length * 5;
long_time_traj = zeros(1,long_world_length+1);
long_u_traj = zeros(1,long_world_length);
long_x_traj = zeros(2,long_world_length+1);
long_omega_traj = zeros(1,long_world_length+1);
long_EF_traj = zeros(1,long_world_length+1);
long_alpha_traj = zeros(1,long_world_length);



% initialize time trajectory
for i=1:(long_world_length + 1)
  long_time_traj(1,i) = (i-1) * delta_t;
end

%% LONG DTMPC
l = 0;
x_final = x_traj(:, 1 + l*5 + 5);%final location
x_0 = x_traj(:, 1 + l * 5); %current global state, angle and angular speed
omega_0 = omega_traj(1, 1);
alpha_0 = alpha_traj(1, 1);
EF_0 = EF_traj(1, 1);
last_u = u_traj(1, 1);
last_alpha = alpha_traj(1, 1);

index_start = 1;
index_short = 6;
while l < num_traj+5
  disp(l)
  % if not at zeroth iteration - grab initial condition from last iteration
  % - that is, value T+1 of last iteration
  if l > 0
      x_0 = long_x_traj(:, index_start);
      omega_0 = long_omega_traj(1, index_start);
      alpha_0 = long_alpha_traj(1, index_start);
      EF_0 = long_EF_traj(1, index_start);
      last_u = long_u_traj(1, index_start-1);
      last_alpha = long_alpha_traj(1, index_start-1);
  end
  
  % set the final values
  if index_short < 24
    x_final = x_traj(:, index_short);%final location
    alpha_final = [alpha_traj(:, index_short-4), alpha_traj(:, index_short-3), alpha_traj(:, index_short-2)];
    params.alpha_final_0 = alpha_final(1);
    params.alpha_final_1 = alpha_final(2);
    params.alpha_final_2 = alpha_final(3);
    params.delta_alpha_final = 0.01;
  end
  if index_short >= 24
      x_final = x_global_final;%final location
      delta_alpha_final = 20.0;
%       alpha_final = alpha_min;
      params.delta_alpha_final = delta_alpha_final;
  end
  
  
  % RESET THE ARRAY PARAMETERS
  uk = zeros(m, T+1); % 0 to T; all generated
  xk = zeros(n, T+2); % 0 to T+1; 0 given, rest generated
  xk(:,1) = x_0; % first val
  
  old_EF = zeros(1, T+2); % 0 to T+1; 0 given, rest generated
  for i=1:T+2, old_EF(1,i) = EF_0; end % first val
  
  alpha = zeros(1, T+1); % 0 to T; all generated
  for i=1:T+1, alpha(1,i) = alpha_min; end % first val
  
  omega = zeros(1, T+2); % 0 to T+1; first given, rest generated
  omega(1,1) = omega_0; % first val
  
  
  % calculate the rest according to the model
  for i = 2:T+2
    xk(1, i) = xk(1, i-1) + delta_t * xk(2, i-1); 
    xk(2, i) = (-Cd *abs(xk(2, i-1))*xk(2, i-1) - L_cm * mass*g * sin( xk(1, i-1) ) + L*uk(i-1) ) * delta_t/I + xk(2, i-1);
  end
  abs_xk_2 = abs(xk(2, :)); % absolute value of x2s
  cos_xk_1 = cos(xk(1, :)); % cosines of x1s
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%            SQP started
    
  k = 0;
  settings.verbose = 0;  % disable output of solver progress.
  settings.max_iters = 1000;
  params.last_u = last_u;
  params.last_alpha = last_alpha;
  while k < 10
    % iitialize parameters
    params.x_final = x_final;
    params.uk_0 = uk(:,1);
    params.xk_0 = xk(:,1);
    params.abs_xk_0 = abs_xk_2(1);
    params.cos_xk_0 = cos_xk_1(1);
    params.omega_0 = omega(1, 1);
    params.old_EF_0 = old_EF(1, 1);
    params.new_EF_0 = old_EF(1, 1);
    params.disturbance_0 = disturbance(1, 1);
    params.alpha_0 = alpha(1,1);
    for i = 1:T
        params.uk{i} = uk(:, i+1);
        params.xk{i} = xk(:, i+1);
        params.abs_xk{i} = abs_xk_2(i+1);
        params.cos_xk{i} = cos_xk_1(i+1);
        params.disturbance{i} = disturbance(1, i+1);
        params.old_EF{i} = old_EF(1, i+1);
        params.alpha{i} = alpha(1, i+1);
    end
    params.xk{T+1} = xk(:, T+2);
    params.abs_xk{T+1} = abs_xk_2(T+2);
    params.cos_xk{T+1} = cos_xk_1(T+2);
    params.old_EF{T+1} = old_EF(T+2);

    % run the solver
    [vars, status] = csolve(params, settings); 

    % Check convergence, and display the optimal variable value.
    if ~status.converged, disp('failed to converge'); end

    % update teh results
    
    % SQP
    xk(:,2:T+2) = xk(:,2:T+2) + [vars.d{:}];
    uk(:, 1) = uk(:, 1) + vars.w_0;
    uk(:,2:T+1) = uk(:, 2:T+1) + [vars.w{:}];
    % recalculate 
    abs_xk_2 = abs(xk(2, :)); % absolute value of x2sw
    cos_xk_1 = cos(xk(1, :)); % cosines of x1s
    for i=1:T+1
        if xk(1, i) > pi/4
            if (l > (num_traj + 5) * apply_disturb)
                disturbance(1, i) = disturb * sin(xk(1,i)-pi/4)^2;
            else
                disturbance(1, i) = 0;
            end
            
        end
    end
    
    %%% DTMPC
    % sqp
    alpha(1,1) = alpha(1,1) + vars.delta_alpha_0;
    alpha(1, 2:T+1) = alpha(1, 2:T+1) + [vars.delta_alpha{:}];
    omega(1,1) = omega_0;
    omega(1, 2:T+2) = [vars.omega{:}];
    old_EF(1, 2:T+2) = [vars.new_EF{:}];
    
    %%% recalculate disturbance 
    
    k = k + 1;
  end
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%            SQP COMPLETED
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  long_u_traj(:, index_start:(T+index_start) ) = uk;
  long_x_traj(:, index_start:(T+1+index_start) ) = xk;
  long_alpha_traj(:, index_start:(T+index_start) ) = alpha;
  long_omega_traj(:, index_start:(T+1+index_start) ) = omega;
  long_EF_traj(:, index_start:(T+1+index_start) ) = old_EF;
  index_start = index_start + 15;
  index_short = index_short + 3;
  
  
  
  
  l = l+1;
  
%   figure(2); subplot(6,1,1); hold on; plot(long_time_traj(1:long_world_length), long_u_traj(1:long_world_length), 'r', 'LineWidth', 3); xlabel('time, seconds'); ylabel('u, N'); xlim([0 long_time_traj(long_world_length+1)]);
%   figure(2); subplot(6,1,2); hold on; plot(long_time_traj(1:long_world_length+1), 180/pi*long_x_traj(1, 1:long_world_length+1),'r', 'LineWidth', 3 ); xlabel('time, seconds'); ylabel('theta, degrees');xlim([0 long_time_traj(long_world_length+1)]);
%   figure(2); subplot(6,1,2); hold on; plot(long_time_traj(1:long_world_length+1), 180/pi*(long_x_traj(1, 1:long_world_length+1)+long_omega_traj(1:long_world_length+1)),'b', 'LineWidth', 2 ); xlabel('time, seconds'); ylabel('theta, degrees');xlim([0 long_time_traj(long_world_length+1)]);
%   figure(2); subplot(6,1,2); hold on; plot(long_time_traj(1:long_world_length+1), 180/pi*(long_x_traj(1, 1:long_world_length+1)-long_omega_traj(1:long_world_length+1)),'b', 'LineWidth', 2 ); xlabel('time, seconds'); ylabel('theta, degrees');xlim([0 long_time_traj(long_world_length+1)]);
% 
%   figure(2); subplot(6,1,3); hold on; plot(long_time_traj(1:long_world_length+1), 180/pi*long_x_traj(2, 1:long_world_length+1), 'LineWidth', 3); ylabel('theta dot');xlim([0 long_time_traj(long_world_length+1)]);
% 
%   figure(2); subplot(6,1,4); hold on; plot(long_time_traj(1:long_world_length+1), long_EF_traj(1, 1:long_world_length+1), 'LineWidth', 3); ylabel('EF');xlim([0 long_time_traj(long_world_length+1)]);
%   figure(2); subplot(6,1,5); hold on; plot(long_time_traj(1:long_world_length+1), 180/pi*long_omega_traj(1, 1:long_world_length+1), 'LineWidth', 3); ylabel('OMEGA');xlim([0 long_time_traj(long_world_length+1)]);
%   figure(2); subplot(6,1,6); hold on; plot(long_time_traj(1:long_world_length), long_alpha_traj(1, 1:long_world_length), 'LineWidth', 3); ylabel('alpha');xlim([0 long_time_traj(long_world_length+1)]);

end
subplot(6,3,2); hold on; plot(long_time_traj(1:long_world_length), long_u_traj(1:long_world_length), 'r', 'LineWidth', 3); xlabel('time, seconds'); ylabel('u, N'); xlim([0 long_time_traj(long_world_length+1)-0.02]);
subplot(6,3,5); hold on; plot(long_time_traj(1:long_world_length+1), 180/pi*long_x_traj(1, 1:long_world_length+1),'r', 'LineWidth', 3 ); xlabel('time, seconds'); ylabel('theta, degrees');xlim([0 long_time_traj(long_world_length+1)-0.02]);
subplot(6,3,5); hold on; plot(long_time_traj(1:long_world_length+1), 180/pi*(long_x_traj(1, 1:long_world_length+1)+long_omega_traj(1:long_world_length+1)),'b', 'LineWidth', 2 ); xlabel('time, seconds'); ylabel('theta, degrees');xlim([0 long_time_traj(long_world_length+1)-0.02]);
subplot(6,3,5); hold on; plot(long_time_traj(1:long_world_length+1), 180/pi*(long_x_traj(1, 1:long_world_length+1)-long_omega_traj(1:long_world_length+1)),'b', 'LineWidth', 2 ); xlabel('time, seconds'); ylabel('theta, degrees');xlim([0 long_time_traj(long_world_length+1)-0.02]);

subplot(6,3,8); hold on; plot(long_time_traj(1:long_world_length+1), 180/pi*long_x_traj(2, 1:long_world_length+1), 'r','LineWidth', 3); ylabel('theta dot');xlim([0 long_time_traj(long_world_length+1)-0.02]);

subplot(6,3,11); hold on; plot(long_time_traj(1:long_world_length+1), long_EF_traj(1, 1:long_world_length+1), 'r','LineWidth', 3); ylabel('EF');xlim([0 long_time_traj(long_world_length+1)-0.02]);
subplot(6,3,14); hold on; plot(long_time_traj(1:long_world_length+1), 180/pi*long_omega_traj(1, 1:long_world_length+1), 'r','LineWidth', 3); ylabel('OMEGA');xlim([0 long_time_traj(long_world_length+1)-0.02]);
subplot(6,3,17); hold on; plot(long_time_traj(1:long_world_length), long_alpha_traj(1, 1:long_world_length), 'r','LineWidth', 3); ylabel('alpha');xlim([0 long_time_traj(long_world_length+1)-0.02]);

%% Simulation


% x_traj and u_traj are the trajectories

bandw = 2;
dt = delta_t / bandw; % run the controller at 5 times the speed 
delta_world = 5;
dt_world = dt/delta_world;

% run the world five times faster than the controller

%initializing variables
wlen = long_world_length * bandw;
u_proper = zeros(1, wlen);
u_sliding = zeros(1, wlen);
u = zeros(1, wlen);
u_ = 0;
x = zeros(2, wlen+1);
x(:,1) = x_global_0;
s = zeros(1, wlen+1);
k = zeros(1, wlen+1);
time = zeros(1,wlen);
for i=1:wlen
  time(i) = (i-1) * dt;
end
tnow_ = 0.0;
i = 1;

while i <= wlen
    u_ = 0.0;
    %figure out which control we should  take
    index = 1;
    while long_time_traj(index)-0.0001 <= tnow_
       index = index + 1; 
    end
    index = index - 1;

    % mpc component
    u_proper(i) = long_u_traj(index);
    
    % differentially figure out desired values for x
    x_des = long_x_traj(1,index) + (long_x_traj(1,index+1) - long_x_traj(1,index)) / delta_t * (tnow_ - long_time_traj(index));
    x_dot_des = long_x_traj(2,index) + (long_x_traj(2,index+1) - long_x_traj(2,index)) / delta_t * (tnow_ - long_time_traj(index));
    
    % compute the sliding control part 
    s(1,i) = (x(2, i) - x_dot_des ) + lambda * ( x(1, i) - x_des );
    k(1,i) = long_alpha_traj(1, index) * long_EF_traj(1, index)/gain_multiplier;
    u_sliding(i) = - k(1, i)/L *   sat(s(1, i)/long_EF_traj(1, index));
    u_ = u_proper(i) + u_sliding(i);
    if u_ < -1.8, u_ = -1.8; end
    if u_ > 1.8, u_ = 1.8; end
    %introduce noise
%     u_ = u_+0.1;
    u(i) = u_;
    %apply the physics - increment,with five steps in between
    x1_temp = x(1, i); % + 1/2*pi/180; %* (rand()-0.5);
    x2_temp = x(2, i);
    for j=1:delta_world
        temp_disturbance = 0;
        x1_temp2 = x1_temp + dt_world * x2_temp;
        if x1_temp > pi/4
            if tnow_  > time(wlen) * apply_disturb;
                temp_disturbance =  0.8  * disturb * sin(x1_temp-pi/4)^2;
%                 disturbance(1, i) = disturb * sin(xk(1,i)-pi/4)^2;
            end
            
        end
        x2_temp = (-Cd *abs(x2_temp)*x2_temp - L_cm * mass*g * sin(x1_temp) + L*u_ +  10 * I * D * 2 * (rand()-0.5) + I * temp_disturbance ) * dt_world/I + x2_temp; % +0.3*(rand()-0.5);
        x1_temp = x1_temp2;
    end
    x(1, i+1) = x1_temp;
    x(2, i+1) = x2_temp;
    tnow_ = tnow_ + dt;
    i = i + 1;
end

subplot(6,3,3); hold on; plot(time(1:wlen), u(1:wlen), 'r', 'LineWidth', 3); xlabel('time, seconds'); ylabel('u, N'); xlim([0 time(wlen)-0.02]);
subplot(6,3,6); hold on; plot(time(1:wlen), 180/pi*x(1, 1:wlen), 'r','LineWidth', 3 ); xlabel('time, seconds'); ylabel('theta, degrees'); xlim([0 time(wlen)-0.02]);
subplot(6,3,9); hold on; plot(time(1:wlen), 180/pi*x(2, 1:wlen), 'r', 'LineWidth', 3); ylabel('theta dot'); xlim([0 time(wlen)-0.02]);
subplot(6,3,12); hold on; plot(time(1:wlen), u_sliding(1:wlen), 'r', 'LineWidth', 3); ylabel('u sliding');xlim([0 time(wlen)-0.02]); ylim([-2 2]);
subplot(6,3,15); hold on; plot(time(1:wlen), s(1:wlen), 'r','LineWidth', 3); ylabel('s, sliding var');xlim([0 time(wlen)-0.02]); 
subplot(6,3,18); hold on; plot(time(1:wlen), k(1:wlen), 'r','LineWidth', 3); ylabel('k, gain');xlim([0 time(wlen)-0.02]); 


%%
function res = sat(a)
    if a <= -1
        res = -1;
    elseif a >= 1 
        res = 1;
    else 
        res = a;
    end 
end





