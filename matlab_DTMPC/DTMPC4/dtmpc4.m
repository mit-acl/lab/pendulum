%% Clear stuff
clear all;
try
    clf(1);
    clf(2);
    clf(3);
%     clf(4);
%     clf(5);
end

%% Initial Inputs
T = 39; % time horizon
m = 1;
n = 2;
d0 = [0; 0]; % initial d state, which is always zero cause it's x - xk
k = 0; %number of times we repeated sqp

% initial guess - all zeros, nothing is  happening
g = 9.81; % m/s2
mass = 0.214; % kg
L_cm = 0.0921; % m, cm of pendulum
L = 0.229; % m, where torque is applied
I = 0.00417; % kgm2 inertia; assuming that all mass is at the tip
Cd = 0.000077; % estimated drag

%% -------------------------------CHANGE ME
figure(1); sgtitle("From 0 to pi/2, disturbance applied always, low omega");
delta_t = 0.005; % time between consecutive steps
num_traj = 1;
apply_disturb = 0.0;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Changeable Values        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Q = 1 * [100 0; 0 0.01]; % state cost
% Q = 1 * [100 0; 0 0.0]; % state cost
R = 0.01 * eye(1); % input cost
M = 0.01*eye(1); % cost function for alpha
Q_final = 1*100 * [1 0; 0 0.1];
% Q_final = 1*100 * [1 0; 0 0];

x_global_final = [pi; 0]; %final location
x_final = x_global_final; %final location
x_global_0 = [0*pi/180; 0*pi/180];
% x_global_0 = [85*pi/180; 301*pi/180];
x_0 = x_global_0; %current global state, angle and angular speed

w_max = 0.5; % maximum w sqp can use
u_max= 1.8; % N, maximum applied thrust
delta_u_max = 2; % N, maximum delta u
x_dot_max = 10*pi; % rad/s; max angular speed
omega_max = 10*pi/180; % rad; maximum tube radius - radius of maximum uncertainty realization
alpha_min = 4;  % rad/s;  minimum bandwidth
alpha_max = 25; % rad/s; maximum  bandwidth
delta_alpha_max = 3; % rad/s; maximum change in bandwidth between concequent steps
% delta_alpha_slew

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DTMPC stuff
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disturbance = zeros(1, T+1); %  i from 0 to T 
omega_0 = 0*pi/180; % initial radius, in radians
Cd_max = 0.001;

disturb = 20;  %1/s2
% disturb = 0;  %1/s2
nu = 1;
D = 5; % 1/s2 max disturbance 
% D = 0;
EF_0 = 6; 
lambda = 60; % sliding variable lambda
% lambda = 1*(nu + D)/(alpha_min* omega_max); % sliding variable lambd
% lambda = 0;
% lambda

%% Setting Permanent Parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

c1 = L_cm * mass * g * delta_t / I;
c2 = 2 * Cd * delta_t / I;
Bd = L * delta_t / I;

% one time parameter intializations
params.c1 = c1;
params.c2 = c2;
params.Bd = Bd;
params.Q = Q;
params.Q_final = Q_final;
params.M = M;
params.R = R;
params.delta_u_max = delta_u_max;
params.d_0 = d0;
params.u_max = u_max;
% w_max = 0.5;
params.w_max = w_max;
params.x_dot_max = x_dot_max;
params.x_final = x_final;
% DTMPC
params.Cd_max = Cd_max;
% params.nu = nu;
% params.D = D;
params.lambda = lambda;
params.omega_max = omega_max;
params.I = I;
params.delta_t = delta_t;
params.alpha_min = alpha_min;
params.alpha_max = alpha_max;
params.delta_alpha_max = delta_alpha_max;
params.M = M;
params.I_over_L = I/L;

%% Creating trajectory vars 
% start the trajectories
uk = zeros(m, T+1); % input uk[i] i from 0 to T
xk = zeros(n, T+2); % states xk[i] i from 0 to T+1
abs_xk_2 = abs(xk(2, :)); % absolute value of x2s
cos_xk_1 = cos(xk(1, :)); % cosines of x1s
old_EF = zeros(1, T+2); % 0 to T+1
alpha = zeros(1, T+1); % 0 to T
omega = zeros(1, T+2); % 0 to T+1

uk = zeros(m, T+1); % input uk[i] i from 0 to T
xk = zeros(n, T+2); % states xk[i] i from 0 to T+1
abs_xk_2 = abs(xk(2, :)); % absolute value of x2s
cos_xk_1 = cos(xk(1, :)); % cosines of x1s
old_EF = zeros(1, T+2); % 0 to T+1
alpha = zeros(1, T+1); % 0 to T
omega = zeros(1, T+2); % 0 to T+1


world_length = num_traj*(T+1);

time_traj = zeros(1,world_length+1);
u_traj = zeros(1,world_length);
x_traj = zeros(2,world_length+1);
omega_traj = zeros(1,world_length+1);
EF_traj = zeros(1,world_length+1);
alpha_traj = zeros(1,world_length);

time_traj_use = zeros(1,world_length+1);
u_traj_use = zeros(1,world_length);
x_traj_use = zeros(2,world_length+1);
omega_traj_use = zeros(1,world_length+1);
EF_traj_use = zeros(1,world_length+1);
alpha_traj_use = zeros(1,world_length);


%% Time Constants and params
t_now = 0; % index
Tworld = 1;
Tblsc = 2;
Tdtmpc = 100;
Tend = 800;
x_now = [0; 0];
x_last = [0; 0];
u_now = 0;
u_last = 0;
t_dtmpc_start = 0;
delta_t_int = delta_t*1000;
% Tworld = delta_t_int;
% Tblsc = delta_t_int;
dt_world = Tworld/1000;

Q = 1 * [1 0; 0 0.0]; % state cost
R = 0.1 * eye(1); % input cost
M = 0.1*eye(1); % cost function for alpha
Q_final = 1*10 * [1 0; 0 0];
params.Q = Q;
params.Q_final = Q_final;
params.M = M;
params.R = R;


% generate the initial trajectory!


%% Pre DTMPC; trajec
t_dtmpc_start = t_now;
% initialize time trajectory
for i=1:(world_length + 1)
    time_traj(1,i) = (i-1) * delta_t_int + t_dtmpc_start;
end
%% DTMPC
uk = zeros(m, T+1); % 0 to T; all generated
xk = zeros(n, T+2); % 0 to T+1; 0 given, rest generated
old_EF = zeros(1, T+2); % 0 to T+1; 0 given, rest generated
for i=1:T+2, old_EF(1,i) = EF_0; end % first val
alpha = zeros(1, T+1); % 0 to T; all generated
for i=1:T+1, alpha(1,i) = alpha_min; end % first val
for i=1:T+1, disturbance(1,i) = D + nu; end % first val

omega = zeros(1, T+2); % 0 to T+1; first given, rest generated
omega(1,1) = omega_0; % first val

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%            SQP started
params.last_alpha = alpha_min;
params.last_u = 0.0;
x_0 = x_now;
xk(:,1) = x_0; % first val
k = 0;
settings.verbose = 0;  % disable output of solver progress.
settings.max_iters = 1000;
% calculate the rest according to the model
for i = 2:T+2
    xk(1, i) = xk(1, i-1) + delta_t * xk(2, i-1); 
    xk(2, i) = (-Cd *abs(xk(2, i-1))*xk(2, i-1) - L_cm * mass*g * sin( xk(1, i-1) ) + L*uk(i-1) ) * delta_t/I + xk(2, i-1);
end
abs_xk_2 = abs(xk(2, :)); % absolute value of x2s
cos_xk_1 = cos(xk(1, :)); % cosines of x1s


while k < 20
% iitialize parameters
params.uk_0 = uk(:,1);
params.xk_0 = xk(:,1);
params.abs_xk_0 = abs_xk_2(1);
params.cos_xk_0 = cos_xk_1(1);
params.omega_0 = omega(1, 1);
params.old_EF_0 = old_EF(1, 1);
params.new_EF_0 = old_EF(1, 1);
params.disturbance_0 = disturbance(1, 1);
params.alpha_0 = alpha(1,1);
for i = 1:T
    params.uk{i} = uk(:, i+1);
    params.xk{i} = xk(:, i+1);
    params.abs_xk{i} = abs_xk_2(i+1);
    params.cos_xk{i} = cos_xk_1(i+1);
    params.disturbance{i} = disturbance(1, i+1);
    params.old_EF{i} = old_EF(1, i+1);
    params.alpha{i} = alpha(1, i+1);
end
params.xk{T+1} = xk(:, T+2);
params.abs_xk{T+1} = abs_xk_2(T+2);
params.cos_xk{T+1} = cos_xk_1(T+2);
params.old_EF{T+1} = old_EF(T+2);

% run the solver
[vars, status] = csolve(params, settings); 

% Check convergence, and display the optimal variable value.
if ~status.converged, disp('failed to converge'); end

% update teh results

% SQP
xk(:,2:T+2) = xk(:,2:T+2) + [vars.d{:}];
uk(:, 1) = uk(:, 1) + vars.w_0;
uk(:,2:T+1) = uk(:, 2:T+1) + [vars.w{:}];
% recalculate 
abs_xk_2 = abs(xk(2, :)); % absolute value of x2sw
cos_xk_1 = cos(xk(1, :)); % cosines of x1s
for i=1:T+1
    if xk(1, i) > pi/4 & xk(1, i) < 5 * pi/4
        disturbance(1, i) = disturb * sin(xk(1,i)-pi/4)^2;
    end
    disturbance(1, i) = disturbance(1, i) + D + nu;
end

%%% DTMPC
% sqp
alpha(1,1) = alpha(1,1) + vars.delta_alpha_0;
alpha(1, 2:T+1) = alpha(1, 2:T+1) + [vars.delta_alpha{:}];
omega(1,1) = omega_0;
omega(1, 2:T+2) = [vars.omega{:}];
old_EF(1, 2:T+2) = [vars.new_EF{:}];

%%% recalculate disturbance 

k = k + 1;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%            SQP COMPLETED
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%         u_traj(:, (1+(T+1)*l):(T+1+(T+1)*l) ) = uk;
%         x_traj(:, (1+(T+1)*l):(T+2+(T+1)*l) ) = xk;
%         alpha_traj(:, (1+(T+1)*l):(T+1+(T+1)*l) ) = alpha;
%         omega_traj(:, (1+(T+1)*l):(T+2+(T+1)*l) ) = omega;
%         EF_traj(:, (1+(T+1)*l):(T+2+(T+1)*l) ) = old_EF;
u_traj = uk;
x_traj = xk;
alpha_traj = alpha;
omega_traj = omega;
EF_traj = old_EF;
%% Plot DTMPC
subplot(5,1,1); hold on; plot(time_traj(1:world_length)/1000, u_traj(1:world_length), 'r', 'LineWidth', 3); xlabel('time, seconds'); ylabel('u, N'); xlim([0 Tend/1000]);
%         subplot(6,1,2); hold on; plot(time_traj(1:world_length+1)/1000, 180/pi*x_traj(1, 1:world_length+1),'r', 'LineWidth', 3 ); xlabel('time, seconds'); ylabel('\theta, degrees'); xlim([0 Tend/1000]);
subplot(5,1,2); hold on; plot(time_traj(1:world_length+1)/1000, 180/pi*(x_traj(1, 1:world_length+1)+omega_traj(1:world_length+1)),'r', 'LineWidth', 2 ); xlabel('time, seconds'); ylabel('\theta, degrees'); xlim([0 Tend/1000]);
subplot(5,1,2); hold on; plot(time_traj(1:world_length+1)/1000, 180/pi*(x_traj(1, 1:world_length+1)-omega_traj(1:world_length+1)),'r', 'LineWidth', 2 ); xlabel('time, seconds'); ylabel('\theta, degrees'); xlim([0 Tend/1000]);

subplot(5,1,3); hold on; plot(time_traj(1:world_length+1)/1000, 180/pi*x_traj(2, 1:world_length+1), 'r','LineWidth', 3); ylabel('$\dot{\theta}$, degree/s','interpreter','latex'); xlim([0 Tend/1000]);

subplot(5,1,4); hold on; plot(time_traj(1:world_length+1)/1000, EF_traj(1, 1:world_length+1), 'r','LineWidth', 3); ylabel('\Phi, 1/s'); xlim([0 Tend/1000]);
%         subplot(6,1,5); hold on; plot(time_traj(1:world_length+1)/1000, 180/pi*omega_traj(1, 1:world_length+1), 'r','LineWidth', 3); ylabel('\Omega, degrees'); xlim([0 Tend/1000]);
subplot(5,1,5); hold on; plot(time_traj(1:world_length)/1000, alpha_traj(1, 1:world_length),'r', 'LineWidth', 3); ylabel('\alpha, 1/s'); xlim([0 Tend/1000]);
%% Running 
while t_now <= Tend
    if abs(t_now - 250) <= 0.001
        w_max = 0.2;
        params.w_max = w_max;
        Q = 1 * [1 0; 0 0.1]; % state cost
        R = 0.1 * eye(1); % input cost
        M = 0.1*eye(1); % cost function for alpha
        Q_final = 1*10 * [1 0; 0 1];
        params.Q = Q;
        params.Q_final = Q_final;
        params.M = M;
        params.R = R;
    end
    if mod(t_now, Tdtmpc) <= 0.001
        t_now
        %% Pre DTMPC; trajec
        % the newly used trajectory is the last computed trajectory
        time_traj_use = time_traj;
        u_traj_use = u_traj;
        x_traj_use = x_traj;
        omega_traj_use = omega_traj;
        EF_traj_use = EF_traj;
        alpha_traj_use = alpha_traj;
        % compute the new trajectory
        
        t_dtmpc_start = t_now + Tdtmpc-1;
        % initialize time trajectory
        for i=1:(world_length + 1)
            time_traj(1,i) = (i-1) * delta_t_int + t_dtmpc_start;
        end
        
        uk = zeros(m, T+1); % 0 to T; all generated
        xk = zeros(n, T+2); % 0 to T+1; 0 given, rest generated
        old_EF = zeros(1, T+2); % 0 to T+1; 0 given, rest generated
        alpha = zeros(1, T+1); % 0 to T; all generated
        for i=1:T+1, alpha(1,i) = alpha_min; end % first val
        for i=1:T+1, disturbance(1,i) = D + nu; end % first val

        omega = zeros(1, T+2); % 0 to T+1; first given, rest generated

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%            SQP started
        index = 1;
        while time_traj_use(index)-0.001 <= t_now + Tdtmpc
           index = index + 1; 
        end
        index = index - 1;
        params.last_alpha = alpha_traj_use(index);
        parms.last_u = u_traj_use(index);
        x_0 = x_traj_use(:, index);

        for i=1:T+2, old_EF(1,i) = EF_traj_use(index); end % first val
        for i=1:T+1, alpha(1,i) = alpha_traj_use(index); end % first val
        for i=1:T+1, uk(1,i) = u_traj_use(index); end % all vals 
        omega_0 = omega_traj_use(index);
        omega(1,1) = omega_0; % first val
        
        xk(:,1) = x_0; % first val
        % calculate the rest according to the model
        for i = 2:T+2
            xk(1, i) = xk(1, i-1) + delta_t * xk(2, i-1); 
            xk(2, i) = (-Cd *abs(xk(2, i-1))*xk(2, i-1) - L_cm * mass*g * sin( xk(1, i-1) ) + L*uk(i-1) ) * delta_t/I + xk(2, i-1);
        end
        abs_xk_2 = abs(xk(2, :)); % absolute value of x2s
        cos_xk_1 = cos(xk(1, :)); % cosines of x1s
        %% DTMPC
        k = 0;
        settings.verbose = 0;  % disable output of solver progress.
        settings.max_iters = 1000;
        while k < 20
        % iitialize parameters
            params.uk_0 = uk(:,1);
            params.xk_0 = xk(:,1);
            params.abs_xk_0 = abs_xk_2(1);
            params.cos_xk_0 = cos_xk_1(1);
            params.omega_0 = omega(1, 1);
            params.old_EF_0 = old_EF(1, 1);
            params.new_EF_0 = old_EF(1, 1);
            params.disturbance_0 = disturbance(1, 1);
            params.alpha_0 = alpha(1,1);
            for i = 1:T
                params.uk{i} = uk(:, i+1);
                params.xk{i} = xk(:, i+1);
                params.abs_xk{i} = abs_xk_2(i+1);
                params.cos_xk{i} = cos_xk_1(i+1);
                params.disturbance{i} = disturbance(1, i+1);
                params.old_EF{i} = old_EF(1, i+1);
                params.alpha{i} = alpha(1, i+1);
            end
            params.xk{T+1} = xk(:, T+2);
            params.abs_xk{T+1} = abs_xk_2(T+2);
            params.cos_xk{T+1} = cos_xk_1(T+2);
            params.old_EF{T+1} = old_EF(T+2);

            % run the solver
            [vars, status] = csolve(params, settings); 

            % Check convergence, and display the optimal variable value.
            if ~status.converged, disp('failed to converge'); end

            % update teh results

            % SQP
            xk(:,2:T+2) = xk(:,2:T+2) + [vars.d{:}];
            uk(:, 1) = uk(:, 1) + vars.w_0;
            uk(:,2:T+1) = uk(:, 2:T+1) + [vars.w{:}];
            % recalculate 
            abs_xk_2 = abs(xk(2, :)); % absolute value of x2sw
            cos_xk_1 = cos(xk(1, :)); % cosines of x1s
            for i=1:T+1
                if xk(1, i) > pi/4 & xk(1, i) < 5 * pi / 4
                    disturbance(1, i) = disturb * sin(xk(1,i)-pi/4)^2;
                end
                disturbance(1, i) = disturbance(1, i) + D + nu;
            end

            %%% DTMPC
            % sqp
            alpha(1,1) = alpha(1,1) + vars.delta_alpha_0;
            alpha(1, 2:T+1) = alpha(1, 2:T+1) + [vars.delta_alpha{:}];
            omega(1,1) = omega_0;
            omega(1, 2:T+2) = [vars.omega{:}];
            old_EF(1, 2:T+2) = [vars.new_EF{:}];

            %%% recalculate disturbance 

            k = k + 1;
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%            SQP COMPLETED
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%         u_traj(:, (1+(T+1)*l):(T+1+(T+1)*l) ) = uk;
%         x_traj(:, (1+(T+1)*l):(T+2+(T+1)*l) ) = xk;
%         alpha_traj(:, (1+(T+1)*l):(T+1+(T+1)*l) ) = alpha;
%         omega_traj(:, (1+(T+1)*l):(T+2+(T+1)*l) ) = omega;
%         EF_traj(:, (1+(T+1)*l):(T+2+(T+1)*l) ) = old_EF;
        u_traj = uk;
        x_traj = xk;
        alpha_traj = alpha;
        omega_traj = omega;
        EF_traj = old_EF;
        %% Plot DTMPC
        subplot(5,1,1); hold on; plot(time_traj(1:world_length)/1000, u_traj(1:world_length), 'r', 'LineWidth', 3); xlabel('time, seconds'); ylabel('u, N'); xlim([0 Tend/1000]);
%         subplot(6,1,2); hold on; plot(time_traj(1:world_length+1)/1000, 180/pi*x_traj(1, 1:world_length+1),'r', 'LineWidth', 3 ); xlabel('time, seconds'); ylabel('\theta, degrees'); xlim([0 Tend/1000]);
        subplot(5,1,2); hold on; plot(time_traj(1:world_length+1)/1000, 180/pi*(x_traj(1, 1:world_length+1)+omega_traj(1:world_length+1)),'r', 'LineWidth', 2 ); xlabel('time, seconds'); ylabel('\theta, degrees'); xlim([0 Tend/1000]);
        subplot(5,1,2); hold on; plot(time_traj(1:world_length+1)/1000, 180/pi*(x_traj(1, 1:world_length+1)-omega_traj(1:world_length+1)),'r', 'LineWidth', 2 ); xlabel('time, seconds'); ylabel('\theta, degrees'); xlim([0 Tend/1000]);

        subplot(5,1,3); hold on; plot(time_traj(1:world_length+1)/1000, 180/pi*x_traj(2, 1:world_length+1), 'r','LineWidth', 3); ylabel('$\dot{\theta}$, degree/s','interpreter','latex'); xlim([0 Tend/1000]);

        subplot(5,1,4); hold on; plot(time_traj(1:world_length+1)/1000, EF_traj(1, 1:world_length+1), 'r','LineWidth', 3); ylabel('\Phi, 1/s'); xlim([0 Tend/1000]);
%         subplot(6,1,5); hold on; plot(time_traj(1:world_length+1)/1000, 180/pi*omega_traj(1, 1:world_length+1), 'r','LineWidth', 3); ylabel('\Omega, degrees'); xlim([0 Tend/1000]);
        subplot(5,1,5); hold on; plot(time_traj(1:world_length)/1000, alpha_traj(1, 1:world_length),'r', 'LineWidth', 3); ylabel('\alpha, 1/s'); xlim([0 Tend/1000]);

        %compute dtmpc
        %plot dtmpc
    end
    
    if mod(t_now, Tblsc) <= 0.001
        %% BLSC math
        % compute blsc
        u_last = u_now;
        u_now = 0.0;
        %figure out which control we should  take
        index = 1;
        while time_traj_use(index)-0.0001 <= t_now
           index = index + 1; 
        end
        index = index - 1;
        
        % mpc component
        if index <= 0.001
            index = 1;
        end
        u_now = u_traj_use(index);

        % differentially figure out desired values for x
        x_des = x_traj_use(1,index) + (x_traj_use(1,index+1) - x_traj_use(1,index)) / delta_t_int * (t_now - time_traj_use(index));
        x_dot_des = x_traj_use(2,index) + (x_traj_use(2,index+1) - x_traj_use(2,index)) / delta_t_int * (t_now - time_traj_use(index));
        
        k = alpha_traj_use(1, index) * EF_traj_use(1, index) * I;
        % compute the sliding control part 
        s = (x_now(2, 1) - x_dot_des ) + lambda * ( x_now(1, 1) - x_des );
        
        % add up all the different terms
        u_now = u_now - k/L *   sat( s /EF_traj_use(1, index));
        u_now = u_now + 1/L * (L_cm*m*g * (sin(x_now(1,1))- sin(x_des) ));
        u_now = u_now + 1/L * Cd * (abs(x_now(2,1))*x_now(2,1) - abs(x_dot_des)*x_dot_des );
        u_now = u_now - 1/L * I * lambda * (x_now(2,1) - x_dot_des) ;
        
        if u_now < -1.8, u_now = -1.8; end
        if u_now > 1.8, u_now = 1.8; end
    end
   
    if mod(t_now, Tworld) <= 0.001
        %% Run the world
        x_last = x_now;
        temp_disturbance = 0;
        if x_last(1,1) > pi/4 & x_last(1,1) < 5 * pi/4
            temp_disturbance = 0.8 * disturb * sin(x_last(1,1)-pi/4)^2;
        end
        
        x_now(1,1) = x_last(1,1) + dt_world * x_last(2,1);    
        x_now(2,1) = (-Cd *abs(x_last(2,1))*x_last(2,1) - L_cm * mass*g * sin(x_last(1,1)) + L*u_now +  4*I * D * 2 * (rand()-0.5) + I * temp_disturbance ) * dt_world/I + x_last(2,1); % +0.3*(rand()-0.5);
        subplot(5,1,1); hold on; line('XData', [t_now/1000, (t_now+1)/1000] , 'YData', [u_last, u_now], 'LineWidth', 3, 'Color', 'b' ); %, 'Color', rgb(i, :), 'LineWidth', 3);
        subplot(5,1,2); hold on; line('XData', [t_now/1000, (t_now+1)/1000] , 'YData', [180/pi*x_last(1,1), 180/pi*x_now(1,1)], 'LineWidth', 3, 'Color', 'b' )
        subplot(5,1,3); hold on; line('XData', [t_now/1000, (t_now+1)/1000] , 'YData', [180/pi*x_last(2,1), 180/pi*x_now(2,1)], 'LineWidth', 3, 'Color', 'b' )
%         if x_last > pi/4
%             if tnow_  > time(wlen) * apply_disturb;
%                 temp_disturbance =  0.8  * disturb * sin(x1_temp-pi/4)^2;
% %                 disturbance(1, i) = disturb * sin(xk(1,i)-pi/4)^2;
%             end
% 
%         end
        
    end
    
    u_last = u_now;
    % increment
    t_now = t_now + 1;
end






%% functions and end stuff
function res = sat(a)
    if a <= -1
        res = -1;
    elseif a >= 1 
        res = 1;
    else 
        res = a;
    end 
end





