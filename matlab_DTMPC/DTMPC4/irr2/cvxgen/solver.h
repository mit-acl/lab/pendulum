/* Produced by CVXGEN, 2020-04-11 22:09:40 -0400.  */
/* CVXGEN is Copyright (C) 2006-2017 Jacob Mattingley, jem@cvxgen.com. */
/* The code in this file is Copyright (C) 2006-2017 Jacob Mattingley. */
/* CVXGEN, or solvers produced by CVXGEN, cannot be used for commercial */
/* applications without prior written permission from Jacob Mattingley. */

/* Filename: solver.h. */
/* Description: Header file with relevant definitions. */
#ifndef SOLVER_H
#define SOLVER_H
/* Uncomment the next line to remove all library dependencies. */
/*#define ZERO_LIBRARY_MODE */
#ifdef MATLAB_MEX_FILE
/* Matlab functions. MATLAB_MEX_FILE will be defined by the mex compiler. */
/* If you are not using the mex compiler, this functionality will not intrude, */
/* as it will be completely disabled at compile-time. */
#include "mex.h"
#else
#ifndef ZERO_LIBRARY_MODE
#include <stdio.h>
#endif
#endif
/* Space must be allocated somewhere (testsolver.c, csolve.c or your own */
/* program) for the global variables vars, params, work and settings. */
/* At the bottom of this file, they are externed. */
#ifndef ZERO_LIBRARY_MODE
#include <math.h>
#define pm(A, m, n) printmatrix(#A, A, m, n, 1)
#endif
typedef struct Params_t {
  double x_final[2];
  double xk_0[2];
  double d_0[2];
  double Q[4];
  double uk_0[1];
  double R[1];
  double alpha_0[1];
  double alpha_min[1];
  double M[1];
  double xk_1[2];
  double uk_1[1];
  double alpha_1[1];
  double xk_2[2];
  double uk_2[1];
  double alpha_2[1];
  double xk_3[2];
  double uk_3[1];
  double alpha_3[1];
  double xk_4[2];
  double uk_4[1];
  double alpha_4[1];
  double xk_5[2];
  double uk_5[1];
  double alpha_5[1];
  double xk_6[2];
  double uk_6[1];
  double alpha_6[1];
  double xk_7[2];
  double uk_7[1];
  double alpha_7[1];
  double xk_8[2];
  double uk_8[1];
  double alpha_8[1];
  double xk_9[2];
  double uk_9[1];
  double alpha_9[1];
  double xk_10[2];
  double uk_10[1];
  double alpha_10[1];
  double xk_11[2];
  double uk_11[1];
  double alpha_11[1];
  double xk_12[2];
  double uk_12[1];
  double alpha_12[1];
  double xk_13[2];
  double uk_13[1];
  double alpha_13[1];
  double xk_14[2];
  double uk_14[1];
  double alpha_14[1];
  double xk_15[2];
  double uk_15[1];
  double alpha_15[1];
  double xk_16[2];
  double uk_16[1];
  double alpha_16[1];
  double xk_17[2];
  double uk_17[1];
  double alpha_17[1];
  double xk_18[2];
  double uk_18[1];
  double alpha_18[1];
  double xk_19[2];
  double uk_19[1];
  double alpha_19[1];
  double xk_20[2];
  double uk_20[1];
  double alpha_20[1];
  double xk_21[2];
  double uk_21[1];
  double alpha_21[1];
  double xk_22[2];
  double uk_22[1];
  double alpha_22[1];
  double xk_23[2];
  double uk_23[1];
  double alpha_23[1];
  double xk_24[2];
  double uk_24[1];
  double alpha_24[1];
  double xk_25[2];
  double uk_25[1];
  double alpha_25[1];
  double xk_26[2];
  double uk_26[1];
  double alpha_26[1];
  double xk_27[2];
  double uk_27[1];
  double alpha_27[1];
  double xk_28[2];
  double uk_28[1];
  double alpha_28[1];
  double xk_29[2];
  double uk_29[1];
  double alpha_29[1];
  double xk_30[2];
  double uk_30[1];
  double alpha_30[1];
  double xk_31[2];
  double uk_31[1];
  double alpha_31[1];
  double xk_32[2];
  double uk_32[1];
  double alpha_32[1];
  double xk_33[2];
  double uk_33[1];
  double alpha_33[1];
  double xk_34[2];
  double uk_34[1];
  double alpha_34[1];
  double xk_35[2];
  double uk_35[1];
  double alpha_35[1];
  double xk_36[2];
  double uk_36[1];
  double alpha_36[1];
  double xk_37[2];
  double uk_37[1];
  double alpha_37[1];
  double xk_38[2];
  double uk_38[1];
  double alpha_38[1];
  double xk_39[2];
  double uk_39[1];
  double alpha_39[1];
  double xk_40[2];
  double uk_40[1];
  double alpha_40[1];
  double xk_41[2];
  double Q_final[4];
  double delta_t[1];
  double c1[1];
  double cos_xk_0[1];
  double c2[1];
  double abs_xk_0[1];
  double Bd[1];
  double cos_xk_1[1];
  double abs_xk_1[1];
  double cos_xk_2[1];
  double abs_xk_2[1];
  double cos_xk_3[1];
  double abs_xk_3[1];
  double cos_xk_4[1];
  double abs_xk_4[1];
  double cos_xk_5[1];
  double abs_xk_5[1];
  double cos_xk_6[1];
  double abs_xk_6[1];
  double cos_xk_7[1];
  double abs_xk_7[1];
  double cos_xk_8[1];
  double abs_xk_8[1];
  double cos_xk_9[1];
  double abs_xk_9[1];
  double cos_xk_10[1];
  double abs_xk_10[1];
  double cos_xk_11[1];
  double abs_xk_11[1];
  double cos_xk_12[1];
  double abs_xk_12[1];
  double cos_xk_13[1];
  double abs_xk_13[1];
  double cos_xk_14[1];
  double abs_xk_14[1];
  double cos_xk_15[1];
  double abs_xk_15[1];
  double cos_xk_16[1];
  double abs_xk_16[1];
  double cos_xk_17[1];
  double abs_xk_17[1];
  double cos_xk_18[1];
  double abs_xk_18[1];
  double cos_xk_19[1];
  double abs_xk_19[1];
  double cos_xk_20[1];
  double abs_xk_20[1];
  double cos_xk_21[1];
  double abs_xk_21[1];
  double cos_xk_22[1];
  double abs_xk_22[1];
  double cos_xk_23[1];
  double abs_xk_23[1];
  double cos_xk_24[1];
  double abs_xk_24[1];
  double cos_xk_25[1];
  double abs_xk_25[1];
  double cos_xk_26[1];
  double abs_xk_26[1];
  double cos_xk_27[1];
  double abs_xk_27[1];
  double cos_xk_28[1];
  double abs_xk_28[1];
  double cos_xk_29[1];
  double abs_xk_29[1];
  double cos_xk_30[1];
  double abs_xk_30[1];
  double cos_xk_31[1];
  double abs_xk_31[1];
  double cos_xk_32[1];
  double abs_xk_32[1];
  double cos_xk_33[1];
  double abs_xk_33[1];
  double cos_xk_34[1];
  double abs_xk_34[1];
  double cos_xk_35[1];
  double abs_xk_35[1];
  double cos_xk_36[1];
  double abs_xk_36[1];
  double cos_xk_37[1];
  double abs_xk_37[1];
  double cos_xk_38[1];
  double abs_xk_38[1];
  double cos_xk_39[1];
  double abs_xk_39[1];
  double cos_xk_40[1];
  double abs_xk_40[1];
  double new_EF_0[1];
  double old_EF_0[1];
  double disturbance_0[1];
  double Cd_max[1];
  double I[1];
  double old_EF_1[1];
  double disturbance_1[1];
  double old_EF_2[1];
  double disturbance_2[1];
  double old_EF_3[1];
  double disturbance_3[1];
  double old_EF_4[1];
  double disturbance_4[1];
  double old_EF_5[1];
  double disturbance_5[1];
  double old_EF_6[1];
  double disturbance_6[1];
  double old_EF_7[1];
  double disturbance_7[1];
  double old_EF_8[1];
  double disturbance_8[1];
  double old_EF_9[1];
  double disturbance_9[1];
  double old_EF_10[1];
  double disturbance_10[1];
  double old_EF_11[1];
  double disturbance_11[1];
  double old_EF_12[1];
  double disturbance_12[1];
  double old_EF_13[1];
  double disturbance_13[1];
  double old_EF_14[1];
  double disturbance_14[1];
  double old_EF_15[1];
  double disturbance_15[1];
  double old_EF_16[1];
  double disturbance_16[1];
  double old_EF_17[1];
  double disturbance_17[1];
  double old_EF_18[1];
  double disturbance_18[1];
  double old_EF_19[1];
  double disturbance_19[1];
  double old_EF_20[1];
  double disturbance_20[1];
  double old_EF_21[1];
  double disturbance_21[1];
  double old_EF_22[1];
  double disturbance_22[1];
  double old_EF_23[1];
  double disturbance_23[1];
  double old_EF_24[1];
  double disturbance_24[1];
  double old_EF_25[1];
  double disturbance_25[1];
  double old_EF_26[1];
  double disturbance_26[1];
  double old_EF_27[1];
  double disturbance_27[1];
  double old_EF_28[1];
  double disturbance_28[1];
  double old_EF_29[1];
  double disturbance_29[1];
  double old_EF_30[1];
  double disturbance_30[1];
  double old_EF_31[1];
  double disturbance_31[1];
  double old_EF_32[1];
  double disturbance_32[1];
  double old_EF_33[1];
  double disturbance_33[1];
  double old_EF_34[1];
  double disturbance_34[1];
  double old_EF_35[1];
  double disturbance_35[1];
  double old_EF_36[1];
  double disturbance_36[1];
  double old_EF_37[1];
  double disturbance_37[1];
  double old_EF_38[1];
  double disturbance_38[1];
  double old_EF_39[1];
  double disturbance_39[1];
  double old_EF_40[1];
  double disturbance_40[1];
  double lambda[1];
  double omega_0[1];
  double w_max[1];
  double u_max[1];
  double delta_u_max[1];
  double last_u[1];
  double alpha_max[1];
  double delta_alpha_max[1];
  double last_alpha[1];
  double omega_max[1];
  double *xk[42];
  double *d[1];
  double *uk[41];
  double *alpha[41];
  double *cos_xk[41];
  double *abs_xk[41];
  double *new_EF[1];
  double *old_EF[41];
  double *disturbance[41];
  double *omega[1];
} Params;
typedef struct Vars_t {
  double *w_0; /* 1 rows. */
  double *delta_alpha_0; /* 1 rows. */
  double *d_1; /* 2 rows. */
  double *w_1; /* 1 rows. */
  double *delta_alpha_1; /* 1 rows. */
  double *d_2; /* 2 rows. */
  double *w_2; /* 1 rows. */
  double *delta_alpha_2; /* 1 rows. */
  double *d_3; /* 2 rows. */
  double *w_3; /* 1 rows. */
  double *delta_alpha_3; /* 1 rows. */
  double *d_4; /* 2 rows. */
  double *w_4; /* 1 rows. */
  double *delta_alpha_4; /* 1 rows. */
  double *d_5; /* 2 rows. */
  double *w_5; /* 1 rows. */
  double *delta_alpha_5; /* 1 rows. */
  double *d_6; /* 2 rows. */
  double *w_6; /* 1 rows. */
  double *delta_alpha_6; /* 1 rows. */
  double *d_7; /* 2 rows. */
  double *w_7; /* 1 rows. */
  double *delta_alpha_7; /* 1 rows. */
  double *d_8; /* 2 rows. */
  double *w_8; /* 1 rows. */
  double *delta_alpha_8; /* 1 rows. */
  double *d_9; /* 2 rows. */
  double *w_9; /* 1 rows. */
  double *delta_alpha_9; /* 1 rows. */
  double *d_10; /* 2 rows. */
  double *w_10; /* 1 rows. */
  double *delta_alpha_10; /* 1 rows. */
  double *d_11; /* 2 rows. */
  double *w_11; /* 1 rows. */
  double *delta_alpha_11; /* 1 rows. */
  double *d_12; /* 2 rows. */
  double *w_12; /* 1 rows. */
  double *delta_alpha_12; /* 1 rows. */
  double *d_13; /* 2 rows. */
  double *w_13; /* 1 rows. */
  double *delta_alpha_13; /* 1 rows. */
  double *d_14; /* 2 rows. */
  double *w_14; /* 1 rows. */
  double *delta_alpha_14; /* 1 rows. */
  double *d_15; /* 2 rows. */
  double *w_15; /* 1 rows. */
  double *delta_alpha_15; /* 1 rows. */
  double *d_16; /* 2 rows. */
  double *w_16; /* 1 rows. */
  double *delta_alpha_16; /* 1 rows. */
  double *d_17; /* 2 rows. */
  double *w_17; /* 1 rows. */
  double *delta_alpha_17; /* 1 rows. */
  double *d_18; /* 2 rows. */
  double *w_18; /* 1 rows. */
  double *delta_alpha_18; /* 1 rows. */
  double *d_19; /* 2 rows. */
  double *w_19; /* 1 rows. */
  double *delta_alpha_19; /* 1 rows. */
  double *d_20; /* 2 rows. */
  double *w_20; /* 1 rows. */
  double *delta_alpha_20; /* 1 rows. */
  double *d_21; /* 2 rows. */
  double *w_21; /* 1 rows. */
  double *delta_alpha_21; /* 1 rows. */
  double *d_22; /* 2 rows. */
  double *w_22; /* 1 rows. */
  double *delta_alpha_22; /* 1 rows. */
  double *d_23; /* 2 rows. */
  double *w_23; /* 1 rows. */
  double *delta_alpha_23; /* 1 rows. */
  double *d_24; /* 2 rows. */
  double *w_24; /* 1 rows. */
  double *delta_alpha_24; /* 1 rows. */
  double *d_25; /* 2 rows. */
  double *w_25; /* 1 rows. */
  double *delta_alpha_25; /* 1 rows. */
  double *d_26; /* 2 rows. */
  double *w_26; /* 1 rows. */
  double *delta_alpha_26; /* 1 rows. */
  double *d_27; /* 2 rows. */
  double *w_27; /* 1 rows. */
  double *delta_alpha_27; /* 1 rows. */
  double *d_28; /* 2 rows. */
  double *w_28; /* 1 rows. */
  double *delta_alpha_28; /* 1 rows. */
  double *d_29; /* 2 rows. */
  double *w_29; /* 1 rows. */
  double *delta_alpha_29; /* 1 rows. */
  double *d_30; /* 2 rows. */
  double *w_30; /* 1 rows. */
  double *delta_alpha_30; /* 1 rows. */
  double *d_31; /* 2 rows. */
  double *w_31; /* 1 rows. */
  double *delta_alpha_31; /* 1 rows. */
  double *d_32; /* 2 rows. */
  double *w_32; /* 1 rows. */
  double *delta_alpha_32; /* 1 rows. */
  double *d_33; /* 2 rows. */
  double *w_33; /* 1 rows. */
  double *delta_alpha_33; /* 1 rows. */
  double *d_34; /* 2 rows. */
  double *w_34; /* 1 rows. */
  double *delta_alpha_34; /* 1 rows. */
  double *d_35; /* 2 rows. */
  double *w_35; /* 1 rows. */
  double *delta_alpha_35; /* 1 rows. */
  double *d_36; /* 2 rows. */
  double *w_36; /* 1 rows. */
  double *delta_alpha_36; /* 1 rows. */
  double *d_37; /* 2 rows. */
  double *w_37; /* 1 rows. */
  double *delta_alpha_37; /* 1 rows. */
  double *d_38; /* 2 rows. */
  double *w_38; /* 1 rows. */
  double *delta_alpha_38; /* 1 rows. */
  double *d_39; /* 2 rows. */
  double *w_39; /* 1 rows. */
  double *delta_alpha_39; /* 1 rows. */
  double *d_40; /* 2 rows. */
  double *w_40; /* 1 rows. */
  double *delta_alpha_40; /* 1 rows. */
  double *d_41; /* 2 rows. */
  double *new_EF_1; /* 1 rows. */
  double *new_EF_2; /* 1 rows. */
  double *new_EF_3; /* 1 rows. */
  double *new_EF_4; /* 1 rows. */
  double *new_EF_5; /* 1 rows. */
  double *new_EF_6; /* 1 rows. */
  double *new_EF_7; /* 1 rows. */
  double *new_EF_8; /* 1 rows. */
  double *new_EF_9; /* 1 rows. */
  double *new_EF_10; /* 1 rows. */
  double *new_EF_11; /* 1 rows. */
  double *new_EF_12; /* 1 rows. */
  double *new_EF_13; /* 1 rows. */
  double *new_EF_14; /* 1 rows. */
  double *new_EF_15; /* 1 rows. */
  double *new_EF_16; /* 1 rows. */
  double *new_EF_17; /* 1 rows. */
  double *new_EF_18; /* 1 rows. */
  double *new_EF_19; /* 1 rows. */
  double *new_EF_20; /* 1 rows. */
  double *new_EF_21; /* 1 rows. */
  double *new_EF_22; /* 1 rows. */
  double *new_EF_23; /* 1 rows. */
  double *new_EF_24; /* 1 rows. */
  double *new_EF_25; /* 1 rows. */
  double *new_EF_26; /* 1 rows. */
  double *new_EF_27; /* 1 rows. */
  double *new_EF_28; /* 1 rows. */
  double *new_EF_29; /* 1 rows. */
  double *new_EF_30; /* 1 rows. */
  double *new_EF_31; /* 1 rows. */
  double *new_EF_32; /* 1 rows. */
  double *new_EF_33; /* 1 rows. */
  double *new_EF_34; /* 1 rows. */
  double *new_EF_35; /* 1 rows. */
  double *new_EF_36; /* 1 rows. */
  double *new_EF_37; /* 1 rows. */
  double *new_EF_38; /* 1 rows. */
  double *new_EF_39; /* 1 rows. */
  double *new_EF_40; /* 1 rows. */
  double *new_EF_41; /* 1 rows. */
  double *omega_1; /* 1 rows. */
  double *omega_2; /* 1 rows. */
  double *omega_3; /* 1 rows. */
  double *omega_4; /* 1 rows. */
  double *omega_5; /* 1 rows. */
  double *omega_6; /* 1 rows. */
  double *omega_7; /* 1 rows. */
  double *omega_8; /* 1 rows. */
  double *omega_9; /* 1 rows. */
  double *omega_10; /* 1 rows. */
  double *omega_11; /* 1 rows. */
  double *omega_12; /* 1 rows. */
  double *omega_13; /* 1 rows. */
  double *omega_14; /* 1 rows. */
  double *omega_15; /* 1 rows. */
  double *omega_16; /* 1 rows. */
  double *omega_17; /* 1 rows. */
  double *omega_18; /* 1 rows. */
  double *omega_19; /* 1 rows. */
  double *omega_20; /* 1 rows. */
  double *omega_21; /* 1 rows. */
  double *omega_22; /* 1 rows. */
  double *omega_23; /* 1 rows. */
  double *omega_24; /* 1 rows. */
  double *omega_25; /* 1 rows. */
  double *omega_26; /* 1 rows. */
  double *omega_27; /* 1 rows. */
  double *omega_28; /* 1 rows. */
  double *omega_29; /* 1 rows. */
  double *omega_30; /* 1 rows. */
  double *omega_31; /* 1 rows. */
  double *omega_32; /* 1 rows. */
  double *omega_33; /* 1 rows. */
  double *omega_34; /* 1 rows. */
  double *omega_35; /* 1 rows. */
  double *omega_36; /* 1 rows. */
  double *omega_37; /* 1 rows. */
  double *omega_38; /* 1 rows. */
  double *omega_39; /* 1 rows. */
  double *omega_40; /* 1 rows. */
  double *omega_41; /* 1 rows. */
  double *w[41];
  double *delta_alpha[41];
  double *d[42];
  double *new_EF[42];
  double *omega[42];
} Vars;
typedef struct Workspace_t {
  double h[531];
  double s_inv[531];
  double s_inv_z[531];
  double b[164];
  double q[246];
  double rhs[1472];
  double x[1472];
  double *s;
  double *z;
  double *y;
  double lhs_aff[1472];
  double lhs_cc[1472];
  double buffer[1472];
  double buffer2[1472];
  double KKT[3015];
  double L[2418];
  double d[1472];
  double v[1472];
  double d_inv[1472];
  double gap;
  double optval;
  double ineq_resid_squared;
  double eq_resid_squared;
  double block_33[1];
  /* Pre-op symbols. */
  double quad_270107893760[1];
  double quad_392898023424[1];
  double quad_445352677376[1];
  double quad_702230208512[1];
  double quad_335242391552[1];
  double quad_175950630912[1];
  double quad_332149305344[1];
  double quad_17263370240[1];
  double quad_302248755200[1];
  double quad_977458573312[1];
  double quad_579963990016[1];
  double quad_487442513920[1];
  double quad_648024653824[1];
  double quad_910448377856[1];
  double quad_341291667456[1];
  double quad_453679702016[1];
  double quad_588369764352[1];
  double quad_610273832960[1];
  double quad_929545818112[1];
  double quad_3726573568[1];
  double quad_566947319808[1];
  double quad_126117388288[1];
  double quad_442566197248[1];
  double quad_565576192000[1];
  double quad_839768125440[1];
  double quad_694648061952[1];
  double quad_338226507776[1];
  double quad_881072074752[1];
  double quad_815321604096[1];
  double quad_409310203904[1];
  double quad_748685393920[1];
  double quad_671950053376[1];
  double quad_46587228160[1];
  double quad_266521808896[1];
  double quad_986003238912[1];
  double quad_585389805568[1];
  double quad_660241698816[1];
  double quad_59129753600[1];
  double quad_749055442944[1];
  double quad_348638896128[1];
  double quad_628438679552[1];
  double quad_782724800512[1];
  double quad_906353664000[1];
  double quad_41312305152[1];
  double quad_232271880192[1];
  double quad_786671919104[1];
  double quad_352455172096[1];
  double quad_326244257792[1];
  double quad_388870012928[1];
  double quad_564407173120[1];
  double quad_607032025088[1];
  double quad_588796252160[1];
  double quad_631891841024[1];
  double quad_46308270080[1];
  double quad_74921062400[1];
  double quad_587445669888[1];
  double quad_271313948672[1];
  double quad_274730606592[1];
  double quad_496943005696[1];
  double quad_223001780224[1];
  double quad_548109938688[1];
  double quad_137830731776[1];
  double quad_662255759360[1];
  double quad_859892768768[1];
  double quad_772709756928[1];
  double quad_645643497472[1];
  double quad_827707936768[1];
  double quad_513757962240[1];
  double quad_111384309760[1];
  double quad_891445882880[1];
  double quad_521918545920[1];
  double quad_971924062208[1];
  double quad_930083958784[1];
  double quad_103559696384[1];
  double quad_595768565760[1];
  double quad_182585827328[1];
  double quad_261409124352[1];
  double quad_885751197696[1];
  double quad_111311339520[1];
  double quad_990800257024[1];
  double quad_533513490432[1];
  double quad_790321422336[1];
  double quad_981469978624[1];
  double quad_852730523648[1];
  double quad_98378784768[1];
  double quad_920235089920[1];
  double quad_987156705280[1];
  double quad_581413453824[1];
  double quad_239742230528[1];
  double quad_847507894272[1];
  double quad_771595739136[1];
  double quad_203847909376[1];
  double quad_638189170688[1];
  double quad_365765689344[1];
  double quad_939478597632[1];
  double quad_933579579392[1];
  double quad_586980057088[1];
  double quad_676266242048[1];
  double quad_362807410688[1];
  double quad_422286278656[1];
  double quad_333950107648[1];
  double quad_141227048960[1];
  double quad_951767871488[1];
  double quad_902207262720[1];
  double quad_674208681984[1];
  double quad_169035161600[1];
  double quad_364804440064[1];
  double quad_681618362368[1];
  double quad_397319585792[1];
  double quad_804355153920[1];
  double quad_592808808448[1];
  double quad_310680621056[1];
  double quad_738775400448[1];
  double quad_364554047488[1];
  double quad_396266446848[1];
  double quad_243252596736[1];
  double quad_183366365184[1];
  double quad_891607912448[1];
  double quad_783557849088[1];
  double quad_238072266752[1];
  double quad_853499805696[1];
  double quad_717977165824[1];
  double quad_233890783232[1];
  double quad_628291096576[1];
  double frac_440034369536;
  double frac_442821914624;
  double frac_74805100544;
  double frac_432494084096;
  double frac_286396239872;
  double frac_57120047104;
  double frac_184596529152;
  double frac_280580362240;
  double frac_268329107456;
  double frac_813883129856;
  double frac_716272693248;
  double frac_249440423936;
  double frac_258379026432;
  double frac_285372968960;
  double frac_230137634816;
  double frac_266644762624;
  double frac_453918560256;
  double frac_874774331392;
  double frac_378318163968;
  double frac_972859576320;
  double frac_531304456192;
  double frac_126340628480;
  double frac_148170981376;
  double frac_630636609536;
  double frac_730757550080;
  double frac_127025135616;
  double frac_397966413824;
  double frac_952075390976;
  double frac_642664460288;
  double frac_420638019584;
  double frac_294151315456;
  double frac_15519064064;
  double frac_924475207680;
  double frac_571255779328;
  double frac_346477019136;
  double frac_815537139712;
  double frac_807673294848;
  double frac_465858555904;
  double frac_946905686016;
  double frac_270528016384;
  double frac_938110726144;
  int converged;
} Workspace;
typedef struct Settings_t {
  double resid_tol;
  double eps;
  int max_iters;
  int refine_steps;
  int better_start;
  /* Better start obviates the need for s_init and z_init. */
  double s_init;
  double z_init;
  int verbose;
  /* Show extra details of the iterative refinement steps. */
  int verbose_refinement;
  int debug;
  /* For regularization. Minimum value of abs(D_ii) in the kkt D factor. */
  double kkt_reg;
} Settings;
extern Vars vars;
extern Params params;
extern Workspace work;
extern Settings settings;
/* Function definitions in ldl.c: */
void ldl_solve(double *target, double *var);
void ldl_factor(void);
double check_factorization(void);
void matrix_multiply(double *result, double *source);
double check_residual(double *target, double *multiplicand);
void fill_KKT(void);

/* Function definitions in matrix_support.c: */
void multbymA(double *lhs, double *rhs);
void multbymAT(double *lhs, double *rhs);
void multbymG(double *lhs, double *rhs);
void multbymGT(double *lhs, double *rhs);
void multbyP(double *lhs, double *rhs);
void fillq(void);
void fillh(void);
void fillb(void);
void pre_ops(void);

/* Function definitions in solver.c: */
double eval_gap(void);
void set_defaults(void);
void setup_pointers(void);
void setup_indexed_params(void);
void setup_indexed_optvars(void);
void setup_indexing(void);
void set_start(void);
double eval_objv(void);
void fillrhs_aff(void);
void fillrhs_cc(void);
void refine(double *target, double *var);
double calc_ineq_resid_squared(void);
double calc_eq_resid_squared(void);
void better_start(void);
void fillrhs_start(void);
long solve(void);

/* Function definitions in testsolver.c: */
int main(int argc, char **argv);
void load_default_data(void);

/* Function definitions in util.c: */
void tic(void);
float toc(void);
float tocq(void);
void printmatrix(char *name, double *A, int m, int n, int sparse);
double unif(double lower, double upper);
float ran1(long*idum, int reset);
float randn_internal(long *idum, int reset);
double randn(void);
void reset_rand(void);

#endif
