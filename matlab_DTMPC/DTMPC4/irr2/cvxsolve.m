% Produced by CVXGEN, 2020-04-11 22:07:21 -0400.
% CVXGEN is Copyright (C) 2006-2017 Jacob Mattingley, jem@cvxgen.com.
% The code in this file is Copyright (C) 2006-2017 Jacob Mattingley.
% CVXGEN, or solvers produced by CVXGEN, cannot be used for commercial
% applications without prior written permission from Jacob Mattingley.

% Filename: cvxsolve.m.
% Description: Solution file, via cvx, for use with sample.m.
function [vars, status] = cvxsolve(params, settings)
Bd = params.Bd;
Cd_max = params.Cd_max;
I = params.I;
M = params.M;
Q = params.Q;
Q_final = params.Q_final;
R = params.R;
abs_xk_0 = params.abs_xk_0;
if isfield(params, 'abs_xk_1')
  abs_xk_1 = params.abs_xk_1;
elseif isfield(params, 'abs_xk')
  abs_xk_1 = params.abs_xk{1};
else
  error 'could not find abs_xk_1'
end
if isfield(params, 'abs_xk_2')
  abs_xk_2 = params.abs_xk_2;
elseif isfield(params, 'abs_xk')
  abs_xk_2 = params.abs_xk{2};
else
  error 'could not find abs_xk_2'
end
if isfield(params, 'abs_xk_3')
  abs_xk_3 = params.abs_xk_3;
elseif isfield(params, 'abs_xk')
  abs_xk_3 = params.abs_xk{3};
else
  error 'could not find abs_xk_3'
end
if isfield(params, 'abs_xk_4')
  abs_xk_4 = params.abs_xk_4;
elseif isfield(params, 'abs_xk')
  abs_xk_4 = params.abs_xk{4};
else
  error 'could not find abs_xk_4'
end
if isfield(params, 'abs_xk_5')
  abs_xk_5 = params.abs_xk_5;
elseif isfield(params, 'abs_xk')
  abs_xk_5 = params.abs_xk{5};
else
  error 'could not find abs_xk_5'
end
if isfield(params, 'abs_xk_6')
  abs_xk_6 = params.abs_xk_6;
elseif isfield(params, 'abs_xk')
  abs_xk_6 = params.abs_xk{6};
else
  error 'could not find abs_xk_6'
end
if isfield(params, 'abs_xk_7')
  abs_xk_7 = params.abs_xk_7;
elseif isfield(params, 'abs_xk')
  abs_xk_7 = params.abs_xk{7};
else
  error 'could not find abs_xk_7'
end
if isfield(params, 'abs_xk_8')
  abs_xk_8 = params.abs_xk_8;
elseif isfield(params, 'abs_xk')
  abs_xk_8 = params.abs_xk{8};
else
  error 'could not find abs_xk_8'
end
if isfield(params, 'abs_xk_9')
  abs_xk_9 = params.abs_xk_9;
elseif isfield(params, 'abs_xk')
  abs_xk_9 = params.abs_xk{9};
else
  error 'could not find abs_xk_9'
end
if isfield(params, 'abs_xk_10')
  abs_xk_10 = params.abs_xk_10;
elseif isfield(params, 'abs_xk')
  abs_xk_10 = params.abs_xk{10};
else
  error 'could not find abs_xk_10'
end
if isfield(params, 'abs_xk_11')
  abs_xk_11 = params.abs_xk_11;
elseif isfield(params, 'abs_xk')
  abs_xk_11 = params.abs_xk{11};
else
  error 'could not find abs_xk_11'
end
if isfield(params, 'abs_xk_12')
  abs_xk_12 = params.abs_xk_12;
elseif isfield(params, 'abs_xk')
  abs_xk_12 = params.abs_xk{12};
else
  error 'could not find abs_xk_12'
end
if isfield(params, 'abs_xk_13')
  abs_xk_13 = params.abs_xk_13;
elseif isfield(params, 'abs_xk')
  abs_xk_13 = params.abs_xk{13};
else
  error 'could not find abs_xk_13'
end
if isfield(params, 'abs_xk_14')
  abs_xk_14 = params.abs_xk_14;
elseif isfield(params, 'abs_xk')
  abs_xk_14 = params.abs_xk{14};
else
  error 'could not find abs_xk_14'
end
if isfield(params, 'abs_xk_15')
  abs_xk_15 = params.abs_xk_15;
elseif isfield(params, 'abs_xk')
  abs_xk_15 = params.abs_xk{15};
else
  error 'could not find abs_xk_15'
end
if isfield(params, 'abs_xk_16')
  abs_xk_16 = params.abs_xk_16;
elseif isfield(params, 'abs_xk')
  abs_xk_16 = params.abs_xk{16};
else
  error 'could not find abs_xk_16'
end
if isfield(params, 'abs_xk_17')
  abs_xk_17 = params.abs_xk_17;
elseif isfield(params, 'abs_xk')
  abs_xk_17 = params.abs_xk{17};
else
  error 'could not find abs_xk_17'
end
if isfield(params, 'abs_xk_18')
  abs_xk_18 = params.abs_xk_18;
elseif isfield(params, 'abs_xk')
  abs_xk_18 = params.abs_xk{18};
else
  error 'could not find abs_xk_18'
end
if isfield(params, 'abs_xk_19')
  abs_xk_19 = params.abs_xk_19;
elseif isfield(params, 'abs_xk')
  abs_xk_19 = params.abs_xk{19};
else
  error 'could not find abs_xk_19'
end
if isfield(params, 'abs_xk_20')
  abs_xk_20 = params.abs_xk_20;
elseif isfield(params, 'abs_xk')
  abs_xk_20 = params.abs_xk{20};
else
  error 'could not find abs_xk_20'
end
if isfield(params, 'abs_xk_21')
  abs_xk_21 = params.abs_xk_21;
elseif isfield(params, 'abs_xk')
  abs_xk_21 = params.abs_xk{21};
else
  error 'could not find abs_xk_21'
end
if isfield(params, 'abs_xk_22')
  abs_xk_22 = params.abs_xk_22;
elseif isfield(params, 'abs_xk')
  abs_xk_22 = params.abs_xk{22};
else
  error 'could not find abs_xk_22'
end
if isfield(params, 'abs_xk_23')
  abs_xk_23 = params.abs_xk_23;
elseif isfield(params, 'abs_xk')
  abs_xk_23 = params.abs_xk{23};
else
  error 'could not find abs_xk_23'
end
if isfield(params, 'abs_xk_24')
  abs_xk_24 = params.abs_xk_24;
elseif isfield(params, 'abs_xk')
  abs_xk_24 = params.abs_xk{24};
else
  error 'could not find abs_xk_24'
end
if isfield(params, 'abs_xk_25')
  abs_xk_25 = params.abs_xk_25;
elseif isfield(params, 'abs_xk')
  abs_xk_25 = params.abs_xk{25};
else
  error 'could not find abs_xk_25'
end
if isfield(params, 'abs_xk_26')
  abs_xk_26 = params.abs_xk_26;
elseif isfield(params, 'abs_xk')
  abs_xk_26 = params.abs_xk{26};
else
  error 'could not find abs_xk_26'
end
if isfield(params, 'abs_xk_27')
  abs_xk_27 = params.abs_xk_27;
elseif isfield(params, 'abs_xk')
  abs_xk_27 = params.abs_xk{27};
else
  error 'could not find abs_xk_27'
end
if isfield(params, 'abs_xk_28')
  abs_xk_28 = params.abs_xk_28;
elseif isfield(params, 'abs_xk')
  abs_xk_28 = params.abs_xk{28};
else
  error 'could not find abs_xk_28'
end
if isfield(params, 'abs_xk_29')
  abs_xk_29 = params.abs_xk_29;
elseif isfield(params, 'abs_xk')
  abs_xk_29 = params.abs_xk{29};
else
  error 'could not find abs_xk_29'
end
if isfield(params, 'abs_xk_30')
  abs_xk_30 = params.abs_xk_30;
elseif isfield(params, 'abs_xk')
  abs_xk_30 = params.abs_xk{30};
else
  error 'could not find abs_xk_30'
end
if isfield(params, 'abs_xk_31')
  abs_xk_31 = params.abs_xk_31;
elseif isfield(params, 'abs_xk')
  abs_xk_31 = params.abs_xk{31};
else
  error 'could not find abs_xk_31'
end
if isfield(params, 'abs_xk_32')
  abs_xk_32 = params.abs_xk_32;
elseif isfield(params, 'abs_xk')
  abs_xk_32 = params.abs_xk{32};
else
  error 'could not find abs_xk_32'
end
if isfield(params, 'abs_xk_33')
  abs_xk_33 = params.abs_xk_33;
elseif isfield(params, 'abs_xk')
  abs_xk_33 = params.abs_xk{33};
else
  error 'could not find abs_xk_33'
end
if isfield(params, 'abs_xk_34')
  abs_xk_34 = params.abs_xk_34;
elseif isfield(params, 'abs_xk')
  abs_xk_34 = params.abs_xk{34};
else
  error 'could not find abs_xk_34'
end
if isfield(params, 'abs_xk_35')
  abs_xk_35 = params.abs_xk_35;
elseif isfield(params, 'abs_xk')
  abs_xk_35 = params.abs_xk{35};
else
  error 'could not find abs_xk_35'
end
if isfield(params, 'abs_xk_36')
  abs_xk_36 = params.abs_xk_36;
elseif isfield(params, 'abs_xk')
  abs_xk_36 = params.abs_xk{36};
else
  error 'could not find abs_xk_36'
end
if isfield(params, 'abs_xk_37')
  abs_xk_37 = params.abs_xk_37;
elseif isfield(params, 'abs_xk')
  abs_xk_37 = params.abs_xk{37};
else
  error 'could not find abs_xk_37'
end
if isfield(params, 'abs_xk_38')
  abs_xk_38 = params.abs_xk_38;
elseif isfield(params, 'abs_xk')
  abs_xk_38 = params.abs_xk{38};
else
  error 'could not find abs_xk_38'
end
if isfield(params, 'abs_xk_39')
  abs_xk_39 = params.abs_xk_39;
elseif isfield(params, 'abs_xk')
  abs_xk_39 = params.abs_xk{39};
else
  error 'could not find abs_xk_39'
end
if isfield(params, 'abs_xk_40')
  abs_xk_40 = params.abs_xk_40;
elseif isfield(params, 'abs_xk')
  abs_xk_40 = params.abs_xk{40};
else
  error 'could not find abs_xk_40'
end
alpha_0 = params.alpha_0;
if isfield(params, 'alpha_1')
  alpha_1 = params.alpha_1;
elseif isfield(params, 'alpha')
  alpha_1 = params.alpha{1};
else
  error 'could not find alpha_1'
end
if isfield(params, 'alpha_2')
  alpha_2 = params.alpha_2;
elseif isfield(params, 'alpha')
  alpha_2 = params.alpha{2};
else
  error 'could not find alpha_2'
end
if isfield(params, 'alpha_3')
  alpha_3 = params.alpha_3;
elseif isfield(params, 'alpha')
  alpha_3 = params.alpha{3};
else
  error 'could not find alpha_3'
end
if isfield(params, 'alpha_4')
  alpha_4 = params.alpha_4;
elseif isfield(params, 'alpha')
  alpha_4 = params.alpha{4};
else
  error 'could not find alpha_4'
end
if isfield(params, 'alpha_5')
  alpha_5 = params.alpha_5;
elseif isfield(params, 'alpha')
  alpha_5 = params.alpha{5};
else
  error 'could not find alpha_5'
end
if isfield(params, 'alpha_6')
  alpha_6 = params.alpha_6;
elseif isfield(params, 'alpha')
  alpha_6 = params.alpha{6};
else
  error 'could not find alpha_6'
end
if isfield(params, 'alpha_7')
  alpha_7 = params.alpha_7;
elseif isfield(params, 'alpha')
  alpha_7 = params.alpha{7};
else
  error 'could not find alpha_7'
end
if isfield(params, 'alpha_8')
  alpha_8 = params.alpha_8;
elseif isfield(params, 'alpha')
  alpha_8 = params.alpha{8};
else
  error 'could not find alpha_8'
end
if isfield(params, 'alpha_9')
  alpha_9 = params.alpha_9;
elseif isfield(params, 'alpha')
  alpha_9 = params.alpha{9};
else
  error 'could not find alpha_9'
end
if isfield(params, 'alpha_10')
  alpha_10 = params.alpha_10;
elseif isfield(params, 'alpha')
  alpha_10 = params.alpha{10};
else
  error 'could not find alpha_10'
end
if isfield(params, 'alpha_11')
  alpha_11 = params.alpha_11;
elseif isfield(params, 'alpha')
  alpha_11 = params.alpha{11};
else
  error 'could not find alpha_11'
end
if isfield(params, 'alpha_12')
  alpha_12 = params.alpha_12;
elseif isfield(params, 'alpha')
  alpha_12 = params.alpha{12};
else
  error 'could not find alpha_12'
end
if isfield(params, 'alpha_13')
  alpha_13 = params.alpha_13;
elseif isfield(params, 'alpha')
  alpha_13 = params.alpha{13};
else
  error 'could not find alpha_13'
end
if isfield(params, 'alpha_14')
  alpha_14 = params.alpha_14;
elseif isfield(params, 'alpha')
  alpha_14 = params.alpha{14};
else
  error 'could not find alpha_14'
end
if isfield(params, 'alpha_15')
  alpha_15 = params.alpha_15;
elseif isfield(params, 'alpha')
  alpha_15 = params.alpha{15};
else
  error 'could not find alpha_15'
end
if isfield(params, 'alpha_16')
  alpha_16 = params.alpha_16;
elseif isfield(params, 'alpha')
  alpha_16 = params.alpha{16};
else
  error 'could not find alpha_16'
end
if isfield(params, 'alpha_17')
  alpha_17 = params.alpha_17;
elseif isfield(params, 'alpha')
  alpha_17 = params.alpha{17};
else
  error 'could not find alpha_17'
end
if isfield(params, 'alpha_18')
  alpha_18 = params.alpha_18;
elseif isfield(params, 'alpha')
  alpha_18 = params.alpha{18};
else
  error 'could not find alpha_18'
end
if isfield(params, 'alpha_19')
  alpha_19 = params.alpha_19;
elseif isfield(params, 'alpha')
  alpha_19 = params.alpha{19};
else
  error 'could not find alpha_19'
end
if isfield(params, 'alpha_20')
  alpha_20 = params.alpha_20;
elseif isfield(params, 'alpha')
  alpha_20 = params.alpha{20};
else
  error 'could not find alpha_20'
end
if isfield(params, 'alpha_21')
  alpha_21 = params.alpha_21;
elseif isfield(params, 'alpha')
  alpha_21 = params.alpha{21};
else
  error 'could not find alpha_21'
end
if isfield(params, 'alpha_22')
  alpha_22 = params.alpha_22;
elseif isfield(params, 'alpha')
  alpha_22 = params.alpha{22};
else
  error 'could not find alpha_22'
end
if isfield(params, 'alpha_23')
  alpha_23 = params.alpha_23;
elseif isfield(params, 'alpha')
  alpha_23 = params.alpha{23};
else
  error 'could not find alpha_23'
end
if isfield(params, 'alpha_24')
  alpha_24 = params.alpha_24;
elseif isfield(params, 'alpha')
  alpha_24 = params.alpha{24};
else
  error 'could not find alpha_24'
end
if isfield(params, 'alpha_25')
  alpha_25 = params.alpha_25;
elseif isfield(params, 'alpha')
  alpha_25 = params.alpha{25};
else
  error 'could not find alpha_25'
end
if isfield(params, 'alpha_26')
  alpha_26 = params.alpha_26;
elseif isfield(params, 'alpha')
  alpha_26 = params.alpha{26};
else
  error 'could not find alpha_26'
end
if isfield(params, 'alpha_27')
  alpha_27 = params.alpha_27;
elseif isfield(params, 'alpha')
  alpha_27 = params.alpha{27};
else
  error 'could not find alpha_27'
end
if isfield(params, 'alpha_28')
  alpha_28 = params.alpha_28;
elseif isfield(params, 'alpha')
  alpha_28 = params.alpha{28};
else
  error 'could not find alpha_28'
end
if isfield(params, 'alpha_29')
  alpha_29 = params.alpha_29;
elseif isfield(params, 'alpha')
  alpha_29 = params.alpha{29};
else
  error 'could not find alpha_29'
end
if isfield(params, 'alpha_30')
  alpha_30 = params.alpha_30;
elseif isfield(params, 'alpha')
  alpha_30 = params.alpha{30};
else
  error 'could not find alpha_30'
end
if isfield(params, 'alpha_31')
  alpha_31 = params.alpha_31;
elseif isfield(params, 'alpha')
  alpha_31 = params.alpha{31};
else
  error 'could not find alpha_31'
end
if isfield(params, 'alpha_32')
  alpha_32 = params.alpha_32;
elseif isfield(params, 'alpha')
  alpha_32 = params.alpha{32};
else
  error 'could not find alpha_32'
end
if isfield(params, 'alpha_33')
  alpha_33 = params.alpha_33;
elseif isfield(params, 'alpha')
  alpha_33 = params.alpha{33};
else
  error 'could not find alpha_33'
end
if isfield(params, 'alpha_34')
  alpha_34 = params.alpha_34;
elseif isfield(params, 'alpha')
  alpha_34 = params.alpha{34};
else
  error 'could not find alpha_34'
end
if isfield(params, 'alpha_35')
  alpha_35 = params.alpha_35;
elseif isfield(params, 'alpha')
  alpha_35 = params.alpha{35};
else
  error 'could not find alpha_35'
end
if isfield(params, 'alpha_36')
  alpha_36 = params.alpha_36;
elseif isfield(params, 'alpha')
  alpha_36 = params.alpha{36};
else
  error 'could not find alpha_36'
end
if isfield(params, 'alpha_37')
  alpha_37 = params.alpha_37;
elseif isfield(params, 'alpha')
  alpha_37 = params.alpha{37};
else
  error 'could not find alpha_37'
end
if isfield(params, 'alpha_38')
  alpha_38 = params.alpha_38;
elseif isfield(params, 'alpha')
  alpha_38 = params.alpha{38};
else
  error 'could not find alpha_38'
end
if isfield(params, 'alpha_39')
  alpha_39 = params.alpha_39;
elseif isfield(params, 'alpha')
  alpha_39 = params.alpha{39};
else
  error 'could not find alpha_39'
end
if isfield(params, 'alpha_40')
  alpha_40 = params.alpha_40;
elseif isfield(params, 'alpha')
  alpha_40 = params.alpha{40};
else
  error 'could not find alpha_40'
end
alpha_max = params.alpha_max;
alpha_min = params.alpha_min;
c1 = params.c1;
c2 = params.c2;
cos_xk_0 = params.cos_xk_0;
if isfield(params, 'cos_xk_1')
  cos_xk_1 = params.cos_xk_1;
elseif isfield(params, 'cos_xk')
  cos_xk_1 = params.cos_xk{1};
else
  error 'could not find cos_xk_1'
end
if isfield(params, 'cos_xk_2')
  cos_xk_2 = params.cos_xk_2;
elseif isfield(params, 'cos_xk')
  cos_xk_2 = params.cos_xk{2};
else
  error 'could not find cos_xk_2'
end
if isfield(params, 'cos_xk_3')
  cos_xk_3 = params.cos_xk_3;
elseif isfield(params, 'cos_xk')
  cos_xk_3 = params.cos_xk{3};
else
  error 'could not find cos_xk_3'
end
if isfield(params, 'cos_xk_4')
  cos_xk_4 = params.cos_xk_4;
elseif isfield(params, 'cos_xk')
  cos_xk_4 = params.cos_xk{4};
else
  error 'could not find cos_xk_4'
end
if isfield(params, 'cos_xk_5')
  cos_xk_5 = params.cos_xk_5;
elseif isfield(params, 'cos_xk')
  cos_xk_5 = params.cos_xk{5};
else
  error 'could not find cos_xk_5'
end
if isfield(params, 'cos_xk_6')
  cos_xk_6 = params.cos_xk_6;
elseif isfield(params, 'cos_xk')
  cos_xk_6 = params.cos_xk{6};
else
  error 'could not find cos_xk_6'
end
if isfield(params, 'cos_xk_7')
  cos_xk_7 = params.cos_xk_7;
elseif isfield(params, 'cos_xk')
  cos_xk_7 = params.cos_xk{7};
else
  error 'could not find cos_xk_7'
end
if isfield(params, 'cos_xk_8')
  cos_xk_8 = params.cos_xk_8;
elseif isfield(params, 'cos_xk')
  cos_xk_8 = params.cos_xk{8};
else
  error 'could not find cos_xk_8'
end
if isfield(params, 'cos_xk_9')
  cos_xk_9 = params.cos_xk_9;
elseif isfield(params, 'cos_xk')
  cos_xk_9 = params.cos_xk{9};
else
  error 'could not find cos_xk_9'
end
if isfield(params, 'cos_xk_10')
  cos_xk_10 = params.cos_xk_10;
elseif isfield(params, 'cos_xk')
  cos_xk_10 = params.cos_xk{10};
else
  error 'could not find cos_xk_10'
end
if isfield(params, 'cos_xk_11')
  cos_xk_11 = params.cos_xk_11;
elseif isfield(params, 'cos_xk')
  cos_xk_11 = params.cos_xk{11};
else
  error 'could not find cos_xk_11'
end
if isfield(params, 'cos_xk_12')
  cos_xk_12 = params.cos_xk_12;
elseif isfield(params, 'cos_xk')
  cos_xk_12 = params.cos_xk{12};
else
  error 'could not find cos_xk_12'
end
if isfield(params, 'cos_xk_13')
  cos_xk_13 = params.cos_xk_13;
elseif isfield(params, 'cos_xk')
  cos_xk_13 = params.cos_xk{13};
else
  error 'could not find cos_xk_13'
end
if isfield(params, 'cos_xk_14')
  cos_xk_14 = params.cos_xk_14;
elseif isfield(params, 'cos_xk')
  cos_xk_14 = params.cos_xk{14};
else
  error 'could not find cos_xk_14'
end
if isfield(params, 'cos_xk_15')
  cos_xk_15 = params.cos_xk_15;
elseif isfield(params, 'cos_xk')
  cos_xk_15 = params.cos_xk{15};
else
  error 'could not find cos_xk_15'
end
if isfield(params, 'cos_xk_16')
  cos_xk_16 = params.cos_xk_16;
elseif isfield(params, 'cos_xk')
  cos_xk_16 = params.cos_xk{16};
else
  error 'could not find cos_xk_16'
end
if isfield(params, 'cos_xk_17')
  cos_xk_17 = params.cos_xk_17;
elseif isfield(params, 'cos_xk')
  cos_xk_17 = params.cos_xk{17};
else
  error 'could not find cos_xk_17'
end
if isfield(params, 'cos_xk_18')
  cos_xk_18 = params.cos_xk_18;
elseif isfield(params, 'cos_xk')
  cos_xk_18 = params.cos_xk{18};
else
  error 'could not find cos_xk_18'
end
if isfield(params, 'cos_xk_19')
  cos_xk_19 = params.cos_xk_19;
elseif isfield(params, 'cos_xk')
  cos_xk_19 = params.cos_xk{19};
else
  error 'could not find cos_xk_19'
end
if isfield(params, 'cos_xk_20')
  cos_xk_20 = params.cos_xk_20;
elseif isfield(params, 'cos_xk')
  cos_xk_20 = params.cos_xk{20};
else
  error 'could not find cos_xk_20'
end
if isfield(params, 'cos_xk_21')
  cos_xk_21 = params.cos_xk_21;
elseif isfield(params, 'cos_xk')
  cos_xk_21 = params.cos_xk{21};
else
  error 'could not find cos_xk_21'
end
if isfield(params, 'cos_xk_22')
  cos_xk_22 = params.cos_xk_22;
elseif isfield(params, 'cos_xk')
  cos_xk_22 = params.cos_xk{22};
else
  error 'could not find cos_xk_22'
end
if isfield(params, 'cos_xk_23')
  cos_xk_23 = params.cos_xk_23;
elseif isfield(params, 'cos_xk')
  cos_xk_23 = params.cos_xk{23};
else
  error 'could not find cos_xk_23'
end
if isfield(params, 'cos_xk_24')
  cos_xk_24 = params.cos_xk_24;
elseif isfield(params, 'cos_xk')
  cos_xk_24 = params.cos_xk{24};
else
  error 'could not find cos_xk_24'
end
if isfield(params, 'cos_xk_25')
  cos_xk_25 = params.cos_xk_25;
elseif isfield(params, 'cos_xk')
  cos_xk_25 = params.cos_xk{25};
else
  error 'could not find cos_xk_25'
end
if isfield(params, 'cos_xk_26')
  cos_xk_26 = params.cos_xk_26;
elseif isfield(params, 'cos_xk')
  cos_xk_26 = params.cos_xk{26};
else
  error 'could not find cos_xk_26'
end
if isfield(params, 'cos_xk_27')
  cos_xk_27 = params.cos_xk_27;
elseif isfield(params, 'cos_xk')
  cos_xk_27 = params.cos_xk{27};
else
  error 'could not find cos_xk_27'
end
if isfield(params, 'cos_xk_28')
  cos_xk_28 = params.cos_xk_28;
elseif isfield(params, 'cos_xk')
  cos_xk_28 = params.cos_xk{28};
else
  error 'could not find cos_xk_28'
end
if isfield(params, 'cos_xk_29')
  cos_xk_29 = params.cos_xk_29;
elseif isfield(params, 'cos_xk')
  cos_xk_29 = params.cos_xk{29};
else
  error 'could not find cos_xk_29'
end
if isfield(params, 'cos_xk_30')
  cos_xk_30 = params.cos_xk_30;
elseif isfield(params, 'cos_xk')
  cos_xk_30 = params.cos_xk{30};
else
  error 'could not find cos_xk_30'
end
if isfield(params, 'cos_xk_31')
  cos_xk_31 = params.cos_xk_31;
elseif isfield(params, 'cos_xk')
  cos_xk_31 = params.cos_xk{31};
else
  error 'could not find cos_xk_31'
end
if isfield(params, 'cos_xk_32')
  cos_xk_32 = params.cos_xk_32;
elseif isfield(params, 'cos_xk')
  cos_xk_32 = params.cos_xk{32};
else
  error 'could not find cos_xk_32'
end
if isfield(params, 'cos_xk_33')
  cos_xk_33 = params.cos_xk_33;
elseif isfield(params, 'cos_xk')
  cos_xk_33 = params.cos_xk{33};
else
  error 'could not find cos_xk_33'
end
if isfield(params, 'cos_xk_34')
  cos_xk_34 = params.cos_xk_34;
elseif isfield(params, 'cos_xk')
  cos_xk_34 = params.cos_xk{34};
else
  error 'could not find cos_xk_34'
end
if isfield(params, 'cos_xk_35')
  cos_xk_35 = params.cos_xk_35;
elseif isfield(params, 'cos_xk')
  cos_xk_35 = params.cos_xk{35};
else
  error 'could not find cos_xk_35'
end
if isfield(params, 'cos_xk_36')
  cos_xk_36 = params.cos_xk_36;
elseif isfield(params, 'cos_xk')
  cos_xk_36 = params.cos_xk{36};
else
  error 'could not find cos_xk_36'
end
if isfield(params, 'cos_xk_37')
  cos_xk_37 = params.cos_xk_37;
elseif isfield(params, 'cos_xk')
  cos_xk_37 = params.cos_xk{37};
else
  error 'could not find cos_xk_37'
end
if isfield(params, 'cos_xk_38')
  cos_xk_38 = params.cos_xk_38;
elseif isfield(params, 'cos_xk')
  cos_xk_38 = params.cos_xk{38};
else
  error 'could not find cos_xk_38'
end
if isfield(params, 'cos_xk_39')
  cos_xk_39 = params.cos_xk_39;
elseif isfield(params, 'cos_xk')
  cos_xk_39 = params.cos_xk{39};
else
  error 'could not find cos_xk_39'
end
if isfield(params, 'cos_xk_40')
  cos_xk_40 = params.cos_xk_40;
elseif isfield(params, 'cos_xk')
  cos_xk_40 = params.cos_xk{40};
else
  error 'could not find cos_xk_40'
end
d_0 = params.d_0;
delta_alpha_max = params.delta_alpha_max;
delta_t = params.delta_t;
delta_u_max = params.delta_u_max;
disturbance_0 = params.disturbance_0;
if isfield(params, 'disturbance_1')
  disturbance_1 = params.disturbance_1;
elseif isfield(params, 'disturbance')
  disturbance_1 = params.disturbance{1};
else
  error 'could not find disturbance_1'
end
if isfield(params, 'disturbance_2')
  disturbance_2 = params.disturbance_2;
elseif isfield(params, 'disturbance')
  disturbance_2 = params.disturbance{2};
else
  error 'could not find disturbance_2'
end
if isfield(params, 'disturbance_3')
  disturbance_3 = params.disturbance_3;
elseif isfield(params, 'disturbance')
  disturbance_3 = params.disturbance{3};
else
  error 'could not find disturbance_3'
end
if isfield(params, 'disturbance_4')
  disturbance_4 = params.disturbance_4;
elseif isfield(params, 'disturbance')
  disturbance_4 = params.disturbance{4};
else
  error 'could not find disturbance_4'
end
if isfield(params, 'disturbance_5')
  disturbance_5 = params.disturbance_5;
elseif isfield(params, 'disturbance')
  disturbance_5 = params.disturbance{5};
else
  error 'could not find disturbance_5'
end
if isfield(params, 'disturbance_6')
  disturbance_6 = params.disturbance_6;
elseif isfield(params, 'disturbance')
  disturbance_6 = params.disturbance{6};
else
  error 'could not find disturbance_6'
end
if isfield(params, 'disturbance_7')
  disturbance_7 = params.disturbance_7;
elseif isfield(params, 'disturbance')
  disturbance_7 = params.disturbance{7};
else
  error 'could not find disturbance_7'
end
if isfield(params, 'disturbance_8')
  disturbance_8 = params.disturbance_8;
elseif isfield(params, 'disturbance')
  disturbance_8 = params.disturbance{8};
else
  error 'could not find disturbance_8'
end
if isfield(params, 'disturbance_9')
  disturbance_9 = params.disturbance_9;
elseif isfield(params, 'disturbance')
  disturbance_9 = params.disturbance{9};
else
  error 'could not find disturbance_9'
end
if isfield(params, 'disturbance_10')
  disturbance_10 = params.disturbance_10;
elseif isfield(params, 'disturbance')
  disturbance_10 = params.disturbance{10};
else
  error 'could not find disturbance_10'
end
if isfield(params, 'disturbance_11')
  disturbance_11 = params.disturbance_11;
elseif isfield(params, 'disturbance')
  disturbance_11 = params.disturbance{11};
else
  error 'could not find disturbance_11'
end
if isfield(params, 'disturbance_12')
  disturbance_12 = params.disturbance_12;
elseif isfield(params, 'disturbance')
  disturbance_12 = params.disturbance{12};
else
  error 'could not find disturbance_12'
end
if isfield(params, 'disturbance_13')
  disturbance_13 = params.disturbance_13;
elseif isfield(params, 'disturbance')
  disturbance_13 = params.disturbance{13};
else
  error 'could not find disturbance_13'
end
if isfield(params, 'disturbance_14')
  disturbance_14 = params.disturbance_14;
elseif isfield(params, 'disturbance')
  disturbance_14 = params.disturbance{14};
else
  error 'could not find disturbance_14'
end
if isfield(params, 'disturbance_15')
  disturbance_15 = params.disturbance_15;
elseif isfield(params, 'disturbance')
  disturbance_15 = params.disturbance{15};
else
  error 'could not find disturbance_15'
end
if isfield(params, 'disturbance_16')
  disturbance_16 = params.disturbance_16;
elseif isfield(params, 'disturbance')
  disturbance_16 = params.disturbance{16};
else
  error 'could not find disturbance_16'
end
if isfield(params, 'disturbance_17')
  disturbance_17 = params.disturbance_17;
elseif isfield(params, 'disturbance')
  disturbance_17 = params.disturbance{17};
else
  error 'could not find disturbance_17'
end
if isfield(params, 'disturbance_18')
  disturbance_18 = params.disturbance_18;
elseif isfield(params, 'disturbance')
  disturbance_18 = params.disturbance{18};
else
  error 'could not find disturbance_18'
end
if isfield(params, 'disturbance_19')
  disturbance_19 = params.disturbance_19;
elseif isfield(params, 'disturbance')
  disturbance_19 = params.disturbance{19};
else
  error 'could not find disturbance_19'
end
if isfield(params, 'disturbance_20')
  disturbance_20 = params.disturbance_20;
elseif isfield(params, 'disturbance')
  disturbance_20 = params.disturbance{20};
else
  error 'could not find disturbance_20'
end
if isfield(params, 'disturbance_21')
  disturbance_21 = params.disturbance_21;
elseif isfield(params, 'disturbance')
  disturbance_21 = params.disturbance{21};
else
  error 'could not find disturbance_21'
end
if isfield(params, 'disturbance_22')
  disturbance_22 = params.disturbance_22;
elseif isfield(params, 'disturbance')
  disturbance_22 = params.disturbance{22};
else
  error 'could not find disturbance_22'
end
if isfield(params, 'disturbance_23')
  disturbance_23 = params.disturbance_23;
elseif isfield(params, 'disturbance')
  disturbance_23 = params.disturbance{23};
else
  error 'could not find disturbance_23'
end
if isfield(params, 'disturbance_24')
  disturbance_24 = params.disturbance_24;
elseif isfield(params, 'disturbance')
  disturbance_24 = params.disturbance{24};
else
  error 'could not find disturbance_24'
end
if isfield(params, 'disturbance_25')
  disturbance_25 = params.disturbance_25;
elseif isfield(params, 'disturbance')
  disturbance_25 = params.disturbance{25};
else
  error 'could not find disturbance_25'
end
if isfield(params, 'disturbance_26')
  disturbance_26 = params.disturbance_26;
elseif isfield(params, 'disturbance')
  disturbance_26 = params.disturbance{26};
else
  error 'could not find disturbance_26'
end
if isfield(params, 'disturbance_27')
  disturbance_27 = params.disturbance_27;
elseif isfield(params, 'disturbance')
  disturbance_27 = params.disturbance{27};
else
  error 'could not find disturbance_27'
end
if isfield(params, 'disturbance_28')
  disturbance_28 = params.disturbance_28;
elseif isfield(params, 'disturbance')
  disturbance_28 = params.disturbance{28};
else
  error 'could not find disturbance_28'
end
if isfield(params, 'disturbance_29')
  disturbance_29 = params.disturbance_29;
elseif isfield(params, 'disturbance')
  disturbance_29 = params.disturbance{29};
else
  error 'could not find disturbance_29'
end
if isfield(params, 'disturbance_30')
  disturbance_30 = params.disturbance_30;
elseif isfield(params, 'disturbance')
  disturbance_30 = params.disturbance{30};
else
  error 'could not find disturbance_30'
end
if isfield(params, 'disturbance_31')
  disturbance_31 = params.disturbance_31;
elseif isfield(params, 'disturbance')
  disturbance_31 = params.disturbance{31};
else
  error 'could not find disturbance_31'
end
if isfield(params, 'disturbance_32')
  disturbance_32 = params.disturbance_32;
elseif isfield(params, 'disturbance')
  disturbance_32 = params.disturbance{32};
else
  error 'could not find disturbance_32'
end
if isfield(params, 'disturbance_33')
  disturbance_33 = params.disturbance_33;
elseif isfield(params, 'disturbance')
  disturbance_33 = params.disturbance{33};
else
  error 'could not find disturbance_33'
end
if isfield(params, 'disturbance_34')
  disturbance_34 = params.disturbance_34;
elseif isfield(params, 'disturbance')
  disturbance_34 = params.disturbance{34};
else
  error 'could not find disturbance_34'
end
if isfield(params, 'disturbance_35')
  disturbance_35 = params.disturbance_35;
elseif isfield(params, 'disturbance')
  disturbance_35 = params.disturbance{35};
else
  error 'could not find disturbance_35'
end
if isfield(params, 'disturbance_36')
  disturbance_36 = params.disturbance_36;
elseif isfield(params, 'disturbance')
  disturbance_36 = params.disturbance{36};
else
  error 'could not find disturbance_36'
end
if isfield(params, 'disturbance_37')
  disturbance_37 = params.disturbance_37;
elseif isfield(params, 'disturbance')
  disturbance_37 = params.disturbance{37};
else
  error 'could not find disturbance_37'
end
if isfield(params, 'disturbance_38')
  disturbance_38 = params.disturbance_38;
elseif isfield(params, 'disturbance')
  disturbance_38 = params.disturbance{38};
else
  error 'could not find disturbance_38'
end
if isfield(params, 'disturbance_39')
  disturbance_39 = params.disturbance_39;
elseif isfield(params, 'disturbance')
  disturbance_39 = params.disturbance{39};
else
  error 'could not find disturbance_39'
end
if isfield(params, 'disturbance_40')
  disturbance_40 = params.disturbance_40;
elseif isfield(params, 'disturbance')
  disturbance_40 = params.disturbance{40};
else
  error 'could not find disturbance_40'
end
lambda = params.lambda;
last_alpha = params.last_alpha;
last_u = params.last_u;
new_EF_0 = params.new_EF_0;
old_EF_0 = params.old_EF_0;
if isfield(params, 'old_EF_1')
  old_EF_1 = params.old_EF_1;
elseif isfield(params, 'old_EF')
  old_EF_1 = params.old_EF{1};
else
  error 'could not find old_EF_1'
end
if isfield(params, 'old_EF_2')
  old_EF_2 = params.old_EF_2;
elseif isfield(params, 'old_EF')
  old_EF_2 = params.old_EF{2};
else
  error 'could not find old_EF_2'
end
if isfield(params, 'old_EF_3')
  old_EF_3 = params.old_EF_3;
elseif isfield(params, 'old_EF')
  old_EF_3 = params.old_EF{3};
else
  error 'could not find old_EF_3'
end
if isfield(params, 'old_EF_4')
  old_EF_4 = params.old_EF_4;
elseif isfield(params, 'old_EF')
  old_EF_4 = params.old_EF{4};
else
  error 'could not find old_EF_4'
end
if isfield(params, 'old_EF_5')
  old_EF_5 = params.old_EF_5;
elseif isfield(params, 'old_EF')
  old_EF_5 = params.old_EF{5};
else
  error 'could not find old_EF_5'
end
if isfield(params, 'old_EF_6')
  old_EF_6 = params.old_EF_6;
elseif isfield(params, 'old_EF')
  old_EF_6 = params.old_EF{6};
else
  error 'could not find old_EF_6'
end
if isfield(params, 'old_EF_7')
  old_EF_7 = params.old_EF_7;
elseif isfield(params, 'old_EF')
  old_EF_7 = params.old_EF{7};
else
  error 'could not find old_EF_7'
end
if isfield(params, 'old_EF_8')
  old_EF_8 = params.old_EF_8;
elseif isfield(params, 'old_EF')
  old_EF_8 = params.old_EF{8};
else
  error 'could not find old_EF_8'
end
if isfield(params, 'old_EF_9')
  old_EF_9 = params.old_EF_9;
elseif isfield(params, 'old_EF')
  old_EF_9 = params.old_EF{9};
else
  error 'could not find old_EF_9'
end
if isfield(params, 'old_EF_10')
  old_EF_10 = params.old_EF_10;
elseif isfield(params, 'old_EF')
  old_EF_10 = params.old_EF{10};
else
  error 'could not find old_EF_10'
end
if isfield(params, 'old_EF_11')
  old_EF_11 = params.old_EF_11;
elseif isfield(params, 'old_EF')
  old_EF_11 = params.old_EF{11};
else
  error 'could not find old_EF_11'
end
if isfield(params, 'old_EF_12')
  old_EF_12 = params.old_EF_12;
elseif isfield(params, 'old_EF')
  old_EF_12 = params.old_EF{12};
else
  error 'could not find old_EF_12'
end
if isfield(params, 'old_EF_13')
  old_EF_13 = params.old_EF_13;
elseif isfield(params, 'old_EF')
  old_EF_13 = params.old_EF{13};
else
  error 'could not find old_EF_13'
end
if isfield(params, 'old_EF_14')
  old_EF_14 = params.old_EF_14;
elseif isfield(params, 'old_EF')
  old_EF_14 = params.old_EF{14};
else
  error 'could not find old_EF_14'
end
if isfield(params, 'old_EF_15')
  old_EF_15 = params.old_EF_15;
elseif isfield(params, 'old_EF')
  old_EF_15 = params.old_EF{15};
else
  error 'could not find old_EF_15'
end
if isfield(params, 'old_EF_16')
  old_EF_16 = params.old_EF_16;
elseif isfield(params, 'old_EF')
  old_EF_16 = params.old_EF{16};
else
  error 'could not find old_EF_16'
end
if isfield(params, 'old_EF_17')
  old_EF_17 = params.old_EF_17;
elseif isfield(params, 'old_EF')
  old_EF_17 = params.old_EF{17};
else
  error 'could not find old_EF_17'
end
if isfield(params, 'old_EF_18')
  old_EF_18 = params.old_EF_18;
elseif isfield(params, 'old_EF')
  old_EF_18 = params.old_EF{18};
else
  error 'could not find old_EF_18'
end
if isfield(params, 'old_EF_19')
  old_EF_19 = params.old_EF_19;
elseif isfield(params, 'old_EF')
  old_EF_19 = params.old_EF{19};
else
  error 'could not find old_EF_19'
end
if isfield(params, 'old_EF_20')
  old_EF_20 = params.old_EF_20;
elseif isfield(params, 'old_EF')
  old_EF_20 = params.old_EF{20};
else
  error 'could not find old_EF_20'
end
if isfield(params, 'old_EF_21')
  old_EF_21 = params.old_EF_21;
elseif isfield(params, 'old_EF')
  old_EF_21 = params.old_EF{21};
else
  error 'could not find old_EF_21'
end
if isfield(params, 'old_EF_22')
  old_EF_22 = params.old_EF_22;
elseif isfield(params, 'old_EF')
  old_EF_22 = params.old_EF{22};
else
  error 'could not find old_EF_22'
end
if isfield(params, 'old_EF_23')
  old_EF_23 = params.old_EF_23;
elseif isfield(params, 'old_EF')
  old_EF_23 = params.old_EF{23};
else
  error 'could not find old_EF_23'
end
if isfield(params, 'old_EF_24')
  old_EF_24 = params.old_EF_24;
elseif isfield(params, 'old_EF')
  old_EF_24 = params.old_EF{24};
else
  error 'could not find old_EF_24'
end
if isfield(params, 'old_EF_25')
  old_EF_25 = params.old_EF_25;
elseif isfield(params, 'old_EF')
  old_EF_25 = params.old_EF{25};
else
  error 'could not find old_EF_25'
end
if isfield(params, 'old_EF_26')
  old_EF_26 = params.old_EF_26;
elseif isfield(params, 'old_EF')
  old_EF_26 = params.old_EF{26};
else
  error 'could not find old_EF_26'
end
if isfield(params, 'old_EF_27')
  old_EF_27 = params.old_EF_27;
elseif isfield(params, 'old_EF')
  old_EF_27 = params.old_EF{27};
else
  error 'could not find old_EF_27'
end
if isfield(params, 'old_EF_28')
  old_EF_28 = params.old_EF_28;
elseif isfield(params, 'old_EF')
  old_EF_28 = params.old_EF{28};
else
  error 'could not find old_EF_28'
end
if isfield(params, 'old_EF_29')
  old_EF_29 = params.old_EF_29;
elseif isfield(params, 'old_EF')
  old_EF_29 = params.old_EF{29};
else
  error 'could not find old_EF_29'
end
if isfield(params, 'old_EF_30')
  old_EF_30 = params.old_EF_30;
elseif isfield(params, 'old_EF')
  old_EF_30 = params.old_EF{30};
else
  error 'could not find old_EF_30'
end
if isfield(params, 'old_EF_31')
  old_EF_31 = params.old_EF_31;
elseif isfield(params, 'old_EF')
  old_EF_31 = params.old_EF{31};
else
  error 'could not find old_EF_31'
end
if isfield(params, 'old_EF_32')
  old_EF_32 = params.old_EF_32;
elseif isfield(params, 'old_EF')
  old_EF_32 = params.old_EF{32};
else
  error 'could not find old_EF_32'
end
if isfield(params, 'old_EF_33')
  old_EF_33 = params.old_EF_33;
elseif isfield(params, 'old_EF')
  old_EF_33 = params.old_EF{33};
else
  error 'could not find old_EF_33'
end
if isfield(params, 'old_EF_34')
  old_EF_34 = params.old_EF_34;
elseif isfield(params, 'old_EF')
  old_EF_34 = params.old_EF{34};
else
  error 'could not find old_EF_34'
end
if isfield(params, 'old_EF_35')
  old_EF_35 = params.old_EF_35;
elseif isfield(params, 'old_EF')
  old_EF_35 = params.old_EF{35};
else
  error 'could not find old_EF_35'
end
if isfield(params, 'old_EF_36')
  old_EF_36 = params.old_EF_36;
elseif isfield(params, 'old_EF')
  old_EF_36 = params.old_EF{36};
else
  error 'could not find old_EF_36'
end
if isfield(params, 'old_EF_37')
  old_EF_37 = params.old_EF_37;
elseif isfield(params, 'old_EF')
  old_EF_37 = params.old_EF{37};
else
  error 'could not find old_EF_37'
end
if isfield(params, 'old_EF_38')
  old_EF_38 = params.old_EF_38;
elseif isfield(params, 'old_EF')
  old_EF_38 = params.old_EF{38};
else
  error 'could not find old_EF_38'
end
if isfield(params, 'old_EF_39')
  old_EF_39 = params.old_EF_39;
elseif isfield(params, 'old_EF')
  old_EF_39 = params.old_EF{39};
else
  error 'could not find old_EF_39'
end
if isfield(params, 'old_EF_40')
  old_EF_40 = params.old_EF_40;
elseif isfield(params, 'old_EF')
  old_EF_40 = params.old_EF{40};
else
  error 'could not find old_EF_40'
end
omega_0 = params.omega_0;
omega_max = params.omega_max;
u_max = params.u_max;
uk_0 = params.uk_0;
if isfield(params, 'uk_1')
  uk_1 = params.uk_1;
elseif isfield(params, 'uk')
  uk_1 = params.uk{1};
else
  error 'could not find uk_1'
end
if isfield(params, 'uk_2')
  uk_2 = params.uk_2;
elseif isfield(params, 'uk')
  uk_2 = params.uk{2};
else
  error 'could not find uk_2'
end
if isfield(params, 'uk_3')
  uk_3 = params.uk_3;
elseif isfield(params, 'uk')
  uk_3 = params.uk{3};
else
  error 'could not find uk_3'
end
if isfield(params, 'uk_4')
  uk_4 = params.uk_4;
elseif isfield(params, 'uk')
  uk_4 = params.uk{4};
else
  error 'could not find uk_4'
end
if isfield(params, 'uk_5')
  uk_5 = params.uk_5;
elseif isfield(params, 'uk')
  uk_5 = params.uk{5};
else
  error 'could not find uk_5'
end
if isfield(params, 'uk_6')
  uk_6 = params.uk_6;
elseif isfield(params, 'uk')
  uk_6 = params.uk{6};
else
  error 'could not find uk_6'
end
if isfield(params, 'uk_7')
  uk_7 = params.uk_7;
elseif isfield(params, 'uk')
  uk_7 = params.uk{7};
else
  error 'could not find uk_7'
end
if isfield(params, 'uk_8')
  uk_8 = params.uk_8;
elseif isfield(params, 'uk')
  uk_8 = params.uk{8};
else
  error 'could not find uk_8'
end
if isfield(params, 'uk_9')
  uk_9 = params.uk_9;
elseif isfield(params, 'uk')
  uk_9 = params.uk{9};
else
  error 'could not find uk_9'
end
if isfield(params, 'uk_10')
  uk_10 = params.uk_10;
elseif isfield(params, 'uk')
  uk_10 = params.uk{10};
else
  error 'could not find uk_10'
end
if isfield(params, 'uk_11')
  uk_11 = params.uk_11;
elseif isfield(params, 'uk')
  uk_11 = params.uk{11};
else
  error 'could not find uk_11'
end
if isfield(params, 'uk_12')
  uk_12 = params.uk_12;
elseif isfield(params, 'uk')
  uk_12 = params.uk{12};
else
  error 'could not find uk_12'
end
if isfield(params, 'uk_13')
  uk_13 = params.uk_13;
elseif isfield(params, 'uk')
  uk_13 = params.uk{13};
else
  error 'could not find uk_13'
end
if isfield(params, 'uk_14')
  uk_14 = params.uk_14;
elseif isfield(params, 'uk')
  uk_14 = params.uk{14};
else
  error 'could not find uk_14'
end
if isfield(params, 'uk_15')
  uk_15 = params.uk_15;
elseif isfield(params, 'uk')
  uk_15 = params.uk{15};
else
  error 'could not find uk_15'
end
if isfield(params, 'uk_16')
  uk_16 = params.uk_16;
elseif isfield(params, 'uk')
  uk_16 = params.uk{16};
else
  error 'could not find uk_16'
end
if isfield(params, 'uk_17')
  uk_17 = params.uk_17;
elseif isfield(params, 'uk')
  uk_17 = params.uk{17};
else
  error 'could not find uk_17'
end
if isfield(params, 'uk_18')
  uk_18 = params.uk_18;
elseif isfield(params, 'uk')
  uk_18 = params.uk{18};
else
  error 'could not find uk_18'
end
if isfield(params, 'uk_19')
  uk_19 = params.uk_19;
elseif isfield(params, 'uk')
  uk_19 = params.uk{19};
else
  error 'could not find uk_19'
end
if isfield(params, 'uk_20')
  uk_20 = params.uk_20;
elseif isfield(params, 'uk')
  uk_20 = params.uk{20};
else
  error 'could not find uk_20'
end
if isfield(params, 'uk_21')
  uk_21 = params.uk_21;
elseif isfield(params, 'uk')
  uk_21 = params.uk{21};
else
  error 'could not find uk_21'
end
if isfield(params, 'uk_22')
  uk_22 = params.uk_22;
elseif isfield(params, 'uk')
  uk_22 = params.uk{22};
else
  error 'could not find uk_22'
end
if isfield(params, 'uk_23')
  uk_23 = params.uk_23;
elseif isfield(params, 'uk')
  uk_23 = params.uk{23};
else
  error 'could not find uk_23'
end
if isfield(params, 'uk_24')
  uk_24 = params.uk_24;
elseif isfield(params, 'uk')
  uk_24 = params.uk{24};
else
  error 'could not find uk_24'
end
if isfield(params, 'uk_25')
  uk_25 = params.uk_25;
elseif isfield(params, 'uk')
  uk_25 = params.uk{25};
else
  error 'could not find uk_25'
end
if isfield(params, 'uk_26')
  uk_26 = params.uk_26;
elseif isfield(params, 'uk')
  uk_26 = params.uk{26};
else
  error 'could not find uk_26'
end
if isfield(params, 'uk_27')
  uk_27 = params.uk_27;
elseif isfield(params, 'uk')
  uk_27 = params.uk{27};
else
  error 'could not find uk_27'
end
if isfield(params, 'uk_28')
  uk_28 = params.uk_28;
elseif isfield(params, 'uk')
  uk_28 = params.uk{28};
else
  error 'could not find uk_28'
end
if isfield(params, 'uk_29')
  uk_29 = params.uk_29;
elseif isfield(params, 'uk')
  uk_29 = params.uk{29};
else
  error 'could not find uk_29'
end
if isfield(params, 'uk_30')
  uk_30 = params.uk_30;
elseif isfield(params, 'uk')
  uk_30 = params.uk{30};
else
  error 'could not find uk_30'
end
if isfield(params, 'uk_31')
  uk_31 = params.uk_31;
elseif isfield(params, 'uk')
  uk_31 = params.uk{31};
else
  error 'could not find uk_31'
end
if isfield(params, 'uk_32')
  uk_32 = params.uk_32;
elseif isfield(params, 'uk')
  uk_32 = params.uk{32};
else
  error 'could not find uk_32'
end
if isfield(params, 'uk_33')
  uk_33 = params.uk_33;
elseif isfield(params, 'uk')
  uk_33 = params.uk{33};
else
  error 'could not find uk_33'
end
if isfield(params, 'uk_34')
  uk_34 = params.uk_34;
elseif isfield(params, 'uk')
  uk_34 = params.uk{34};
else
  error 'could not find uk_34'
end
if isfield(params, 'uk_35')
  uk_35 = params.uk_35;
elseif isfield(params, 'uk')
  uk_35 = params.uk{35};
else
  error 'could not find uk_35'
end
if isfield(params, 'uk_36')
  uk_36 = params.uk_36;
elseif isfield(params, 'uk')
  uk_36 = params.uk{36};
else
  error 'could not find uk_36'
end
if isfield(params, 'uk_37')
  uk_37 = params.uk_37;
elseif isfield(params, 'uk')
  uk_37 = params.uk{37};
else
  error 'could not find uk_37'
end
if isfield(params, 'uk_38')
  uk_38 = params.uk_38;
elseif isfield(params, 'uk')
  uk_38 = params.uk{38};
else
  error 'could not find uk_38'
end
if isfield(params, 'uk_39')
  uk_39 = params.uk_39;
elseif isfield(params, 'uk')
  uk_39 = params.uk{39};
else
  error 'could not find uk_39'
end
if isfield(params, 'uk_40')
  uk_40 = params.uk_40;
elseif isfield(params, 'uk')
  uk_40 = params.uk{40};
else
  error 'could not find uk_40'
end
w_max = params.w_max;
x_final = params.x_final;
xk_0 = params.xk_0;
if isfield(params, 'xk_1')
  xk_1 = params.xk_1;
elseif isfield(params, 'xk')
  xk_1 = params.xk{1};
else
  error 'could not find xk_1'
end
if isfield(params, 'xk_2')
  xk_2 = params.xk_2;
elseif isfield(params, 'xk')
  xk_2 = params.xk{2};
else
  error 'could not find xk_2'
end
if isfield(params, 'xk_3')
  xk_3 = params.xk_3;
elseif isfield(params, 'xk')
  xk_3 = params.xk{3};
else
  error 'could not find xk_3'
end
if isfield(params, 'xk_4')
  xk_4 = params.xk_4;
elseif isfield(params, 'xk')
  xk_4 = params.xk{4};
else
  error 'could not find xk_4'
end
if isfield(params, 'xk_5')
  xk_5 = params.xk_5;
elseif isfield(params, 'xk')
  xk_5 = params.xk{5};
else
  error 'could not find xk_5'
end
if isfield(params, 'xk_6')
  xk_6 = params.xk_6;
elseif isfield(params, 'xk')
  xk_6 = params.xk{6};
else
  error 'could not find xk_6'
end
if isfield(params, 'xk_7')
  xk_7 = params.xk_7;
elseif isfield(params, 'xk')
  xk_7 = params.xk{7};
else
  error 'could not find xk_7'
end
if isfield(params, 'xk_8')
  xk_8 = params.xk_8;
elseif isfield(params, 'xk')
  xk_8 = params.xk{8};
else
  error 'could not find xk_8'
end
if isfield(params, 'xk_9')
  xk_9 = params.xk_9;
elseif isfield(params, 'xk')
  xk_9 = params.xk{9};
else
  error 'could not find xk_9'
end
if isfield(params, 'xk_10')
  xk_10 = params.xk_10;
elseif isfield(params, 'xk')
  xk_10 = params.xk{10};
else
  error 'could not find xk_10'
end
if isfield(params, 'xk_11')
  xk_11 = params.xk_11;
elseif isfield(params, 'xk')
  xk_11 = params.xk{11};
else
  error 'could not find xk_11'
end
if isfield(params, 'xk_12')
  xk_12 = params.xk_12;
elseif isfield(params, 'xk')
  xk_12 = params.xk{12};
else
  error 'could not find xk_12'
end
if isfield(params, 'xk_13')
  xk_13 = params.xk_13;
elseif isfield(params, 'xk')
  xk_13 = params.xk{13};
else
  error 'could not find xk_13'
end
if isfield(params, 'xk_14')
  xk_14 = params.xk_14;
elseif isfield(params, 'xk')
  xk_14 = params.xk{14};
else
  error 'could not find xk_14'
end
if isfield(params, 'xk_15')
  xk_15 = params.xk_15;
elseif isfield(params, 'xk')
  xk_15 = params.xk{15};
else
  error 'could not find xk_15'
end
if isfield(params, 'xk_16')
  xk_16 = params.xk_16;
elseif isfield(params, 'xk')
  xk_16 = params.xk{16};
else
  error 'could not find xk_16'
end
if isfield(params, 'xk_17')
  xk_17 = params.xk_17;
elseif isfield(params, 'xk')
  xk_17 = params.xk{17};
else
  error 'could not find xk_17'
end
if isfield(params, 'xk_18')
  xk_18 = params.xk_18;
elseif isfield(params, 'xk')
  xk_18 = params.xk{18};
else
  error 'could not find xk_18'
end
if isfield(params, 'xk_19')
  xk_19 = params.xk_19;
elseif isfield(params, 'xk')
  xk_19 = params.xk{19};
else
  error 'could not find xk_19'
end
if isfield(params, 'xk_20')
  xk_20 = params.xk_20;
elseif isfield(params, 'xk')
  xk_20 = params.xk{20};
else
  error 'could not find xk_20'
end
if isfield(params, 'xk_21')
  xk_21 = params.xk_21;
elseif isfield(params, 'xk')
  xk_21 = params.xk{21};
else
  error 'could not find xk_21'
end
if isfield(params, 'xk_22')
  xk_22 = params.xk_22;
elseif isfield(params, 'xk')
  xk_22 = params.xk{22};
else
  error 'could not find xk_22'
end
if isfield(params, 'xk_23')
  xk_23 = params.xk_23;
elseif isfield(params, 'xk')
  xk_23 = params.xk{23};
else
  error 'could not find xk_23'
end
if isfield(params, 'xk_24')
  xk_24 = params.xk_24;
elseif isfield(params, 'xk')
  xk_24 = params.xk{24};
else
  error 'could not find xk_24'
end
if isfield(params, 'xk_25')
  xk_25 = params.xk_25;
elseif isfield(params, 'xk')
  xk_25 = params.xk{25};
else
  error 'could not find xk_25'
end
if isfield(params, 'xk_26')
  xk_26 = params.xk_26;
elseif isfield(params, 'xk')
  xk_26 = params.xk{26};
else
  error 'could not find xk_26'
end
if isfield(params, 'xk_27')
  xk_27 = params.xk_27;
elseif isfield(params, 'xk')
  xk_27 = params.xk{27};
else
  error 'could not find xk_27'
end
if isfield(params, 'xk_28')
  xk_28 = params.xk_28;
elseif isfield(params, 'xk')
  xk_28 = params.xk{28};
else
  error 'could not find xk_28'
end
if isfield(params, 'xk_29')
  xk_29 = params.xk_29;
elseif isfield(params, 'xk')
  xk_29 = params.xk{29};
else
  error 'could not find xk_29'
end
if isfield(params, 'xk_30')
  xk_30 = params.xk_30;
elseif isfield(params, 'xk')
  xk_30 = params.xk{30};
else
  error 'could not find xk_30'
end
if isfield(params, 'xk_31')
  xk_31 = params.xk_31;
elseif isfield(params, 'xk')
  xk_31 = params.xk{31};
else
  error 'could not find xk_31'
end
if isfield(params, 'xk_32')
  xk_32 = params.xk_32;
elseif isfield(params, 'xk')
  xk_32 = params.xk{32};
else
  error 'could not find xk_32'
end
if isfield(params, 'xk_33')
  xk_33 = params.xk_33;
elseif isfield(params, 'xk')
  xk_33 = params.xk{33};
else
  error 'could not find xk_33'
end
if isfield(params, 'xk_34')
  xk_34 = params.xk_34;
elseif isfield(params, 'xk')
  xk_34 = params.xk{34};
else
  error 'could not find xk_34'
end
if isfield(params, 'xk_35')
  xk_35 = params.xk_35;
elseif isfield(params, 'xk')
  xk_35 = params.xk{35};
else
  error 'could not find xk_35'
end
if isfield(params, 'xk_36')
  xk_36 = params.xk_36;
elseif isfield(params, 'xk')
  xk_36 = params.xk{36};
else
  error 'could not find xk_36'
end
if isfield(params, 'xk_37')
  xk_37 = params.xk_37;
elseif isfield(params, 'xk')
  xk_37 = params.xk{37};
else
  error 'could not find xk_37'
end
if isfield(params, 'xk_38')
  xk_38 = params.xk_38;
elseif isfield(params, 'xk')
  xk_38 = params.xk{38};
else
  error 'could not find xk_38'
end
if isfield(params, 'xk_39')
  xk_39 = params.xk_39;
elseif isfield(params, 'xk')
  xk_39 = params.xk{39};
else
  error 'could not find xk_39'
end
if isfield(params, 'xk_40')
  xk_40 = params.xk_40;
elseif isfield(params, 'xk')
  xk_40 = params.xk{40};
else
  error 'could not find xk_40'
end
if isfield(params, 'xk_41')
  xk_41 = params.xk_41;
elseif isfield(params, 'xk')
  xk_41 = params.xk{41};
else
  error 'could not find xk_41'
end
cvx_begin
  % Caution: automatically generated by cvxgen. May be incorrect.
  variable w_0;
  variable delta_alpha_0;
  variable d_1(2, 1);
  variable w_1;
  variable delta_alpha_1;
  variable d_2(2, 1);
  variable w_2;
  variable delta_alpha_2;
  variable d_3(2, 1);
  variable w_3;
  variable delta_alpha_3;
  variable d_4(2, 1);
  variable w_4;
  variable delta_alpha_4;
  variable d_5(2, 1);
  variable w_5;
  variable delta_alpha_5;
  variable d_6(2, 1);
  variable w_6;
  variable delta_alpha_6;
  variable d_7(2, 1);
  variable w_7;
  variable delta_alpha_7;
  variable d_8(2, 1);
  variable w_8;
  variable delta_alpha_8;
  variable d_9(2, 1);
  variable w_9;
  variable delta_alpha_9;
  variable d_10(2, 1);
  variable w_10;
  variable delta_alpha_10;
  variable d_11(2, 1);
  variable w_11;
  variable delta_alpha_11;
  variable d_12(2, 1);
  variable w_12;
  variable delta_alpha_12;
  variable d_13(2, 1);
  variable w_13;
  variable delta_alpha_13;
  variable d_14(2, 1);
  variable w_14;
  variable delta_alpha_14;
  variable d_15(2, 1);
  variable w_15;
  variable delta_alpha_15;
  variable d_16(2, 1);
  variable w_16;
  variable delta_alpha_16;
  variable d_17(2, 1);
  variable w_17;
  variable delta_alpha_17;
  variable d_18(2, 1);
  variable w_18;
  variable delta_alpha_18;
  variable d_19(2, 1);
  variable w_19;
  variable delta_alpha_19;
  variable d_20(2, 1);
  variable w_20;
  variable delta_alpha_20;
  variable d_21(2, 1);
  variable w_21;
  variable delta_alpha_21;
  variable d_22(2, 1);
  variable w_22;
  variable delta_alpha_22;
  variable d_23(2, 1);
  variable w_23;
  variable delta_alpha_23;
  variable d_24(2, 1);
  variable w_24;
  variable delta_alpha_24;
  variable d_25(2, 1);
  variable w_25;
  variable delta_alpha_25;
  variable d_26(2, 1);
  variable w_26;
  variable delta_alpha_26;
  variable d_27(2, 1);
  variable w_27;
  variable delta_alpha_27;
  variable d_28(2, 1);
  variable w_28;
  variable delta_alpha_28;
  variable d_29(2, 1);
  variable w_29;
  variable delta_alpha_29;
  variable d_30(2, 1);
  variable w_30;
  variable delta_alpha_30;
  variable d_31(2, 1);
  variable w_31;
  variable delta_alpha_31;
  variable d_32(2, 1);
  variable w_32;
  variable delta_alpha_32;
  variable d_33(2, 1);
  variable w_33;
  variable delta_alpha_33;
  variable d_34(2, 1);
  variable w_34;
  variable delta_alpha_34;
  variable d_35(2, 1);
  variable w_35;
  variable delta_alpha_35;
  variable d_36(2, 1);
  variable w_36;
  variable delta_alpha_36;
  variable d_37(2, 1);
  variable w_37;
  variable delta_alpha_37;
  variable d_38(2, 1);
  variable w_38;
  variable delta_alpha_38;
  variable d_39(2, 1);
  variable w_39;
  variable delta_alpha_39;
  variable d_40(2, 1);
  variable w_40;
  variable delta_alpha_40;
  variable d_41(2, 1);
  variable new_EF_1;
  variable new_EF_2;
  variable new_EF_3;
  variable new_EF_4;
  variable new_EF_5;
  variable new_EF_6;
  variable new_EF_7;
  variable new_EF_8;
  variable new_EF_9;
  variable new_EF_10;
  variable new_EF_11;
  variable new_EF_12;
  variable new_EF_13;
  variable new_EF_14;
  variable new_EF_15;
  variable new_EF_16;
  variable new_EF_17;
  variable new_EF_18;
  variable new_EF_19;
  variable new_EF_20;
  variable new_EF_21;
  variable new_EF_22;
  variable new_EF_23;
  variable new_EF_24;
  variable new_EF_25;
  variable new_EF_26;
  variable new_EF_27;
  variable new_EF_28;
  variable new_EF_29;
  variable new_EF_30;
  variable new_EF_31;
  variable new_EF_32;
  variable new_EF_33;
  variable new_EF_34;
  variable new_EF_35;
  variable new_EF_36;
  variable new_EF_37;
  variable new_EF_38;
  variable new_EF_39;
  variable new_EF_40;
  variable new_EF_41;
  variable omega_1;
  variable omega_2;
  variable omega_3;
  variable omega_4;
  variable omega_5;
  variable omega_6;
  variable omega_7;
  variable omega_8;
  variable omega_9;
  variable omega_10;
  variable omega_11;
  variable omega_12;
  variable omega_13;
  variable omega_14;
  variable omega_15;
  variable omega_16;
  variable omega_17;
  variable omega_18;
  variable omega_19;
  variable omega_20;
  variable omega_21;
  variable omega_22;
  variable omega_23;
  variable omega_24;
  variable omega_25;
  variable omega_26;
  variable omega_27;
  variable omega_28;
  variable omega_29;
  variable omega_30;
  variable omega_31;
  variable omega_32;
  variable omega_33;
  variable omega_34;
  variable omega_35;
  variable omega_36;
  variable omega_37;
  variable omega_38;
  variable omega_39;
  variable omega_40;
  variable omega_41;

  minimize(quad_form(x_final - (xk_0 + d_0), Q) + quad_form(uk_0 + w_0, R) + quad_form(alpha_0 + delta_alpha_0 - alpha_min, M) + quad_form(x_final - (xk_1 + d_1), Q) + quad_form(uk_1 + w_1, R) + quad_form(alpha_1 + delta_alpha_1 - alpha_min, M) + quad_form(x_final - (xk_2 + d_2), Q) + quad_form(uk_2 + w_2, R) + quad_form(alpha_2 + delta_alpha_2 - alpha_min, M) + quad_form(x_final - (xk_3 + d_3), Q) + quad_form(uk_3 + w_3, R) + quad_form(alpha_3 + delta_alpha_3 - alpha_min, M) + quad_form(x_final - (xk_4 + d_4), Q) + quad_form(uk_4 + w_4, R) + quad_form(alpha_4 + delta_alpha_4 - alpha_min, M) + quad_form(x_final - (xk_5 + d_5), Q) + quad_form(uk_5 + w_5, R) + quad_form(alpha_5 + delta_alpha_5 - alpha_min, M) + quad_form(x_final - (xk_6 + d_6), Q) + quad_form(uk_6 + w_6, R) + quad_form(alpha_6 + delta_alpha_6 - alpha_min, M) + quad_form(x_final - (xk_7 + d_7), Q) + quad_form(uk_7 + w_7, R) + quad_form(alpha_7 + delta_alpha_7 - alpha_min, M) + quad_form(x_final - (xk_8 + d_8), Q) + quad_form(uk_8 + w_8, R) + quad_form(alpha_8 + delta_alpha_8 - alpha_min, M) + quad_form(x_final - (xk_9 + d_9), Q) + quad_form(uk_9 + w_9, R) + quad_form(alpha_9 + delta_alpha_9 - alpha_min, M) + quad_form(x_final - (xk_10 + d_10), Q) + quad_form(uk_10 + w_10, R) + quad_form(alpha_10 + delta_alpha_10 - alpha_min, M) + quad_form(x_final - (xk_11 + d_11), Q) + quad_form(uk_11 + w_11, R) + quad_form(alpha_11 + delta_alpha_11 - alpha_min, M) + quad_form(x_final - (xk_12 + d_12), Q) + quad_form(uk_12 + w_12, R) + quad_form(alpha_12 + delta_alpha_12 - alpha_min, M) + quad_form(x_final - (xk_13 + d_13), Q) + quad_form(uk_13 + w_13, R) + quad_form(alpha_13 + delta_alpha_13 - alpha_min, M) + quad_form(x_final - (xk_14 + d_14), Q) + quad_form(uk_14 + w_14, R) + quad_form(alpha_14 + delta_alpha_14 - alpha_min, M) + quad_form(x_final - (xk_15 + d_15), Q) + quad_form(uk_15 + w_15, R) + quad_form(alpha_15 + delta_alpha_15 - alpha_min, M) + quad_form(x_final - (xk_16 + d_16), Q) + quad_form(uk_16 + w_16, R) + quad_form(alpha_16 + delta_alpha_16 - alpha_min, M) + quad_form(x_final - (xk_17 + d_17), Q) + quad_form(uk_17 + w_17, R) + quad_form(alpha_17 + delta_alpha_17 - alpha_min, M) + quad_form(x_final - (xk_18 + d_18), Q) + quad_form(uk_18 + w_18, R) + quad_form(alpha_18 + delta_alpha_18 - alpha_min, M) + quad_form(x_final - (xk_19 + d_19), Q) + quad_form(uk_19 + w_19, R) + quad_form(alpha_19 + delta_alpha_19 - alpha_min, M) + quad_form(x_final - (xk_20 + d_20), Q) + quad_form(uk_20 + w_20, R) + quad_form(alpha_20 + delta_alpha_20 - alpha_min, M) + quad_form(x_final - (xk_21 + d_21), Q) + quad_form(uk_21 + w_21, R) + quad_form(alpha_21 + delta_alpha_21 - alpha_min, M) + quad_form(x_final - (xk_22 + d_22), Q) + quad_form(uk_22 + w_22, R) + quad_form(alpha_22 + delta_alpha_22 - alpha_min, M) + quad_form(x_final - (xk_23 + d_23), Q) + quad_form(uk_23 + w_23, R) + quad_form(alpha_23 + delta_alpha_23 - alpha_min, M) + quad_form(x_final - (xk_24 + d_24), Q) + quad_form(uk_24 + w_24, R) + quad_form(alpha_24 + delta_alpha_24 - alpha_min, M) + quad_form(x_final - (xk_25 + d_25), Q) + quad_form(uk_25 + w_25, R) + quad_form(alpha_25 + delta_alpha_25 - alpha_min, M) + quad_form(x_final - (xk_26 + d_26), Q) + quad_form(uk_26 + w_26, R) + quad_form(alpha_26 + delta_alpha_26 - alpha_min, M) + quad_form(x_final - (xk_27 + d_27), Q) + quad_form(uk_27 + w_27, R) + quad_form(alpha_27 + delta_alpha_27 - alpha_min, M) + quad_form(x_final - (xk_28 + d_28), Q) + quad_form(uk_28 + w_28, R) + quad_form(alpha_28 + delta_alpha_28 - alpha_min, M) + quad_form(x_final - (xk_29 + d_29), Q) + quad_form(uk_29 + w_29, R) + quad_form(alpha_29 + delta_alpha_29 - alpha_min, M) + quad_form(x_final - (xk_30 + d_30), Q) + quad_form(uk_30 + w_30, R) + quad_form(alpha_30 + delta_alpha_30 - alpha_min, M) + quad_form(x_final - (xk_31 + d_31), Q) + quad_form(uk_31 + w_31, R) + quad_form(alpha_31 + delta_alpha_31 - alpha_min, M) + quad_form(x_final - (xk_32 + d_32), Q) + quad_form(uk_32 + w_32, R) + quad_form(alpha_32 + delta_alpha_32 - alpha_min, M) + quad_form(x_final - (xk_33 + d_33), Q) + quad_form(uk_33 + w_33, R) + quad_form(alpha_33 + delta_alpha_33 - alpha_min, M) + quad_form(x_final - (xk_34 + d_34), Q) + quad_form(uk_34 + w_34, R) + quad_form(alpha_34 + delta_alpha_34 - alpha_min, M) + quad_form(x_final - (xk_35 + d_35), Q) + quad_form(uk_35 + w_35, R) + quad_form(alpha_35 + delta_alpha_35 - alpha_min, M) + quad_form(x_final - (xk_36 + d_36), Q) + quad_form(uk_36 + w_36, R) + quad_form(alpha_36 + delta_alpha_36 - alpha_min, M) + quad_form(x_final - (xk_37 + d_37), Q) + quad_form(uk_37 + w_37, R) + quad_form(alpha_37 + delta_alpha_37 - alpha_min, M) + quad_form(x_final - (xk_38 + d_38), Q) + quad_form(uk_38 + w_38, R) + quad_form(alpha_38 + delta_alpha_38 - alpha_min, M) + quad_form(x_final - (xk_39 + d_39), Q) + quad_form(uk_39 + w_39, R) + quad_form(alpha_39 + delta_alpha_39 - alpha_min, M) + quad_form(x_final - (xk_40 + d_40), Q) + quad_form(uk_40 + w_40, R) + quad_form(alpha_40 + delta_alpha_40 - alpha_min, M) + quad_form(x_final - (xk_41 + d_41), Q_final));
  subject to
    d_1(1) == delta_t*d_0(2) + d_0(1);
    d_2(1) == delta_t*d_1(2) + d_1(1);
    d_3(1) == delta_t*d_2(2) + d_2(1);
    d_4(1) == delta_t*d_3(2) + d_3(1);
    d_5(1) == delta_t*d_4(2) + d_4(1);
    d_6(1) == delta_t*d_5(2) + d_5(1);
    d_7(1) == delta_t*d_6(2) + d_6(1);
    d_8(1) == delta_t*d_7(2) + d_7(1);
    d_9(1) == delta_t*d_8(2) + d_8(1);
    d_10(1) == delta_t*d_9(2) + d_9(1);
    d_11(1) == delta_t*d_10(2) + d_10(1);
    d_12(1) == delta_t*d_11(2) + d_11(1);
    d_13(1) == delta_t*d_12(2) + d_12(1);
    d_14(1) == delta_t*d_13(2) + d_13(1);
    d_15(1) == delta_t*d_14(2) + d_14(1);
    d_16(1) == delta_t*d_15(2) + d_15(1);
    d_17(1) == delta_t*d_16(2) + d_16(1);
    d_18(1) == delta_t*d_17(2) + d_17(1);
    d_19(1) == delta_t*d_18(2) + d_18(1);
    d_20(1) == delta_t*d_19(2) + d_19(1);
    d_21(1) == delta_t*d_20(2) + d_20(1);
    d_22(1) == delta_t*d_21(2) + d_21(1);
    d_23(1) == delta_t*d_22(2) + d_22(1);
    d_24(1) == delta_t*d_23(2) + d_23(1);
    d_25(1) == delta_t*d_24(2) + d_24(1);
    d_26(1) == delta_t*d_25(2) + d_25(1);
    d_27(1) == delta_t*d_26(2) + d_26(1);
    d_28(1) == delta_t*d_27(2) + d_27(1);
    d_29(1) == delta_t*d_28(2) + d_28(1);
    d_30(1) == delta_t*d_29(2) + d_29(1);
    d_31(1) == delta_t*d_30(2) + d_30(1);
    d_32(1) == delta_t*d_31(2) + d_31(1);
    d_33(1) == delta_t*d_32(2) + d_32(1);
    d_34(1) == delta_t*d_33(2) + d_33(1);
    d_35(1) == delta_t*d_34(2) + d_34(1);
    d_36(1) == delta_t*d_35(2) + d_35(1);
    d_37(1) == delta_t*d_36(2) + d_36(1);
    d_38(1) == delta_t*d_37(2) + d_37(1);
    d_39(1) == delta_t*d_38(2) + d_38(1);
    d_40(1) == delta_t*d_39(2) + d_39(1);
    d_41(1) == delta_t*d_40(2) + d_40(1);
    d_1(2) ==  - c1*cos_xk_0*d_0(1) + (1 - abs_xk_0*c2)*d_0(2) + Bd*w_0;
    d_2(2) ==  - c1*cos_xk_1*d_1(1) + (1 - abs_xk_1*c2)*d_1(2) + Bd*w_1;
    d_3(2) ==  - c1*cos_xk_2*d_2(1) + (1 - abs_xk_2*c2)*d_2(2) + Bd*w_2;
    d_4(2) ==  - c1*cos_xk_3*d_3(1) + (1 - abs_xk_3*c2)*d_3(2) + Bd*w_3;
    d_5(2) ==  - c1*cos_xk_4*d_4(1) + (1 - abs_xk_4*c2)*d_4(2) + Bd*w_4;
    d_6(2) ==  - c1*cos_xk_5*d_5(1) + (1 - abs_xk_5*c2)*d_5(2) + Bd*w_5;
    d_7(2) ==  - c1*cos_xk_6*d_6(1) + (1 - abs_xk_6*c2)*d_6(2) + Bd*w_6;
    d_8(2) ==  - c1*cos_xk_7*d_7(1) + (1 - abs_xk_7*c2)*d_7(2) + Bd*w_7;
    d_9(2) ==  - c1*cos_xk_8*d_8(1) + (1 - abs_xk_8*c2)*d_8(2) + Bd*w_8;
    d_10(2) ==  - c1*cos_xk_9*d_9(1) + (1 - abs_xk_9*c2)*d_9(2) + Bd*w_9;
    d_11(2) ==  - c1*cos_xk_10*d_10(1) + (1 - abs_xk_10*c2)*d_10(2) + Bd*w_10;
    d_12(2) ==  - c1*cos_xk_11*d_11(1) + (1 - abs_xk_11*c2)*d_11(2) + Bd*w_11;
    d_13(2) ==  - c1*cos_xk_12*d_12(1) + (1 - abs_xk_12*c2)*d_12(2) + Bd*w_12;
    d_14(2) ==  - c1*cos_xk_13*d_13(1) + (1 - abs_xk_13*c2)*d_13(2) + Bd*w_13;
    d_15(2) ==  - c1*cos_xk_14*d_14(1) + (1 - abs_xk_14*c2)*d_14(2) + Bd*w_14;
    d_16(2) ==  - c1*cos_xk_15*d_15(1) + (1 - abs_xk_15*c2)*d_15(2) + Bd*w_15;
    d_17(2) ==  - c1*cos_xk_16*d_16(1) + (1 - abs_xk_16*c2)*d_16(2) + Bd*w_16;
    d_18(2) ==  - c1*cos_xk_17*d_17(1) + (1 - abs_xk_17*c2)*d_17(2) + Bd*w_17;
    d_19(2) ==  - c1*cos_xk_18*d_18(1) + (1 - abs_xk_18*c2)*d_18(2) + Bd*w_18;
    d_20(2) ==  - c1*cos_xk_19*d_19(1) + (1 - abs_xk_19*c2)*d_19(2) + Bd*w_19;
    d_21(2) ==  - c1*cos_xk_20*d_20(1) + (1 - abs_xk_20*c2)*d_20(2) + Bd*w_20;
    d_22(2) ==  - c1*cos_xk_21*d_21(1) + (1 - abs_xk_21*c2)*d_21(2) + Bd*w_21;
    d_23(2) ==  - c1*cos_xk_22*d_22(1) + (1 - abs_xk_22*c2)*d_22(2) + Bd*w_22;
    d_24(2) ==  - c1*cos_xk_23*d_23(1) + (1 - abs_xk_23*c2)*d_23(2) + Bd*w_23;
    d_25(2) ==  - c1*cos_xk_24*d_24(1) + (1 - abs_xk_24*c2)*d_24(2) + Bd*w_24;
    d_26(2) ==  - c1*cos_xk_25*d_25(1) + (1 - abs_xk_25*c2)*d_25(2) + Bd*w_25;
    d_27(2) ==  - c1*cos_xk_26*d_26(1) + (1 - abs_xk_26*c2)*d_26(2) + Bd*w_26;
    d_28(2) ==  - c1*cos_xk_27*d_27(1) + (1 - abs_xk_27*c2)*d_27(2) + Bd*w_27;
    d_29(2) ==  - c1*cos_xk_28*d_28(1) + (1 - abs_xk_28*c2)*d_28(2) + Bd*w_28;
    d_30(2) ==  - c1*cos_xk_29*d_29(1) + (1 - abs_xk_29*c2)*d_29(2) + Bd*w_29;
    d_31(2) ==  - c1*cos_xk_30*d_30(1) + (1 - abs_xk_30*c2)*d_30(2) + Bd*w_30;
    d_32(2) ==  - c1*cos_xk_31*d_31(1) + (1 - abs_xk_31*c2)*d_31(2) + Bd*w_31;
    d_33(2) ==  - c1*cos_xk_32*d_32(1) + (1 - abs_xk_32*c2)*d_32(2) + Bd*w_32;
    d_34(2) ==  - c1*cos_xk_33*d_33(1) + (1 - abs_xk_33*c2)*d_33(2) + Bd*w_33;
    d_35(2) ==  - c1*cos_xk_34*d_34(1) + (1 - abs_xk_34*c2)*d_34(2) + Bd*w_34;
    d_36(2) ==  - c1*cos_xk_35*d_35(1) + (1 - abs_xk_35*c2)*d_35(2) + Bd*w_35;
    d_37(2) ==  - c1*cos_xk_36*d_36(1) + (1 - abs_xk_36*c2)*d_36(2) + Bd*w_36;
    d_38(2) ==  - c1*cos_xk_37*d_37(1) + (1 - abs_xk_37*c2)*d_37(2) + Bd*w_37;
    d_39(2) ==  - c1*cos_xk_38*d_38(1) + (1 - abs_xk_38*c2)*d_38(2) + Bd*w_38;
    d_40(2) ==  - c1*cos_xk_39*d_39(1) + (1 - abs_xk_39*c2)*d_39(2) + Bd*w_39;
    d_41(2) ==  - c1*cos_xk_40*d_40(1) + (1 - abs_xk_40*c2)*d_40(2) + Bd*w_40;
    new_EF_1 == new_EF_0*(1 - alpha_0*delta_t) - delta_t*old_EF_0*delta_alpha_0 + delta_t*(disturbance_0 + Cd_max*abs_xk_0*(abs_xk_0/I));
    new_EF_2 == (1 - alpha_1*delta_t)*new_EF_1 - delta_t*old_EF_1*delta_alpha_1 + delta_t*(disturbance_1 + Cd_max*abs_xk_1*(abs_xk_1/I));
    new_EF_3 == (1 - alpha_2*delta_t)*new_EF_2 - delta_t*old_EF_2*delta_alpha_2 + delta_t*(disturbance_2 + Cd_max*abs_xk_2*(abs_xk_2/I));
    new_EF_4 == (1 - alpha_3*delta_t)*new_EF_3 - delta_t*old_EF_3*delta_alpha_3 + delta_t*(disturbance_3 + Cd_max*abs_xk_3*(abs_xk_3/I));
    new_EF_5 == (1 - alpha_4*delta_t)*new_EF_4 - delta_t*old_EF_4*delta_alpha_4 + delta_t*(disturbance_4 + Cd_max*abs_xk_4*(abs_xk_4/I));
    new_EF_6 == (1 - alpha_5*delta_t)*new_EF_5 - delta_t*old_EF_5*delta_alpha_5 + delta_t*(disturbance_5 + Cd_max*abs_xk_5*(abs_xk_5/I));
    new_EF_7 == (1 - alpha_6*delta_t)*new_EF_6 - delta_t*old_EF_6*delta_alpha_6 + delta_t*(disturbance_6 + Cd_max*abs_xk_6*(abs_xk_6/I));
    new_EF_8 == (1 - alpha_7*delta_t)*new_EF_7 - delta_t*old_EF_7*delta_alpha_7 + delta_t*(disturbance_7 + Cd_max*abs_xk_7*(abs_xk_7/I));
    new_EF_9 == (1 - alpha_8*delta_t)*new_EF_8 - delta_t*old_EF_8*delta_alpha_8 + delta_t*(disturbance_8 + Cd_max*abs_xk_8*(abs_xk_8/I));
    new_EF_10 == (1 - alpha_9*delta_t)*new_EF_9 - delta_t*old_EF_9*delta_alpha_9 + delta_t*(disturbance_9 + Cd_max*abs_xk_9*(abs_xk_9/I));
    new_EF_11 == (1 - alpha_10*delta_t)*new_EF_10 - delta_t*old_EF_10*delta_alpha_10 + delta_t*(disturbance_10 + Cd_max*abs_xk_10*(abs_xk_10/I));
    new_EF_12 == (1 - alpha_11*delta_t)*new_EF_11 - delta_t*old_EF_11*delta_alpha_11 + delta_t*(disturbance_11 + Cd_max*abs_xk_11*(abs_xk_11/I));
    new_EF_13 == (1 - alpha_12*delta_t)*new_EF_12 - delta_t*old_EF_12*delta_alpha_12 + delta_t*(disturbance_12 + Cd_max*abs_xk_12*(abs_xk_12/I));
    new_EF_14 == (1 - alpha_13*delta_t)*new_EF_13 - delta_t*old_EF_13*delta_alpha_13 + delta_t*(disturbance_13 + Cd_max*abs_xk_13*(abs_xk_13/I));
    new_EF_15 == (1 - alpha_14*delta_t)*new_EF_14 - delta_t*old_EF_14*delta_alpha_14 + delta_t*(disturbance_14 + Cd_max*abs_xk_14*(abs_xk_14/I));
    new_EF_16 == (1 - alpha_15*delta_t)*new_EF_15 - delta_t*old_EF_15*delta_alpha_15 + delta_t*(disturbance_15 + Cd_max*abs_xk_15*(abs_xk_15/I));
    new_EF_17 == (1 - alpha_16*delta_t)*new_EF_16 - delta_t*old_EF_16*delta_alpha_16 + delta_t*(disturbance_16 + Cd_max*abs_xk_16*(abs_xk_16/I));
    new_EF_18 == (1 - alpha_17*delta_t)*new_EF_17 - delta_t*old_EF_17*delta_alpha_17 + delta_t*(disturbance_17 + Cd_max*abs_xk_17*(abs_xk_17/I));
    new_EF_19 == (1 - alpha_18*delta_t)*new_EF_18 - delta_t*old_EF_18*delta_alpha_18 + delta_t*(disturbance_18 + Cd_max*abs_xk_18*(abs_xk_18/I));
    new_EF_20 == (1 - alpha_19*delta_t)*new_EF_19 - delta_t*old_EF_19*delta_alpha_19 + delta_t*(disturbance_19 + Cd_max*abs_xk_19*(abs_xk_19/I));
    new_EF_21 == (1 - alpha_20*delta_t)*new_EF_20 - delta_t*old_EF_20*delta_alpha_20 + delta_t*(disturbance_20 + Cd_max*abs_xk_20*(abs_xk_20/I));
    new_EF_22 == (1 - alpha_21*delta_t)*new_EF_21 - delta_t*old_EF_21*delta_alpha_21 + delta_t*(disturbance_21 + Cd_max*abs_xk_21*(abs_xk_21/I));
    new_EF_23 == (1 - alpha_22*delta_t)*new_EF_22 - delta_t*old_EF_22*delta_alpha_22 + delta_t*(disturbance_22 + Cd_max*abs_xk_22*(abs_xk_22/I));
    new_EF_24 == (1 - alpha_23*delta_t)*new_EF_23 - delta_t*old_EF_23*delta_alpha_23 + delta_t*(disturbance_23 + Cd_max*abs_xk_23*(abs_xk_23/I));
    new_EF_25 == (1 - alpha_24*delta_t)*new_EF_24 - delta_t*old_EF_24*delta_alpha_24 + delta_t*(disturbance_24 + Cd_max*abs_xk_24*(abs_xk_24/I));
    new_EF_26 == (1 - alpha_25*delta_t)*new_EF_25 - delta_t*old_EF_25*delta_alpha_25 + delta_t*(disturbance_25 + Cd_max*abs_xk_25*(abs_xk_25/I));
    new_EF_27 == (1 - alpha_26*delta_t)*new_EF_26 - delta_t*old_EF_26*delta_alpha_26 + delta_t*(disturbance_26 + Cd_max*abs_xk_26*(abs_xk_26/I));
    new_EF_28 == (1 - alpha_27*delta_t)*new_EF_27 - delta_t*old_EF_27*delta_alpha_27 + delta_t*(disturbance_27 + Cd_max*abs_xk_27*(abs_xk_27/I));
    new_EF_29 == (1 - alpha_28*delta_t)*new_EF_28 - delta_t*old_EF_28*delta_alpha_28 + delta_t*(disturbance_28 + Cd_max*abs_xk_28*(abs_xk_28/I));
    new_EF_30 == (1 - alpha_29*delta_t)*new_EF_29 - delta_t*old_EF_29*delta_alpha_29 + delta_t*(disturbance_29 + Cd_max*abs_xk_29*(abs_xk_29/I));
    new_EF_31 == (1 - alpha_30*delta_t)*new_EF_30 - delta_t*old_EF_30*delta_alpha_30 + delta_t*(disturbance_30 + Cd_max*abs_xk_30*(abs_xk_30/I));
    new_EF_32 == (1 - alpha_31*delta_t)*new_EF_31 - delta_t*old_EF_31*delta_alpha_31 + delta_t*(disturbance_31 + Cd_max*abs_xk_31*(abs_xk_31/I));
    new_EF_33 == (1 - alpha_32*delta_t)*new_EF_32 - delta_t*old_EF_32*delta_alpha_32 + delta_t*(disturbance_32 + Cd_max*abs_xk_32*(abs_xk_32/I));
    new_EF_34 == (1 - alpha_33*delta_t)*new_EF_33 - delta_t*old_EF_33*delta_alpha_33 + delta_t*(disturbance_33 + Cd_max*abs_xk_33*(abs_xk_33/I));
    new_EF_35 == (1 - alpha_34*delta_t)*new_EF_34 - delta_t*old_EF_34*delta_alpha_34 + delta_t*(disturbance_34 + Cd_max*abs_xk_34*(abs_xk_34/I));
    new_EF_36 == (1 - alpha_35*delta_t)*new_EF_35 - delta_t*old_EF_35*delta_alpha_35 + delta_t*(disturbance_35 + Cd_max*abs_xk_35*(abs_xk_35/I));
    new_EF_37 == (1 - alpha_36*delta_t)*new_EF_36 - delta_t*old_EF_36*delta_alpha_36 + delta_t*(disturbance_36 + Cd_max*abs_xk_36*(abs_xk_36/I));
    new_EF_38 == (1 - alpha_37*delta_t)*new_EF_37 - delta_t*old_EF_37*delta_alpha_37 + delta_t*(disturbance_37 + Cd_max*abs_xk_37*(abs_xk_37/I));
    new_EF_39 == (1 - alpha_38*delta_t)*new_EF_38 - delta_t*old_EF_38*delta_alpha_38 + delta_t*(disturbance_38 + Cd_max*abs_xk_38*(abs_xk_38/I));
    new_EF_40 == (1 - alpha_39*delta_t)*new_EF_39 - delta_t*old_EF_39*delta_alpha_39 + delta_t*(disturbance_39 + Cd_max*abs_xk_39*(abs_xk_39/I));
    new_EF_41 == (1 - alpha_40*delta_t)*new_EF_40 - delta_t*old_EF_40*delta_alpha_40 + delta_t*(disturbance_40 + Cd_max*abs_xk_40*(abs_xk_40/I));
    omega_1 == delta_t*( - lambda*omega_0 + new_EF_0) + omega_0;
    omega_2 == delta_t*( - lambda*omega_1 + new_EF_1) + omega_1;
    omega_3 == delta_t*( - lambda*omega_2 + new_EF_2) + omega_2;
    omega_4 == delta_t*( - lambda*omega_3 + new_EF_3) + omega_3;
    omega_5 == delta_t*( - lambda*omega_4 + new_EF_4) + omega_4;
    omega_6 == delta_t*( - lambda*omega_5 + new_EF_5) + omega_5;
    omega_7 == delta_t*( - lambda*omega_6 + new_EF_6) + omega_6;
    omega_8 == delta_t*( - lambda*omega_7 + new_EF_7) + omega_7;
    omega_9 == delta_t*( - lambda*omega_8 + new_EF_8) + omega_8;
    omega_10 == delta_t*( - lambda*omega_9 + new_EF_9) + omega_9;
    omega_11 == delta_t*( - lambda*omega_10 + new_EF_10) + omega_10;
    omega_12 == delta_t*( - lambda*omega_11 + new_EF_11) + omega_11;
    omega_13 == delta_t*( - lambda*omega_12 + new_EF_12) + omega_12;
    omega_14 == delta_t*( - lambda*omega_13 + new_EF_13) + omega_13;
    omega_15 == delta_t*( - lambda*omega_14 + new_EF_14) + omega_14;
    omega_16 == delta_t*( - lambda*omega_15 + new_EF_15) + omega_15;
    omega_17 == delta_t*( - lambda*omega_16 + new_EF_16) + omega_16;
    omega_18 == delta_t*( - lambda*omega_17 + new_EF_17) + omega_17;
    omega_19 == delta_t*( - lambda*omega_18 + new_EF_18) + omega_18;
    omega_20 == delta_t*( - lambda*omega_19 + new_EF_19) + omega_19;
    omega_21 == delta_t*( - lambda*omega_20 + new_EF_20) + omega_20;
    omega_22 == delta_t*( - lambda*omega_21 + new_EF_21) + omega_21;
    omega_23 == delta_t*( - lambda*omega_22 + new_EF_22) + omega_22;
    omega_24 == delta_t*( - lambda*omega_23 + new_EF_23) + omega_23;
    omega_25 == delta_t*( - lambda*omega_24 + new_EF_24) + omega_24;
    omega_26 == delta_t*( - lambda*omega_25 + new_EF_25) + omega_25;
    omega_27 == delta_t*( - lambda*omega_26 + new_EF_26) + omega_26;
    omega_28 == delta_t*( - lambda*omega_27 + new_EF_27) + omega_27;
    omega_29 == delta_t*( - lambda*omega_28 + new_EF_28) + omega_28;
    omega_30 == delta_t*( - lambda*omega_29 + new_EF_29) + omega_29;
    omega_31 == delta_t*( - lambda*omega_30 + new_EF_30) + omega_30;
    omega_32 == delta_t*( - lambda*omega_31 + new_EF_31) + omega_31;
    omega_33 == delta_t*( - lambda*omega_32 + new_EF_32) + omega_32;
    omega_34 == delta_t*( - lambda*omega_33 + new_EF_33) + omega_33;
    omega_35 == delta_t*( - lambda*omega_34 + new_EF_34) + omega_34;
    omega_36 == delta_t*( - lambda*omega_35 + new_EF_35) + omega_35;
    omega_37 == delta_t*( - lambda*omega_36 + new_EF_36) + omega_36;
    omega_38 == delta_t*( - lambda*omega_37 + new_EF_37) + omega_37;
    omega_39 == delta_t*( - lambda*omega_38 + new_EF_38) + omega_38;
    omega_40 == delta_t*( - lambda*omega_39 + new_EF_39) + omega_39;
    omega_41 == delta_t*( - lambda*omega_40 + new_EF_40) + omega_40;
    w_0 <= w_max;
    w_1 <= w_max;
    w_2 <= w_max;
    w_3 <= w_max;
    w_4 <= w_max;
    w_5 <= w_max;
    w_6 <= w_max;
    w_7 <= w_max;
    w_8 <= w_max;
    w_9 <= w_max;
    w_10 <= w_max;
    w_11 <= w_max;
    w_12 <= w_max;
    w_13 <= w_max;
    w_14 <= w_max;
    w_15 <= w_max;
    w_16 <= w_max;
    w_17 <= w_max;
    w_18 <= w_max;
    w_19 <= w_max;
    w_20 <= w_max;
    w_21 <= w_max;
    w_22 <= w_max;
    w_23 <= w_max;
    w_24 <= w_max;
    w_25 <= w_max;
    w_26 <= w_max;
    w_27 <= w_max;
    w_28 <= w_max;
    w_29 <= w_max;
    w_30 <= w_max;
    w_31 <= w_max;
    w_32 <= w_max;
    w_33 <= w_max;
    w_34 <= w_max;
    w_35 <= w_max;
    w_36 <= w_max;
    w_37 <= w_max;
    w_38 <= w_max;
    w_39 <= w_max;
    w_40 <= w_max;
    -w_0 <= w_max;
    -w_1 <= w_max;
    -w_2 <= w_max;
    -w_3 <= w_max;
    -w_4 <= w_max;
    -w_5 <= w_max;
    -w_6 <= w_max;
    -w_7 <= w_max;
    -w_8 <= w_max;
    -w_9 <= w_max;
    -w_10 <= w_max;
    -w_11 <= w_max;
    -w_12 <= w_max;
    -w_13 <= w_max;
    -w_14 <= w_max;
    -w_15 <= w_max;
    -w_16 <= w_max;
    -w_17 <= w_max;
    -w_18 <= w_max;
    -w_19 <= w_max;
    -w_20 <= w_max;
    -w_21 <= w_max;
    -w_22 <= w_max;
    -w_23 <= w_max;
    -w_24 <= w_max;
    -w_25 <= w_max;
    -w_26 <= w_max;
    -w_27 <= w_max;
    -w_28 <= w_max;
    -w_29 <= w_max;
    -w_30 <= w_max;
    -w_31 <= w_max;
    -w_32 <= w_max;
    -w_33 <= w_max;
    -w_34 <= w_max;
    -w_35 <= w_max;
    -w_36 <= w_max;
    -w_37 <= w_max;
    -w_38 <= w_max;
    -w_39 <= w_max;
    -w_40 <= w_max;
    uk_0 + w_0 <= u_max;
    uk_1 + w_1 <= u_max;
    uk_2 + w_2 <= u_max;
    uk_3 + w_3 <= u_max;
    uk_4 + w_4 <= u_max;
    uk_5 + w_5 <= u_max;
    uk_6 + w_6 <= u_max;
    uk_7 + w_7 <= u_max;
    uk_8 + w_8 <= u_max;
    uk_9 + w_9 <= u_max;
    uk_10 + w_10 <= u_max;
    uk_11 + w_11 <= u_max;
    uk_12 + w_12 <= u_max;
    uk_13 + w_13 <= u_max;
    uk_14 + w_14 <= u_max;
    uk_15 + w_15 <= u_max;
    uk_16 + w_16 <= u_max;
    uk_17 + w_17 <= u_max;
    uk_18 + w_18 <= u_max;
    uk_19 + w_19 <= u_max;
    uk_20 + w_20 <= u_max;
    uk_21 + w_21 <= u_max;
    uk_22 + w_22 <= u_max;
    uk_23 + w_23 <= u_max;
    uk_24 + w_24 <= u_max;
    uk_25 + w_25 <= u_max;
    uk_26 + w_26 <= u_max;
    uk_27 + w_27 <= u_max;
    uk_28 + w_28 <= u_max;
    uk_29 + w_29 <= u_max;
    uk_30 + w_30 <= u_max;
    uk_31 + w_31 <= u_max;
    uk_32 + w_32 <= u_max;
    uk_33 + w_33 <= u_max;
    uk_34 + w_34 <= u_max;
    uk_35 + w_35 <= u_max;
    uk_36 + w_36 <= u_max;
    uk_37 + w_37 <= u_max;
    uk_38 + w_38 <= u_max;
    uk_39 + w_39 <= u_max;
    uk_40 + w_40 <= u_max;
    -(uk_0 + w_0) <= u_max;
    -(uk_1 + w_1) <= u_max;
    -(uk_2 + w_2) <= u_max;
    -(uk_3 + w_3) <= u_max;
    -(uk_4 + w_4) <= u_max;
    -(uk_5 + w_5) <= u_max;
    -(uk_6 + w_6) <= u_max;
    -(uk_7 + w_7) <= u_max;
    -(uk_8 + w_8) <= u_max;
    -(uk_9 + w_9) <= u_max;
    -(uk_10 + w_10) <= u_max;
    -(uk_11 + w_11) <= u_max;
    -(uk_12 + w_12) <= u_max;
    -(uk_13 + w_13) <= u_max;
    -(uk_14 + w_14) <= u_max;
    -(uk_15 + w_15) <= u_max;
    -(uk_16 + w_16) <= u_max;
    -(uk_17 + w_17) <= u_max;
    -(uk_18 + w_18) <= u_max;
    -(uk_19 + w_19) <= u_max;
    -(uk_20 + w_20) <= u_max;
    -(uk_21 + w_21) <= u_max;
    -(uk_22 + w_22) <= u_max;
    -(uk_23 + w_23) <= u_max;
    -(uk_24 + w_24) <= u_max;
    -(uk_25 + w_25) <= u_max;
    -(uk_26 + w_26) <= u_max;
    -(uk_27 + w_27) <= u_max;
    -(uk_28 + w_28) <= u_max;
    -(uk_29 + w_29) <= u_max;
    -(uk_30 + w_30) <= u_max;
    -(uk_31 + w_31) <= u_max;
    -(uk_32 + w_32) <= u_max;
    -(uk_33 + w_33) <= u_max;
    -(uk_34 + w_34) <= u_max;
    -(uk_35 + w_35) <= u_max;
    -(uk_36 + w_36) <= u_max;
    -(uk_37 + w_37) <= u_max;
    -(uk_38 + w_38) <= u_max;
    -(uk_39 + w_39) <= u_max;
    -(uk_40 + w_40) <= u_max;
    uk_1 + w_1 - uk_0 - w_0 <= delta_u_max;
    uk_2 + w_2 - uk_1 - w_1 <= delta_u_max;
    uk_3 + w_3 - uk_2 - w_2 <= delta_u_max;
    uk_4 + w_4 - uk_3 - w_3 <= delta_u_max;
    uk_5 + w_5 - uk_4 - w_4 <= delta_u_max;
    uk_6 + w_6 - uk_5 - w_5 <= delta_u_max;
    uk_7 + w_7 - uk_6 - w_6 <= delta_u_max;
    uk_8 + w_8 - uk_7 - w_7 <= delta_u_max;
    uk_9 + w_9 - uk_8 - w_8 <= delta_u_max;
    uk_10 + w_10 - uk_9 - w_9 <= delta_u_max;
    uk_11 + w_11 - uk_10 - w_10 <= delta_u_max;
    uk_12 + w_12 - uk_11 - w_11 <= delta_u_max;
    uk_13 + w_13 - uk_12 - w_12 <= delta_u_max;
    uk_14 + w_14 - uk_13 - w_13 <= delta_u_max;
    uk_15 + w_15 - uk_14 - w_14 <= delta_u_max;
    uk_16 + w_16 - uk_15 - w_15 <= delta_u_max;
    uk_17 + w_17 - uk_16 - w_16 <= delta_u_max;
    uk_18 + w_18 - uk_17 - w_17 <= delta_u_max;
    uk_19 + w_19 - uk_18 - w_18 <= delta_u_max;
    uk_20 + w_20 - uk_19 - w_19 <= delta_u_max;
    uk_21 + w_21 - uk_20 - w_20 <= delta_u_max;
    uk_22 + w_22 - uk_21 - w_21 <= delta_u_max;
    uk_23 + w_23 - uk_22 - w_22 <= delta_u_max;
    uk_24 + w_24 - uk_23 - w_23 <= delta_u_max;
    uk_25 + w_25 - uk_24 - w_24 <= delta_u_max;
    uk_26 + w_26 - uk_25 - w_25 <= delta_u_max;
    uk_27 + w_27 - uk_26 - w_26 <= delta_u_max;
    uk_28 + w_28 - uk_27 - w_27 <= delta_u_max;
    uk_29 + w_29 - uk_28 - w_28 <= delta_u_max;
    uk_30 + w_30 - uk_29 - w_29 <= delta_u_max;
    uk_31 + w_31 - uk_30 - w_30 <= delta_u_max;
    uk_32 + w_32 - uk_31 - w_31 <= delta_u_max;
    uk_33 + w_33 - uk_32 - w_32 <= delta_u_max;
    uk_34 + w_34 - uk_33 - w_33 <= delta_u_max;
    uk_35 + w_35 - uk_34 - w_34 <= delta_u_max;
    uk_36 + w_36 - uk_35 - w_35 <= delta_u_max;
    uk_37 + w_37 - uk_36 - w_36 <= delta_u_max;
    uk_38 + w_38 - uk_37 - w_37 <= delta_u_max;
    uk_39 + w_39 - uk_38 - w_38 <= delta_u_max;
    uk_40 + w_40 - uk_39 - w_39 <= delta_u_max;
    -(uk_1 + w_1 - uk_0 - w_0) <= delta_u_max;
    -(uk_2 + w_2 - uk_1 - w_1) <= delta_u_max;
    -(uk_3 + w_3 - uk_2 - w_2) <= delta_u_max;
    -(uk_4 + w_4 - uk_3 - w_3) <= delta_u_max;
    -(uk_5 + w_5 - uk_4 - w_4) <= delta_u_max;
    -(uk_6 + w_6 - uk_5 - w_5) <= delta_u_max;
    -(uk_7 + w_7 - uk_6 - w_6) <= delta_u_max;
    -(uk_8 + w_8 - uk_7 - w_7) <= delta_u_max;
    -(uk_9 + w_9 - uk_8 - w_8) <= delta_u_max;
    -(uk_10 + w_10 - uk_9 - w_9) <= delta_u_max;
    -(uk_11 + w_11 - uk_10 - w_10) <= delta_u_max;
    -(uk_12 + w_12 - uk_11 - w_11) <= delta_u_max;
    -(uk_13 + w_13 - uk_12 - w_12) <= delta_u_max;
    -(uk_14 + w_14 - uk_13 - w_13) <= delta_u_max;
    -(uk_15 + w_15 - uk_14 - w_14) <= delta_u_max;
    -(uk_16 + w_16 - uk_15 - w_15) <= delta_u_max;
    -(uk_17 + w_17 - uk_16 - w_16) <= delta_u_max;
    -(uk_18 + w_18 - uk_17 - w_17) <= delta_u_max;
    -(uk_19 + w_19 - uk_18 - w_18) <= delta_u_max;
    -(uk_20 + w_20 - uk_19 - w_19) <= delta_u_max;
    -(uk_21 + w_21 - uk_20 - w_20) <= delta_u_max;
    -(uk_22 + w_22 - uk_21 - w_21) <= delta_u_max;
    -(uk_23 + w_23 - uk_22 - w_22) <= delta_u_max;
    -(uk_24 + w_24 - uk_23 - w_23) <= delta_u_max;
    -(uk_25 + w_25 - uk_24 - w_24) <= delta_u_max;
    -(uk_26 + w_26 - uk_25 - w_25) <= delta_u_max;
    -(uk_27 + w_27 - uk_26 - w_26) <= delta_u_max;
    -(uk_28 + w_28 - uk_27 - w_27) <= delta_u_max;
    -(uk_29 + w_29 - uk_28 - w_28) <= delta_u_max;
    -(uk_30 + w_30 - uk_29 - w_29) <= delta_u_max;
    -(uk_31 + w_31 - uk_30 - w_30) <= delta_u_max;
    -(uk_32 + w_32 - uk_31 - w_31) <= delta_u_max;
    -(uk_33 + w_33 - uk_32 - w_32) <= delta_u_max;
    -(uk_34 + w_34 - uk_33 - w_33) <= delta_u_max;
    -(uk_35 + w_35 - uk_34 - w_34) <= delta_u_max;
    -(uk_36 + w_36 - uk_35 - w_35) <= delta_u_max;
    -(uk_37 + w_37 - uk_36 - w_36) <= delta_u_max;
    -(uk_38 + w_38 - uk_37 - w_37) <= delta_u_max;
    -(uk_39 + w_39 - uk_38 - w_38) <= delta_u_max;
    -(uk_40 + w_40 - uk_39 - w_39) <= delta_u_max;
    uk_0 + w_0 - last_u <= delta_u_max;
    new_EF_1 >= 0;
    new_EF_2 >= 0;
    new_EF_3 >= 0;
    new_EF_4 >= 0;
    new_EF_5 >= 0;
    new_EF_6 >= 0;
    new_EF_7 >= 0;
    new_EF_8 >= 0;
    new_EF_9 >= 0;
    new_EF_10 >= 0;
    new_EF_11 >= 0;
    new_EF_12 >= 0;
    new_EF_13 >= 0;
    new_EF_14 >= 0;
    new_EF_15 >= 0;
    new_EF_16 >= 0;
    new_EF_17 >= 0;
    new_EF_18 >= 0;
    new_EF_19 >= 0;
    new_EF_20 >= 0;
    new_EF_21 >= 0;
    new_EF_22 >= 0;
    new_EF_23 >= 0;
    new_EF_24 >= 0;
    new_EF_25 >= 0;
    new_EF_26 >= 0;
    new_EF_27 >= 0;
    new_EF_28 >= 0;
    new_EF_29 >= 0;
    new_EF_30 >= 0;
    new_EF_31 >= 0;
    new_EF_32 >= 0;
    new_EF_33 >= 0;
    new_EF_34 >= 0;
    new_EF_35 >= 0;
    new_EF_36 >= 0;
    new_EF_37 >= 0;
    new_EF_38 >= 0;
    new_EF_39 >= 0;
    new_EF_40 >= 0;
    new_EF_41 >= 0;
    alpha_0 + delta_alpha_0 >= alpha_min;
    alpha_1 + delta_alpha_1 >= alpha_min;
    alpha_2 + delta_alpha_2 >= alpha_min;
    alpha_3 + delta_alpha_3 >= alpha_min;
    alpha_4 + delta_alpha_4 >= alpha_min;
    alpha_5 + delta_alpha_5 >= alpha_min;
    alpha_6 + delta_alpha_6 >= alpha_min;
    alpha_7 + delta_alpha_7 >= alpha_min;
    alpha_8 + delta_alpha_8 >= alpha_min;
    alpha_9 + delta_alpha_9 >= alpha_min;
    alpha_10 + delta_alpha_10 >= alpha_min;
    alpha_11 + delta_alpha_11 >= alpha_min;
    alpha_12 + delta_alpha_12 >= alpha_min;
    alpha_13 + delta_alpha_13 >= alpha_min;
    alpha_14 + delta_alpha_14 >= alpha_min;
    alpha_15 + delta_alpha_15 >= alpha_min;
    alpha_16 + delta_alpha_16 >= alpha_min;
    alpha_17 + delta_alpha_17 >= alpha_min;
    alpha_18 + delta_alpha_18 >= alpha_min;
    alpha_19 + delta_alpha_19 >= alpha_min;
    alpha_20 + delta_alpha_20 >= alpha_min;
    alpha_21 + delta_alpha_21 >= alpha_min;
    alpha_22 + delta_alpha_22 >= alpha_min;
    alpha_23 + delta_alpha_23 >= alpha_min;
    alpha_24 + delta_alpha_24 >= alpha_min;
    alpha_25 + delta_alpha_25 >= alpha_min;
    alpha_26 + delta_alpha_26 >= alpha_min;
    alpha_27 + delta_alpha_27 >= alpha_min;
    alpha_28 + delta_alpha_28 >= alpha_min;
    alpha_29 + delta_alpha_29 >= alpha_min;
    alpha_30 + delta_alpha_30 >= alpha_min;
    alpha_31 + delta_alpha_31 >= alpha_min;
    alpha_32 + delta_alpha_32 >= alpha_min;
    alpha_33 + delta_alpha_33 >= alpha_min;
    alpha_34 + delta_alpha_34 >= alpha_min;
    alpha_35 + delta_alpha_35 >= alpha_min;
    alpha_36 + delta_alpha_36 >= alpha_min;
    alpha_37 + delta_alpha_37 >= alpha_min;
    alpha_38 + delta_alpha_38 >= alpha_min;
    alpha_39 + delta_alpha_39 >= alpha_min;
    alpha_40 + delta_alpha_40 >= alpha_min;
    alpha_0 + delta_alpha_0 <= alpha_max;
    alpha_1 + delta_alpha_1 <= alpha_max;
    alpha_2 + delta_alpha_2 <= alpha_max;
    alpha_3 + delta_alpha_3 <= alpha_max;
    alpha_4 + delta_alpha_4 <= alpha_max;
    alpha_5 + delta_alpha_5 <= alpha_max;
    alpha_6 + delta_alpha_6 <= alpha_max;
    alpha_7 + delta_alpha_7 <= alpha_max;
    alpha_8 + delta_alpha_8 <= alpha_max;
    alpha_9 + delta_alpha_9 <= alpha_max;
    alpha_10 + delta_alpha_10 <= alpha_max;
    alpha_11 + delta_alpha_11 <= alpha_max;
    alpha_12 + delta_alpha_12 <= alpha_max;
    alpha_13 + delta_alpha_13 <= alpha_max;
    alpha_14 + delta_alpha_14 <= alpha_max;
    alpha_15 + delta_alpha_15 <= alpha_max;
    alpha_16 + delta_alpha_16 <= alpha_max;
    alpha_17 + delta_alpha_17 <= alpha_max;
    alpha_18 + delta_alpha_18 <= alpha_max;
    alpha_19 + delta_alpha_19 <= alpha_max;
    alpha_20 + delta_alpha_20 <= alpha_max;
    alpha_21 + delta_alpha_21 <= alpha_max;
    alpha_22 + delta_alpha_22 <= alpha_max;
    alpha_23 + delta_alpha_23 <= alpha_max;
    alpha_24 + delta_alpha_24 <= alpha_max;
    alpha_25 + delta_alpha_25 <= alpha_max;
    alpha_26 + delta_alpha_26 <= alpha_max;
    alpha_27 + delta_alpha_27 <= alpha_max;
    alpha_28 + delta_alpha_28 <= alpha_max;
    alpha_29 + delta_alpha_29 <= alpha_max;
    alpha_30 + delta_alpha_30 <= alpha_max;
    alpha_31 + delta_alpha_31 <= alpha_max;
    alpha_32 + delta_alpha_32 <= alpha_max;
    alpha_33 + delta_alpha_33 <= alpha_max;
    alpha_34 + delta_alpha_34 <= alpha_max;
    alpha_35 + delta_alpha_35 <= alpha_max;
    alpha_36 + delta_alpha_36 <= alpha_max;
    alpha_37 + delta_alpha_37 <= alpha_max;
    alpha_38 + delta_alpha_38 <= alpha_max;
    alpha_39 + delta_alpha_39 <= alpha_max;
    alpha_40 + delta_alpha_40 <= alpha_max;
    alpha_1 + delta_alpha_1 - alpha_0 - delta_alpha_0 <= delta_alpha_max;
    alpha_2 + delta_alpha_2 - alpha_1 - delta_alpha_1 <= delta_alpha_max;
    alpha_3 + delta_alpha_3 - alpha_2 - delta_alpha_2 <= delta_alpha_max;
    alpha_4 + delta_alpha_4 - alpha_3 - delta_alpha_3 <= delta_alpha_max;
    alpha_5 + delta_alpha_5 - alpha_4 - delta_alpha_4 <= delta_alpha_max;
    alpha_6 + delta_alpha_6 - alpha_5 - delta_alpha_5 <= delta_alpha_max;
    alpha_7 + delta_alpha_7 - alpha_6 - delta_alpha_6 <= delta_alpha_max;
    alpha_8 + delta_alpha_8 - alpha_7 - delta_alpha_7 <= delta_alpha_max;
    alpha_9 + delta_alpha_9 - alpha_8 - delta_alpha_8 <= delta_alpha_max;
    alpha_10 + delta_alpha_10 - alpha_9 - delta_alpha_9 <= delta_alpha_max;
    alpha_11 + delta_alpha_11 - alpha_10 - delta_alpha_10 <= delta_alpha_max;
    alpha_12 + delta_alpha_12 - alpha_11 - delta_alpha_11 <= delta_alpha_max;
    alpha_13 + delta_alpha_13 - alpha_12 - delta_alpha_12 <= delta_alpha_max;
    alpha_14 + delta_alpha_14 - alpha_13 - delta_alpha_13 <= delta_alpha_max;
    alpha_15 + delta_alpha_15 - alpha_14 - delta_alpha_14 <= delta_alpha_max;
    alpha_16 + delta_alpha_16 - alpha_15 - delta_alpha_15 <= delta_alpha_max;
    alpha_17 + delta_alpha_17 - alpha_16 - delta_alpha_16 <= delta_alpha_max;
    alpha_18 + delta_alpha_18 - alpha_17 - delta_alpha_17 <= delta_alpha_max;
    alpha_19 + delta_alpha_19 - alpha_18 - delta_alpha_18 <= delta_alpha_max;
    alpha_20 + delta_alpha_20 - alpha_19 - delta_alpha_19 <= delta_alpha_max;
    alpha_21 + delta_alpha_21 - alpha_20 - delta_alpha_20 <= delta_alpha_max;
    alpha_22 + delta_alpha_22 - alpha_21 - delta_alpha_21 <= delta_alpha_max;
    alpha_23 + delta_alpha_23 - alpha_22 - delta_alpha_22 <= delta_alpha_max;
    alpha_24 + delta_alpha_24 - alpha_23 - delta_alpha_23 <= delta_alpha_max;
    alpha_25 + delta_alpha_25 - alpha_24 - delta_alpha_24 <= delta_alpha_max;
    alpha_26 + delta_alpha_26 - alpha_25 - delta_alpha_25 <= delta_alpha_max;
    alpha_27 + delta_alpha_27 - alpha_26 - delta_alpha_26 <= delta_alpha_max;
    alpha_28 + delta_alpha_28 - alpha_27 - delta_alpha_27 <= delta_alpha_max;
    alpha_29 + delta_alpha_29 - alpha_28 - delta_alpha_28 <= delta_alpha_max;
    alpha_30 + delta_alpha_30 - alpha_29 - delta_alpha_29 <= delta_alpha_max;
    alpha_31 + delta_alpha_31 - alpha_30 - delta_alpha_30 <= delta_alpha_max;
    alpha_32 + delta_alpha_32 - alpha_31 - delta_alpha_31 <= delta_alpha_max;
    alpha_33 + delta_alpha_33 - alpha_32 - delta_alpha_32 <= delta_alpha_max;
    alpha_34 + delta_alpha_34 - alpha_33 - delta_alpha_33 <= delta_alpha_max;
    alpha_35 + delta_alpha_35 - alpha_34 - delta_alpha_34 <= delta_alpha_max;
    alpha_36 + delta_alpha_36 - alpha_35 - delta_alpha_35 <= delta_alpha_max;
    alpha_37 + delta_alpha_37 - alpha_36 - delta_alpha_36 <= delta_alpha_max;
    alpha_38 + delta_alpha_38 - alpha_37 - delta_alpha_37 <= delta_alpha_max;
    alpha_39 + delta_alpha_39 - alpha_38 - delta_alpha_38 <= delta_alpha_max;
    alpha_40 + delta_alpha_40 - alpha_39 - delta_alpha_39 <= delta_alpha_max;
    -(alpha_1 + delta_alpha_1 - alpha_0 - delta_alpha_0) <= delta_alpha_max;
    -(alpha_2 + delta_alpha_2 - alpha_1 - delta_alpha_1) <= delta_alpha_max;
    -(alpha_3 + delta_alpha_3 - alpha_2 - delta_alpha_2) <= delta_alpha_max;
    -(alpha_4 + delta_alpha_4 - alpha_3 - delta_alpha_3) <= delta_alpha_max;
    -(alpha_5 + delta_alpha_5 - alpha_4 - delta_alpha_4) <= delta_alpha_max;
    -(alpha_6 + delta_alpha_6 - alpha_5 - delta_alpha_5) <= delta_alpha_max;
    -(alpha_7 + delta_alpha_7 - alpha_6 - delta_alpha_6) <= delta_alpha_max;
    -(alpha_8 + delta_alpha_8 - alpha_7 - delta_alpha_7) <= delta_alpha_max;
    -(alpha_9 + delta_alpha_9 - alpha_8 - delta_alpha_8) <= delta_alpha_max;
    -(alpha_10 + delta_alpha_10 - alpha_9 - delta_alpha_9) <= delta_alpha_max;
    -(alpha_11 + delta_alpha_11 - alpha_10 - delta_alpha_10) <= delta_alpha_max;
    -(alpha_12 + delta_alpha_12 - alpha_11 - delta_alpha_11) <= delta_alpha_max;
    -(alpha_13 + delta_alpha_13 - alpha_12 - delta_alpha_12) <= delta_alpha_max;
    -(alpha_14 + delta_alpha_14 - alpha_13 - delta_alpha_13) <= delta_alpha_max;
    -(alpha_15 + delta_alpha_15 - alpha_14 - delta_alpha_14) <= delta_alpha_max;
    -(alpha_16 + delta_alpha_16 - alpha_15 - delta_alpha_15) <= delta_alpha_max;
    -(alpha_17 + delta_alpha_17 - alpha_16 - delta_alpha_16) <= delta_alpha_max;
    -(alpha_18 + delta_alpha_18 - alpha_17 - delta_alpha_17) <= delta_alpha_max;
    -(alpha_19 + delta_alpha_19 - alpha_18 - delta_alpha_18) <= delta_alpha_max;
    -(alpha_20 + delta_alpha_20 - alpha_19 - delta_alpha_19) <= delta_alpha_max;
    -(alpha_21 + delta_alpha_21 - alpha_20 - delta_alpha_20) <= delta_alpha_max;
    -(alpha_22 + delta_alpha_22 - alpha_21 - delta_alpha_21) <= delta_alpha_max;
    -(alpha_23 + delta_alpha_23 - alpha_22 - delta_alpha_22) <= delta_alpha_max;
    -(alpha_24 + delta_alpha_24 - alpha_23 - delta_alpha_23) <= delta_alpha_max;
    -(alpha_25 + delta_alpha_25 - alpha_24 - delta_alpha_24) <= delta_alpha_max;
    -(alpha_26 + delta_alpha_26 - alpha_25 - delta_alpha_25) <= delta_alpha_max;
    -(alpha_27 + delta_alpha_27 - alpha_26 - delta_alpha_26) <= delta_alpha_max;
    -(alpha_28 + delta_alpha_28 - alpha_27 - delta_alpha_27) <= delta_alpha_max;
    -(alpha_29 + delta_alpha_29 - alpha_28 - delta_alpha_28) <= delta_alpha_max;
    -(alpha_30 + delta_alpha_30 - alpha_29 - delta_alpha_29) <= delta_alpha_max;
    -(alpha_31 + delta_alpha_31 - alpha_30 - delta_alpha_30) <= delta_alpha_max;
    -(alpha_32 + delta_alpha_32 - alpha_31 - delta_alpha_31) <= delta_alpha_max;
    -(alpha_33 + delta_alpha_33 - alpha_32 - delta_alpha_32) <= delta_alpha_max;
    -(alpha_34 + delta_alpha_34 - alpha_33 - delta_alpha_33) <= delta_alpha_max;
    -(alpha_35 + delta_alpha_35 - alpha_34 - delta_alpha_34) <= delta_alpha_max;
    -(alpha_36 + delta_alpha_36 - alpha_35 - delta_alpha_35) <= delta_alpha_max;
    -(alpha_37 + delta_alpha_37 - alpha_36 - delta_alpha_36) <= delta_alpha_max;
    -(alpha_38 + delta_alpha_38 - alpha_37 - delta_alpha_37) <= delta_alpha_max;
    -(alpha_39 + delta_alpha_39 - alpha_38 - delta_alpha_38) <= delta_alpha_max;
    -(alpha_40 + delta_alpha_40 - alpha_39 - delta_alpha_39) <= delta_alpha_max;
    alpha_0 + delta_alpha_0 - last_alpha <= delta_alpha_max;
    omega_1 >= 0;
    omega_2 >= 0;
    omega_3 >= 0;
    omega_4 >= 0;
    omega_5 >= 0;
    omega_6 >= 0;
    omega_7 >= 0;
    omega_8 >= 0;
    omega_9 >= 0;
    omega_10 >= 0;
    omega_11 >= 0;
    omega_12 >= 0;
    omega_13 >= 0;
    omega_14 >= 0;
    omega_15 >= 0;
    omega_16 >= 0;
    omega_17 >= 0;
    omega_18 >= 0;
    omega_19 >= 0;
    omega_20 >= 0;
    omega_21 >= 0;
    omega_22 >= 0;
    omega_23 >= 0;
    omega_24 >= 0;
    omega_25 >= 0;
    omega_26 >= 0;
    omega_27 >= 0;
    omega_28 >= 0;
    omega_29 >= 0;
    omega_30 >= 0;
    omega_31 >= 0;
    omega_32 >= 0;
    omega_33 >= 0;
    omega_34 >= 0;
    omega_35 >= 0;
    omega_36 >= 0;
    omega_37 >= 0;
    omega_38 >= 0;
    omega_39 >= 0;
    omega_40 >= 0;
    omega_41 >= 0;
    omega_1 <= omega_max;
    omega_2 <= omega_max;
    omega_3 <= omega_max;
    omega_4 <= omega_max;
    omega_5 <= omega_max;
    omega_6 <= omega_max;
    omega_7 <= omega_max;
    omega_8 <= omega_max;
    omega_9 <= omega_max;
    omega_10 <= omega_max;
    omega_11 <= omega_max;
    omega_12 <= omega_max;
    omega_13 <= omega_max;
    omega_14 <= omega_max;
    omega_15 <= omega_max;
    omega_16 <= omega_max;
    omega_17 <= omega_max;
    omega_18 <= omega_max;
    omega_19 <= omega_max;
    omega_20 <= omega_max;
    omega_21 <= omega_max;
    omega_22 <= omega_max;
    omega_23 <= omega_max;
    omega_24 <= omega_max;
    omega_25 <= omega_max;
    omega_26 <= omega_max;
    omega_27 <= omega_max;
    omega_28 <= omega_max;
    omega_29 <= omega_max;
    omega_30 <= omega_max;
    omega_31 <= omega_max;
    omega_32 <= omega_max;
    omega_33 <= omega_max;
    omega_34 <= omega_max;
    omega_35 <= omega_max;
    omega_36 <= omega_max;
    omega_37 <= omega_max;
    omega_38 <= omega_max;
    omega_39 <= omega_max;
    omega_40 <= omega_max;
    omega_41 <= omega_max;
cvx_end
vars.d_1 = d_1;
vars.d{1} = d_1;
vars.d_2 = d_2;
vars.d{2} = d_2;
vars.d_3 = d_3;
vars.d{3} = d_3;
vars.d_4 = d_4;
vars.d{4} = d_4;
vars.d_5 = d_5;
vars.d{5} = d_5;
vars.d_6 = d_6;
vars.d{6} = d_6;
vars.d_7 = d_7;
vars.d{7} = d_7;
vars.d_8 = d_8;
vars.d{8} = d_8;
vars.d_9 = d_9;
vars.d{9} = d_9;
vars.d_10 = d_10;
vars.d{10} = d_10;
vars.d_11 = d_11;
vars.d{11} = d_11;
vars.d_12 = d_12;
vars.d{12} = d_12;
vars.d_13 = d_13;
vars.d{13} = d_13;
vars.d_14 = d_14;
vars.d{14} = d_14;
vars.d_15 = d_15;
vars.d{15} = d_15;
vars.d_16 = d_16;
vars.d{16} = d_16;
vars.d_17 = d_17;
vars.d{17} = d_17;
vars.d_18 = d_18;
vars.d{18} = d_18;
vars.d_19 = d_19;
vars.d{19} = d_19;
vars.d_20 = d_20;
vars.d{20} = d_20;
vars.d_21 = d_21;
vars.d{21} = d_21;
vars.d_22 = d_22;
vars.d{22} = d_22;
vars.d_23 = d_23;
vars.d{23} = d_23;
vars.d_24 = d_24;
vars.d{24} = d_24;
vars.d_25 = d_25;
vars.d{25} = d_25;
vars.d_26 = d_26;
vars.d{26} = d_26;
vars.d_27 = d_27;
vars.d{27} = d_27;
vars.d_28 = d_28;
vars.d{28} = d_28;
vars.d_29 = d_29;
vars.d{29} = d_29;
vars.d_30 = d_30;
vars.d{30} = d_30;
vars.d_31 = d_31;
vars.d{31} = d_31;
vars.d_32 = d_32;
vars.d{32} = d_32;
vars.d_33 = d_33;
vars.d{33} = d_33;
vars.d_34 = d_34;
vars.d{34} = d_34;
vars.d_35 = d_35;
vars.d{35} = d_35;
vars.d_36 = d_36;
vars.d{36} = d_36;
vars.d_37 = d_37;
vars.d{37} = d_37;
vars.d_38 = d_38;
vars.d{38} = d_38;
vars.d_39 = d_39;
vars.d{39} = d_39;
vars.d_40 = d_40;
vars.d{40} = d_40;
vars.d_41 = d_41;
vars.d{41} = d_41;
vars.delta_alpha_0 = delta_alpha_0;
vars.delta_alpha_1 = delta_alpha_1;
vars.delta_alpha{1} = delta_alpha_1;
vars.delta_alpha_2 = delta_alpha_2;
vars.delta_alpha{2} = delta_alpha_2;
vars.delta_alpha_3 = delta_alpha_3;
vars.delta_alpha{3} = delta_alpha_3;
vars.delta_alpha_4 = delta_alpha_4;
vars.delta_alpha{4} = delta_alpha_4;
vars.delta_alpha_5 = delta_alpha_5;
vars.delta_alpha{5} = delta_alpha_5;
vars.delta_alpha_6 = delta_alpha_6;
vars.delta_alpha{6} = delta_alpha_6;
vars.delta_alpha_7 = delta_alpha_7;
vars.delta_alpha{7} = delta_alpha_7;
vars.delta_alpha_8 = delta_alpha_8;
vars.delta_alpha{8} = delta_alpha_8;
vars.delta_alpha_9 = delta_alpha_9;
vars.delta_alpha{9} = delta_alpha_9;
vars.delta_alpha_10 = delta_alpha_10;
vars.delta_alpha{10} = delta_alpha_10;
vars.delta_alpha_11 = delta_alpha_11;
vars.delta_alpha{11} = delta_alpha_11;
vars.delta_alpha_12 = delta_alpha_12;
vars.delta_alpha{12} = delta_alpha_12;
vars.delta_alpha_13 = delta_alpha_13;
vars.delta_alpha{13} = delta_alpha_13;
vars.delta_alpha_14 = delta_alpha_14;
vars.delta_alpha{14} = delta_alpha_14;
vars.delta_alpha_15 = delta_alpha_15;
vars.delta_alpha{15} = delta_alpha_15;
vars.delta_alpha_16 = delta_alpha_16;
vars.delta_alpha{16} = delta_alpha_16;
vars.delta_alpha_17 = delta_alpha_17;
vars.delta_alpha{17} = delta_alpha_17;
vars.delta_alpha_18 = delta_alpha_18;
vars.delta_alpha{18} = delta_alpha_18;
vars.delta_alpha_19 = delta_alpha_19;
vars.delta_alpha{19} = delta_alpha_19;
vars.delta_alpha_20 = delta_alpha_20;
vars.delta_alpha{20} = delta_alpha_20;
vars.delta_alpha_21 = delta_alpha_21;
vars.delta_alpha{21} = delta_alpha_21;
vars.delta_alpha_22 = delta_alpha_22;
vars.delta_alpha{22} = delta_alpha_22;
vars.delta_alpha_23 = delta_alpha_23;
vars.delta_alpha{23} = delta_alpha_23;
vars.delta_alpha_24 = delta_alpha_24;
vars.delta_alpha{24} = delta_alpha_24;
vars.delta_alpha_25 = delta_alpha_25;
vars.delta_alpha{25} = delta_alpha_25;
vars.delta_alpha_26 = delta_alpha_26;
vars.delta_alpha{26} = delta_alpha_26;
vars.delta_alpha_27 = delta_alpha_27;
vars.delta_alpha{27} = delta_alpha_27;
vars.delta_alpha_28 = delta_alpha_28;
vars.delta_alpha{28} = delta_alpha_28;
vars.delta_alpha_29 = delta_alpha_29;
vars.delta_alpha{29} = delta_alpha_29;
vars.delta_alpha_30 = delta_alpha_30;
vars.delta_alpha{30} = delta_alpha_30;
vars.delta_alpha_31 = delta_alpha_31;
vars.delta_alpha{31} = delta_alpha_31;
vars.delta_alpha_32 = delta_alpha_32;
vars.delta_alpha{32} = delta_alpha_32;
vars.delta_alpha_33 = delta_alpha_33;
vars.delta_alpha{33} = delta_alpha_33;
vars.delta_alpha_34 = delta_alpha_34;
vars.delta_alpha{34} = delta_alpha_34;
vars.delta_alpha_35 = delta_alpha_35;
vars.delta_alpha{35} = delta_alpha_35;
vars.delta_alpha_36 = delta_alpha_36;
vars.delta_alpha{36} = delta_alpha_36;
vars.delta_alpha_37 = delta_alpha_37;
vars.delta_alpha{37} = delta_alpha_37;
vars.delta_alpha_38 = delta_alpha_38;
vars.delta_alpha{38} = delta_alpha_38;
vars.delta_alpha_39 = delta_alpha_39;
vars.delta_alpha{39} = delta_alpha_39;
vars.delta_alpha_40 = delta_alpha_40;
vars.delta_alpha{40} = delta_alpha_40;
vars.new_EF_1 = new_EF_1;
vars.new_EF{1} = new_EF_1;
vars.new_EF_2 = new_EF_2;
vars.new_EF{2} = new_EF_2;
vars.new_EF_3 = new_EF_3;
vars.new_EF{3} = new_EF_3;
vars.new_EF_4 = new_EF_4;
vars.new_EF{4} = new_EF_4;
vars.new_EF_5 = new_EF_5;
vars.new_EF{5} = new_EF_5;
vars.new_EF_6 = new_EF_6;
vars.new_EF{6} = new_EF_6;
vars.new_EF_7 = new_EF_7;
vars.new_EF{7} = new_EF_7;
vars.new_EF_8 = new_EF_8;
vars.new_EF{8} = new_EF_8;
vars.new_EF_9 = new_EF_9;
vars.new_EF{9} = new_EF_9;
vars.new_EF_10 = new_EF_10;
vars.new_EF{10} = new_EF_10;
vars.new_EF_11 = new_EF_11;
vars.new_EF{11} = new_EF_11;
vars.new_EF_12 = new_EF_12;
vars.new_EF{12} = new_EF_12;
vars.new_EF_13 = new_EF_13;
vars.new_EF{13} = new_EF_13;
vars.new_EF_14 = new_EF_14;
vars.new_EF{14} = new_EF_14;
vars.new_EF_15 = new_EF_15;
vars.new_EF{15} = new_EF_15;
vars.new_EF_16 = new_EF_16;
vars.new_EF{16} = new_EF_16;
vars.new_EF_17 = new_EF_17;
vars.new_EF{17} = new_EF_17;
vars.new_EF_18 = new_EF_18;
vars.new_EF{18} = new_EF_18;
vars.new_EF_19 = new_EF_19;
vars.new_EF{19} = new_EF_19;
vars.new_EF_20 = new_EF_20;
vars.new_EF{20} = new_EF_20;
vars.new_EF_21 = new_EF_21;
vars.new_EF{21} = new_EF_21;
vars.new_EF_22 = new_EF_22;
vars.new_EF{22} = new_EF_22;
vars.new_EF_23 = new_EF_23;
vars.new_EF{23} = new_EF_23;
vars.new_EF_24 = new_EF_24;
vars.new_EF{24} = new_EF_24;
vars.new_EF_25 = new_EF_25;
vars.new_EF{25} = new_EF_25;
vars.new_EF_26 = new_EF_26;
vars.new_EF{26} = new_EF_26;
vars.new_EF_27 = new_EF_27;
vars.new_EF{27} = new_EF_27;
vars.new_EF_28 = new_EF_28;
vars.new_EF{28} = new_EF_28;
vars.new_EF_29 = new_EF_29;
vars.new_EF{29} = new_EF_29;
vars.new_EF_30 = new_EF_30;
vars.new_EF{30} = new_EF_30;
vars.new_EF_31 = new_EF_31;
vars.new_EF{31} = new_EF_31;
vars.new_EF_32 = new_EF_32;
vars.new_EF{32} = new_EF_32;
vars.new_EF_33 = new_EF_33;
vars.new_EF{33} = new_EF_33;
vars.new_EF_34 = new_EF_34;
vars.new_EF{34} = new_EF_34;
vars.new_EF_35 = new_EF_35;
vars.new_EF{35} = new_EF_35;
vars.new_EF_36 = new_EF_36;
vars.new_EF{36} = new_EF_36;
vars.new_EF_37 = new_EF_37;
vars.new_EF{37} = new_EF_37;
vars.new_EF_38 = new_EF_38;
vars.new_EF{38} = new_EF_38;
vars.new_EF_39 = new_EF_39;
vars.new_EF{39} = new_EF_39;
vars.new_EF_40 = new_EF_40;
vars.new_EF{40} = new_EF_40;
vars.new_EF_41 = new_EF_41;
vars.new_EF{41} = new_EF_41;
vars.omega_1 = omega_1;
vars.omega{1} = omega_1;
vars.omega_2 = omega_2;
vars.omega{2} = omega_2;
vars.omega_3 = omega_3;
vars.omega{3} = omega_3;
vars.omega_4 = omega_4;
vars.omega{4} = omega_4;
vars.omega_5 = omega_5;
vars.omega{5} = omega_5;
vars.omega_6 = omega_6;
vars.omega{6} = omega_6;
vars.omega_7 = omega_7;
vars.omega{7} = omega_7;
vars.omega_8 = omega_8;
vars.omega{8} = omega_8;
vars.omega_9 = omega_9;
vars.omega{9} = omega_9;
vars.omega_10 = omega_10;
vars.omega{10} = omega_10;
vars.omega_11 = omega_11;
vars.omega{11} = omega_11;
vars.omega_12 = omega_12;
vars.omega{12} = omega_12;
vars.omega_13 = omega_13;
vars.omega{13} = omega_13;
vars.omega_14 = omega_14;
vars.omega{14} = omega_14;
vars.omega_15 = omega_15;
vars.omega{15} = omega_15;
vars.omega_16 = omega_16;
vars.omega{16} = omega_16;
vars.omega_17 = omega_17;
vars.omega{17} = omega_17;
vars.omega_18 = omega_18;
vars.omega{18} = omega_18;
vars.omega_19 = omega_19;
vars.omega{19} = omega_19;
vars.omega_20 = omega_20;
vars.omega{20} = omega_20;
vars.omega_21 = omega_21;
vars.omega{21} = omega_21;
vars.omega_22 = omega_22;
vars.omega{22} = omega_22;
vars.omega_23 = omega_23;
vars.omega{23} = omega_23;
vars.omega_24 = omega_24;
vars.omega{24} = omega_24;
vars.omega_25 = omega_25;
vars.omega{25} = omega_25;
vars.omega_26 = omega_26;
vars.omega{26} = omega_26;
vars.omega_27 = omega_27;
vars.omega{27} = omega_27;
vars.omega_28 = omega_28;
vars.omega{28} = omega_28;
vars.omega_29 = omega_29;
vars.omega{29} = omega_29;
vars.omega_30 = omega_30;
vars.omega{30} = omega_30;
vars.omega_31 = omega_31;
vars.omega{31} = omega_31;
vars.omega_32 = omega_32;
vars.omega{32} = omega_32;
vars.omega_33 = omega_33;
vars.omega{33} = omega_33;
vars.omega_34 = omega_34;
vars.omega{34} = omega_34;
vars.omega_35 = omega_35;
vars.omega{35} = omega_35;
vars.omega_36 = omega_36;
vars.omega{36} = omega_36;
vars.omega_37 = omega_37;
vars.omega{37} = omega_37;
vars.omega_38 = omega_38;
vars.omega{38} = omega_38;
vars.omega_39 = omega_39;
vars.omega{39} = omega_39;
vars.omega_40 = omega_40;
vars.omega{40} = omega_40;
vars.omega_41 = omega_41;
vars.omega{41} = omega_41;
vars.w_0 = w_0;
vars.w_1 = w_1;
vars.w{1} = w_1;
vars.w_2 = w_2;
vars.w{2} = w_2;
vars.w_3 = w_3;
vars.w{3} = w_3;
vars.w_4 = w_4;
vars.w{4} = w_4;
vars.w_5 = w_5;
vars.w{5} = w_5;
vars.w_6 = w_6;
vars.w{6} = w_6;
vars.w_7 = w_7;
vars.w{7} = w_7;
vars.w_8 = w_8;
vars.w{8} = w_8;
vars.w_9 = w_9;
vars.w{9} = w_9;
vars.w_10 = w_10;
vars.w{10} = w_10;
vars.w_11 = w_11;
vars.w{11} = w_11;
vars.w_12 = w_12;
vars.w{12} = w_12;
vars.w_13 = w_13;
vars.w{13} = w_13;
vars.w_14 = w_14;
vars.w{14} = w_14;
vars.w_15 = w_15;
vars.w{15} = w_15;
vars.w_16 = w_16;
vars.w{16} = w_16;
vars.w_17 = w_17;
vars.w{17} = w_17;
vars.w_18 = w_18;
vars.w{18} = w_18;
vars.w_19 = w_19;
vars.w{19} = w_19;
vars.w_20 = w_20;
vars.w{20} = w_20;
vars.w_21 = w_21;
vars.w{21} = w_21;
vars.w_22 = w_22;
vars.w{22} = w_22;
vars.w_23 = w_23;
vars.w{23} = w_23;
vars.w_24 = w_24;
vars.w{24} = w_24;
vars.w_25 = w_25;
vars.w{25} = w_25;
vars.w_26 = w_26;
vars.w{26} = w_26;
vars.w_27 = w_27;
vars.w{27} = w_27;
vars.w_28 = w_28;
vars.w{28} = w_28;
vars.w_29 = w_29;
vars.w{29} = w_29;
vars.w_30 = w_30;
vars.w{30} = w_30;
vars.w_31 = w_31;
vars.w{31} = w_31;
vars.w_32 = w_32;
vars.w{32} = w_32;
vars.w_33 = w_33;
vars.w{33} = w_33;
vars.w_34 = w_34;
vars.w{34} = w_34;
vars.w_35 = w_35;
vars.w{35} = w_35;
vars.w_36 = w_36;
vars.w{36} = w_36;
vars.w_37 = w_37;
vars.w{37} = w_37;
vars.w_38 = w_38;
vars.w{38} = w_38;
vars.w_39 = w_39;
vars.w{39} = w_39;
vars.w_40 = w_40;
vars.w{40} = w_40;
status.cvx_status = cvx_status;
% Provide a drop-in replacement for csolve.
status.optval = cvx_optval;
status.converged = strcmp(cvx_status, 'Solved');
