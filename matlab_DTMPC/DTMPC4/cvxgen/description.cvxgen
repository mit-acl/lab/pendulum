# Produced by CVXGEN, 2020-04-12 21:12:25 -0400.
# CVXGEN is Copyright (C) 2006-2017 Jacob Mattingley, jem@cvxgen.com.
# The code in this file is Copyright (C) 2006-2017 Jacob Mattingley.
# CVXGEN, or solvers produced by CVXGEN, cannot be used for commercial
# applications without prior written permission from Jacob Mattingley.

# Filename: description.cvxgen.
# Description: A description of the CVXGEN problem.

dimensions
  T = 39 # horizon
end

parameters

  Q (2,2) psd  # state cost.
  Q_final (2,2) psd  # final state cost.
  R (1,1) psd  # input cost.
  M (1,1)  psd # stage cost matrix for bandwidth

  w_max nonnegative  # w limit, delta
  u_max nonnegative  # amplitude limit.
  delta_u_max nonnegative  # max change in u
  omega_max nonnegative # maximum radius
  alpha_min nonnegative # min bandwidth
  alpha_max nonnegative # max bandwidth
  delta_alpha_max nonnegative # max change in alpha
  delta_alpha_slew nonnegative
  x_dot_max nonnegative #bound on the speed

  xk[t] (2), t=0..T+1 # previous state functio
  uk[t] (1), t=0..T  # previous ipnut function
  disturbance[t] (1) nonnegative, t=0..T # nu + D + disturbance
  old_EF[t] (1) nonnegative, t=0..T+1 # last predicted EF
  alpha[t] (1) nonnegative, t=0..T # last predicted frequency

  cos_xk[t] (1), t=0..T
  abs_xk[t] (1) nonnegative, t=0..T

  d[0] (2)  # initial state.
  new_EF[0] (1) nonnegative # initial tube geometry
  omega[0] (1) nonnegative
  x_final (2) # final state
  last_u (1) nonnegative
  last_alpha (1) nonnegative

  c1 nonnegative # constant that goes along with d1 term
  c2 nonnegative # contant that goes along with d2 term
  Bd nonnegative # constant that goes with u term
  I nonnegative # intertia
  delta_t nonnegative # period
  # nu nonnegative # rate of convergence of sliding controller
  Cd_max nonnegative #maximum drag
  lambda nonnegative # sliding variable related coef
  I_over_L nonnegative
end


variables
  d[t] (2), t=1..T+1  # state
  w[t] (1), t=0..T  # input
  new_EF[t] (1), t=1..T+1 # tube geometry
  delta_alpha[t] (1), t=0..T # bandwidth
  omega[t] (1), t=1..T+1 # radius of the tube
end


minimize
  # cost function:
  sum[t=0..T](quad(x_final -(xk[t] + d[t]), Q)+ quad(uk[t] + w[t], R) + quad( (alpha[t] + delta_alpha[t])-alpha_min, M)  )    +   quad(x_final - (xk[T+1] + d[T+1]), Q_final) # cost function

subject to
  # physics
  d[t+1][1] == delta_t*d[t][2] + d[t][1], t=0..T
  d[t+1][2] == -c1*( cos_xk[t] )*d[t][1] + (1 - c2 * abs_xk[t] )*d[t][2] + Bd * w[t], t=0..T
  #dtmpc stuff
  new_EF[t+1] == new_EF[t]*(1-delta_t*alpha[t])   -   old_EF[t]*delta_alpha[t]*delta_t    +   delta_t * (disturbance[t] + Cd_max *abs_xk[t]*abs_xk[t]/I), t = 0..T
  omega[t+1] == delta_t * (-lambda * omega[t] + new_EF[t]) + omega[t], t=0..T

  # constraints
  w[t] <= w_max, t=0..T  # maximum w box constraint.
  -1 * w[t] <= w_max, t=0..T  # maximum w box constraint.

  (uk[t] + w[t]) <= u_max, t=0..T  # maximum w box constraint.
  -1 * (uk[t] + w[t]) <= u_max, t=0..T  # maximum w box constraint.

  # abs(xk[t][2] + d[t][2]) <= x_dot_max, t=1..T+1 # maximum speed
  (uk[t+1] + w[t+1] - uk[t] - w[t]) <= delta_u_max, t=0..T-1  # slew rate constraint.
  -(uk[t+1] + w[t+1] - uk[t] - w[t]) <= delta_u_max, t=0..T-1  # slew rate constraint.
  uk[0] + w[0] - last_u <= delta_u_max

  (xk[t][2] + d[t][2]) <= x_dot_max, t=1..T+1 # maximum speed
  -(xk[t][2] + d[t][2]) <= x_dot_max, t=1..T+1 # maximum speed

  # dtmpc constraints
  new_EF[t] >= 0, t = 1..T+1

  alpha[t] + delta_alpha[t] >= alpha_min, t=0..T # alpha at least that
  alpha[t] + delta_alpha[t] <= alpha_max, t=0..T # alpha no more than that

  (alpha[t] + delta_alpha[t] - alpha[t-1] - delta_alpha[t-1]) <= delta_alpha_max, t=1..T # change in alpha no more than that
  -1 * (alpha[t] + delta_alpha[t] - alpha[t-1] - delta_alpha[t-1]) <= delta_alpha_max, t=1..T # change in alpha no more than that
  alpha[0] + delta_alpha[0] - last_alpha <= delta_alpha_max

  #delta_alpha[t] <= delta_alpha_slew, t=0..T
  #-1 * delta_alpha[t] <= delta_alpha_slew, t=0..T
  #norminf(delta_alpha[t]) <= delta_alpha_slew, t=0..T

  omega[t] >= 0, t=1..T+1 # min radius; can't be zero...
  omega[t] <= omega_max, t=1..T+1 # max radius
end
