/**
 * @file pendulum.h
 * @brief Pendulum control and estimation
 * @author Savva Morozov <savva@mit.edu>
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 Dec 2019
 */

#pragma once

#include <cstdint>

#include <Eigen/Dense>
#include <cmath>
#include <iostream>
#include "dtmpc_pendulum/pendulum.h"


namespace acl {
namespace pendulum {
  /// \brief Eigen matrix types with problem dimensions
  using Qmat = Eigen::Matrix<double, dim::n, dim::n>; // matrix Q, 2x2
  using Qfinalmat = Eigen::Matrix<double, dim::n, dim::n>; // matrix q_final, 2x2
  using Rmat = Eigen::Matrix<double, dim::m, dim::m>;  //m atrix r, 1x1
  using State = Eigen::Matrix<double, dim::n, 1>; // one state, 2x1
  using XStateStack = Eigen::Matrix<double, dim::n, dim::T+2>; //  0 to T+1     all states except for initial one. 2x21
  using tp1Stack  = Eigen::Matrix<double, 1, dim::T+1>; // 0 to T+1       for cosine and abs value, 1x21
  using tp2Stack  = Eigen::Matrix<double, 1, dim::T+2>; // 0 to T+2       for cosine and abs value, 1x21

  using DStateStack = Eigen::Matrix<double, dim::n, dim::T+1>; //  1 to T+1       all states except for initial one. 2x21
  using CommandStack = Eigen::Matrix<double, dim::m, dim::T+1>;// 0 to T         all commands with initial one, 1x21

  using Command = Eigen::Matrix<double, dim::m, 1>; //just the command to be output, 1x1

  using CommandTrajectory = Eigen::Matrix<double, dim::m, dim::traj*(dim::T+1)>;// 0 to T         all commands with initial one, 1x21
  using StateTrajectory = Eigen::Matrix<double, dim::n, dim::traj*(dim::T+1)>;// 0 to T         all commands with initial one, 1x21


  class DTMPC_Pendulum
  {
  public:
    Eigen::Map<Qmat> Q_; ///< state transition matrix (dynamics)
    Eigen::Map<Qfinalmat> Qfinal_; ///< input matrix (how commands affect states)
    Eigen::Map<Rmat> R_; ///< p.s.d terminal cost for soft constraint
    Eigen::Map<Rmat> M_; ///< p.s.d terminal cost for soft constraint
    Eigen::Map<State> d0_; ///< initial d state
    Eigen::Map<State> x0_; ///< initial d state
    Eigen::Map<State> xf_; ///< final state
    Eigen::Map<Command> u0_; ///< initial 8 state

    Eigen::Map<XStateStack> xk_; ///< states from x0 to x_T, designed via MPC
    Eigen::Map<CommandStack> uk_; ///< commands from x0 to x_T, designed via MPC

    Eigen::Map<DStateStack> d_; ///< states from x0 to x_T, designed via MPC
    Eigen::Map<CommandStack> w_; ///< commands from x0 to x_T, designed via MPC
    Eigen::Map<tp1Stack> delta_alpha_; ///< commands from x0 to x_T, designed via MPC
    Eigen::Map<tp1Stack> omega_; ///< commands from x0 to x_T, designed via MPC
    Eigen::Map<tp1Stack> new_phi_; ///< commands from x0 to x_T, designed via MPC

    Eigen::Map<tp1Stack> cos_x1_; ///< commands from x0 to x_T, designed via MPC
    Eigen::Map<tp1Stack> abs_x2_; ///< commands from x0 to x_T, designed via MPC

    Eigen::Map<tp1Stack> disturbance_; ///< disturbance from 0 to T, designed via MPC
    Eigen::Map<tp1Stack> old_phi_; ///< disturbance from 0 to T, designed via MPC
    Eigen::Map<tp1Stack> alpha_; ///< disturbance from 0 to T, designed via MPC

    Eigen::Matrix<double, dim::n, 1 > x_global_0_;

    Eigen::Matrix<double, dim::n, 1 > x_final_;
    Eigen::Matrix<double, dim::n, 1 > x_0_;

    //short trajectories
    Eigen::Matrix<uint64_t, 1, dim::traj * (dim::T+1)> t_traj_;
    Eigen::Matrix<double, dim::m, dim::traj * (dim::T+1)> u_traj_;
    Eigen::Matrix<double, dim::n, dim::traj * (dim::T+1)> x_traj_;
    Eigen::Matrix<double, 1, dim::traj * (dim::T+1)> omega_traj_;
    Eigen::Matrix<double, 1, dim::traj * (dim::T+1)> phi_traj_;
    Eigen::Matrix<double, 1, dim::traj * (dim::T+1)> alpha_traj_;


    struct DTMPC_inits{
      double last_u = 0.0;
      double last_alpha = 4.0;
      double omega_0 = 0.0;
      double phi_0 = 6.0;
      double theta_0 = 0.0;
      double d_theta_0 = 0.0;
      uint64_t t_0 = 0;
      int req_id = 0;
    };


    struct MPC_params{
        // matrix cost related values
        double x_final = M_PI/2;
        double delta_t = 0.005;
        int delta_t_ms = 5;

        double Q1 = 1;
        double Q2 = 0.1;
        double R = 0.1;
        double M = 0.1;
        double Q_final1 = 10;
        double Q_final2 = 10;

        // double w_max_coarse = 0.5; // maximum w sqp can use
        // double w_max_fine = 0.2; // maximum w sqp can use
        double delta_u_max = 0.04; // slew rate; maximum delta u
        // double delta_u_max_fine = 0.01; // slew rate; maximum delta u
        // double u_cutoff = 0.3;

        double u_max= 1.8; // N, maximum applied thrust
        double x_dot_max = 10*M_PI; // rad/s; max angular speed
        double omega_max = 5*M_PI/180; // rad; maximum tube radius - radius of maximum uncertainty realization
        double alpha_min = 4; // rad/s;  minimum bandwidth
        double alpha_max = 25; // rad/s; maximum  bandwidth
        double delta_alpha_max = 3;// rad/s; maximum change in bandwidth between concequent steps

        double last_alpha = 0;
        double last_u = 0.0;

        double phi_0 = 0;
        double omega_0 = 0;

        double eta = 0;
        double D = 0; // 1/s2 max disturbance
        double lambda = 0; // sliding variable lambda

        // double num_stops = 1;


        // double last_alpha = alpha_min;

        // double omega_0 = 0.5*M_PI/180; ////initial radius, in radians

        double Cd_max = 0.002;
        double disturb = 0;  //1/s2
        double disturb_type = 0;

        double delta_l_cm = 0.0025;
        double delta_alpha_slew = 1.0;
        double grav_term = 1.0;

        double verbose = 1.0;

        // double phi_0 = 6;

      };

    struct Finals {
      double num_stops = 4.0;
      int stops[10] = {0,0,0,0,0, 0,0,0,0,0};
    };

    struct Params {
      /*
      low pass filter parameters;
      motor multipliers;
      */
      /// \brief Lowpass filter parameters, in [0 1]. Zero means no filtering.
      float lpf_acc_alpha; ///< LPF param for accel meas.
      float lpf_gyr_alpha; ///< LPF param for gyro meas.
      float m6;
      float m7;
    };

    struct Physical_Quantities {
      /// physical quantitites that define  the pendulum
      float g = 9.81; // m/s2
      float m = 0.214; // kg
      float L_cm = 0.0921; //m, cm of pendulum
      float L = 0.229; // m, where torque is applied
      float I = 0.00417; // kgm2 inertia; assuming that all mass is at the tip
      float Cd = 0.000077; //estimated drag
      float umax = 1.8; // N, max allowed force
    };

  public:
    DTMPC_Pendulum();
    ~DTMPC_Pendulum() = default;


    /*      DTMPC Methods          */
    void set_MPC_params();
    void DTMPC_cost1();
    void DTMPC_cost2();
    void setDTMPCinits();

    // void setX0toNow();
    // void priorToFirstDTMPC();
    void DTMPC();



    void setParameters(const Params& params, const MPC_params& mpcs, const Physical_Quantities& pqs,  const Finals& fins);


    void computeTrajectory(); //get control -change this to change controller





    /// \brief Getters for filtered IMU data
    float u_last_= 0.0;

    uint64_t dtmpc_time_start_ = 0; // start of the dtmpc.
    uint64_t time_start_ = 0; //time that the controller was started
    uint64_t tnow_; // time passed since time_start, in ms.
    uint64_t last_t_0 = 0;

    float t_; //tnow except in seconds
    float theta_des_ = M_PI/2; // desired location
    // float T_ = 2; // time constant in seconds in whihc we need to finish the run
    float theta_start_; //location at which controller was started

    double theta_; ///< roll angle (x-axis) extracted from quat
    double d_theta_; ///< roll angular velocity

    float s_; // sliding variable
    float k_; // gain
    float u_r_; //sliding tube thrust component
    float u_eq_; //feedforward thrust component
    float u_, u_neg_, u_pos_; // thrust output
    float a_; // bandwidth
    float phi_; //phi diameter

    bool running_ = false; // boolean - whether the controller is running

    Params params_; ///< current parameters
    MPC_params mpc_;
    Physical_Quantities pq_; /// current physical parameters of the pendulum
    DTMPC_inits inits_;
    Finals fin_;

  private:

    float sat(float a);

  };

} // ns pendulum
} // ns acl
