/**
 * @file pendulum_ros.h
 * @brief ROS wrapper for pendulum components
 * @author Savva Morozov <savva@mit.edu>
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 Dec 2019
 */

#pragma once

#include <cstdint>
#include <iostream>
#include <ros/ros.h>

#include <tf2_ros/transform_broadcaster.h>

#include <std_srvs/Trigger.h>
#include <std_srvs/SetBool.h>
#include <std_msgs/Float32.h>
#include <std_msgs/UInt64.h>
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/MultiArrayDimension.h"
#include "std_msgs/Float64MultiArray.h"
#include "std_msgs/UInt64MultiArray.h"

#include <pendulum_msgs/srv_float.h>
#include <pendulum_msgs/pendulum_state.h>
#include <pendulum_msgs/trajectory.h>
#include <pendulum_msgs/traj_req.h>

#include <sensor_msgs/Imu.h>
#include <geometry_msgs/Vector3Stamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TransformStamped.h>
#include <snapstack_msgs/Motors.h>

#include "dtmpc_pendulum/imu_man.h"
#include "dtmpc_pendulum/esc_man.h"
#include "dtmpc_pendulum/pendulum.h"

namespace acl {
namespace pendulum {

  class PendulumROS : public Imu_IEventListener
  {
  public:
    PendulumROS(const ros::NodeHandle nh, const ros::NodeHandle nhp);
    ~PendulumROS() = default;

  /**
   * @brief      The IMU callback handler to process accel/gyro data
   *
   * @param      samples  IMU samples to be processed
   * @param[in]  count    Number of samples in buffer
   *
   * @return     true if successful
   */
  bool Imu_IEventListener_ProcessSamples(sensor_imu* samples, uint32_t count);

  private:
    ros::NodeHandle nh_, nhp_;
    ros::Publisher pub_imu_, pub_imu_filtered_, pub_angle_;
    ros::Publisher pub_torque_, pub_motors_, pub_pose_;
    ros::Publisher pub_pend_, pub_traj_req_;

    ros::Subscriber sub_trajectory_;

    ros::ServiceServer srv_arm_, srv_isarmed_, srv_runController_;
    tf2_ros::TransformBroadcaster br_;

    /// \brief Components
    ImuMan imuman_; ///< Asynch. passes IMU data to callbacks as available
    EscMan escman_; ///< Sends PWM signals to ESCs via the esc_interface lib
    BLSC_Pendulum pendulum_; ///< Pendulum algorithm implementations

    /// \brief Internal state
    bool is_imu_calibrated_ = false; ///< the IMU should calibrate during startup

    /**
     * @brief      Main processing loop, driven by IMU rate
     *
     * @param[in]  time_us  Timestamp of IMU sample in microseconds
     * @param      acc      Accelerometer data
     * @param      gyr      Gyroscope data
     */
    void process(uint64_t time_us, float acc[3], float gyr[3]);

    void processTrajectory(const pendulum_msgs::trajectory& msg) ;

    /**
     * @brief      Retrieve ROS parameters and other initialization
     */
    void init();

    /// \brief ROS callbacks
    bool armCB(std_srvs::SetBool::Request&, std_srvs::SetBool::Response&);
    bool isarmedCB(std_srvs::Trigger::Request&, std_srvs::Trigger::Response&);
    bool runController(std_srvs::SetBool::Request &,std_srvs::SetBool::Response &);



    // bool setThetaDes(pendulum_msgs::srv_float::Request &, pendulum_msgs::srv_float::Response&);
  };

} // ns pendulum
} // ns acl
