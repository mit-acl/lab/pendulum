/**
 * @file pendulum_ros.h
 * @brief ROS wrapper for pendulum components
 * @author Savva Morozov <savva@mit.edu>
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 Dec 2019
 */

#pragma once

#include <cstdint>
#include <iostream>
#include <ros/ros.h>

#include <tf2_ros/transform_broadcaster.h>

#include <std_srvs/Trigger.h>
#include <std_srvs/SetBool.h>
#include <std_msgs/Float32.h>
#include <std_msgs/UInt64.h>
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/MultiArrayDimension.h"
#include "std_msgs/Float64MultiArray.h"
#include "std_msgs/UInt64MultiArray.h"

#include <pendulum_msgs/srv_float.h>
#include <pendulum_msgs/pendulum_state.h>
#include <pendulum_msgs/trajectory.h>
#include <pendulum_msgs/traj_req.h>

#include <sensor_msgs/Imu.h>
#include <geometry_msgs/Vector3Stamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TransformStamped.h>
#include <snapstack_msgs/Motors.h>

#include "dtmpc_pendulum/imu_man.h"
#include "dtmpc_pendulum/esc_man.h"
#include "dtmpc_pendulum/pendulum.h"
#include "dtmpc_pendulum/dtmpc_pendulum.h"

namespace acl {
namespace pendulum {

  class DTMPC_ROS {
  public:
    DTMPC_ROS(const ros::NodeHandle nh, const ros::NodeHandle nhp);
    ~DTMPC_ROS() = default;

  private:
    ros::NodeHandle nh_, nhp_;
    ros::Publisher pub_trajectory_;
    ros::Subscriber sub_request_;

    DTMPC_Pendulum dtmpc_pendulum_;

    void processRequest(const pendulum_msgs::traj_req& msg);

    void init();
























  };

} // ns pendulum
} // ns acl
