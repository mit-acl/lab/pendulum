/**
 * @file pendulum.h
 * @brief Pendulum control and estimation
 * @author Savva Morozov <savva@mit.edu>
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 Dec 2019
 */

#pragma once

#include <cstdint>

#include <Eigen/Dense>
//    #include <Eigen3/Eigen/Dense>
#include <cmath>
#include <iostream>
#include <chrono>


namespace acl {
namespace pendulum {

  namespace dim {
    // static constexpr int T = 33; ///< number of timesteps in horizon
    static constexpr int T = 36; ///< number of timesteps in horizon

    static constexpr int n = 2; ///< dim of state (x, y, z, vx, vy, vz)
    static constexpr int m = 1; ///< dim of input (ax, ay, az)
    static constexpr int traj = 1; ///< dim of input (ax, ay, az)
  } // ns dim


  class BLSC_Pendulum
  {
  public:
    Eigen::Matrix<double, dim::n, 1 > x_global_0_;
    Eigen::Matrix<double, dim::n, 1 > x_final_;
    Eigen::Matrix<double, dim::n, 1 > x_0_;

    //short trajectories
    Eigen::Matrix<uint64_t, 1, dim::traj * (dim::T+1)> t_traj_use_;
    Eigen::Matrix<double, dim::m, dim::traj * (dim::T+1)> u_traj_use_;
    Eigen::Matrix<double, dim::n, dim::traj * (dim::T+1)> x_traj_use_;
    Eigen::Matrix<double, 1, dim::traj * (dim::T+1)> omega_traj_use_;
    Eigen::Matrix<double, 1, dim::traj * (dim::T+1)> phi_traj_use_;
    Eigen::Matrix<double, 1, dim::traj * (dim::T+1)> alpha_traj_use_;

    Eigen::Matrix<uint64_t, 1, dim::traj * (dim::T+1)> t_traj_future_;
    Eigen::Matrix<double, dim::m, dim::traj * (dim::T+1)> u_traj_future_;
    Eigen::Matrix<double, dim::n, dim::traj * (dim::T+1)> x_traj_future_;
    Eigen::Matrix<double, 1, dim::traj * (dim::T+1)> omega_traj_future_;
    Eigen::Matrix<double, 1, dim::traj * (dim::T+1)> phi_traj_future_;
    Eigen::Matrix<double, 1, dim::traj * (dim::T+1)> alpha_traj_future_;

    bool use_available_ = false;
    bool future_available_ = false;
    bool request_available_ = false;
    bool running_ = false;
    bool first_time_flag = true;


    uint64_t dtmpc_time_start_ = 0; // start of the dtmpc.
    uint64_t time_start_ = 0; //time that the controller was started
    uint64_t tnow_ = 0; // time passed since time_start, in ms.

    int DTMPC_period_ms = 85; //in ms
    int DTMPC_period = 17; // time steps

    struct DTMPC_inits{
      double last_u = 0.0;
      double last_alpha = 10.0;
      double omega_0 = 0.0;
      double phi_0 = 6.0;
      double theta_0 = 0.0;
      double d_theta_0 = 0.0;
      uint64_t t_0 = 0;
      int req_id = 0;
    };


    struct MPC_params{
      // matrix cost related values
      double x_final = M_PI/2;
      double delta_t = 0.005;
      int delta_t_ms = 5;

      double Q1 = 1;
      double Q2 = 0.1;
      double R = 0.1;
      double M = 0.1;
      double Q_final1 = 10;
      double Q_final2 = 10;

      // double w_max_coarse = 0.5; // maximum w sqp can use
      // double w_max_fine = 0.2; // maximum w sqp can use
      double delta_u_max = 2; // slew rate; maximum delta u
      double u_max= 1.8; // N, maximum applied thrust
      double omega_max = 5*M_PI/180; // rad; maximum tube radius - radius of maximum uncertainty realization
      double alpha_min = 4; // rad/s;  minimum bandwidth
      double alpha_max = 25; // rad/s; maximum  bandwidth
      double delta_alpha_max = 3;// rad/s; maximum change in bandwidth between concequent steps
      double x_dot_max = 10*M_PI; // rad/s; max angular speed

      double last_alpha = 0;
      double last_u = 0.0;

      double phi_0 = 0;
      double omega_0 = 0;

      double eta = 0;
      double D = 0; // 1/s2 max disturbance
      double lambda = 0; // sliding variable lambda

      double grav_term = 1.0;

      // double last_alpha = alpha_min;

      // double omega_0 = 0.5*M_PI/180; ////initial radius, in radians

      double Cd_max = 0.001;
      double disturb = 0;  //1/s2
      double disturb_type = 0;
      double disturbance_factor = 0.9;

      double verbose = 1.0;

      // double phi_0 = 6;

    };

    struct Params {
      /*
      low pass filter parameters;
      motor multipliers;
      */
      /// \brief Lowpass filter parameters, in [0 1]. Zero means no filtering.
      float lpf_acc_alpha; ///< LPF param for accel meas.
      float lpf_gyr_alpha; ///< LPF param for gyro meas.
      float m6;
      float m7;
    };

    struct Physical_Quantities {
      /// physical quantitites that define  the pendulum
      float g = 9.81; // m/s2
      float m = 0.214; // kg
      float L_cm = 0.0921; //m, cm of pendulum
      float L = 0.229; // m, where torque is applied
      float I = 0.00417; // kgm2 inertia; assuming that all mass is at the tip
      float Cd = 0.000077; //estimated drag
      float umax = 1.8; // N, max allowed force
    };

    struct PID_params {
      float Kp = 1.0;
      float Kd = 0.0;
      float onset = 0.5;
    };

  public:

    BLSC_Pendulum();
    ~BLSC_Pendulum() = default;


    void setFirstDTMPCinits();
    void setNextDTMPCinits();
    void processNewTrajectory();
    void useFutureTrajectory();
    void checkToSwitchTrajectory();
    void doBLSC();
    void idlePID();

    /*      DTMPC Methods          */

    void setParameters(const Params& params, const MPC_params& mpcs, const Physical_Quantities& pqs);
    void setPID(const PID_params& pids);

    void runController(); //start running the controller
    void stopController(); // stop running the controller,

    void estimateAngle(uint64_t time_us, const Eigen::Vector3f& acc,
                      const Eigen::Vector3f& gyr); // determine angle based on IMU

    Eigen::Vector3d computeControl(uint16_t * pwm); //get control -change this to change controller
    void findPWM(uint16_t *pwm); // compute PWM based on U values that we get
    int lookup(int m_num, float thrust); // thrust to esc look up tables for motors
    void addUp(); // add up the thrust components produced by different subsections of controller

    const Eigen::Vector3f& getFilteredAccel() const { return acc_; }
    const Eigen::Vector3f& getFilteredGyro() const { return gyr_; }
    const Eigen::Quaterniond& getQuaternion() const { return quat_; }
    const double& getAngle() const { return theta_; }


    std::chrono::time_point<std::chrono::system_clock> start;
    std::chrono::time_point<std::chrono::system_clock> end;
    std::chrono::duration<double> diff;

    double max(double a, double b){
      if (a>=b){
        return a;
      }
      else {
        return b;
      }
    }


    /// \brief Getters for filtered IMU data
    float u_last_= 0.0;


    float t_; //tnow except in seconds
    float theta_des_ = M_PI/2; // desired location
    // float T_ = 2; // time constant in seconds in whihc we need to finish the run
    float theta_start_; //location at which controller was started

    double theta_; ///< roll angle (x-axis) extracted from quat
    double d_theta_; ///< roll angular velocity

    double s_; // sliding variable
    double k_; // gain
    double u_rec_; //sliding tube thrust component
    double u_sliding_; //feedforward thrust component
    double u_, u_neg_, u_pos_; // thrust output
    double omega_;
    double a_; // bandwidth
    double phi_; //phi diameter
    double u_mpc_;
    double x_des, d_x_des;

    float thrust_[44] = {0.0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1.0, 1.05, 1.1, 1.15, 1.2, 1.25, 1.3, 1.35, 1.4, 1.45, 1.5, 1.55, 1.6, 1.65, 1.7, 1.75, 1.8, 1.85, 1.9, 1.95, 2.0, 2.05, 2.1, 2.15};
    float esc_m6_[44] = {1023, 1026, 1033, 1062, 1088, 1108, 1126, 1149, 1172, 1188, 1202, 1217, 1234, 1249, 1263, 1277, 1290, 1304, 1317, 1329, 1342, 1354, 1365, 1376, 1389, 1403, 1417, 1428, 1437, 1447, 1457, 1471, 1486, 1497, 1510, 1523, 1535, 1545, 1554, 1565, 1580, 1595, 1608, 1619};
    float esc_m7_[44] = {1023, 1026, 1044, 1070, 1093, 1113, 1131, 1152, 1174, 1190, 1206, 1220, 1236, 1251, 1263, 1275, 1287, 1300, 1312, 1324, 1336, 1349, 1360, 1370, 1379, 1390, 1403, 1418, 1430, 1440, 1450, 1462, 1476, 1492, 1505, 1515, 1527, 1537, 1548, 1560, 1573, 1587, 1600, 1612};

    MPC_params mpc_;
    Params params_; ///< current parameters
    Physical_Quantities pq_; /// current physical parameters of the pendulum
    DTMPC_inits inits_;
    PID_params pid_;

  private:
    /// \brief Internal state
    bool lpf_initialized_ = false; ///< so we can use first measurement
    Eigen::Vector3f acc_; ///< filtered accel data
    Eigen::Vector3f gyr_; ///< filtered gyro data
    Eigen::Quaterniond quat_; ///< base_link (pivot point) w.r.t base

    float sat(float a);

    /**
     * @brief      First-order lowpass filter following
     *
     *                y = alpha*y + (1-alpha)*x
     *
     * @param[in]  alpha  LPF parameter in [0 1]. Zero means no filtering.
     * @param[in]  meas   The new measurement, x.
     * @param      out    The current filtered value and output, y.
     *
     * @tparam     T      The templated type of the signal to LPF.
     */

    template<typename T>
    static void LPF(float alpha, const T& meas, T& out)
    {
      out = alpha*out + (1-alpha)*meas;
    }
  };

}; // ns pendulum
} // ns acl
