Snapdragon Pendulum
===================
This ROS package contains code to allow the control and estimation of a Snapdragon Flight based pendulum.
The package allows is developed incrementally with end goal of writing ['Adaptive Dynamic Tube Model Predictive Controller'](https://dspace.mit.edu/handle/1721.1/122395) developed by Dr Brett Lopez.


[[_TOC_]]

## Theory

### Pendulum Dynamics and DTMPC
Pendulum dynamics are decsribed by 
```math
(1) \qquad I \ddot{\theta} = L u - C_d |\dot{\theta}| \dot{\theta} - L_{cm} m g sin(\theta) + d I 
```
where $`I`$ is intertia of the pendulum around the axis of the pendulum, [kg m<sup>2</sup>], $` \theta`$ is the angle of the pendulum, [1/s<sup>2</sup>], with zero defined as the pendulum pointing down, $`L`$ is the distance from axis to the point of thrust application, [m], $`u`$ is the thrust force due to the propellers, [kg m/s<sup>2</sup>], $`C_d`$ is the drag coefficient of the pendulum, [kg m<sup>2</sup>], $`L_{cm}`$ is the distance from the axle to the center of mass of the pendulum, [m], $`m`$ is the mass of the pendulum, [kg],  $`g`$ is the gravitational acceleration, [m/s<sup>2</sup>], and $`d`$ is the angular acceleration disturbance of the system, [1/s<sup>2</sup>]. 

### Control Input Policy Derivation

In the derivation below, I use the folloiwng notation: <sup>*</sup> as in $`u^*, \theta^*`$ suggests that the value is the desired value that comes from solving the optimization problem, while the value itself (like $`u, \theta`$) are values used during the runtime of the controller.

![alt text](img/derivation1.png "Control Input Policy math 1")
![alt text](img/derivation2.png "Control Input Policy math 2")
![alt text](img/derivation3.png "Control Input Policy math 3")

<!-- 
$`\qquad\qquad\qquad\qquad\qquad\qquad\qquad(1.1) \qquad I \ddot{\theta} = L u - C_d |\dot{\theta}| \dot{\theta} - L_{cm} m g sin(\theta) + d I \\ \\ `$

$`\qquad\qquad\qquad\qquad\qquad\qquad\qquad(1.2) \qquad s = \tilde{\dot{\theta}} + \lambda \tilde{\theta}`$

$`\qquad\qquad\qquad\qquad\qquad\qquad\qquad(1.3) \qquad s = \tilde{\ddot{\theta}} + \lambda \tilde{\dot{\theta}} = ( \ddot{\theta} - \ddot{\theta}^*) + \lambda \tilde{\dot{\theta}} = \frac{1} {I} (Lu - C_d |\dot{\theta}|\dot{\theta} - L_{cm}mg sin(\theta)) + d - \ddot{\theta}^* + \lambda\tilde{\dot{\theta}} \\ \\ `$

$`\qquad\qquad\qquad\qquad\qquad\qquad\qquad(1.4) \qquad C_d = \hat{C_d} + \tilde{C_d} \text{ (similar to how $\theta = \theta^* + \tilde{\theta}$) } \\`$

$`\qquad\qquad\qquad\qquad\qquad\qquad\text{ Let $u = u_{bl} + u_{eq}$, where: }\\ \\ `$ 

$`\qquad\qquad\qquad\qquad\qquad\qquad\qquad(1.5) \qquad u_{eq} = \frac {1} {L} (L_{cm}mg sin(\theta) + \hat C_d |\dot\theta|\dot\theta ) + \frac{I}{L} (\ddot{\theta} ^* - \lambda \tilde{\dot{\theta}} ) \\ \\ `$

$`\\ \qquad\qquad\qquad\qquad\qquad\qquad\text{Plugging (1.5) into (1.3) and using (1.4), we get: } \\ \\ `$

$`\qquad\qquad\qquad\qquad\qquad\qquad\qquad(1.6) \qquad \dot{s} \; = \; `$$` d \; + \;  \frac {1} {I} (Lu_{bl} \; - \; \tilde{C_d} |\dot\theta| \dot\theta ) \\ \\ `$

$`\\ \qquad\qquad\qquad\qquad\qquad\qquad\text{when solving the optimization problem with SQP, we acquire $u^*$, such that: } \\ \\ `$

$`\qquad\qquad\qquad\qquad\qquad\qquad\qquad(1.7) \qquad I \ddot\theta ^* = Lu^* - \hat C_d |\dot\theta ^*|\dot\theta ^* -L_{cm} mg sin(\theta ^*) \\ \\ `$

$`\qquad\qquad\qquad\qquad\qquad\qquad\text{Plugging (1.7) into (1.5), we get:} \\ \\ `$

$`\qquad\qquad\qquad\qquad\qquad\qquad\qquad(1.8) \qquad u_{eq} = u^* + \frac {1}{L} [ L_{cm}mg(sin\theta - sin\theta^*) + \hat C_d (|\dot\theta|\dot\theta - |\dot\theta ^*|\dot\theta ^*)  - I \lambda \tilde{\dot\theta} ] \\ \\ `$

$`\qquad\qquad\qquad\qquad\qquad\qquad \text{Let's now derive the boundary layer term and related equations. } \\ \\  `$
$`\qquad\qquad\qquad\qquad\qquad\qquad \text{With $s^2$ being a Lyapunov function, the following defines an attractive tube: } `$

$`\qquad\qquad\qquad\qquad\qquad\qquad\qquad(1.9) \qquad s \dot{s} \; = \; `$$` \frac{1}{2} \frac{d}{dt} (s^2) \; \leq \; (\dot\Phi - \eta) |s| `$

$`\qquad\qquad\qquad\qquad\qquad\qquad \text{Plugging (1.6) into (1.9), we get: } `$

$`\qquad\qquad\qquad\qquad\qquad\qquad\qquad(1.10) \qquad s (d + \frac {Lu_{bl} - \tilde{C_d}|\dot\theta|\dot\theta } {I}) \leq (\dot\Phi - \eta) |s| `$

$`\qquad\qquad\qquad\qquad\qquad\qquad\qquad(1.11) \qquad \text{Let} \;\; |d| \leq D(\theta ^*), `$ 

$`\qquad\qquad\qquad\qquad\qquad\qquad \text{where $D(\theta^*)$ is variable max disturbance given by the user (the leaf blower, for instance)} `$ 

$`\qquad\qquad\qquad\qquad\qquad\qquad\qquad(1.12) `$$`\qquad \text{Let } \; u_{bl} \; = \; -K \; sat(\frac{s} {\Phi}) \;/ \; L`$ 

$`\qquad\qquad\qquad\qquad\qquad\qquad \text{Plugging (1.11) and (1.12) into (1.10) and running some inequalities: } `$

$`\qquad\qquad\qquad\qquad\qquad\qquad\qquad(1.13) \qquad `$$` s \; (d \; + \frac { -\tilde{C_d}|\dot\theta|\dot\theta \;} {I}) `$$`\; \leq\; |s|\; (D \;+  \frac {\;\tilde{C_d} |\dot\theta| |\dot\theta|} {I}) `$

$`\qquad\qquad\qquad\qquad\qquad\qquad\qquad(1.14) \qquad s\dot{s} `$$` \; \leq\; |s|\; (D \;+  \frac {\;\tilde{C_d} |\dot\theta| |\dot\theta|} {I}) `$$` \;- \;   s \frac{K}{I} sat(\frac{s}{\Phi}) `$

$`\qquad\qquad\qquad\qquad\qquad\qquad \text{Let $K$ be such that: } `$

$`\qquad\qquad\qquad\qquad\qquad\qquad\qquad(1.15) \qquad  `$$` |s|\; (D \;+  \frac {\;\tilde{C_d} |\dot\theta| |\dot\theta|} {I}) `$$` \;- \;   s \frac{K}{I} sat(\frac{s}{\Phi}) \; \leq\; `$$` (\dot\Phi - \eta) \; |s|`$

$`\qquad\qquad\qquad\qquad\qquad\qquad \text{Plugging in for the case outside the tube, $ 1 \leq |\frac{s}{\Phi}| $, we get: } `$

$`\qquad\qquad\qquad\qquad\qquad\qquad\qquad(1.16) \qquad  `$$` \tilde{C_d} \;|\dot\theta| \; |\dot\theta| \; `$$`+ \;I D \;-\; K  \leq \; `$$` (\dot\Phi - \eta) \; I `$ $`\text{, and rearranging: }`$

$`\qquad\qquad\qquad\qquad\qquad\qquad\qquad(1.17) \qquad  `$$` \tilde{C_d} \; |\dot\theta| \; |\dot\theta| + I \; `$$` (D \; + \; \eta \; - \; \dot{\Phi} ) \; `$$` \leq \; K`$

$`\qquad\qquad\qquad\qquad\qquad\qquad \text{ The solution from (1.17) also works for the case when $  |\frac{s}{\Phi}| \leq 1 $. Let's plug (1.12) into (1.6): } `$

$`\qquad\qquad\qquad\qquad\qquad\qquad\qquad(1.18) \qquad \dot{s} \; = \; `$$` (d + \frac {1} {I} ( Lu_{bl} \; - \; \tilde{C_d}|\dot\theta|\dot\theta ) ) \; `$$` = \; d \; - \; `$$` \frac{1} {I} ( K sat(s / \Phi) \; - \; \tilde{C_d} |\dot\theta|\dot\theta ) \;  `$$` = \; d \; - \; `$$` \frac{1} {I} ( K \frac{s} {\Phi} \; - \; \tilde{C_d} |\dot\theta|\dot\theta )`$ -->

<!-- $`\qquad\qquad\qquad\qquad\qquad\qquad \text{ Rearranging (1.18) we observe that it is a first order filter on $s$ with $ \frac{K}{\Phi I}$ as the cut-off frequency: }`$

$`\qquad\qquad\qquad\qquad\qquad\qquad\qquad(1.19) \qquad \dot{s} \; = \; \frac{K}{\Phi I} s \; - `$$` \; \frac{1}{I} \tilde{C_d} |\dot\theta|\dot\theta + d `$

$`\qquad\qquad\qquad\qquad\qquad\qquad \text{ Defining $\alpha$ to be the cutoff frequency and plugging in (1.17), we get: }`$

$`\qquad\qquad\qquad\qquad\qquad\qquad\qquad(1.20) \qquad \alpha \; = \; `$$` \frac{K}{\Phi I} \; = \; \frac{1}{\Phi I} ( \tilde{C_d} |\dot\theta| |\dot\theta|`$$` \; + \; I (D \; + \; \eta \; - \; \dot\Phi) )   `$  $`\text{, and rearranging: }`$

$`\qquad\qquad\qquad\qquad\qquad\qquad\qquad(1.21) \qquad \dot\Phi \;= \; `$$` -\alpha \Phi \; + \; D \; + \; \eta \; + `$$`\; \frac{1}{I} \tilde{C_d} |\dot\theta| |\dot\theta|   `$

$`\qquad\qquad\qquad\qquad\qquad\qquad \text{ This concludes the derivation of the control policy.} \\`$
$`\qquad\qquad\qquad\qquad\qquad\qquad \text{ The last equation we need is that for the tube dynamics, whihc is done by putting $\tilde{\dot\theta} + \lambda \tilde\theta = s $ into controllable canonical form:  } \\`$

$`\qquad\qquad\qquad\qquad\qquad\qquad\qquad(1.22) \qquad `$$` \tilde{\dot\theta} \; =\; -\lambda \tilde\theta + s `$ $`\text{, and using that } |s| \leq \Phi\text{, we get: }`$

$`\qquad\qquad\qquad\qquad\qquad\qquad\qquad(1.23) \qquad `$$` \tilde{\dot\Omega} \; = \; -\lambda \Omega \; + \; \Phi `$ -->


### DTMPC Problem Statement
Putting all of the above equations together:

![alt text](img/dtmpc_all.png "DTMPC equations")

<!-- ```math
\begin{aligned}
& (1) \qquad I \ddot{\theta} = L u - C_d |\dot{\theta}| \dot{\theta} - L_{cm} m g sin(\theta) + d I \\
& (2) \qquad u = u_{eq} + u_{bl}  \\
& (3.1) \quad u_{bl} = -\frac{K(\theta^*)}{L} sat(\frac{s}{\Phi(\theta^*)}) \\
& (3.2) \quad u_{eq} = u^* + \frac {1}{L} [ L_{cm}mg(sin\theta - sin\theta^*) + \hat C_d (|\dot\theta|\dot\theta - |\dot\theta ^*|\dot\theta ^*)  - I \lambda \tilde{\dot\theta}    \\
& (4) \qquad K(\theta^*) = \tilde{C_d} |\dot{\theta}| \dot{\theta} + (D(\theta^*) + \eta -\dot{\Phi}(\theta^*)) I \\
& (5) \qquad \dot{\Phi}(\theta^*) = - \alpha \Phi(\theta^*) + \frac {\tilde{C_d}} {I} |\dot{\theta}| |\dot{\theta}|  + D(\theta^*) + \eta \\
& (6) \qquad \alpha(\theta^*) = \frac {K(\theta^*)}{\Phi(\theta^*) I} \\
& (7) \qquad s = \tilde{\dot{\theta}} + \lambda \tilde{\theta} \\
& (8) \qquad \dot{\Omega}(\theta^*) = -\lambda \Omega(\theta^*) + \Phi(\theta^*) \\
\end{aligned}
``` -->

where $`u`$ is the total input term, $`\theta^*`$ and $`u^*`$ are the desired trajectory and the desired input terms that come from solving the optimization problem, $`u_{bl}`$ is the input term that comes from the boundary layer controller, $`K(\theta^*)`$ is boundary layer controller gain, [kg m<sup>2</sup>/s<sup>2</sup>], $`s`$ is the sliding variable defined in (7), $`\Phi`$ is the tube geometry term, [1/s], governed by the differential equation in (5), $`\alpha`$ is the maneuver aggressiveness bandwidth determined from solving the optimization problem, [1/s], $`\tilde{C_d}`$ is the uncertainty in drag coefficient, $`D`$ is the uncertainty in dynamics, such that $`|d| <= D`$, [1/s<sup>2</sup>], $`\eta`$ is the rate of convergence term, [1/s<sup>2</sup>], $`\lambda`$ is the sliding variable constant, [1/s], $`\Omega`$ is the radius of the RCI tube - the radius of all possible realizations of the tube dynamics.

The following terms are determined as a result of solving the optimization problem: $`\theta^*, \dot{\theta}^*, u_{mpc}(\theta^*), \alpha(\theta^*), \Phi(\theta^*), \Omega(\theta^*)`$. 

### Optimization Problem and SQP

Due to the fact that the dynamics of the system are non linear and non convex, Sequantial Quadratic Programming is used to solve the optimization problem, similar to what is described in THIS paper. The following is the MPC iteration (since MPC is only run once, it's not really MPC, it's really an optimization problem). 

Note on the notation: $`\boldsymbol{\theta^k} = `$$`{\theta^k} \atop {\dot{\theta}^k}`$, $`\;\bold{d} = `$$`d \atop \dot{d}`$. Superscript $`k`$ indcates that the current variable was acquirred at the k-th iteration of SQP. The initial 0-th trajectory is static: it assumes 0 inputs. At every step of SQP we linearize around the previous trajectory defined by $`\boldsymbol{\theta^k}`$, $`u^k`$, and $`\alpha^k`$, solve the optimization problem to acquire adjustments $`\bold{d}`$, $`w`$, and $`\varDelta\alpha`$ to the trajecotry, input, and bandwidth respectively, and determine the next trajectory by adding them together.

![alt text](img/math4.png "DTMPC problem statement")

<!-- $` \qquad \text{at iteration k+1 of SQP:} \\ `$
$` \qquad\qquad \text{previous trajectory: } \\ `$
$` \qquad\qquad\qquad \{ \alpha^k _i\}_{i=0}^N , \quad \{u^k_i\}_{i=0}^N, \quad \{ \boldsymbol{\theta^k _i}\}_{i=1}^N, \quad \{ \Phi^k _i\}_{i=0}^N, \quad \{ \Omega^k _i\}_{i=0}^N, \quad \{ \delta^k _i\}_{i=0}^N \\`$
$` \qquad\qquad \text{the following variables are also passed to the solver due to limitations of CVXGEN: } \\`$
$` \qquad\qquad\qquad \{ cos(\theta^k _i) \}_{i=1}^N,\quad \{ |\dot{\theta}^k_i| \}_{i=0}^N  \\ \\`$

$` \qquad\qquad\qquad \underset{ \{\varDelta\alpha _i\}_{i=0}^N,\{w_i\}_{i=0}^N,\,\{\bold{d_i}\}_{i=1}^N}{\text{minimize} } `$
$` \qquad (\boldsymbol{\theta_{final}} - (\boldsymbol{\theta^k_{N+1}} + \bold{d_{N+1}}) )^\top\mathbf{Q_{final}} (\boldsymbol{\theta_{final}} - (\boldsymbol{\theta^k_{N+1}} + \bold{d_{N+1}}) \; + \\`$
$` \qquad\qquad\qquad\qquad\qquad\qquad\qquad\qquad + \sum_{i=0}^N [ \mathbf{R} (u^k_i + w_i)^2 + \mathbf{M} (\alpha^k_i + \varDelta\alpha_i)^2 + (\boldsymbol{\theta_{final}} - (\boldsymbol{\theta^k_i} + \bold{d_i}) )^\top\mathbf{Q} (\boldsymbol{\theta_{final}} - (\boldsymbol{\theta^k_i} + \bold{d_i}) )] \\ `$

$` \qquad\qquad\qquad\qquad \text{subject to} \\ `$
$` \qquad\qquad\qquad\qquad\qquad\qquad (9.1)\qquad i=0,\dots,N \qquad\qquad `$
$` d_{i+1} \; = \; d_i \; + \; \varDelta t \; (\dot{d}_i) `$  $`\\`$ 
$` \qquad\qquad\qquad\qquad\qquad\qquad (9.2)\qquad i=0,\dots,N \qquad\qquad `$
$`\dot{d}_{i+1} \; = \; \dot{d}_i + \varDelta t \; `$$`[ \; {-L_cmg \over I} cos(\theta^k_i) d_i \; - \; \frac {2\hat{C}_d}{I} | \dot{\theta}^k_i| \dot{d}_i \; + \; \frac{L}{I}w_i \; ] \\ `$
$` \qquad\qquad\qquad\qquad\qquad\qquad (9.3)\qquad i=0,\dots,N \qquad\qquad `$
$` \Phi ^{k+1}_{i+1}\; =\; \Phi^{k+1}_i \; + \; \varDelta t \; [ \; - `$$` (\Phi^{k+1}_i \alpha^k_i \; + \; \Phi^{k}_i \varDelta \alpha_i) \; + \; (\eta + D + \delta^k_i + \frac{\bar{C_d} | \dot{\theta}^k_i| | \dot{\theta}^k_i|}{I}) \; ]\\ `$
$` \qquad\qquad\qquad\qquad\qquad\qquad (9.4)\qquad i=0,\dots,N \qquad\qquad `$
$`\Omega^{k+1}_{i+1} \; = \; \Omega^{k+1}_{i} \; + \; \varDelta t \;`$$` (-\lambda \Omega^{k+1}_{i} \; + \; \Phi ^{k+1}_{i}) \\`$
$` \qquad\qquad\qquad\qquad\qquad\qquad (9.5) \qquad  i=0,\dots,N \qquad\qquad `$
$`\text{\textbar}w_i\text{\textbar} \; \leq \; w_{max}  \\ `$
$` \qquad\qquad\qquad\qquad\qquad\qquad (9.6) \qquad  i=0,\dots,N \qquad\qquad `$
$`\text{\textbar}w_i \; + \; u^k_i\text{\textbar} \; \leq \; u_{max} \\ `$
$` \qquad\qquad\qquad\qquad\qquad\qquad (9.7) \qquad  i=0,\dots,N-1 \qquad\;\, `$
$`\text{\textbar}(w_{i+1}  + u^k_{i+1}) \; - \; (w_i + u^k_i)\text{\textbar} \; \leq \; \varDelta u_{max}\\ `$
$` \qquad\qquad\qquad\qquad\qquad\qquad (9.8) \qquad i=0,\dots,N \qquad\qquad `$
$`\alpha _{min} \; \leq \; (\alpha ^k_i + \varDelta \alpha _i) \; \leq \; \alpha _{max}\\ `$
$` \qquad\qquad\qquad\qquad\qquad\qquad (9.9) \qquad  i=0,\dots,N-1 \qquad\;\, `$
$`\text{\textbar}(\alpha^k_{i+1} + \varDelta \alpha_{i+1}) \; - \; (\alpha^k_i + \varDelta \alpha_i)\text{\textbar} \; \leq \; \varDelta \alpha_{max} \\ `$
$` \qquad\qquad\qquad\qquad\qquad\qquad (9.10) \quad\;\, `$
$`i=0,\dots,N \qquad\qquad 0 \; \leq \; \Omega^{k+1}_i \; \leq \; \Omega_{max} \\ `$
$` \qquad\qquad\qquad\qquad\qquad\qquad (9.11) \quad\;\, `$
$`i=1,\dots,N+1 \qquad\;\; \text{\textbar} \dot{\theta}^k_i + \dot{d}_i \text{\textbar} \leq \; \dot{\theta}_{max} \\ `$

$` \qquad\qquad\qquad\qquad\qquad  \text{ initial conditions: } \\`$
$` \qquad\qquad\qquad\qquad\qquad\qquad\qquad\qquad \boldsymbol{\theta^{k+1}_0} = \boldsymbol{\theta_{init}} \qquad \bold{d_0} = {0 \atop 0} \qquad \Phi^{k+1}_0 = \Phi_{init} \qquad \Omega^{k+1}_0 = \Omega_{init} \\ `$

$` \qquad\qquad\qquad\qquad  \text{define values that come from this iteration of SQP: } \\ `$
$` \qquad\qquad\qquad\qquad\qquad\qquad  \boldsymbol{\theta^{k+1}_i} \; = \; `$$`\boldsymbol{\theta^{k}_i} \; + \; \bold{d_i} \qquad\qquad\qquad i=0,\dots,N+1 \\ `$
$` \qquad\qquad\qquad\qquad\qquad\qquad  u^{k+1}_i \; = \; u^k_i \; `$$`+ \; w_i\qquad\qquad\qquad\, i=0,\dots,N \\ `$
$` \qquad\qquad\qquad\qquad\qquad\qquad  \alpha^{k+1}_i \; = \; `$$`\alpha^k_i \; + \; \varDelta \alpha_i \qquad\qquad\quad\; i=0,\dots,N \\ `$
$` \qquad\qquad\qquad\qquad\qquad\qquad  \Phi^{k+1}_i, \Omega^{k+1}_i \; \text{ are already computed directly in the optimization.} \\  `$
$` \qquad\qquad\qquad\qquad\qquad\qquad  \delta^{k+1}_i = f(\boldsymbol{\theta^{k+1}_i}) \quad \text{disturbance is calculated as a function of state.} \\ \\  `$

$` \qquad\qquad\qquad\qquad  \text{ SQP is terminated if for each of the above variables the differences between respective values}\\  `$
$` \qquad\qquad\qquad\qquad  \text{ in concequent iterations are sufficiently small. } `$ -->

Equations (9.1) and (9.2) are acquirred as in the SQP paper mentioned above - by taking the Jocobians of the state space equation (1) and descretizing it. Equations (9.3) and (9.4) come from descretizing equations (5) and (8). (9.5) restricts how much the trajectory can be changed per iteration of SQP (necessary for the trajectory linearization approximation to remain true), (9.6) is a physical constraint on the magnitude of the input, (9.7) is the physical constraint on the magnitude of change of the input, (9.8) puts bounds on aggressiveness bandwidth, (9.9) constrains the magnitude of change of the input, (9.10) constrains the diameter of the tube around the trajectory, and(9.11) bounds the angular speed of the pendulum arm.

### Parameters
This section goes into detail about used parameters and the means by which they are acquirred.

| Param. | Value | Meaning / Source | Concern |
| :---:        | :---:    | :-------          | :--: |
|  $`N`$      |   25     |    Time horizon. Capped by CVXGEN capability to generate solvers.       | |
|  $`\varDelta t`$      |   0.012 s     |    Time step. Must be that high to ensure that time horizon covers the entire trajectory.        | |
|  $`L`$      |   0.229 m     |     Distance from axis to point of thrust application; modeled.      | |
|  $`L_cm`$      |   0.0921 m     |  Distance from axis to center of mass of the pendulum; modeled.         | |
|  $`I`$      |   0.00417 kg m<sup>2</sup>    |   Inertia of the pendulum around the axis; modeled.        | |
|  $`m`$      |   0.214 kg      |    Mass of the pendulum; modeled.       | |
|  $`g`$      |    9.81 m/s<sup>2</sup>    |    Gravitational acceleration.       | |
|  $`\hat{C_d}`$      |    0.000077 kg m<sup>2</sup>   |     Estimated drag coefficient value; modeled.      | |
|  $`\bar{C_d}`$      |    0.005 kg m<sup>2</sup>   |     Estimated max value of drag; artificially increased      | THIS |
|  $`\boldsymbol{\theta_{init}}`$    |   $`{0} \atop 0`$  $`\; \atop 1/s`$     |     Initial condition: pendulum arm down.      | |
|  $`\boldsymbol{\theta_{final}}`$      |   $`\pi/2 \atop 0`$  $` \; \atop 1/s`$   |    Final condition: pendulum arm at 90$`\degree`$       | |
|  $`\dot{\theta}_{max}`$      |   $`3 \pi/s`$   |    Maximum angular speed.       | |
|  $`\mathbf{Q_{final}}`$      |  $`\begin{matrix} \\ 100 & 0 \\ 0 & 100 \end{matrix}`$      |    Final state weight matrix; Imperically determined       | |
|  $`\mathbf{Q}`$      |  $`\begin{matrix} \\ 100 & 0 \\ 0 & 0.001 \end{matrix}`$      |    State weight matrix; Imperically determined       ||
|  $`\mathbf{R}`$      |    0.01    |    Input weight matrix; Imperically determined       ||
|  $`\mathbf{M}`$      |     0.01   |    Bandwidth weight matrix; Imperically determined       ||
|  $`w_{max}`$      |    1.0 N    |    Maximum change in $`w`$ between consecutive SQP iterations. Arbitrarily selected.       ||
|  $`u_{max}`$      |    1.8 N    |    Maximum input thrust from each motor; a physical constraint       ||
|  $` \varDelta u_{max}`$      |    2.0 N    |    Maximum change in input thrust between two consecutive time steps; a "sounds about right" constraint    ||
|  $`\Omega_{max}`$      |   $`5\degree = 0.087 rads`$     |    A 3 degree radius tube around the trajectory - all realizaitons of disturbances must be withint he tube.       ||
|  $`\Omega_{init}`$      |    0    |     Initial tube radius is 0 because initial position is known exactly.      ||
|  $`D`$      |   5 s<sup>-2</sup>     |    This corresponds to approximately 5% of the max possible u, which seemed reasonable for noise/disturbance.        | THIS |
|  $`\eta`$      |   20 s<sup>-2</sup>     |     Manually fitted to find best performance       | THIS |
|  $`\alpha_{min}`$      |    4 rad/s    |     Manually fitted to find best performance      | THIS |
|  $`\alpha_{max}`$      |    30 rad/s    |      Chosen to be just high enough that this variable doesn't affect the solver.     | THIS |
|  $`\varDelta \alpha_{max}`$      |    5 rad/s    |   This value seems reasonable based on above two values.        | THIS |
|  $`\lambda`$      |    71.6 s<sup>-1</sup>     |     Fitted to produce reasonable steady state $`\Omega`$      | THIS |
|  $`\Phi_{init}`$      |   6 s<sup>-1</sup>     |      That's about the steady state value of $`\Phi`$     |   THIS |

<!-- ### Current Implementation and Problems

#### 1. Arbitrarily chosen parameters + Physical Meanings

All parameters above marked with THIS on the fourth column are the parameters that were fitted manually until the simulation performed nicely. Parameters that work in simulation don't quite work in reality. I do not have a sense for the physical meanings of $`\Phi, \alpha, \lambda, \eta`$, and I do not know how to determine their values not emperically (I want at least bulk park approximations).

So far I would look at steady state behavior, write a couple of equations based on the kind of behavior that I would want to observe. But that's not enough to determine all values. And that's still an emerical approach.

_**Q1: What's the physical meaning of $`\alpha, \Phi`$?**_ _**Is there a way to physically approximate their values, as well as other values list above with the THIS mark?**_

#### 2. is DTMPC an MPC
DTMPC creates a trajectory open loop and then runs the boundary layer controller to slide onto the trajectory - hence it's really a trajectory following controller, not an MPC controller. In its nature, MPC repeatedly solves an optimization problem at every time step. From my understanding, this isn't the case for DTMPC.

_**Q2: did I get smth wrong?**_ _**If not, why do we call it an MPC? Should I be solving the optimization problem repeatedly? Except that with my implementation, running DTMPC takes a very long time; see more below.**_


#### 3. Descritization and greediness
I need to create the full trajectory open loop. For that, the time horizon has to be long enough to cover the entire trajectory. Thing si, I pass too many previous-trajectory-parameter-arrays into the SQP solver (namely, $`\{ \alpha^k _i\}_{i=0}^N , \quad \{u^k_i\}_{i=0}^N, \quad \{ \boldsymbol{\theta^k _i}\}_{i=1}^N, \quad \{ \Phi^k _i\}_{i=0}^N, \quad \{ \Omega^k _i\}_{i=0}^N, \quad \{ \delta^k _i\}_{i=0}^N, \{ cos(\theta^k _i) \}_{i=1}^N,\quad \{ |\dot{\theta}^k_i| \}_{i=0}^N`$). Each of these arrays has the length of the time horizon, making the total number of parameters quite high, and then CVXGEN starts cursing at me (CVXGEN limits solver generation time to 10 minutes). Because of that, the time horizon must be short (N = 25). But I need to cover the entire trajectory; hence my timestep $`\varDelta t`$ must be relatively high - 0.012s = 12ms. Apparently, linearizing with this time step generates noticeable large errors:

![alt text](img/graphs2.png "Errors")

The graph above shows the trajectory generated as a result of running DTMPC with parameters from the chart above. The bottom graph shows the _angle trajectory vs time_, the top graph gives _input vs time_. As we see, the trajectory goes smoothly to 90 degrees, as expected. The problem is with the steady state input; generated trajectory has steady state input of 1.337N, which is too high: proper steady state input should be $`L_{cm}mg = Lu <=> u = \frac{L_{cm}mg}{L} = \frac{0.214x9.81x0.0921}{0.229} = 0.844 N`$. This problem does not occur if smaller $`\varDelta t`$ is used. 

There are four solutions that I see here:
* Increase time horizon by changing the number of input variables in CVXGEN (perhaps drop cosine and abs value terms somehow?)
* Use a different solver (MATLAB's YALMIP / MOSEK?)
* Work around it: use the "coarse" trajectory as generated above, split it into a dozen smaller "sub-trajectories," and run a finer DTMPC over each subtrajectory. 

I chose to go with the 3rd option. The problem with the third option is that:
* this approach is greedy: MPC minimizes the sum over each of the small subtrajectory intervals rather than the entire trajectory.
* there are discontinuities in the input, bandwidth, and tube diameter. It's not smooth.
* The solver doesn't always converge for small $`\varDelta t`$ (i'm using 0.0024s for small DTMPC; so it's 12 ms for long vs 2.4ms for short).

Once again, there are ways to work around these problems as well. However, the more work arounds I do, the further away I am from a proper rigorous way of doing research. In theory, every work-around must be justified. It's getting harder and harder to justify my work-arounds.

_**Q3: Is my approach correct?**_ _**Should I keep trying to solve the optimization problem over the entire trajectory (ahem that's not MPC ahem)?**_ _**Cause that's taugh computationally.**_

_**Q4: Solving over the entire trajectory is also not scalable to larger problems (going from 0 to 180, for istance).**_ _**Because of that the coarse/fine trajectory generation approach seems logical;**_ _**yet it's comptuationally consuming; and suffers from discontinuities/unsolvability/mathematical instability.**_

#### 4. Scaling boundary layer input (AKA scaling gain)

Under certain parameter values the value for u_bound would be either too small or too high. If too small, the response would be slow or not sufficient to correct the pendulum. If too high, the input would saturate and the controller would jitter. To resolve that, I would scale the boundary layer input term - I would scale K. 

_**Q5: This sounds illegal and non-research like. Any suggestions on how to work around this?**_ -->

### Current Implementation Results
Current implementation, which still has all the problems described above, does work. 
Below is the trajectory generated by DTMPC with the parameters set above: moving from 0 to 90 degrees, with the max tube diameter of 5 degrees. The colorful line represents the trajectory, with the warmer clors representing high bandwidth, and colder colors representing lower bandwidth. Two black lines represent the tube around the trajectory. 
The following disturbance is applied to the trajectory:
```math
\delta(\theta) = \begin{cases}
   \;0 &\text{if } \theta < \pi/4 \;\text{or}\; \theta > 5 \pi/4 \\
   \;sin(\theta-\pi/4)^2 &\text{else } 
\end{cases}
```
![alt text](img/trajectory.jpg "Trajectory")

The graph below represents the system's actual response: 25 realizations of disturbances are depicted in red, with the tube in blue. As we see, all realizations are within the tube.

![alt text](img/realizations.jpg "Realizations")

## Hardware testing

Below video shows DTMPC controller running on the actual pendulum. Note that the controller running ont he pendulum uses very different parameters; it's also the case that the boundary layer input gain is multiplied by a factor.
![alt text](img/DTMPC.mp4 "DTMPC on pendulum")

## Next Steps

1. Fix the "janky-not-proper-research-hack-around" solutions. Answer to this question should have come from the discussion above.

2. Determine proper good parameters - both for simulation and for the pendulum. Ensure that the solver converges for these parameters.

3. Quantify performance: PID, MPC, TMPC, DTMPC. Compare the following:
* rise time, settling time
* power consumption - equivalent to amount of work done by thrust. This is where DTMPC should theoretically shine. 
* disturbance rejection; leaf blowers!
* Anything else?


## Hardware

### Summary and CAD
CAD files (.step) of the pendulum are available in the folder ---FOLDER---. The pendulum is made of delrin and is epoxied together into one structure; an aluminum tube is used for a shaft. A pair of bearings are used to attach the shaft to the wooden structure. Two motors and propellers are used to actuate the pendulum.

![alt text](img/pendulum.png "Pendulum")

### Motor and ESC calibration
First calibrate the ESCs to indicate that 1000us and 2000us pwm signals are the min and max input throttle. You may also use the esc_calibration tool available with the esc interface tool --LINK--. 

Thrust curves for the motors have been acquired with a RCbenchmark dynamometer. If using rcbenchmark, you may generate the curves using --THIS CODE-- jupyter notebook. More information on the format of the thrust curves is available in that notebook.
Note that despite the acquisition of the thrust curves, when attached to the pendulum, the motors behave differently from expected thrust - perhaps due to the complexity of involved aerodynamics (namely, that of blowing air into the table). Theoretically, if the motors are tuned properly, one could use the gravity feed forward and have the pendulum be static at any given position. In practice, that will likely not be the case. To mitigate the problem, after running the controller, I would multiply the produced thrust to the motors by some manually determined constant until I would reach some satisfactory expected thrust.

## Software setup


### Dependencies and Prerequisites 

You will need [`snapstack_msgs`](https://gitlab.com/mit-acl/fsw/snap-stack/snapstack_msgs) and ['pendulum_msgs']()in your catkin workspace. 

### Installation

### Running
The code is run on a Snapdragon Flight. Push the packages into a catkin workspace on the snapdragon. 

Before running the main code, make sure to start the IMU server: 
```bash
imu_app -s 1`. 
```

The main code can be run (on the Snapdragon) with
```bash
roslaunch pendulum pendulum.launch
```

If the IMU server is running correctly, you should see a message like
```bash
[ INFO] [1575572482.304984377]: IMU calibration complete
```

For safety, you must arm the motors using the `/Pend01/arm` service call: 
```bash
rosservice call /Pend01/arm true
```

The other option is to use the `esc_safety` node to do this with the spacebar. In a new terminal window (on the Snapdragon), run
```bash
rosrun pendulum esc_safety __ns:=Pend01
```

### Features while running:

The following rosservices are available and might be useful:


## Software Logic


1. pendulum: service calls
   - run service call issued to pendulum; pendulum's using PD to warm start up the mototrs remain at 0.
   - runController service call issued to Pendulum node

2. pendulum: trajectory request
   - request_available flag is on, dtmpc inits variables are set in cpp
   - on ros level, pendulum sends a message to dtmpc node contaiing initial conditions for optimization; 
   - executed as apart of the imu process() callback

2. dtmpc: dtmpc node receives request
   - initial conditions passed down from ros to cpp
   - computeTrajectory() is called; upon completion, the trajectory is sent to pendulum
   - executed as part of the traj_req topic processRequest() callback

3. dtmpc: computeTrajectory()
   - set problem specific initial conditions - setDTMPCinits()
   - sovle the problem usgin convexification - DTMPC()
   - note that constant mpc parameters such as physical stuff are set at the contructor using set_MPC_params()

4. dtmpc: setDTMPCinits()
   - set ALL inital conditions + initialize trajectories
   - warm starts the trajectories generation using the previously generated trajectory

5. dtmpc: DTMPC()
   - repeatedly solve the problem using convexificaiton until convergence

6. pendulum: trajectory received on a trajectory topic callback processsTrajectory()
   - receieved trajectory is passed into the future trajectory variable (from ros to cpp level)
   - cpp level function processNewTrjacecotry() is called

7. pendulum: processNewTrajectory()
   - set future_available tag to true

8. pendulum: choice between use_trajectory and future_trajectory
   - the goal of this is to always have an up to date relevant trajectory available for us; 
   - it might be reasonable to do this two steps ahead rather than only one step ahead - as in, have two trajcetoryes - future, and future-future trajceotries available, ina ddition to use_trajectory. this would give a larger buffer for length trajectory generations. This would add a slight initial computational overhead, additional memory oberhead, but will also ensure smooth mission progression
   - future_trajectory becomes use_trajectory when the current time is the inital time of the future_trajecotry
   - at that point a request for a new future_trajcetory is sent
   - another option for smother operation would be to send a request for a enw trajectory immediately after the new future trajectory is processed on the ros level. when a new trajectory arrives on a ros level, we might hold it up on a ros level until cpp level is ready to accept a enw future trajcetory. such scenario indirectly allows having ~3 trajecotries buffered at any time.

9. pendulum: BLSC()
   - separaely from trajectory requests, BLSC() controller is constantly running in the backgorund.








